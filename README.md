# Kingsland Assess

## Getting Started with the development

### Backend

1. Install dependencies
   ```
   npm install
   ```
2. Create `.env.development` file and add your database url:
   ```
   DATABASE_URL=postgres://{user}:{password}@{hostname}:{port}/{database-name}
   ```
3. Apply existing migrations to your database
   ```
   npm run migrate:dev
   ```
4. Generate resolvers types
   ```
   npm run generate
   ```
5. Run the backend server
   ```
   npm run start:dev
   ```
   See Backend documentation for the list of all available scripts

### Frontend

1. Install dependencies
   ```
   npm install
   ```
2. Generate resolvers types
   ```
   npm run generate
   ```
   **_or_**  
   To keep watching on any changes and automatically generate resolvers types on-save
   ```
   npm run generate:watch
   ```
3. Run the application
   ```
   npm start
   ```

### To start working on the admin side of the project you need to prepare a few things:

#### Setting up an admin account:

1. In the backend folder, go to `prisma>seedData>admins.ts` and add your email
2. Now in your backend terminal command tab, run `npm run prisma:seed`
3. Then add your `JWT_SECRET` in the backend `.env.development` file, and restart the backend server.
4. Next, in the frontend folder create `.env` file, and setup your environment variables:\
   `.env.example`
   ```
   REACT_APP_GOOGLE_CLIENT_ID=
   REACT_APP_ROLES_KEY=
   ```
   Consult senior developers for the proper environment variables. After adding, restart the frontend server.

module.exports = {
  roots: ['./src'],
  transform: {
    '^.+\\.ts$': 'ts-jest',
  },
  setupFilesAfterEnv: ['jest-extended/all', './src/tests/utils/setupTests.ts'],
  globalSetup: './src/tests/utils/globalSetup.ts',
  globalTeardown: './src/tests/utils/globalTeardown.ts',
  clearMocks: true,
  testMatch: ['**/tests/**/*.(spec|test).{ts,js}'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
};

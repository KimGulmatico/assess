"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = require("lodash");

var _resolvers = _interopRequireDefault(require("./global/resolvers"));

var _resolvers2 = _interopRequireDefault(require("./users/resolvers"));

var _resolvers3 = _interopRequireDefault(require("./questions/resolvers"));

var _resolvers4 = _interopRequireDefault(require("./exams/resolvers"));

var _resolvers5 = _interopRequireDefault(require("./students/resolvers"));

var _resolvers6 = _interopRequireDefault(require("./admins/resolvers"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const resolvers = (0, _lodash.merge)(_resolvers.default, _resolvers2.default, _resolvers3.default, _resolvers4.default, _resolvers5.default, _resolvers6.default);
var _default = resolvers;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServer = require("apollo-server");

const typeDefs = (0, _apolloServer.gql)`
  type User {
    id: String
    email: String
    firstName: String
    lastName: String
  }

  input GoogleSignInInput {
    tokenId: String!
  }

  type UserProfile {
    id: String
    email: String
    firstName: String
    lastName: String
    roles: [Role]
  }
  type Query {
    users: [User] @auth(requires: ADMIN)
    user(id: String!): User @auth
    currentUser: UserProfile @auth
  }

  type Mutation {
    googleSignIn(input: GoogleSignInInput): UserProfile
    signOut: Boolean!
  }
`;
var _default = typeDefs;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _runtime = require("@prisma/client/runtime");

var _apolloServerExpress = require("apollo-server-express");

var _googleAuthLibrary = require("google-auth-library");

var _auth = require("../../middleware/auth");

var _utils = require("../utils");

require('dotenv').config();

const googleClient = new _googleAuthLibrary.OAuth2Client({
  clientId: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET
});
const defaultRole = 'STUDENT';
const cookieConfig = {
  httpOnly: true,
  sameSite: 'none',
  secure: true,
  maxAge: (0, _utils.daysToMilliseconds)(3)
};
const resolvers = {
  Query: {
    users: async (_parent, _args, {
      db
    }) => {
      try {
        const users = await db.user.findMany();
        return users;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    user: async (_parent, {
      id
    }, {
      db
    }) => {
      try {
        const user = await db.user.findUnique({
          where: {
            id
          }
        });
        return user;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    currentUser: async (_parent, _args, {
      payload
    }) => {
      if (!payload) throw new _apolloServerExpress.AuthenticationError('You must be logged in');
      const {
        userId,
        email,
        firstName,
        lastName,
        roles
      } = payload;
      const currentUser = {
        id: userId,
        email,
        firstName,
        lastName,
        roles: roles
      };
      return currentUser;
    }
  },
  Mutation: {
    googleSignIn: async (_parent, {
      input
    }, {
      res,
      db
    }) => {
      var _input$tokenId;

      if (!(input !== null && input !== void 0 && (_input$tokenId = input.tokenId) !== null && _input$tokenId !== void 0 && _input$tokenId.trim())) throw new _apolloServerExpress.UserInputError('Token id not provided');
      let ticket;

      try {
        ticket = await googleClient.verifyIdToken({
          idToken: input.tokenId,
          audience: process.env.GOOGLE_CLIENT_ID
        });
      } catch (error) {
        throw new _apolloServerExpress.UserInputError('Invalid google token id');
      }

      const payload = ticket.getPayload();

      if (payload) {
        const {
          email,
          given_name: firstName,
          family_name: lastName,
          sub: googleId
        } = payload;

        if (!(email && firstName && lastName && googleId)) {
          throw new Error('Internal Server Error');
        }

        const user = await db.user.upsert({
          create: {
            email,
            firstName,
            lastName,
            googleAccounts: {
              create: {
                googleId
              }
            }
          },
          where: {
            email
          },
          update: {
            email
          }
        });
        const roles = [defaultRole];
        const adminUser = await db.admin.findFirst({
          where: {
            email
          }
        });

        if (adminUser) {
          roles.push('ADMIN');
        }

        const token = (0, _auth.generateToken)({
          email,
          firstName,
          lastName,
          roles,
          userId: user.id
        });
        const userProfile = {
          id: user.id,
          email,
          firstName,
          lastName,
          roles: roles
        };
        res.cookie('jwt', token, cookieConfig);
        return userProfile;
      }

      throw new Error('Internal Server Error');
    },
    signOut: async (_parent, _args, {
      res
    }) => {
      res.clearCookie('jwt', cookieConfig);
      return true;
    }
  }
};
var _default = resolvers;
exports.default = _default;
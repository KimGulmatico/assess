"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServer = require("apollo-server");

const typeDefs = (0, _apolloServer.gql)`
  type Admin {
    id: String
    email: String
  }

  type AdminsConnection {
    nodes: [Admin]
    pageInfo: PageInfo!
  }

  input CreateAdminInput {
    email: String!
  }

  input DeleteAdminInput {
    id: String!
  }

  input UpdateAdminInput {
    id: String!
    email: String!
  }

  type Query {
    admins: [Admin] @auth(requires: ADMIN)
    admin(id: String!): Admin @auth(requires: ADMIN)
    adminsConnection(
      first: Int
      last: Int
      after: String
      before: String
    ): AdminsConnection @auth
  }

  type Mutation {
    createAdmin(input: CreateAdminInput!): Admin @auth(requires: ADMIN)
    deleteAdmin(input: DeleteAdminInput!): Admin @auth(requires: ADMIN)
    updateAdmin(input: UpdateAdminInput!): Admin @auth(requires: ADMIN)
  }
`;
var _default = typeDefs;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _runtime = require("@prisma/client/runtime");

var _apolloServerExpress = require("apollo-server-express");

var _utils = require("../utils");

const resolvers = {
  Query: {
    admins: async (_parent, _args, {
      db
    }) => {
      try {
        const admins = await db.admin.findMany();
        return admins;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    admin: async (_parent, {
      id
    }, {
      db
    }) => {
      try {
        const admin = await db.admin.findUnique({
          where: {
            id
          }
        });
        return admin;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    adminsConnection: async (_parent, args, {
      db
    }) => {
      const {
        take,
        limit
      } = (0, _utils.determinePaginationArgs)(args);
      const {
        after,
        before
      } = args;
      const cursor = after || before ? {
        id: after || before || undefined
      } : undefined;

      try {
        const admins = await db.admin.findMany({
          take,
          cursor,
          orderBy: {
            createdAt: 'desc'
          }
        });
        const {
          nodes,
          pageInfo
        } = (0, _utils.getNodesAndPageInfo)(admins, limit, args);
        return {
          nodes,
          pageInfo
        };
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    }
  },
  Mutation: {
    createAdmin: async (_parent, {
      input
    }, {
      payload,
      db
    }) => {
      if (!payload) throw new _apolloServerExpress.AuthenticationError('You must be logged in');
      const {
        email
      } = input;
      const {
        userId
      } = payload;

      try {
        const admin = await db.admin.create({
          data: {
            email: email,
            createdById: userId
          }
        });
        return admin;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    deleteAdmin: async (_parent, {
      input
    }, {
      payload,
      db
    }) => {
      if (!payload) throw new _apolloServerExpress.AuthenticationError('You must be logged in');
      const {
        id
      } = input;

      try {
        const admin = await db.admin.delete({
          where: {
            id
          }
        });
        return admin;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    updateAdmin: async (_parent, {
      input
    }, {
      payload,
      db
    }) => {
      if (!payload) throw new _apolloServerExpress.AuthenticationError('You must be logged in');
      const {
        id,
        email
      } = input;

      try {
        const admin = await db.admin.update({
          where: {
            id
          },
          data: {
            email
          }
        });
        return admin;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    }
  }
};
var _default = resolvers;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServerCore = require("apollo-server-core");

const inputsOnExamTypesErrorHandler = input => {
  const {
    examType,
    startDate,
    endDate,
    duration
  } = input;

  switch (examType) {
    case 'FIXED':
      if (!(startDate && endDate)) throw new _apolloServerCore.UserInputError('Must provide start and end date on fixed type exam');
      if (duration) throw new _apolloServerCore.UserInputError('Providing duration on fixed type exam is not supported');
      break;

    case 'FLEXIBLE':
      if (!duration) throw new _apolloServerCore.UserInputError('Must provide duration on flexible type exam');
      if (startDate || endDate) throw new _apolloServerCore.UserInputError('Providing start and end dates on flexible type exam is not supported');
      break;

    case 'FREE':
      if (startDate || endDate || duration) throw new _apolloServerCore.UserInputError('Providing duration, start and end dates on flexible type exam is not supported');
      break;

    default:
      throw new _apolloServerCore.UserInputError('Invalid exam type');
  }
};

var _default = inputsOnExamTypesErrorHandler;
exports.default = _default;
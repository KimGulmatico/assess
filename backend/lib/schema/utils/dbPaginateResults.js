"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getNodesAndPageInfo = exports.determinePaginationArgs = void 0;

var _apolloServerExpress = require("apollo-server-express");

const determinePaginationArgs = args => {
  const {
    first,
    last,
    after,
    before
  } = args;
  if (!(first || last)) throw new _apolloServerExpress.UserInputError('You must provide one of first or last');
  if (first && last) throw new _apolloServerExpress.UserInputError('Providing both first and last is not supported');

  if (first && before || last && after) {
    const message = first ? {
      limit: 'first',
      cursor: 'before'
    } : {
      limit: 'last',
      cursor: 'after'
    };
    throw new _apolloServerExpress.UserInputError(`Using ${message.limit} with ${message.cursor} is not supported`);
  }

  if (first && first < 0 || last && last < 0) throw new _apolloServerExpress.UserInputError('Limit must not be negative'); // limit returns the original length of nodes to be sent to the client

  const limit = first || last || 0; // take returns the number of nodes to be retrieve from the database

  let take = limit; // additional nodes are used to determine the next and previous page,
  // any extra nodes are removed before sending to client

  if (first || last) take += 1;
  if (after || before) take += 1; // indicates paginating backwards

  if (last) take *= -1;
  return {
    take,
    limit
  };
};

exports.determinePaginationArgs = determinePaginationArgs;

const getNodesAndPageInfo = (nodes, limit, args) => {
  const {
    first,
    last,
    after,
    before
  } = args;
  const hasExtraNode = nodes.length > limit;

  if (hasExtraNode) {
    if (first || before) nodes.pop();
    if (last || after) nodes.shift();
  }

  const pageInfo = {
    startCursor: nodes.length ? nodes[0].id : null,
    endCursor: nodes.length ? nodes[nodes.length - 1].id : null,
    hasNextPage: first ? hasExtraNode : !!before,
    hasPreviousPage: first ? !!after : hasExtraNode
  };
  return {
    nodes,
    pageInfo
  };
};

exports.getNodesAndPageInfo = getNodesAndPageInfo;
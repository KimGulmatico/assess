"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

const getQuery = keywords => {
  const specialCharacters = /[()|&:*!]/g;
  return keywords.replace(specialCharacters, ' ').trim().split(/\s+/).join(' | ');
};

var _default = getQuery;
exports.default = _default;
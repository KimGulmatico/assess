"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.daysToSeconds = exports.daysToMilliseconds = void 0;

const daysToSeconds = days => days * 60 * 60 * 24;

exports.daysToSeconds = daysToSeconds;

const daysToMilliseconds = days => daysToSeconds(days) * 1000;

exports.daysToMilliseconds = daysToMilliseconds;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

const prismaErrorHandler = error => {
  switch (error.code) {
    case 'P1000':
      throw new Error('Authentication failed against database server. The provided database credentials are not valid');

    case 'P1001':
      throw new Error("Can't reach database server. Please make sure your database server is running");

    case 'P1002':
      throw new Error('Database server timed out');

    case 'P1003':
      throw new Error('Database does not exist');

    case 'P1008':
      throw new Error('Operation timed out');

    case 'P1009':
      throw new Error('Database already exist on the database server');

    case 'P1010':
      throw new Error('User was denied access on the database');

    case 'P1011':
      throw new Error('Error opening a TLS connection');

    case 'P2000':
      throw new Error("The provided value for the column is too long for the column's type");

    case 'P2001':
      throw new Error('The record searched for in the where condition does not exist');

    case 'P2002':
      throw new Error('Unique constraint failed');

    case 'P2003':
      throw new Error('Foreign key constraint failed');

    case 'P2004':
      throw new Error('A constraint failed on the database');

    case 'P2005':
      throw new Error("The value stored in the database is invalid for the field's type");

    case 'P2006':
      throw new Error('The provided value is not valid');

    case 'P2007':
      throw new Error('Data validation error');

    case 'P2008':
      throw new Error('Failed to parse the query');

    case 'P2009':
      throw new Error('Failed to validate the query');

    case 'P2010':
      throw new Error('Raw query failed');

    case 'P2011':
      throw new Error('Null constraint violation');

    case 'P2012':
      throw new Error('Missing a required value');

    case 'P2015':
      throw new Error('A related record could not be found');

    case 'P2016':
      throw new Error('Query interpretation error');

    case 'P2017':
      throw new Error('Failed to parse the query');

    case 'P2018':
      throw new Error('The required connected records were not found');

    case 'P2021':
      throw new Error('The table does not exist');

    case 'P2022':
      throw new Error('The column does not exist');

    case 'P2024':
      throw new Error('Timed out fetching a new connection from the connection pool');

    case 'P2027':
      throw new Error('Multiple errors occurred on the database during query execution');

    case 'P2030':
      throw new Error('Cannot find a fulltext index to use for the search');

    default:
      throw new Error('Internal server error');
  }
};

var _default = prismaErrorHandler;
exports.default = _default;
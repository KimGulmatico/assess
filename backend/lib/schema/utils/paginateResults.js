"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServerExpress = require("apollo-server-express");

var _lodash = require("lodash");

const paginateResults = (results, first, last, after, before) => {
  let minIndex = 0;
  let maxIndex = results.length - 1;
  if (!(first || last)) throw new _apolloServerExpress.UserInputError('You must provide one of first or last');
  if (first && last) throw new _apolloServerExpress.UserInputError('Providing both first and last is not supported');

  if (first && before || last && after) {
    const message = first ? {
      limit: 'first',
      cursor: 'before'
    } : {
      limit: 'last',
      cursor: 'after'
    };
    throw new _apolloServerExpress.UserInputError(`Using ${message.limit} with ${message.cursor} is not supported`);
  }

  if (first && first < 0 || last && last < 0) throw new _apolloServerExpress.UserInputError('Limit must not be negative');

  if (after) {
    const afterIndex = (0, _lodash.findIndex)(results, {
      id: after
    });
    if (afterIndex < 0) throw new _apolloServerExpress.UserInputError('Invalid cursor');
    minIndex = afterIndex + 1;
  }

  if (first) {
    maxIndex = minIndex + first - 1;
  }

  if (before) {
    const beforeIndex = (0, _lodash.findIndex)(results, {
      id: before
    });
    if (beforeIndex < 0) throw new _apolloServerExpress.UserInputError('Invalid cursor');
    maxIndex = beforeIndex - 1;
  }

  if (last) {
    minIndex = maxIndex - last + 1;
  }

  if (maxIndex >= results.length) {
    maxIndex = results.length - 1;
  }

  if (minIndex < 0) {
    minIndex = 0;
  }

  if (!results.length || minIndex >= results.length || maxIndex < 0) return {
    paginatedResults: [],
    pageInfo: {
      startCursor: null,
      endCursor: null,
      hasNextPage: false,
      hasPreviousPage: false
    }
  };
  const startCursor = results[minIndex].id;
  const endCursor = results[maxIndex].id;
  const hasNextPage = maxIndex + 1 < results.length;
  const hasPreviousPage = minIndex - 1 > 0;
  const paginatedResults = (0, _lodash.slice)(results, minIndex, maxIndex + 1);
  const pageInfo = {
    startCursor,
    endCursor,
    hasNextPage,
    hasPreviousPage
  };
  return {
    paginatedResults,
    pageInfo
  };
};

var _default = paginateResults;
exports.default = _default;
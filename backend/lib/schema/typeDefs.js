"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServerExpress = require("apollo-server-express");

var _typeDefs = _interopRequireDefault(require("./global/typeDefs"));

var _typeDefs2 = _interopRequireDefault(require("./users/typeDefs"));

var _typeDefs3 = _interopRequireDefault(require("./questions/typeDefs"));

var _typeDefs4 = _interopRequireDefault(require("./exams/typeDefs"));

var _typeDefs5 = _interopRequireDefault(require("./students/typeDefs"));

var _typeDefs6 = _interopRequireDefault(require("./admins/typeDefs"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const defaultTypeDefs = (0, _apolloServerExpress.gql)`
  type Query {
    _: Boolean
  }
  type Mutation {
    _: Boolean
  }
`;
const typeDefs = [defaultTypeDefs, _typeDefs.default, _typeDefs2.default, _typeDefs3.default, _typeDefs4.default, _typeDefs5.default, _typeDefs6.default];
var _default = typeDefs;
exports.default = _default;
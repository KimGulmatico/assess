"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

var _utils = require("@graphql-tools/utils");

var _apolloServerExpress = require("apollo-server-express");

const authDirectiveTransformer = (schema, directiveName) => {
  const typeDirectiveArgumentMaps = {};
  return (0, _utils.mapSchema)(schema, {
    [_utils.MapperKind.TYPE]: type => {
      var _getDirective;

      const authDirective = (_getDirective = (0, _utils.getDirective)(schema, type, directiveName)) === null || _getDirective === void 0 ? void 0 : _getDirective[0];

      if (authDirective) {
        typeDirectiveArgumentMaps[type.name] = authDirective;
      }

      return undefined;
    },
    [_utils.MapperKind.OBJECT_FIELD]: (fieldConfig, _fieldName, typeName) => {
      var _getDirective2;

      const authDirective = ((_getDirective2 = (0, _utils.getDirective)(schema, fieldConfig, directiveName)) === null || _getDirective2 === void 0 ? void 0 : _getDirective2[0]) || typeDirectiveArgumentMaps[typeName];

      if (authDirective) {
        const {
          requires
        } = authDirective;

        if (requires) {
          const {
            resolve = _graphql.defaultFieldResolver
          } = fieldConfig; // eslint-disable-next-line no-param-reassign

          fieldConfig.resolve = (parent, args, context, info) => {
            var _payload$roles;

            const {
              payload
            } = context;
            if (!payload) return new _apolloServerExpress.AuthenticationError('You must be logged in');
            if (!(payload !== null && payload !== void 0 && (_payload$roles = payload.roles) !== null && _payload$roles !== void 0 && _payload$roles.includes(requires))) return new _apolloServerExpress.ForbiddenError(`You must be logged in as ${requires}`);
            return resolve(parent, args, context, info);
          };

          return fieldConfig;
        }
      }

      return undefined;
    }
  });
};

var _default = authDirectiveTransformer;
exports.default = _default;
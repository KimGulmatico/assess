"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = require("lodash");

var _Question = _interopRequireDefault(require("./Question"));

var _MultipleChoiceQuestion = _interopRequireDefault(require("./MultipleChoiceQuestion"));

var _MultipleSelectionQuestion = _interopRequireDefault(require("./MultipleSelectionQuestion"));

var _CodeQuestion = _interopRequireDefault(require("./CodeQuestion"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const resolvers = (0, _lodash.merge)(_Question.default, _MultipleChoiceQuestion.default, _MultipleSelectionQuestion.default, _CodeQuestion.default);
var _default = resolvers;
exports.default = _default;
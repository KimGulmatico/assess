"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _runtime = require("@prisma/client/runtime");

var _apolloServerExpress = require("apollo-server-express");

var _utils = require("../../utils");

const resolvers = {
  Query: {
    codeQuestions: async (_parent, _args, {
      db
    }) => {
      try {
        const codeQuestions = await db.questionBase.findMany({
          where: {
            questionType: 'CODE_QUESTION'
          }
        });
        return codeQuestions;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    codeQuestion: async (_parent, {
      id
    }, {
      db
    }) => {
      try {
        const codeQuestion = await db.questionBase.findFirst({
          where: {
            id
          }
        });
        return codeQuestion;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    codeActivities: async (_parent, _args, {
      dataSources
    }) => {
      if (!dataSources) throw new Error('Internal Server Error');

      try {
        const codeActivities = await dataSources.codeRunnerAPI.getCodeActivities();
        return codeActivities;
      } catch (error) {
        throw new Error('Internal Server Error');
      }
    }
  },
  CodeQuestion: {
    createdBy: async (parent, _args, {
      db
    }) => {
      const {
        id,
        createdBy
      } = parent;
      if (createdBy) return createdBy;
      if (!id) throw new Error('Question Id does not exist');

      try {
        const user = await db.questionBase.findFirst({
          where: {
            id
          },
          select: {
            createdBy: {
              select: {
                id: true,
                email: true,
                firstName: true,
                lastName: true
              }
            }
          }
        });
        if (!user) throw new Error('User not found');
        return user.createdBy;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    activityId: async (parent, _args, {
      db
    }) => {
      const {
        id,
        activityId
      } = parent;
      if (activityId) return activityId;
      if (!id) throw new Error('Question Id does not exist');

      try {
        const question = await db.codeQuestion.findFirst({
          where: {
            id
          }
        });
        if (!question) throw new Error('Code question not found');
        return question.activityId;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    }
  },
  Mutation: {
    createCodeQuestion: async (_parent, {
      input
    }, {
      payload,
      db
    }) => {
      if (!payload) throw new _apolloServerExpress.AuthenticationError('You must be logged in');
      const {
        userId
      } = payload;
      const {
        question,
        program,
        module,
        activityId
      } = input;

      try {
        const createCodeQuestionPayload = await db.questionBase.create({
          data: {
            question,
            createdById: userId,
            program,
            module,
            questionType: 'CODE_QUESTION',
            codeQuestion: {
              create: {
                activityId
              }
            }
          }
        });
        return createCodeQuestionPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    updateCodeQuestion: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        id,
        question,
        program,
        module,
        activityId
      } = input;

      try {
        const updateCodeQuestionPayload = await db.questionBase.update({
          where: {
            id
          },
          data: {
            question: question || undefined,
            program: program || undefined,
            module: module || undefined,
            codeQuestion: {
              update: {
                activityId: activityId || undefined
              }
            }
          }
        });
        return updateCodeQuestionPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    deleteCodeQuestion: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        id
      } = input;

      try {
        const deleteMultipleChoiceQuestionPayload = await db.questionBase.delete({
          where: {
            id
          },
          include: {
            codeQuestion: {
              select: {
                activityId: true
              }
            },
            createdBy: true
          }
        });
        const {
          codeQuestion,
          ...questionDetails
        } = deleteMultipleChoiceQuestionPayload;
        return {
          activityId: codeQuestion === null || codeQuestion === void 0 ? void 0 : codeQuestion.activityId,
          ...questionDetails
        };
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    runCodeQuestionAnswer: async (_parent, {
      input
    }, {
      dataSources
    }) => {
      if (!dataSources) throw new Error('Internal Server Error');

      try {
        const examResult = await dataSources.codeRunnerAPI.submitExam(input);
        return examResult;
      } catch (error) {
        throw new Error('Internal Server Error');
      }
    }
  }
};
var _default = resolvers;
exports.default = _default;
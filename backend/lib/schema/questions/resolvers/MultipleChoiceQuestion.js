"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _runtime = require("@prisma/client/runtime");

var _apolloServerExpress = require("apollo-server-express");

var _utils = require("../../utils");

const resolvers = {
  Query: {
    multipleChoiceQuestions: async (_parent, _args, {
      db
    }) => {
      try {
        const multipleChoiceQuestions = await db.questionBase.findMany({
          where: {
            questionType: 'MULTIPLE_CHOICE_QUESTION'
          }
        });
        return multipleChoiceQuestions;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    multipleChoiceQuestion: async (_parent, {
      id
    }, {
      db
    }) => {
      try {
        const multipleChoiceQuestion = await db.questionBase.findFirst({
          where: {
            id
          }
        });
        return multipleChoiceQuestion;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    }
  },
  MultipleChoiceQuestion: {
    options: async (parent, _args, {
      db
    }) => {
      const {
        id,
        options
      } = parent;
      if (options) return options;
      if (!id) throw new Error('Question Id does not exist');

      try {
        const multipleChoiceQuestion = await db.multipleChoiceQuestion.findFirst({
          where: {
            id
          },
          select: {
            options: true
          }
        });
        if (!multipleChoiceQuestion) throw new Error('Question options not found');
        return multipleChoiceQuestion.options;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    createdBy: async (parent, _args, {
      db
    }) => {
      const {
        id,
        createdBy
      } = parent;
      if (createdBy) return createdBy;
      if (!id) throw new Error('Question not found');

      try {
        const user = await db.questionBase.findFirst({
          where: {
            id
          },
          select: {
            createdBy: {
              select: {
                id: true,
                email: true,
                firstName: true,
                lastName: true
              }
            }
          }
        });
        if (!user) throw new Error('User not found');
        return user.createdBy;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    }
  },
  Mutation: {
    createMultipleChoiceQuestion: async (_parent, {
      input
    }, {
      payload,
      db
    }) => {
      if (!payload) throw new _apolloServerExpress.AuthenticationError('You must be logged in');
      const {
        userId
      } = payload;
      const {
        question,
        program,
        module,
        options
      } = input;

      try {
        const createMultipleChoiceQuestionPayload = await db.questionBase.create({
          data: {
            question,
            program,
            module,
            createdById: userId,
            questionType: 'MULTIPLE_CHOICE_QUESTION',
            multipleChoiceQuestion: {
              create: {
                options: {
                  createMany: {
                    data: options
                  }
                }
              }
            }
          }
        });
        return createMultipleChoiceQuestionPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    updateMultipleChoiceQuestion: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        id,
        question,
        program,
        module
      } = input;

      try {
        const updateMultipleChoiceQuestionPayload = await db.questionBase.update({
          where: {
            id
          },
          data: {
            question,
            program: program || undefined,
            module: module || undefined
          }
        });
        return updateMultipleChoiceQuestionPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    deleteMultipleChoiceQuestion: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        id
      } = input;

      try {
        const deleteMultipleChoiceQuestionPayload = await db.questionBase.delete({
          where: {
            id
          },
          include: {
            multipleChoiceQuestion: {
              include: {
                options: true
              }
            },
            createdBy: true
          }
        });
        const {
          multipleChoiceQuestion,
          ...questionDetails
        } = deleteMultipleChoiceQuestionPayload;
        return {
          options: multipleChoiceQuestion === null || multipleChoiceQuestion === void 0 ? void 0 : multipleChoiceQuestion.options,
          ...questionDetails
        };
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    createMultipleChoiceOption: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        description,
        isCorrect,
        multipleChoiceQuestionId
      } = input;
      if (!multipleChoiceQuestionId) throw new _apolloServerExpress.UserInputError('Multiple Choice Question Id must be provided');

      try {
        const addMultipleChoiceOptionPayload = await db.multipleChoiceOption.create({
          data: {
            description,
            isCorrect,
            multipleChoiceQuestionId
          }
        });
        return addMultipleChoiceOptionPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    updateMultipleChoiceOption: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        description,
        isCorrect,
        id
      } = input;

      try {
        const addMultipleChoiceOptionPayload = await db.multipleChoiceOption.update({
          where: {
            id
          },
          data: {
            description,
            isCorrect
          }
        });
        return addMultipleChoiceOptionPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    deleteMultipleChoiceOption: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        id
      } = input;

      try {
        const deleteMultipleChoiceOptionPayload = await db.multipleChoiceOption.delete({
          where: {
            id
          }
        });
        return deleteMultipleChoiceOptionPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    }
  }
};
var _default = resolvers;
exports.default = _default;
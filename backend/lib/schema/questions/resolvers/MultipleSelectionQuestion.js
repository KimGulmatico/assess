"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _runtime = require("@prisma/client/runtime");

var _apolloServerExpress = require("apollo-server-express");

var _utils = require("../../utils");

const resolvers = {
  Query: {
    multipleSelectionQuestions: async (_parent, _args, {
      db
    }) => {
      try {
        const multipleSelectionQuestions = await db.questionBase.findMany({
          where: {
            questionType: 'MULTIPLE_SELECTION_QUESTION'
          }
        });
        return multipleSelectionQuestions;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    multipleSelectionQuestion: async (_parent, {
      id
    }, {
      db
    }) => {
      try {
        const multipleSelectionQuestion = await db.questionBase.findFirst({
          where: {
            id
          }
        });
        return multipleSelectionQuestion;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    }
  },
  MultipleSelectionQuestion: {
    choices: async (parent, _args, {
      db
    }) => {
      const {
        id,
        choices
      } = parent;
      if (choices) return choices;
      if (!id) throw new Error('Question Id does not exists');

      try {
        const multipleSelectionQuestion = await db.multipleSelectionQuestion.findFirst({
          where: {
            id
          },
          select: {
            choices: true
          }
        });
        if (!multipleSelectionQuestion) throw new Error('Question options not found');
        return multipleSelectionQuestion.choices;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    createdBy: async (parent, _args, {
      db
    }) => {
      const {
        id,
        createdBy
      } = parent;
      if (createdBy) return createdBy;
      if (!id) throw new Error('Question not found');

      try {
        const user = await db.questionBase.findFirst({
          where: {
            id
          },
          select: {
            createdBy: {
              select: {
                id: true,
                email: true,
                firstName: true,
                lastName: true
              }
            }
          }
        });
        if (!user) throw new Error('User not found');
        return user.createdBy;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    }
  },
  Mutation: {
    createMultipleSelectionQuestion: async (_parent, {
      input
    }, {
      payload,
      db
    }) => {
      if (!payload) throw new _apolloServerExpress.AuthenticationError('You must be logged in');
      const {
        userId
      } = payload;
      const {
        question,
        program,
        module,
        choices
      } = input;

      try {
        const createMultipleSelectionQuestionPayload = await db.questionBase.create({
          data: {
            question,
            program,
            module,
            createdById: userId,
            questionType: 'MULTIPLE_SELECTION_QUESTION',
            multipleSelectionQuestion: {
              create: {
                choices: {
                  createMany: {
                    data: choices
                  }
                }
              }
            }
          },
          include: {
            multipleSelectionQuestion: true
          }
        });
        return createMultipleSelectionQuestionPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    updateMultipleSelectionQuestion: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        id,
        question,
        program,
        module
      } = input;

      try {
        const updateMultipleSelectionQuestionPayload = await db.questionBase.update({
          where: {
            id
          },
          data: {
            question,
            program: program || undefined,
            module: module || undefined
          }
        });
        return updateMultipleSelectionQuestionPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    deleteMultipleSelectionQuestion: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        id
      } = input;

      try {
        const deleteMultipleSelectionQuestionPayload = await db.questionBase.delete({
          where: {
            id
          },
          include: {
            multipleSelectionQuestion: {
              include: {
                choices: true
              }
            },
            createdBy: true
          }
        });
        const {
          multipleSelectionQuestion,
          ...questionDetails
        } = deleteMultipleSelectionQuestionPayload;
        return {
          choices: multipleSelectionQuestion === null || multipleSelectionQuestion === void 0 ? void 0 : multipleSelectionQuestion.choices,
          ...questionDetails
        };
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    createMultipleSelectionChoice: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        description,
        isCorrect,
        multipleSelectionQuestionId
      } = input;
      if (!multipleSelectionQuestionId) throw new _apolloServerExpress.UserInputError('Multiple Selection Question Id must be provided');

      try {
        const addMultipleSelectionChoicePayload = await db.multipleSelectionOption.create({
          data: {
            description,
            isCorrect,
            multipleSelectionQuestionId
          }
        });
        return addMultipleSelectionChoicePayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    updateMultipleSelectionChoice: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        description,
        isCorrect,
        id
      } = input;

      try {
        const addMultipleSelectionChoicePayload = await db.multipleSelectionOption.update({
          where: {
            id
          },
          data: {
            description,
            isCorrect
          }
        });
        return addMultipleSelectionChoicePayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    deleteMultipleSelectionChoice: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        id
      } = input;

      try {
        const deleteMultipleSelectionOptionPayload = await db.multipleSelectionOption.delete({
          where: {
            id
          }
        });
        return deleteMultipleSelectionOptionPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    }
  }
};
var _default = resolvers;
exports.default = _default;
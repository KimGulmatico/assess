"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServer = require("apollo-server");

const typeDefs = (0, _apolloServer.gql)`
  type CodeQuestion implements Question {
    id: String
    question: String
    program: Program
    module: String
    createdAt: Date
    updatedAt: Date
    createdById: String
    createdBy: User
    questionType: QuestionType
    activityId: String
  }

  input CreateCodeQuestionInput {
    question: String!
    program: Program!
    module: String!
    activityId: String!
  }

  input UpdateCodeQuestionInput {
    id: String!
    question: String
    activityId: String
    program: Program
    module: String
  }

  input DeleteCodeQuestionInput {
    id: String!
  }

  type CodeActivities @cacheControl(maxAge: 86400) {
    id: String
    description: String
  }

  input CodeQuestionAnswerInput {
    submitterID: String!
    activityID: String!
    code: String!
  }

  type CodeQuestionOutput {
    passed: Boolean
    inputs: [String]
    expectedOutput: String
    actualOutput: String
  }

  type CodeQuestionTestResult {
    id: String
    output: [CodeQuestionOutput]
    showTestCase: Boolean
  }

  type Query {
    codeQuestions: [CodeQuestion] @auth
    codeQuestion(id: String!): CodeQuestion @auth
    codeActivities: [CodeActivities] @auth
  }

  type Mutation {
    createCodeQuestion(input: CreateCodeQuestionInput!): CodeQuestion
      @auth(requires: ADMIN)
    updateCodeQuestion(input: UpdateCodeQuestionInput!): CodeQuestion
      @auth(requires: ADMIN)
    deleteCodeQuestion(input: DeleteCodeQuestionInput!): CodeQuestion
      @auth(requires: ADMIN)
    runCodeQuestionAnswer(
      input: CodeQuestionAnswerInput!
    ): CodeQuestionTestResult @auth
  }
`;
var _default = typeDefs;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServer = require("apollo-server");

const typeDefs = (0, _apolloServer.gql)`
  type Option {
    id: String
    description: String
    isCorrect: Boolean @auth(requires: ADMIN)
  }

  type MultipleChoiceQuestion implements Question {
    id: String
    question: String
    program: Program
    module: String
    createdAt: Date
    updatedAt: Date
    createdById: String
    createdBy: User
    questionType: QuestionType
    options: [Option]
  }

  input CreateOptionInput {
    description: String!
    isCorrect: Boolean!
    multipleChoiceQuestionId: String
  }

  input CreateMultipleChoiceQuestionInput {
    question: String!
    program: Program!
    module: String!
    options: [CreateOptionInput!]!
  }

  input UpdateOptionInput {
    id: String!
    description: String!
    isCorrect: Boolean!
  }

  input UpdateMultipleChoiceQuestionInput {
    id: String!
    question: String!
    program: Program
    module: String
  }

  input DeleteMultipleChoiceQuestionInput {
    id: String!
  }

  input DeleteOptionInput {
    id: String!
  }

  type Query {
    multipleChoiceQuestions: [MultipleChoiceQuestion] @auth
    multipleChoiceQuestion(id: String!): MultipleChoiceQuestion @auth
  }

  type Mutation {
    createMultipleChoiceQuestion(
      input: CreateMultipleChoiceQuestionInput!
    ): MultipleChoiceQuestion @auth(requires: ADMIN)
    updateMultipleChoiceQuestion(
      input: UpdateMultipleChoiceQuestionInput!
    ): MultipleChoiceQuestion @auth(requires: ADMIN)
    deleteMultipleChoiceQuestion(
      input: DeleteMultipleChoiceQuestionInput!
    ): MultipleChoiceQuestion @auth(requires: ADMIN)
    createMultipleChoiceOption(input: CreateOptionInput!): Option
      @auth(requires: ADMIN)
    updateMultipleChoiceOption(input: UpdateOptionInput!): Option
      @auth(requires: ADMIN)
    deleteMultipleChoiceOption(input: DeleteOptionInput!): Option
      @auth(requires: ADMIN)
  }
`;
var _default = typeDefs;
exports.default = _default;
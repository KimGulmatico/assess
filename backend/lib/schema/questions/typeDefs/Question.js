"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServer = require("apollo-server");

const typeDefs = (0, _apolloServer.gql)`
  interface Question {
    id: String
    question: String
    program: Program
    module: String
    createdAt: Date
    updatedAt: Date
    createdById: String
    createdBy: User
    questionType: QuestionType
  }

  enum QuestionType {
    CODE_QUESTION
    MULTIPLE_CHOICE_QUESTION
    MULTIPLE_SELECTION_QUESTION
  }

  type QuestionConnection {
    nodes: [Question]
    pageInfo: PageInfo!
  }

  type QuestionConnection {
    nodes: [Question]
    pageInfo: PageInfo!
  }

  type Query {
    questions(program: Program, module: String): [Question] @auth
    searchQuestions(
      first: Int
      last: Int
      after: String
      before: String
      keywords: String
      filters: String
    ): QuestionConnection @auth
    questionsConnection(
      first: Int
      last: Int
      after: String
      before: String
    ): QuestionConnection @auth
  }
`;
var _default = typeDefs;
exports.default = _default;
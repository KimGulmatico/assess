"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _graphql = require("graphql");

const resolvers = {
  Date: new _graphql.GraphQLScalarType({
    name: 'Date',
    description: 'Date custom scalar type',

    serialize(value) {
      return new Date(value).getTime();
    },

    parseValue(value) {
      return new Date(value);
    },

    parseLiteral(ast) {
      if (ast.kind === _graphql.Kind.INT) {
        return new Date(parseInt(ast.value, 10));
      }

      return null;
    }

  })
};
var _default = resolvers;
exports.default = _default;
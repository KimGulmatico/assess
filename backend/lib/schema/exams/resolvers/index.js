"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = require("lodash");

var _Exam = _interopRequireDefault(require("./Exam"));

var _QuestionsOnExams = _interopRequireDefault(require("./QuestionsOnExams"));

var _studentExam = _interopRequireDefault(require("../../students/resolvers/studentExam"));

var _resolvers = _interopRequireDefault(require("../../users/resolvers"));

var _ExamSubscription = _interopRequireDefault(require("./ExamSubscription"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const resolvers = (0, _lodash.merge)(_Exam.default, _QuestionsOnExams.default, _studentExam.default, _resolvers.default, _ExamSubscription.default);
var _default = resolvers;
exports.default = _default;
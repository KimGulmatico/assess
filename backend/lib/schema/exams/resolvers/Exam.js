"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _runtime = require("@prisma/client/runtime");

var _apolloServerExpress = require("apollo-server-express");

var _utils = require("../../utils");

const resolvers = {
  Query: {
    exams: async (_parent, _args, {
      db
    }) => {
      try {
        const exams = await db.exam.findMany({
          orderBy: {
            createdAt: 'desc'
          }
        });
        return exams;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    examsConnection: async (_parent, args, {
      db
    }) => {
      const {
        take,
        limit
      } = (0, _utils.determinePaginationArgs)(args);
      const {
        after,
        before
      } = args;
      const cursor = after || before ? {
        id: after || before || undefined
      } : undefined;

      try {
        const exams = await db.exam.findMany({
          take,
          cursor,
          orderBy: {
            createdAt: 'desc'
          }
        });
        const examsWithHistoryQuery = exams.map(async exam => {
          const studentExams = await db.studentExam.findMany({
            where: {
              examId: exam.id
            },
            include: {
              student: true
            }
          });
          return { ...exam,
            studentExams
          };
        });
        const examsWithHistory = await Promise.all(examsWithHistoryQuery);
        const {
          nodes,
          pageInfo
        } = (0, _utils.getNodesAndPageInfo)(examsWithHistory, limit, args);
        return {
          nodes,
          pageInfo
        };
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    exam: async (_parent, {
      id
    }, {
      db
    }) => {
      try {
        const exam = await db.exam.findFirst({
          where: {
            id
          }
        });
        return exam;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    }
  },
  Exam: {
    questions: async (parent, _args, {
      db
    }) => {
      const {
        id: examId
      } = parent;
      if (!examId) throw new _apolloServerExpress.UserInputError('Exam Id must be provided');

      try {
        const questionsOnExams = await db.questionsOnExams.findMany({
          where: {
            examId
          },
          orderBy: {
            addedAt: 'asc'
          },
          select: {
            question: true
          }
        });
        return questionsOnExams.map(questionBase => questionBase.question);
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    questionsConnection: async (parent, args, {
      db
    }) => {
      const {
        id: examId
      } = parent;
      if (!examId) throw new _apolloServerExpress.UserInputError('Exam Id must be provided');
      const {
        take,
        limit
      } = (0, _utils.determinePaginationArgs)(args);
      const {
        after,
        before
      } = args;
      const cursor = after || before ? {
        examId_questionId: {
          questionId: after || before || '',
          examId: after || before || ''
        }
      } : undefined;

      try {
        const questionsOnExams = await db.questionsOnExams.findMany({
          take,
          cursor,
          where: {
            examId
          },
          orderBy: {
            addedAt: 'asc'
          },
          select: {
            question: true
          }
        });
        const questions = questionsOnExams.map(questionBase => questionBase.question);
        const {
          nodes,
          pageInfo
        } = (0, _utils.getNodesAndPageInfo)(questions, limit, args);
        return {
          nodes,
          pageInfo
        };
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    createdBy: async (parent, _args, {
      db
    }) => {
      const {
        id
      } = parent;
      if (!id) throw new _apolloServerExpress.UserInputError('CreatedBy Id must be provided');

      try {
        const user = await db.exam.findFirst({
          where: {
            id
          },
          select: {
            createdBy: {
              select: {
                id: true,
                email: true,
                firstName: true,
                lastName: true
              }
            }
          }
        });
        if (!user) throw new Error('User not found');
        return user.createdBy;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    }
  },
  Mutation: {
    createExam: async (_parent, {
      input
    }, {
      payload,
      db
    }) => {
      if (!payload) throw new _apolloServerExpress.AuthenticationError('You must be logged in');
      (0, _utils.inputsOnExamTypesErrorHandler)(input);
      const {
        userId
      } = payload;
      const {
        title,
        description,
        program,
        module,
        year,
        quarter,
        maxQuestionCount,
        examType,
        startDate,
        endDate,
        duration
      } = input;

      try {
        const createExamPayload = await db.exam.create({
          data: {
            title,
            description,
            program,
            module,
            year,
            quarter,
            maxQuestionCount,
            endDate,
            startDate,
            duration,
            examType,
            createdById: userId
          }
        });
        return createExamPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    updateExam: async (_parent, {
      input
    }, {
      db
    }) => {
      (0, _utils.inputsOnExamTypesErrorHandler)(input);
      const {
        id,
        title,
        description,
        program,
        module,
        year,
        quarter,
        maxQuestionCount,
        examType,
        startDate,
        endDate,
        duration,
        isPublished
      } = input;

      try {
        const updateExamPayload = await db.exam.update({
          where: {
            id
          },
          data: {
            title: title || undefined,
            description: description || undefined,
            program: program || undefined,
            module: module || undefined,
            year: year || undefined,
            quarter: quarter || undefined,
            maxQuestionCount,
            endDate: endDate || null,
            startDate: startDate || null,
            duration: duration || null,
            isPublished: isPublished ?? undefined,
            examType
          }
        });
        return updateExamPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    },
    deleteExam: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        id
      } = input;

      try {
        const deleteExamPayload = await db.exam.delete({
          where: {
            id
          }
        });
        return deleteExamPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    }
  }
};
var _default = resolvers;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServerCore = require("apollo-server-core");

var _apolloServerExpress = require("apollo-server-express");

var _mailgun = require("../../../API/mailgun");

const subscribeMutationErrorHandling = error => {
  switch (error.code) {
    case 'P2002':
      throw new _apolloServerExpress.UserInputError('Already subscribed to this exam');

    default:
      throw new Error('Internal server error');
  }
};

const unsubscribeMutationErrorHandling = error => {
  switch (error.code) {
    case 'P2025':
      throw new _apolloServerExpress.UserInputError('Exam to unsubscribe with does not exist');

    default:
      throw new Error('Internal server error');
  }
};

const resolvers = {
  Query: {
    subscription: async (_parent, {
      examId
    }, {
      db,
      payload
    }) => {
      if (!payload) throw new _apolloServerCore.AuthenticationError('You must be logged in');
      const {
        userId
      } = payload;

      try {
        const subscription = await db.examSubscription.findUnique({
          where: {
            userId_examId: {
              userId: userId,
              examId: examId
            }
          }
        });
        return subscription;
      } catch (error) {
        throw new Error('Internal server error');
      }
    }
  },
  Mutation: {
    subscribe: async (_parent, {
      examId
    }, {
      db,
      payload
    }) => {
      if (!payload) throw new _apolloServerCore.AuthenticationError('You must be logged in');
      const {
        userId
      } = payload;

      try {
        const subscribeToExamPayload = await db.examSubscription.create({
          data: {
            userId,
            examId
          }
        });
        return subscribeToExamPayload;
      } catch (error) {
        return subscribeMutationErrorHandling(error);
      }
    },
    unsubscribe: async (_parent, {
      examId
    }, {
      db,
      payload
    }) => {
      if (!payload) throw new _apolloServerCore.AuthenticationError('You must be logged in');
      const {
        userId
      } = payload;

      try {
        const unsubscribeToExamPayload = await db.examSubscription.delete({
          where: {
            userId_examId: {
              userId: userId,
              examId: examId
            }
          }
        });
        return unsubscribeToExamPayload;
      } catch (error) {
        return unsubscribeMutationErrorHandling(error);
      }
    },
    sendEmailNotification: async (_parent, {
      input
    }, {
      db,
      payload
    }) => {
      if (!payload) throw new _apolloServerCore.AuthenticationError('You must be logged in');
      const {
        email,
        firstName,
        lastName,
        userId
      } = payload;
      const {
        examId,
        event
      } = input;

      try {
        const subscriptions = await db.examSubscription.findMany({
          where: {
            examId: examId
          }
        });
        subscriptions.map(async subscription => {
          const admin = await db.user.findUnique({
            where: {
              id: subscription.userId
            }
          });
          const exam = await db.exam.findUnique({
            where: {
              id: examId
            }
          });
          let variables = {
            student_fullname: `${firstName} ${lastName}`,
            event: `${event}`,
            exam_title: `${exam === null || exam === void 0 ? void 0 : exam.title}`,
            viewexam_link: ''
          };
          let data = {
            from: 'Kingsland Assess <assess@notifications.kingsland.io>',
            to: `${admin === null || admin === void 0 ? void 0 : admin.email}`,
            subject: '',
            template: '',
            'h:X-Mailgun-Variables': JSON.stringify(variables)
          };

          if (event === 'started') {
            data = { ...data,
              template: 'kingsland-assess-start-exam',
              subject: `Start Exam Notification - ${firstName} ${lastName} - ${exam === null || exam === void 0 ? void 0 : exam.title}`
            };
          } else if (event === 'finished') {
            variables = { ...variables,
              viewexam_link: `https://assess.kingsland.io/viewexam/${userId}/${examId}`
            };
            data = { ...data,
              template: 'kingsland-assess-complete-exam',
              subject: `Exam Complete Notification - ${firstName} ${lastName} - ${exam === null || exam === void 0 ? void 0 : exam.title}`,
              'h:X-Mailgun-Variables': JSON.stringify(variables)
            };
          }

          await _mailgun.mailgun.messages.create(process.env.MAILGUN_DOMAIN, data);
        });
        return subscriptions;
      } catch (error) {
        throw new Error('Internal server error');
      }
    }
  }
};
var _default = resolvers;
exports.default = _default;
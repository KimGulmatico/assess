"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServerExpress = require("apollo-server-express");

const addQuestionOnExamErrorHandling = error => {
  switch (error.code) {
    case 'P2002':
      throw new _apolloServerExpress.UserInputError('This question has already been added');

    case 'P2003':
      throw new _apolloServerExpress.UserInputError('Record to add does not exist');

    default:
      throw new Error('Internal server error');
  }
};

const removeQuestionOnExamErrorHandling = error => {
  switch (error.code) {
    case 'P2025':
      throw new _apolloServerExpress.UserInputError('Record to delete does not exist');

    default:
      throw new Error('Internal server error');
  }
};

const resolvers = {
  Mutation: {
    addQuestionOnExam: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        examId,
        questionId
      } = input;

      try {
        const addQuestionOnExamPayload = await db.questionsOnExams.create({
          data: {
            questionId,
            examId
          }
        });
        return addQuestionOnExamPayload;
      } catch (error) {
        return addQuestionOnExamErrorHandling(error);
      }
    },
    removeQuestionOnExam: async (_parent, {
      input
    }, {
      db
    }) => {
      const {
        examId,
        questionId
      } = input;

      try {
        const removeQuestionOnExamPayload = await db.questionsOnExams.delete({
          where: {
            examId_questionId: {
              examId,
              questionId
            }
          }
        });
        return removeQuestionOnExamPayload;
      } catch (error) {
        return removeQuestionOnExamErrorHandling(error);
      }
    }
  }
};
var _default = resolvers;
exports.default = _default;
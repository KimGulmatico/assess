"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _Exam = _interopRequireDefault(require("./Exam"));

var _QuestionsOnExams = _interopRequireDefault(require("./QuestionsOnExams"));

var _ExamSubscription = _interopRequireDefault(require("./ExamSubscription"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const typeDefs = [_Exam.default, _QuestionsOnExams.default, _ExamSubscription.default];
var _default = typeDefs;
exports.default = _default;
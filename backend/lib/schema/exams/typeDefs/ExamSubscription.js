"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServer = require("apollo-server");

const typeDefs = (0, _apolloServer.gql)`
  input SendEmailNotificationInput {
    examId: String!
    event: String!
  }

  type ExamSubscription {
    userId: String!
    examId: String!
  }

  type SubscriptionPayload {
    examId: String
  }

  type Query {
    subscription(examId: String!): SubscriptionPayload @auth(requires: ADMIN)
  }

  type Mutation {
    subscribe(examId: String!): SubscriptionPayload @auth(requires: ADMIN)
    unsubscribe(examId: String!): SubscriptionPayload @auth(requires: ADMIN)
    sendEmailNotification(
      input: SendEmailNotificationInput!
    ): [ExamSubscription]
  }
`;
var _default = typeDefs;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServer = require("apollo-server");

const typeDefs = (0, _apolloServer.gql)`
  type Exam {
    id: String
    title: String
    description: String
    program: Program
    module: String
    year: Int
    quarter: Quarter
    isPublished: Boolean
    maxQuestionCount: Int
    createdAt: Date
    updatedAt: Date
    createdById: String
    createdBy: User
    startDate: Date
    endDate: Date
    duration: Int
    questions: [Question]
    questionsConnection(
      first: Int
      last: Int
      after: String
      before: String
    ): QuestionConnection
    examType: ExamType
    studentExams: [StudentExam]
  }

  type ExamsConnection {
    nodes: [Exam]
    pageInfo: PageInfo!
  }

  input CreateExamInput {
    title: String!
    description: String!
    program: Program!
    module: String!
    year: Int!
    quarter: Quarter!
    maxQuestionCount: Int
    startDate: Date
    endDate: Date
    duration: Int
    examType: ExamType!
  }

  input UpdateExamInput {
    id: String!
    title: String
    description: String
    program: Program
    module: String
    year: Int
    quarter: Quarter
    maxQuestionCount: Int
    startDate: Date
    endDate: Date
    duration: Int
    examType: ExamType!
    isPublished: Boolean
  }

  input DeleteExamInput {
    id: String!
  }

  type DeleteExamPayload {
    id: String
    title: String
    description: String
    program: Program
    year: Int
    quarter: Quarter
    isPublished: Boolean
    maxQuestionCount: Int
    createdAt: Date
    updatedAt: Date
    createdById: String
    examType: ExamType
  }

  enum Program {
    BLOCKCHAIN
    FULL_STACK
    CYBERSECURITY
    TECH_SALES
  }

  enum Quarter {
    Q1
    Q2
    Q3
    Q4
  }

  enum ExamType {
    FREE
    FIXED
    FLEXIBLE
  }

  type Query {
    exams: [Exam] @auth
    exam(id: String!): Exam @auth
    examsConnection(
      first: Int
      last: Int
      after: String
      before: String
    ): ExamsConnection @auth
  }

  type Mutation {
    createExam(input: CreateExamInput!): Exam @auth(requires: ADMIN)
    updateExam(input: UpdateExamInput!): Exam @auth(requires: ADMIN)
    deleteExam(input: DeleteExamInput!): DeleteExamPayload
      @auth(requires: ADMIN)
  }
`;
var _default = typeDefs;
exports.default = _default;
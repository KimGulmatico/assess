"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServer = require("apollo-server");

const typeDefs = (0, _apolloServer.gql)`
  input QuestionOnExamInput {
    examId: String!
    questionId: String!
  }

  type QuestionOnExamPayload {
    examId: String
    questionId: String
  }

  type MultipleChoiceQuestionOnExamPayload {
    examId: String
    multipleChoiceQuestionId: String
  }

  type MultipleChoiceQuestionOnExamPayload {
    examId: String
    multipleChoiceQuestionId: String
  }

  type MultipleSelectionQuestionOnExamPayload {
    examId: String
    multipleSelectionQuestionId: String
  }

  type CodeQuestionOnExamPayload {
    examId: String
    codeQuestionId: String
  }

  type Mutation {
    addQuestionOnExam(input: QuestionOnExamInput!): QuestionOnExamPayload
      @auth(requires: ADMIN)
    removeQuestionOnExam(input: QuestionOnExamInput!): QuestionOnExamPayload
      @auth(requires: ADMIN)
  }
`;
var _default = typeDefs;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloServer = require("apollo-server");

const typeDefs = (0, _apolloServer.gql)`
  type ExamDetails {
    id: String
    title: String
    description: String
    program: Program
    module: String
    year: Int
    quarter: Quarter
    maxQuestionCount: Int
    createdAt: Date
    updatedAt: Date
    createdById: String
    startDate: Date
    endDate: Date
    duration: Int
    examType: ExamType
  }

  type CodeAnswer {
    questionId: String!
    code: String
  }

  type MultipleChoiceAnswer {
    questionId: String!
    option: String
  }

  type MultipleSelectionAnswer {
    questionId: String!
    choices: [String!]
  }

  union Answer = CodeAnswer | MultipleChoiceAnswer | MultipleSelectionAnswer

  type MultipleChoiceQuestionOutput {
    option: Option
    selected: Boolean
  }

  type MultipleSelectionQuestionOutput {
    choice: Choice
    selected: Boolean
  }

  union Output =
      CodeQuestionOutput
    | MultipleChoiceQuestionOutput
    | MultipleSelectionQuestionOutput

  type QuestionOutput {
    question: Question
    studentAnswer: Answer
    points: Float!
    output: [Output]
  }

  type SubmissionDetails {
    examStartTimestamp: Date
    examEndTimestamp: Date
  }

  type StudentExam {
    id: String
    student: User
    examDetails: ExamDetails
    examQuestions: [Question]
    studentAnswers: [Answer]
    examSummary: [QuestionOutput] @auth(requires: ADMIN)
    score: Float
    submittedAt: Date
    openedAt: Date
  }

  input ExamDetailsInput {
    id: String!
    title: String!
    description: String!
    program: Program!
    module: String!
    year: Int!
    quarter: Quarter!
    maxQuestionCount: Int
    createdAt: Date
    updatedAt: Date
    createdById: String!
    startDate: Date
    endDate: Date
    duration: Int
    examType: ExamType!
  }

  input AnswerInput {
    questionId: String!
    code: String
    option: String
    choices: [String!]
  }

  input OpenStudentExamInput {
    examDetails: ExamDetailsInput!
    examQuestions: [JSON!]!
    openedAt: Date!
  }

  type OpenStudentExamPayload {
    id: String!
    student: User
    examDetails: ExamDetails
    studentAnswers: [JSON]
    examQuestions: [JSON]
    openedAt: Date!
  }

  input SubmitStudentExamInput {
    id: String!
    examDetails: ExamDetailsInput!
    examQuestions: [JSON!]!
    studentAnswers: [AnswerInput!]!
    submittedAt: Date
  }

  type SubmitStudentExamPayload {
    student: User
    examDetails: ExamDetails
    score: Float
    submittedAt: Date
  }

  input UpdateStudentExamAnswersInput {
    id: String!
    studentAnswers: [AnswerInput!]!
  }

  type UpdateStudentExamAnswersPayload {
    id: String
    student: User
    examDetails: ExamDetails
    openedAt: Date
  }

  type StudentExamsConnection {
    nodes: [StudentExam]
    pageInfo: PageInfo!
  }

  type Query {
    studentExam(studentId: String!, examId: String!): StudentExam @auth
    studentExams: [StudentExam] @auth
    studentExamsConnection(
      first: Int
      last: Int
      after: String
      before: String
    ): StudentExamsConnection @auth
  }

  type Mutation {
    submitStudentExam(input: SubmitStudentExamInput!): SubmitStudentExamPayload
      @auth
    openStudentExam(input: OpenStudentExamInput!): OpenStudentExamPayload @auth
    updateStudentExamAnswers(
      input: UpdateStudentExamAnswersInput!
    ): UpdateStudentExamAnswersPayload @auth
  }
`;
var _default = typeDefs;
exports.default = _default;
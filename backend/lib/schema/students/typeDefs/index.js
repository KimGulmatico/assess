"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _studentExam = _interopRequireDefault(require("./studentExam"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const typeDefs = [_studentExam.default];
var _default = typeDefs;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.multipleSelectionQuestionChecker = exports.multipleChoiceQuestionChecker = exports.codeQuestionChecker = exports.checkExam = void 0;

var _runtime = require("@prisma/client/runtime");

var _apolloServerCore = require("apollo-server-core");

var _lodash = require("lodash");

var _utils = require("../../utils");

const codeQuestionChecker = async (codeQuestion, studentAnswer, studentId, dataSources) => {
  var _questionOutput$outpu;

  const questionOutput = {
    points: 0,
    question: codeQuestion,
    studentAnswer,
    output: []
  };
  const {
    activityId
  } = codeQuestion;
  if (!activityId) throw new Error('No activity Id provided');
  let result;

  try {
    result = await dataSources.codeRunnerAPI.submitExam({
      submitterID: studentId,
      code: (studentAnswer === null || studentAnswer === void 0 ? void 0 : studentAnswer.code) || null,
      activityID: activityId
    });
  } catch {
    throw new Error('Internal Server Error');
  }

  if (!result.output) throw new Error('Unable to retrieve code question test result');
  const {
    output
  } = result;
  (_questionOutput$outpu = questionOutput.output) === null || _questionOutput$outpu === void 0 ? void 0 : _questionOutput$outpu.push(...output);
  const pointsPerTestCase = 1 / output.length;
  output.forEach(testCase => {
    if (testCase !== null && testCase !== void 0 && testCase.passed) questionOutput.points += pointsPerTestCase;
  });
  return questionOutput;
};

exports.codeQuestionChecker = codeQuestionChecker;

const multipleChoiceQuestionChecker = async (multipleChoiceQuestion, studentAnswer, db) => {
  const questionOutput = {
    points: 0,
    question: multipleChoiceQuestion,
    studentAnswer,
    output: []
  };
  const {
    id: multipleChoiceQuestionId
  } = multipleChoiceQuestion;
  if (!multipleChoiceQuestionId) throw new _apolloServerCore.UserInputError('Question id not provided');
  let allOptions;

  try {
    allOptions = await db.multipleChoiceOption.findMany({
      where: {
        multipleChoiceQuestionId
      }
    });
  } catch (error) {
    if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
    throw new Error('Internal Server Error');
  }

  allOptions.forEach(option => {
    var _questionOutput$outpu2;

    const output = {
      option,
      selected: false
    };

    if ((studentAnswer === null || studentAnswer === void 0 ? void 0 : studentAnswer.option) === option.id) {
      output.selected = true;
      if (option.isCorrect) questionOutput.points += 1;
    }

    (_questionOutput$outpu2 = questionOutput.output) === null || _questionOutput$outpu2 === void 0 ? void 0 : _questionOutput$outpu2.push(output);
  });
  return questionOutput;
};

exports.multipleChoiceQuestionChecker = multipleChoiceQuestionChecker;

const multipleSelectionQuestionChecker = async (multipleSelectionQuestion, studentAnswer, db) => {
  const questionSummary = {
    points: 0,
    question: multipleSelectionQuestion,
    studentAnswer,
    output: []
  };
  const {
    id: multipleSelectionQuestionId,
    choices
  } = multipleSelectionQuestion;
  if (!multipleSelectionQuestionId) throw new _apolloServerCore.UserInputError('Question Id not provided');
  if (!choices) return questionSummary;
  let allChoices;

  try {
    allChoices = await db.multipleSelectionOption.findMany({
      where: {
        multipleSelectionQuestionId
      }
    });
  } catch (error) {
    if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
    throw new Error('Internal Server Error');
  }

  const correctAnswers = (0, _lodash.filter)(allChoices, {
    isCorrect: true
  });
  const deductionPoints = 1 / allChoices.length;
  const pointsPerCorrectAnswer = 1 / correctAnswers.length;
  allChoices.forEach(choice => {
    var _questionSummary$outp;

    const output = {
      choice,
      selected: false
    };

    if ((0, _lodash.includes)(studentAnswer === null || studentAnswer === void 0 ? void 0 : studentAnswer.choices, choice.id)) {
      output.selected = true;

      if (choice.isCorrect) {
        questionSummary.points += pointsPerCorrectAnswer;
      } else {
        questionSummary.points -= deductionPoints;
      }
    }

    (_questionSummary$outp = questionSummary.output) === null || _questionSummary$outp === void 0 ? void 0 : _questionSummary$outp.push(output);
  });
  questionSummary.points = questionSummary.points < 0 ? 0 : questionSummary.points;
  return questionSummary;
};

exports.multipleSelectionQuestionChecker = multipleSelectionQuestionChecker;

const checkExam = async (examQuestions, studentAnswers, studentId, dataSources, db) => {
  let score = 0;
  const examSummary = [];
  await Promise.all(examQuestions.map(async examQuestion => {
    const {
      questionType,
      id
    } = examQuestion;
    if (!questionType) throw new _apolloServerCore.UserInputError('Question type not provided');
    const studentAnswer = (0, _lodash.find)(studentAnswers, {
      questionId: id
    });

    switch (questionType) {
      case 'CODE_QUESTION':
        {
          const questionOutput = await codeQuestionChecker(examQuestion, studentAnswer, studentId, dataSources);
          score += questionOutput.points;
          examSummary.push(questionOutput);
          break;
        }

      case 'MULTIPLE_CHOICE_QUESTION':
        {
          const questionOutput = await multipleChoiceQuestionChecker(examQuestion, studentAnswer, db);
          score += questionOutput.points;
          examSummary.push(questionOutput);
          break;
        }

      case 'MULTIPLE_SELECTION_QUESTION':
        {
          const questionOutput = await multipleSelectionQuestionChecker(examQuestion, studentAnswer, db);
          score += questionOutput.points;
          examSummary.push(questionOutput);
          break;
        }

      default:
        throw new _apolloServerCore.UserInputError('Invalid question type');
    }
  }));
  return {
    score,
    examSummary
  };
};

exports.checkExam = checkExam;
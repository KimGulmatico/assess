"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = require("lodash");

var _studentExam = _interopRequireDefault(require("./studentExam"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const resolvers = (0, _lodash.merge)(_studentExam.default);
var _default = resolvers;
exports.default = _default;
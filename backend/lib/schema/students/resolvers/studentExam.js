"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _runtime = require("@prisma/client/runtime");

var _apolloServerCore = require("apollo-server-core");

var _utils = require("../../utils");

var _examChecker = require("../utils/examChecker");

const examQuestionsErrorHandler = examQuestions => {
  examQuestions.forEach(examQuestion => {
    if (typeof examQuestion !== 'object') throw new _apolloServerCore.UserInputError('Please provide an array of questions');
    if (!('id' in examQuestion && 'question' in examQuestion && 'program' in examQuestion && 'module' in examQuestion && 'createdById' in examQuestion && 'questionType' in examQuestion)) throw new _apolloServerCore.UserInputError('Invalid question');

    switch (examQuestion.questionType) {
      case 'MULTIPLE_CHOICE_QUESTION':
        if (!('options' in examQuestion)) throw new _apolloServerCore.UserInputError('Invalid multiple choice question');
        break;

      case 'MULTIPLE_SELECTION_QUESTION':
        if (!('choices' in examQuestion)) throw new _apolloServerCore.UserInputError('Invalid multiple selection question');
        break;

      case 'CODE_QUESTION':
        if (!('activityId' in examQuestion)) throw new _apolloServerCore.UserInputError('Invalid code question');
        break;

      default:
        throw new _apolloServerCore.UserInputError('Invalid questionType');
    }
  });
};

const submissionDetailsErrorHandler = (examType, submittedAt) => {
  switch (examType) {
    case 'FLEXIBLE':
      if (!submittedAt) throw new _apolloServerCore.UserInputError('SubmittedAt not provided correctly');
      break;

    default:
      break;
  }
};

const resolvers = {
  Output: {
    __resolveType: output => {
      if ('passed' in output) return 'CodeQuestionOutput';
      if ('choice' in output) return 'MultipleSelectionQuestionOutput';
      if ('option' in output) return 'MultipleChoiceQuestionOutput';
      return null;
    }
  },
  Answer: {
    __resolveType: answer => {
      if ('code' in answer) return 'CodeAnswer';
      if ('option' in answer) return 'MultipleChoiceAnswer';
      if ('choices' in answer) return 'MultipleSelectionAnswer';
      return null;
    }
  },
  Query: {
    studentExam: async (_parent, {
      examId,
      studentId
    }, {
      db,
      payload
    }) => {
      if (!payload) throw new _apolloServerCore.AuthenticationError('You must be logged in');
      const {
        userId,
        roles
      } = payload;
      if (!(userId === studentId || roles.includes('ADMIN'))) throw new _apolloServerCore.ForbiddenError('You must be logged in as an ADMIN or user of this account');
      const studentExam = await db.studentExam.findFirst({
        where: {
          examId,
          studentId
        },
        include: {
          student: true
        }
      });
      const response = { ...studentExam
      };
      return response;
    },
    studentExams: async (_parent, _args, {
      db,
      payload
    }) => {
      if (!payload) throw new _apolloServerCore.AuthenticationError('You must be logged in');
      const {
        userId
      } = payload;
      if (!userId) throw new _apolloServerCore.ForbiddenError('You must be logged in as an ADMIN or user of this account');
      const studentExams = await db.studentExam.findMany({
        where: {
          studentId: userId
        },
        include: {
          student: true
        }
      });
      return studentExams;
    },
    studentExamsConnection: async (_parent, args, {
      db,
      payload
    }) => {
      if (!payload) throw new _apolloServerCore.AuthenticationError('You must be logged in');
      const {
        userId
      } = payload;
      const {
        take,
        limit
      } = (0, _utils.determinePaginationArgs)(args);
      const {
        after,
        before
      } = args;
      const cursor = after || before ? {
        id: after || before || undefined
      } : undefined;

      try {
        const exams = await db.studentExam.findMany({
          take,
          cursor,
          orderBy: {
            submittedAt: 'desc'
          },
          where: {
            studentId: userId
          }
        });
        const examsWithHistoryQuery = exams.map(async exam => {
          const studentExams = await db.studentExam.findMany({
            where: {
              studentId: userId
            },
            include: {
              student: true
            }
          });
          return { ...exam,
            studentExams
          };
        });
        const examsWithHistory = await Promise.all(examsWithHistoryQuery);
        const {
          nodes,
          pageInfo
        } = (0, _utils.getNodesAndPageInfo)(examsWithHistory, limit, args);
        return {
          nodes,
          pageInfo
        };
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError) (0, _utils.prismaErrorHandler)(error);
        throw new Error('Internal Server Error');
      }
    }
  },
  Mutation: {
    openStudentExam: async (_parent, {
      input
    }, {
      dataSources,
      payload,
      db
    }) => {
      if (!payload) throw new _apolloServerCore.AuthenticationError('You must be logged in');
      if (!dataSources) throw new Error('Internal Server Error');
      const {
        examQuestions,
        examDetails,
        openedAt
      } = input;
      examQuestionsErrorHandler(examQuestions);
      (0, _utils.inputsOnExamTypesErrorHandler)(examDetails);
      const {
        userId: studentId
      } = payload;
      const {
        id: examId
      } = examDetails;
      const examExists = await db.studentExam.findFirst({
        where: {
          studentId,
          examId
        },
        include: {
          student: true
        }
      });

      if (examExists) {
        if (examExists.submittedAt) {
          throw new Error(JSON.stringify({
            message: 'Exam has already been taken',
            data: examExists
          }));
        }

        return examExists;
      }

      try {
        const studentExamPayload = await db.studentExam.create({
          data: {
            studentId,
            examDetails,
            examQuestions,
            examId,
            openedAt: openedAt
          },
          include: {
            student: true
          }
        });
        return studentExamPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError && error.code === 'P2002') throw new Error('Exam has already been taken');
        throw new Error('Internal Server Error');
      }
    },
    updateStudentExamAnswers: async (_parent, {
      input
    }, {
      db,
      payload,
      dataSources
    }) => {
      if (!payload) throw new _apolloServerCore.AuthenticationError('You must be logged in');
      if (!dataSources) throw new Error('Internal Server Error');
      const {
        id,
        studentAnswers
      } = input;

      try {
        const studentExamPayload = await db.studentExam.update({
          where: {
            id
          },
          data: {
            studentAnswers
          },
          include: {
            student: true
          }
        });
        return studentExamPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError && error.code === 'P2002') throw new Error('Exam has already been taken');
        throw new Error('Internal Server Error');
      }
    },
    submitStudentExam: async (_parent, {
      input
    }, {
      db,
      payload,
      dataSources
    }) => {
      if (!payload) throw new _apolloServerCore.AuthenticationError('You must be logged in');
      if (!dataSources) throw new Error('Internal Server Error');
      const {
        id,
        examQuestions,
        examDetails,
        studentAnswers,
        submittedAt
      } = input;
      examQuestionsErrorHandler(examQuestions);
      (0, _utils.inputsOnExamTypesErrorHandler)(examDetails);
      submissionDetailsErrorHandler(examDetails.examType, submittedAt);
      const {
        userId: studentId
      } = payload;
      let examResult;

      try {
        examResult = await (0, _examChecker.checkExam)(examQuestions, studentAnswers, studentId, dataSources, db);
      } catch (error) {
        throw new Error(error.message);
      }

      const {
        score,
        examSummary
      } = examResult;

      try {
        const studentExamPayload = await db.studentExam.update({
          where: {
            id
          },
          data: {
            examDetails,
            examQuestions,
            studentAnswers,
            score,
            examSummary,
            submittedAt: submittedAt || undefined
          },
          include: {
            student: true
          }
        });
        return studentExamPayload;
      } catch (error) {
        if (error instanceof _runtime.PrismaClientKnownRequestError && error.code === 'P2002') throw new Error('Exam has already been taken');
        throw new Error('Internal Server Error');
      }
    }
  }
};
var _default = resolvers;
exports.default = _default;
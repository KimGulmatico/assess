"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _schema = require("@graphql-tools/schema");

var _resolvers = _interopRequireDefault(require("./resolvers"));

var _typeDefs = _interopRequireDefault(require("./typeDefs"));

var _auth = _interopRequireDefault(require("./directives/auth"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let executableSchema = (0, _schema.makeExecutableSchema)({
  resolvers: _resolvers.default,
  typeDefs: _typeDefs.default
}); // apply any directive transformer here before exporting schema

executableSchema = (0, _auth.default)(executableSchema, 'auth');
const schema = executableSchema;
var _default = schema;
exports.default = _default;
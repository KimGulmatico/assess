"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.verifyToken = exports.generateToken = void 0;

var _jsonwebtoken = require("jsonwebtoken");

require('dotenv').config();

const generateToken = payload => {
  if (typeof process.env.JWT_SECRET !== 'string') {
    throw new Error('provide a JWT secret');
  }

  return (0, _jsonwebtoken.sign)(payload, process.env.JWT_SECRET, {
    expiresIn: '3days'
  });
};

exports.generateToken = generateToken;

const verifyToken = token => {
  if (!token) {
    return null;
  }

  if (typeof process.env.JWT_SECRET !== 'string') {
    throw new Error('provide a JWT secret');
  }

  try {
    const payload = (0, _jsonwebtoken.verify)(token, process.env.JWT_SECRET);
    return payload;
  } catch (error) {
    return null;
  }
};

exports.verifyToken = verifyToken;
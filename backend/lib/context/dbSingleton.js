"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _jestMockExtended = require("jest-mock-extended");

var _database = _interopRequireDefault(require("./database"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

jest.mock('./database', () => ({
  __esModule: true,
  default: (0, _jestMockExtended.mockDeep)()
}));
const prismaMock = _database.default;
beforeEach(() => {
  (0, _jestMockExtended.mockReset)(prismaMock);
});
var _default = prismaMock;
exports.default = _default;
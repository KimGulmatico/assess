"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _auth = require("../middleware/auth");

var _database = _interopRequireDefault(require("./database"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const context = async ({
  req,
  res
}) => {
  const authHeader = req.cookies.jwt || '';
  const payload = (0, _auth.verifyToken)(authHeader);
  return {
    req,
    res,
    payload,
    db: _database.default
  };
};

var _default = context;
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Role = exports.QuestionType = exports.Quarter = exports.Program = exports.ExamType = exports.CacheControlScope = void 0;

/** All built-in and custom scalars, mapped to their actual values */
let CacheControlScope;
exports.CacheControlScope = CacheControlScope;

(function (CacheControlScope) {
  CacheControlScope["Private"] = "PRIVATE";
  CacheControlScope["Public"] = "PUBLIC";
})(CacheControlScope || (exports.CacheControlScope = CacheControlScope = {}));

let ExamType;
exports.ExamType = ExamType;

(function (ExamType) {
  ExamType["Fixed"] = "FIXED";
  ExamType["Flexible"] = "FLEXIBLE";
  ExamType["Free"] = "FREE";
})(ExamType || (exports.ExamType = ExamType = {}));

let Program;
exports.Program = Program;

(function (Program) {
  Program["Blockchain"] = "BLOCKCHAIN";
  Program["Cybersecurity"] = "CYBERSECURITY";
  Program["FullStack"] = "FULL_STACK";
  Program["TechSales"] = "TECH_SALES";
})(Program || (exports.Program = Program = {}));

let Quarter;
exports.Quarter = Quarter;

(function (Quarter) {
  Quarter["Q1"] = "Q1";
  Quarter["Q2"] = "Q2";
  Quarter["Q3"] = "Q3";
  Quarter["Q4"] = "Q4";
})(Quarter || (exports.Quarter = Quarter = {}));

let QuestionType;
exports.QuestionType = QuestionType;

(function (QuestionType) {
  QuestionType["CodeQuestion"] = "CODE_QUESTION";
  QuestionType["MultipleChoiceQuestion"] = "MULTIPLE_CHOICE_QUESTION";
  QuestionType["MultipleSelectionQuestion"] = "MULTIPLE_SELECTION_QUESTION";
})(QuestionType || (exports.QuestionType = QuestionType = {}));

let Role;
exports.Role = Role;

(function (Role) {
  Role["Admin"] = "ADMIN";
  Role["Student"] = "STUDENT";
})(Role || (exports.Role = Role = {}));
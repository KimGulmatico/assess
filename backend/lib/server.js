"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.startApolloServer = exports.default = void 0;

var _apolloServerExpress = require("apollo-server-express");

var _apolloServerCore = require("apollo-server-core");

var _express = _interopRequireDefault(require("express"));

var _http = require("http");

var _cookieParser = _interopRequireDefault(require("cookie-parser"));

var _apolloServerPluginResponseCache = _interopRequireDefault(require("apollo-server-plugin-response-cache"));

var _schema = _interopRequireDefault(require("./schema"));

var _context = _interopRequireDefault(require("./context"));

var _codeRunnerAPI = _interopRequireDefault(require("./dataSources/codeRunnerAPI"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const port = process.env.PORT || 4000;
const corsConfig = {
  origin: ['https://assess.kingsland.io', 'https://studio.apollographql.com', 'http://localhost:3000', 'https://assess.kingslandtesting.com'],
  credentials: true
};

const startApolloServer = async () => {
  const app = (0, _express.default)();
  const httpServer = (0, _http.createServer)(app);
  const server = new _apolloServerExpress.ApolloServer({
    schema: _schema.default,
    plugins: [(0, _apolloServerCore.ApolloServerPluginDrainHttpServer)({
      httpServer
    }), (0, _apolloServerPluginResponseCache.default)()],
    dataSources: () => ({
      codeRunnerAPI: new _codeRunnerAPI.default()
    }),
    context: _context.default
  });
  await server.start();
  app.use((0, _cookieParser.default)());
  server.applyMiddleware({
    app,
    path: '/',
    cors: corsConfig
  });

  if (process.env.NODE_ENV !== 'test') {
    await new Promise(resolve => {
      httpServer.listen({
        port
      }, resolve);
    });
    console.warn(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`);
  }

  return httpServer;
};

exports.startApolloServer = startApolloServer;
startApolloServer();
var _default = {
  startApolloServer
};
exports.default = _default;
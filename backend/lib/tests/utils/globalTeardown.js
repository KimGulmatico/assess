"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _database = _interopRequireDefault(require("../../context/database"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const deleteData = async () => {
  await _database.default.studentExam.deleteMany();
  await _database.default.questionBase.deleteMany();
  await _database.default.examSubscription.deleteMany();
  await _database.default.exam.deleteMany();
  await _database.default.user.deleteMany();
  await _database.default.admin.deleteMany();
};

var _default = deleteData;
exports.default = _default;
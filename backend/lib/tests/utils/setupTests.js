"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mockStudentJwtPayload = exports.mockCookie = exports.mockAdminJwtPayload = void 0;

require("jest-extended");

const mockAdminJwtPayload = {
  userId: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
  email: 'ku@kingslanduniversity.com',
  firstName: 'Kingsland',
  lastName: 'Assess',
  roles: ['STUDENT', 'ADMIN'],
  iat: 1647495757,
  exp: 1647754957
};
exports.mockAdminJwtPayload = mockAdminJwtPayload;
const mockStudentJwtPayload = {
  userId: 'ffffffff-gggg-hhhh-iiii-jjjjjjjjjjjj',
  email: 'jdoe@gmail.com',
  firstName: 'John',
  lastName: 'Doe',
  roles: ['STUDENT'],
  iat: 1647495757,
  exp: 1647754957
};
exports.mockStudentJwtPayload = mockStudentJwtPayload;
const mockCookie = 'jwt=eyJhbGci; Path=/; Secure; HttpOnly;';
exports.mockCookie = mockCookie;
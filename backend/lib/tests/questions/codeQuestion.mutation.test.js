"use strict";

var _supertest = _interopRequireDefault(require("supertest"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _server = require("../../server");

var _setupTests = require("../utils/setupTests");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const jwtSpy = jest.spyOn(_jsonwebtoken.default, 'verify');
const mockSubmitExam = jest.fn().mockReturnValue({
  id: '12345-CHARITY-CAMPAIGN',
  output: [{
    actualOutput: '9308369967.75',
    expectedOutput: '9308369967.75',
    inputs: ['270', '519', '1386', '1375', '1741'],
    passed: true
  }, {
    actualOutput: '10438915046.70',
    expectedOutput: '10438915046.70',
    inputs: ['222', '673', '1621', '501', '1250'],
    passed: true
  }, {
    actualOutput: '3996511917.30',
    expectedOutput: '3996511917.30',
    inputs: ['226', '378', '958', '1285', '907'],
    passed: true
  }],
  showTestCase: false
});
jest.mock('../../dataSources/codeRunnerAPI', () => jest.fn().mockImplementation(() => ({
  submitExam: mockSubmitExam
})));
let app;
const createMutation = {
  query: `
    mutation CreateCodeQuestion($input: CreateCodeQuestionInput!) {
      createCodeQuestion(input: $input) {
        id
        question
        createdAt
        updatedAt
        activityId
      }
    }
  `,
  variables: {
    input: {
      question: 'Print all primes',
      program: 'TECH_SALES',
      module: 'M2',
      activityId: 'PRIMES'
    }
  }
};
const updateMutation = {
  query: `
    mutation UpdateCodeQuestion($input: UpdateCodeQuestionInput!) {
      updateCodeQuestion(input: $input) {
        id
        question
        program
        module
        createdAt
        updatedAt
        activityId
      }
    }
  `,
  variables: {
    input: {
      id: 'code-0002-1234-1234-codequestion',
      question: 'Print the first 100 primes',
      activityId: 'PRIMES-100',
      program: 'FULL_STACK',
      module: 'M4'
    }
  }
};
const deleteMutation = {
  query: `
  mutation DeleteCodeQuestion($input: DeleteCodeQuestionInput!) {
  deleteCodeQuestion(input: $input) {
    id
    question
    createdAt
    updatedAt
    activityId
  }
}
  `,
  variables: {
    input: {
      id: 'code-0003-1234-1234-codequestion'
    }
  }
};
const runCodeQuestionAnswerMutation = {
  query: `
    mutation RunCodeQuestionAnswer($input: CodeQuestionAnswerInput!) {
      runCodeQuestionAnswer(input: $input) {
        id
        output {
          passed
          inputs
          expectedOutput
          actualOutput
        }
        showTestCase
      }
    }
  `,
  variables: {
    input: {
      activityID: 'CHARITY-CAMPAIGN',
      submitterID: '12-23-45',
      code: "const mock = 'mock'"
    }
  }
};
beforeAll(async () => {
  app = await (0, _server.startApolloServer)();
});
afterAll(() => {
  app.close();
});
describe('create code questions', () => {
  it('add question as unauthenticated user', async () => {
    const response = await (0, _supertest.default)(app).post('/').send(createMutation).set('Accept', 'application/json').set('Cookie', []);
    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });
  it('add questions as student', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockStudentJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(createMutation).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.errors[0].message).toBe('You must be logged in as ADMIN');
    expect(response.body.errors[0].extensions.code).toBe('FORBIDDEN');
  });
  it('add questions as admin', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(createMutation).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data).toBeObject();
  });
});
describe('update question', () => {
  it('update activity ID', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(updateMutation).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.updateCodeQuestion.activityId).toBe('PRIMES-100');
    expect(response.body.data.updateCodeQuestion.program).toBe('FULL_STACK');
    expect(response.body.data.updateCodeQuestion.module).toBe('M4');
    expect(response.body.data.updateCodeQuestion.question).toBe('Print the first 100 primes');
  });
});
describe('delete question', () => {
  it('delete question', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(deleteMutation).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.deleteCodeQuestion.activityId).toBe('PRINT-PRIMES');
    expect(response.body.data.deleteCodeQuestion.question).toBe('Print all primes');
    expect(response.body.data.deleteCodeQuestion.id).toBe('code-0003-1234-1234-codequestion');
  });
});
describe('run code question answer', () => {
  it('run code question answer', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockStudentJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(runCodeQuestionAnswerMutation).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.runCodeQuestionAnswer.id).toBe('12345-CHARITY-CAMPAIGN');
    expect(response.body.data.runCodeQuestionAnswer.showTestCase).toBeFalse();
    expect(response.body.data.runCodeQuestionAnswer.output).toBeArrayOfSize(3);
  });
});
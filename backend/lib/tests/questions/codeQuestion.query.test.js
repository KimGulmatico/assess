"use strict";

var _supertest = _interopRequireDefault(require("supertest"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _server = require("../../server");

var _setupTests = require("../utils/setupTests");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const jwtSpy = jest.spyOn(_jsonwebtoken.default, 'verify');
let app;
const mockGetCodeActivities = jest.fn().mockReturnValue([{
  id: 'BONUS-POINTS',
  description: 'BONUS POINTS'
}, {
  id: 'CHARITY-CAMPAIGN',
  description: 'CHARITY CAMPAIGN'
}, {
  id: 'CHARITY',
  description: 'CHARITY'
}]);
jest.mock('../../dataSources/codeRunnerAPI', () => jest.fn().mockImplementation(() => ({
  getCodeActivities: mockGetCodeActivities
})));
const codeQuestionsQuery = {
  query: `
    query CodeQuestions {
      codeQuestions {
        id
        question
        createdAt
        updatedAt
        activityId
      }
    }
  `
};
const codeQuestionQuery = {
  query: `
    query CodeQuestion($codeQuestionId: String!) {
      codeQuestion(id: $codeQuestionId) {
        id
        question
        createdAt
        updatedAt
        activityId
      }
    }
  `,
  variables: {
    codeQuestionId: 'code-0001-1234-1234-codequestion'
  }
};
const codeActivitiesQuery = {
  query: `
    query CodeActivities {
      codeActivities {
        id
        description
      }
    }
  `
};
beforeAll(async () => {
  app = await (0, _server.startApolloServer)();
});
afterAll(() => {
  app.close();
});
describe('retrieve all code questions', () => {
  it('view code questions as unauthenticated user', async () => {
    const response = await (0, _supertest.default)(app).post('/').send(codeQuestionsQuery).set('Accept', 'application/json').set('Cookie', []);
    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });
  it('view questions as authenticated user', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockStudentJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(codeQuestionsQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.codeQuestions).toBeArray();
  });
});
describe('retrieve single code questions', () => {
  it('view code questions as authenticated user', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockStudentJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(codeQuestionQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.codeQuestion.question).toBe('Print all odd numbers below 100');
    expect(response.body.data.codeQuestion.activityId).toBe('PRINT-ODD');
  });
});
describe('code activities test', () => {
  it('retrieve all code activities', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockStudentJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(codeActivitiesQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.codeActivities).toBeArrayOfSize(3);
  });
});
"use strict";

var _supertest = _interopRequireDefault(require("supertest"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _server = require("../../server");

var _setupTests = require("../utils/setupTests");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const jwtSpy = jest.spyOn(_jsonwebtoken.default, 'verify');
let app;
const multipleChoiceQuestionsQuery = {
  query: `
    query MultipleChoiceQuestions {
      multipleChoiceQuestions {
        id
        question
        createdAt
        updatedAt
        options {
          id
          description
        }
      }
    }
  `
};
const viewCorrectAnswerQuery = {
  query: `
    query MultipleChoiceQuestions {
      multipleChoiceQuestions {
        options {
          id
          isCorrect
        }
      }
    }
  `
};
const multipleChoiceQuestionQuery = {
  query: `
    query MultipleChoiceQuestion($multipleChoiceQuestionId: String!) {
      multipleChoiceQuestion(id: $multipleChoiceQuestionId) {
        id
        question
        createdAt
        updatedAt
        program
        module
        options {
          id
          description
          isCorrect
        }
      }
    }
  `,
  variables: {
    multipleChoiceQuestionId: 'mc12-0001-1234-1234-mcquestionff'
  }
};
beforeAll(async () => {
  app = await (0, _server.startApolloServer)();
});
afterAll(() => {
  app.close();
});
describe('retrieve multiple choice questions', () => {
  it('view MC questions as unauthenticated user', async () => {
    const response = await (0, _supertest.default)(app).post('/').send(multipleChoiceQuestionsQuery).set('Accept', 'application/json').set('Cookie', []);
    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });
  it('view questions as student', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockStudentJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(multipleChoiceQuestionsQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.multipleChoiceQuestions).toBeArray();
  });
  it('view correct answer as student', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockStudentJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(viewCorrectAnswerQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.multipleChoiceQuestions[0].options[0].isCorrect).toBe(null);
    expect(response.body.errors[0].message).toBe('You must be logged in as ADMIN');
    expect(response.body.errors[0].extensions.code).toBe('FORBIDDEN');
  });
  it('view correct answer as ADMIN', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(viewCorrectAnswerQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.multipleChoiceQuestions).toBeArray();
    expect(response.body.data.multipleChoiceQuestions[0]).toBeObject();
  });
});
describe('view single multiple choice question', () => {
  it('view multiple choice question as ADMIN', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(multipleChoiceQuestionQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.multipleChoiceQuestion).toBeObject();
    expect(response.body.data.multipleChoiceQuestion.module).toBe('M1');
    expect(response.body.data.multipleChoiceQuestion.program).toBe('BLOCKCHAIN');
    expect(response.body.data.multipleChoiceQuestion.question).toBe('Which is not a mammal?');
  });
});
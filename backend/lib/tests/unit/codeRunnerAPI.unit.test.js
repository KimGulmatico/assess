"use strict";

var _codeRunnerAPI = _interopRequireDefault(require("../../dataSources/codeRunnerAPI"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const mockGet = jest.fn();
const mockPost = jest.fn();
jest.mock('apollo-datasource-rest', () => {
  class MockRESTDataSource {
    baseUrl = '';
    get = mockGet;
    post = mockPost;
  }

  return {
    RESTDataSource: MockRESTDataSource
  };
});
describe('CodeRunnerAPI', () => {
  it('getCodeActivities gets data from correct URL', async () => {
    const datasource = new _codeRunnerAPI.default();
    await datasource.getCodeActivities();
    expect(mockGet).toBeCalledWith('activities');
  });
  it('submitExam posts data f=on correct URL', async () => {
    const datasource = new _codeRunnerAPI.default();
    const exam = {
      activityID: '1-2-3',
      submitterID: '12-23-45',
      code: "const mock = 'mock'"
    };
    await datasource.submitExam(exam);
    expect(mockPost).toBeCalledWith('exam/submit', exam);
  });
});
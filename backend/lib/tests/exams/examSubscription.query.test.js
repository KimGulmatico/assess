"use strict";

var _supertest = _interopRequireDefault(require("supertest"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _server = require("../../server");

var _setupTests = require("../utils/setupTests");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const jwtSpy = jest.spyOn(_jsonwebtoken.default, 'verify');
let app;
const subscriptionQuery = {
  query: `
    query Subscription($examId: String!) {
      subscription(examId: $examId) {
        examId
      }
    }
  `,
  variables: {
    examId: 'exam-0003-1234-1234-examffffffff'
  }
};
describe('subscription query', () => {
  beforeAll(async () => {
    app = await (0, _server.startApolloServer)();
  });
  afterAll(() => {
    app.close();
  });
  it('throws error when user is unauthenticated', async () => {
    const response = await (0, _supertest.default)(app).post('/').send(subscriptionQuery).set('Accept', 'application/json').set('Cookie', []);
    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });
  it('returns the examId if the subscription exists', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(subscriptionQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.subscription.examId).toBe('exam-0003-1234-1234-examffffffff');
  });
});
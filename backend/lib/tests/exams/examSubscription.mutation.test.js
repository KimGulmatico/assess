"use strict";

var _supertest = _interopRequireDefault(require("supertest"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _server = require("../../server");

var _setupTests = require("../utils/setupTests");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const jwtSpy = jest.spyOn(_jsonwebtoken.default, 'verify');
let app;
const subscribeToExamMutation = {
  query: `
    mutation SubscribeToExam($examId: String!) {
      subscribe(examId: $examId) {
        examId
      }
    }
  `,
  variables: {
    examId: 'exam-0001-1234-1234-examffffffff'
  }
};
const unsubscribeFromExamMutation = {
  query: `
    mutation UnsubscribeFromExam($examId: String!) {
      unsubscribe(examId: $examId) {
        examId
      }
    }
  `,
  variables: {
    examId: 'exam-0001-1234-1234-examffffffff'
  }
};
const sendEmailNotificationMutation = {
  query: `
    mutation SendEmailNotification($input: SendEmailNotificationInput!) {
      sendEmailNotification(input: $input) {
        examId
        userId
      }
    }
  `,
  variables: {
    input: {
      examId: 'exam-0001-1234-1234-examffffffff',
      event: 'started'
    }
  }
};
beforeAll(async () => {
  app = await (0, _server.startApolloServer)();
});
afterAll(() => {
  app.close();
});
describe('subscribing to exam', () => {
  it('throws error when subscribing to an exam while being unauthenticated', async () => {
    const response = await (0, _supertest.default)(app).post('/').send(subscribeToExamMutation).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });
  it('subscribes the admin user to an exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(subscribeToExamMutation).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.subscribe.examId).toBe('exam-0001-1234-1234-examffffffff');
  });
  it('throws error when subscribing to a preexisting exam subscription', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(subscribeToExamMutation).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.errors[0].message).toBe('Already subscribed to this exam');
  });
});
describe('sending email notification', () => {
  it('sends email notification to admin when user takes an exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(sendEmailNotificationMutation).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.sendEmailNotification[0].examId).toBe('exam-0001-1234-1234-examffffffff');
    expect(response.body.data.sendEmailNotification[0].userId).toBe('aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee');
  });
});
describe('unsubscribing from exam', () => {
  it('unsubscribes the admin user to an exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(unsubscribeFromExamMutation).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.unsubscribe.examId).toBe('exam-0001-1234-1234-examffffffff');
  });
  it('throws error when unsubscribing to a nonexistent exam subscription', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(unsubscribeFromExamMutation).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.errors[0].message).toBe('Exam to unsubscribe with does not exist');
  });
});
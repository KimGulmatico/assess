"use strict";

var _supertest = _interopRequireDefault(require("supertest"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _server = require("../../server");

var _setupTests = require("../utils/setupTests");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const jwtSpy = jest.spyOn(_jsonwebtoken.default, 'verify');
let app;
const addMultipleChoiceQuestionOnExamQuery = {
  query: `
    mutation AddQuestionOnExam($input: QuestionOnExamInput!) {
      addQuestionOnExam(input: $input) {
        examId
        questionId
      }
    }
  `,
  variables: {
    input: {
      examId: 'exam-0002-1234-1234-examffffffff',
      questionId: 'mc12-0002-1234-1234-mcquestionff'
    }
  }
};
const addMultipleSelectionQuestionOnExamQuery = {
  query: `
    mutation AddQuestionOnExam($input: QuestionOnExamInput!) {
      addQuestionOnExam(input: $input) {
        examId
        questionId
      }
    }
  `,
  variables: {
    input: {
      examId: 'exam-0002-1234-1234-examffffffff',
      questionId: 'ms12-0002-1234-1234-msquestionff'
    }
  }
};
const addCodeQuestionOnExamQuery = {
  query: `
    mutation AddQuestionOnExam($input: QuestionOnExamInput!) {
      addQuestionOnExam(input: $input) {
        examId
        questionId
      }
    }
  `,
  variables: {
    input: {
      examId: 'exam-0002-1234-1234-examffffffff',
      questionId: 'code-0002-1234-1234-codequestion'
    }
  }
};
const removeMultipleChoiceQuestionOnExamQuery = {
  query: `
    mutation RemoveQuestionOnExam($input: QuestionOnExamInput!) {
      removeQuestionOnExam(input: $input) {
        examId
        questionId
      }
    }
  `,
  variables: {
    input: {
      examId: 'exam-0002-1234-1234-examffffffff',
      questionId: 'mc12-0004-1234-1234-mcquestionff'
    }
  }
};
const removeMultipleSelectionQuestionOnExamQuery = {
  query: `
    mutation RemoveQuestionOnExam($input: QuestionOnExamInput!) {
      removeQuestionOnExam(input: $input) {
        examId
        questionId
      }
    }
  `,
  variables: {
    input: {
      examId: 'exam-0002-1234-1234-examffffffff',
      questionId: 'ms12-0004-1234-1234-msquestionff'
    }
  }
};
const removeCodeQuestionOnExamQuery = {
  query: `
    mutation RemoveQuestionOnExam($input: QuestionOnExamInput!) {
      removeQuestionOnExam(input: $input) {
        examId
        questionId
      }
    }
  `,
  variables: {
    input: {
      examId: 'exam-0002-1234-1234-examffffffff',
      questionId: 'code-0004-1234-1234-codequestion'
    }
  }
};
beforeAll(async () => {
  app = await (0, _server.startApolloServer)();
});
afterAll(() => {
  app.close();
});
describe('adding and removing of questions', () => {
  it('add multiple choice question', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(addMultipleChoiceQuestionOnExamQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.addQuestionOnExam.questionId).toBe('mc12-0002-1234-1234-mcquestionff');
  });
  it('add multiple selection question', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(addMultipleSelectionQuestionOnExamQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.addQuestionOnExam.questionId).toBe('ms12-0002-1234-1234-msquestionff');
  });
  it('add code question', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(addCodeQuestionOnExamQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.addQuestionOnExam.questionId).toBe('code-0002-1234-1234-codequestion');
  });
});
describe('removing questions', () => {
  it('remove multiple choice question', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(removeMultipleChoiceQuestionOnExamQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.removeQuestionOnExam.questionId).toBe('mc12-0004-1234-1234-mcquestionff');
  });
  it('remove multiple selection question', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(removeMultipleSelectionQuestionOnExamQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.removeQuestionOnExam.questionId).toBe('ms12-0004-1234-1234-msquestionff');
  });
  it('remove code question', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(removeCodeQuestionOnExamQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.removeQuestionOnExam.questionId).toBe('code-0004-1234-1234-codequestion');
  });
});
describe('handling errors', () => {
  it('the question has already been added', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    addMultipleChoiceQuestionOnExamQuery.variables = {
      input: {
        examId: 'exam-0001-1234-1234-examffffffff',
        questionId: 'mc12-0001-1234-1234-mcquestionff'
      }
    };
    const response = await (0, _supertest.default)(app).post('/').send(addMultipleChoiceQuestionOnExamQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.errors[0].message).toBe('This question has already been added');
  });
  it('invalid exam id', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    addMultipleSelectionQuestionOnExamQuery.variables = {
      input: {
        examId: 'exam-0005-1234-1234-examffffffff',
        questionId: 'ms12-0001-1234-1234-msquestionff'
      }
    };
    const response = await (0, _supertest.default)(app).post('/').send(addMultipleSelectionQuestionOnExamQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.errors[0].message).toBe('Record to add does not exist');
  });
  it('invalid question id', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    addCodeQuestionOnExamQuery.variables = {
      input: {
        examId: 'exam-0001-1234-1234-examffffffff',
        questionId: 'code-0005-1234-1234-codequestion'
      }
    };
    const response = await (0, _supertest.default)(app).post('/').send(addCodeQuestionOnExamQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.errors[0].message).toBe('Record to add does not exist');
  });
  it('invalid multiple choice question to remove', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    removeMultipleChoiceQuestionOnExamQuery.variables = {
      input: {
        examId: 'exam-0005-1234-1234-examffffffff',
        questionId: 'mc12-0005-1234-1234-mcquestionff'
      }
    };
    const response = await (0, _supertest.default)(app).post('/').send(removeMultipleChoiceQuestionOnExamQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.errors[0].message).toBe('Record to delete does not exist');
  });
  it('invalid multiple choice question to remove', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    removeMultipleSelectionQuestionOnExamQuery.variables = {
      input: {
        examId: 'exam-0005-1234-1234-examffffffff',
        questionId: 'ms12-0005-1234-1234-msquestionff'
      }
    };
    const response = await (0, _supertest.default)(app).post('/').send(removeMultipleSelectionQuestionOnExamQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.errors[0].message).toBe('Record to delete does not exist');
  });
  it('invalid multiple choice question to remove', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    removeCodeQuestionOnExamQuery.variables = {
      input: {
        examId: 'exam-0005-1234-1234-examffffffff',
        questionId: 'code-0005-1234-1234-codequestion'
      }
    };
    const response = await (0, _supertest.default)(app).post('/').send(removeCodeQuestionOnExamQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.errors[0].message).toBe('Record to delete does not exist');
  });
});
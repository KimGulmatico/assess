"use strict";

var _supertest = _interopRequireDefault(require("supertest"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _server = require("../../server");

var _setupTests = require("../utils/setupTests");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const jwtSpy = jest.spyOn(_jsonwebtoken.default, 'verify');
let app;
const studentExamQuery = {
  query: `
    query StudentExam($studentId: String!, $examId: String!) {
      studentExam(studentId: $studentId, examId: $examId) {
        student {
          id
          email
          firstName
          lastName
        }
        score
        examDetails {
          id
          title
          description
          program
          module
          year
          quarter
          maxQuestionCount
          createdAt
          updatedAt
          createdById
          startDate
          endDate
          duration
          examType
        }
        examSummary {
          question {
            __typename
            id
            question
            program
            module
            createdAt
            updatedAt
            createdById
            createdBy {
              id
              email
              firstName
              lastName
            }
            questionType
            ... on CodeQuestion {
              activityId
            }
          }
          points
          output {
            ... on CodeQuestionOutput {
              passed
              inputs
              expectedOutput
              actualOutput
            }
            ... on MultipleChoiceQuestionOutput {
              option {
                id
                description
                isCorrect
              }
              selected
            }
            ... on MultipleSelectionQuestionOutput {
              choice {
                id
                description
                isCorrect
              }
              selected
            }
          }
          studentAnswer {
            ... on CodeAnswer {
              questionId
              code
            }
            ... on MultipleChoiceAnswer {
              questionId
              option
            }
            ... on MultipleSelectionAnswer {
              questionId
              choices
            }
          }
        }
      }
    }
  `,
  variables: {
    studentId: 'ffffffff-gggg-hhhh-iiii-jjjjjjjjjjjj',
    examId: 'exam-0001-1234-1234-examffffffff'
  }
};
const allStudentExamsQuery = {
  query: `query AllStudentExams(
    $first: Int
    $last: Int
    $after: String
    $before: String
  ) {
    studentExamsConnection(
      first: $first
      last: $last
      after: $after
      before: $before
    ) {
      nodes {
        id
        score
        submittedAt
        examDetails {
          description
          title
          module
          maxQuestionCount
          endDate
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }`,
  variables: {
    first: 10,
    last: null,
    after: null,
    before: null
  }
};
beforeAll(async () => {
  app = await (0, _server.startApolloServer)();
});
afterAll(() => {
  app.close();
});
describe('retrieve student exam', () => {
  it('view all questions as unauthenticated user', async () => {
    const response = await (0, _supertest.default)(app).post('/').send(studentExamQuery).set('Accept', 'application/json').set('Cookie', []);
    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });
  it('view exam summary as student', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockStudentJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(studentExamQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.errors[0].message).toBe('You must be logged in as ADMIN');
    expect(response.body.errors[0].extensions.code).toBe('FORBIDDEN');
  });
  it('view questions as admin', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(studentExamQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.studentExam).toBeObject();
  });
});
describe('student exam history', () => {
  it('retrieve exams as unauthenticated user', async () => {
    const response = await (0, _supertest.default)(app).post('/').send(allStudentExamsQuery).set('Accept', 'application/json').set('Cookie', []);
    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });
  it('returns empty list of exams when retrieving as admin', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(allStudentExamsQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.studentExamsConnection.nodes).toStrictEqual([]);
  });
  it("retrieve all student's exams", async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockStudentJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(allStudentExamsQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.studentExamsConnection).toBeObject();
  });
});
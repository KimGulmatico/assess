"use strict";

var _supertest = _interopRequireDefault(require("supertest"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _server = require("../../server");

var _setupTests = require("../utils/setupTests");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const jwtSpy = jest.spyOn(_jsonwebtoken.default, 'verify');
let app;
const usersQuery = {
  query: `
    query Users {
      users {
        id
        email
        firstName
        lastName
      }
    }
  `
};
const userQuery = {
  query: `
    query User($userId: String!) {
      user(id: $userId) {
        id
        email
        firstName
        lastName
      }
    }
  `,
  variables: {
    userId: 'ffffffff-gggg-hhhh-iiii-jjjjjjjjjjjj'
  }
};
const currentUserQuery = {
  query: `
    query CurrentUser {
      currentUser {
        id
        email
        firstName
        lastName
        roles
      }
    }
  `
};
beforeAll(async () => {
  app = await (0, _server.startApolloServer)();
});
afterAll(() => {
  app.close();
});
describe('retrieve all users', () => {
  it('not logged in', async () => {
    const response = await (0, _supertest.default)(app).post('/').send(usersQuery).set('Accept', 'application/json').set('Cookie', []);
    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });
  it('non admin log in', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockStudentJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(usersQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.errors[0].message).toBe('You must be logged in as ADMIN');
    expect(response.body.errors[0].extensions.code).toBe('FORBIDDEN');
  });
  it('invalid cookie', async () => {
    jwtSpy.mockImplementation(() => {
      throw new Error('Invalid cookie');
    });
    const response = await (0, _supertest.default)(app).post('/').send(usersQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });
  it('view as admin', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(usersQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.users).toBeArray();
  });
});
describe('retrieve single free type exam', () => {
  it('view free exam as authenticated user', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockStudentJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(userQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.user.email).toBe('jdoe@gmail.com');
    expect(response.body.data.user.firstName).toBe('John');
    expect(response.body.data.user.lastName).toBe('Doe');
  });
});
describe('retrieve current user profile', () => {
  it('user with student role', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockStudentJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(currentUserQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.currentUser.email).toBe('jdoe@gmail.com');
    expect(response.body.data.currentUser.firstName).toBe('John');
    expect(response.body.data.currentUser.lastName).toBe('Doe');
    expect(response.body.data.currentUser.roles).toStrictEqual(['STUDENT']);
  });
  it('user with admin role', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(_setupTests.mockAdminJwtPayload));
    const response = await (0, _supertest.default)(app).post('/').send(currentUserQuery).set('Accept', 'application/json').set('Cookie', [_setupTests.mockCookie]);
    expect(response.body.data.currentUser.email).toBe('ku@kingslanduniversity.com');
    expect(response.body.data.currentUser.firstName).toBe('Kingsland');
    expect(response.body.data.currentUser.lastName).toBe('Assess');
    expect(response.body.data.currentUser.roles).toStrictEqual(['STUDENT', 'ADMIN']);
  });
});
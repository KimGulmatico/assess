"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mailgun = void 0;

var _formData = _interopRequireDefault(require("form-data"));

var _mailgun = _interopRequireDefault(require("mailgun.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const mg = new _mailgun.default(_formData.default);
const mailgun = mg.client({
  username: process.env.MAILGUN_USERNAME,
  key: process.env.MAILGUN_API_KEY
});
exports.mailgun = mailgun;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _apolloDatasourceRest = require("apollo-datasource-rest");

class CodeRunnerAPI extends _apolloDatasourceRest.RESTDataSource {
  constructor() {
    super();
    this.baseURL = 'https://classroom.kingsland.io/';
  }

  async getCodeActivities() {
    const codeActivities = await this.get('activities');
    return codeActivities;
  }

  async submitExam(exam) {
    const submittedExam = await this.post('exam/submit', exam);
    return submittedExam;
  }

}

var _default = CodeRunnerAPI;
exports.default = _default;
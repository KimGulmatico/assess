import { ApolloServer } from 'apollo-server-express';
import { ApolloServerPluginDrainHttpServer } from 'apollo-server-core';
import express from 'express';
import { createServer, Server } from 'http';
import cookieParser from 'cookie-parser';
import responseCachePlugin from 'apollo-server-plugin-response-cache';
import path from 'path';
import schema from './schema';
import context from './context';
import { checkoutCompleteCallback } from './schema/utils/checkoutCompleteCallback';

require('dotenv').config();

const stripe = require('stripe')(process.env.STRIPE_API_KEY);
const port = process.env.PORT || 4000;

const corsConfig = {
  origin: [
    'https://tzcert.merkle-trees.com',
    'https://studio.apollographql.com',
    'http://localhost:3000',
    'https://assess.kingslandtesting.com',
  ],
  credentials: true,
};

export const startApolloServer = async () => {
  const app = express();

  const httpServer: Server = createServer(app);

  const server = new ApolloServer({
    schema,
    plugins: [
      ApolloServerPluginDrainHttpServer({ httpServer }),
      responseCachePlugin(),
    ],
    context,
  });

  await server.start();

  app.use(cookieParser());

  app.use(express.static('public'));

  const endpointSecret = process.env.STRIPE_WEBHOOK_KEY;

  app.post(
    '/webhook',
    express.raw({ type: 'application/json' }),
    async (request, response) => {
      const signature = request.headers['stripe-signature'];

      let event;

      try {
        event = stripe.webhooks.constructEvent(
          request.body,
          signature,
          endpointSecret
        );
      } catch (err) {
        response.status(400).send(`Webhook Error: ${err}`);
        return;
      }

      switch (event.type) {
        case 'checkout.session.completed':
          const userDetails = event.data.object;
          const {
            customer_details: { email },
          } = userDetails;

          await checkoutCompleteCallback(email);

          break;
        default:
          console.log(`Unhandled event type ${event.type}`);
      }

      response.send();
    }
  );

  app.get('/certificates/:filename', async (req, res) => {
    try {
      const { filename } = req.params;
      res.sendFile(path.join(__dirname, `./public/certificates/${filename}`));
    } catch (err) {
      res.status(500);
    }
  });

  server.applyMiddleware({ app, path: '/', cors: corsConfig });

  if (process.env.NODE_ENV !== 'test') {
    await new Promise<void>((resolve) => {
      httpServer.listen({ port }, resolve);
    });

    console.warn(
      `🚀 Server ready at http://localhost:4000${server.graphqlPath}`
    );
  }
  return httpServer;
};

startApolloServer();

export default { startApolloServer };

// server.js
//
// Use this sample code to handle webhook events in your integration.
//
// 1) Paste this code into a new file (server.js)
//
// 2) Install dependencies
//   npm install stripe
//   npm install express
//
// 3) Run the server on http://localhost:4242
//   node server.js

// The library needs to be configured with your account's secret key.
// Ensure the key is kept out of any version control system you might be using.

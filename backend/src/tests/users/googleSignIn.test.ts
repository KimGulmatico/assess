import request from 'supertest';
import { Server } from 'http';
import { OAuth2Client } from 'google-auth-library';
import jwt from 'jsonwebtoken';
import { startApolloServer } from '../../server';

jest.mock('google-auth-library');

const googleClient = new OAuth2Client({
  clientId: '12233asdfgh',
  clientSecret: 'some secret',
});

const verifyIdTokenSpy = jest.spyOn(googleClient, 'verifyIdToken');
const jwtSpy = jest.spyOn(jwt, 'sign');

let app: Server;

const googleSignInMutation = {
  query: `
    mutation GoogleSignIn($input: GoogleSignInInput) {
      googleSignIn(input: $input) {
        id
        email
        firstName
        lastName
        roles
      }
    }
  `,
  variables: {
    input: {
      tokenId: '',
    },
  },
};

const mockToken = '123qeert';

const mockGooglePayload = {
  email: 'ku@kingslanduniversity.com',
  given_name: 'Kingsland',
  family_name: 'Assess',
  sub: '1234332',
};

describe('google sign in', () => {
  beforeAll(async () => {
    app = await startApolloServer();
  });

  afterAll(() => {
    app.close();
  });

  it('no token id provided', async () => {
    const response = await request(app)
      .post('/')
      .send(googleSignInMutation)
      .set('Accept', 'application/json');

    expect(response.body.errors[0].message).toBe('Token id not provided');
    expect(response.body.errors[0].extensions.code).toBe('BAD_USER_INPUT');
  });

  it('invalid token provided', async () => {
    googleSignInMutation.variables.input.tokenId = '1234asdfgq';

    verifyIdTokenSpy.mockImplementation(() => {
      throw new Error('Invalid');
    });

    const response = await request(app)
      .post('/')
      .send(googleSignInMutation)
      .set('Accept', 'application/json');

    expect(response.body.errors[0].message).toBe('Invalid google token id');
    expect(response.body.errors[0].extensions.code).toBe('BAD_USER_INPUT');
  });

  it('successful login', async () => {
    googleSignInMutation.variables.input.tokenId = '12234aasdf';

    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockToken));

    verifyIdTokenSpy.mockImplementation(
      jest.fn().mockReturnValue({
        getPayload: () => mockGooglePayload,
      })
    );

    const response = await request(app)
      .post('/')
      .send(googleSignInMutation)
      .set('Accept', 'application/json');

    expect(response.body.data.googleSignIn).toBeObject();
    expect(response.body.data.googleSignIn.email).toEqual(
      'ku@kingslanduniversity.com'
    );
    expect(response.body.data.googleSignIn.roles).toStrictEqual([
      'STUDENT',
      'ADMIN',
    ]);
  });
});

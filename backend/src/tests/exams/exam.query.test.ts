import request from 'supertest';
import jwt from 'jsonwebtoken';
import { Server } from 'http';
import { startApolloServer } from '../../server';
import { mockCookie, mockStudentJwtPayload } from '../utils/setupTests';

const jwtSpy = jest.spyOn(jwt, 'verify');

let app: Server;

const examsQuery = {
  query: `
    query Exams {
      exams {
        id
        title
        description
        program
      }
    }
  `,
};

const onNextExamsConnectionQuery = {
  query: `
    query ExamsConnection($first: Int) {
      examsConnection(first: $first) {
        nodes {
          id
          title
          description
          program
          year
          quarter
        }
        pageInfo {
          hasNextPage
          hasPreviousPage
          startCursor
          endCursor
        }
      }
    }
  `,
  variables: {
    first: 2,
  },
};

const onPreviousExamsConnectionQuery = {
  query: `
    query ExamsConnection($last: Int) {
      examsConnection(last: $last) {
        nodes {
          id
          title
          description
          program
          year
          quarter
        }
        pageInfo {
          hasNextPage
          hasPreviousPage
          startCursor
          endCursor
        }
      }
    }
  `,
  variables: {
    last: 2,
  },
};

const examQuery = {
  query: `
    query Exam($examId: String!) {
      exam(id: $examId) {
        id
        title
        description
        program
        year
        quarter
        isPublished
        createdAt
        updatedAt
        createdById
        startDate
        endDate
        duration
        examType
        createdBy {
          id
          email
          firstName
          lastName
        }
      }
    }
  `,
  variables: {
    examId: 'exam-0001-1234-1234-examffffffff',
  },
};

const questionsOnExamQuery = {
  query: `
    query Exam($examId: String!) {
      exam(id: $examId) {
        questions {
          __typename
          id
          question
          createdAt
          updatedAt
          createdById
          ... on MultipleChoiceQuestion {
            options {
              id
              description
              isCorrect
            }
          }
          ... on MultipleSelectionQuestion {
            choices {
              id
              description
              isCorrect
            }
          }
        }
      }
    }
  `,
  variables: {
    examId: 'exam-0001-1234-1234-examffffffff',
  },
};

describe('retrieve all exam types', () => {
  beforeAll(async () => {
    app = await startApolloServer();
  });

  afterAll(() => {
    app.close();
  });

  it('view exams as unauthenticated user', async () => {
    const response = await request(app)
      .post('/')
      .send(examsQuery)
      .set('Accept', 'application/json')
      .set('Cookie', []);

    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });

  it('view exams as authenticated user', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(examsQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.exams).toBeArray();
  });
});

describe('paginate exams', () => {
  it('view first two exams as authenticated user', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(onNextExamsConnectionQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.examsConnection.nodes).toBeArrayOfSize(2);
    expect(
      response.body.data.examsConnection.pageInfo.hasPreviousPage
    ).toBeFalse();
  });

  it('view last two exams as authenticated user', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(onPreviousExamsConnectionQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.examsConnection.nodes).toBeArrayOfSize(2);
    expect(response.body.data.examsConnection.pageInfo.hasNextPage).toBeFalse();
  });
});

describe('retrieve single exam', () => {
  it('view existing exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(examQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.exam.title).toBe('Prelim Examination');
    expect(response.body.data.exam.quarter).toBe('Q1');
    expect(response.body.data.exam.program).toBe('BLOCKCHAIN');
    expect(response.body.data.exam.isPublished).toBeFalse();
    expect(response.body.data.exam.createdBy.email).toBe(
      'ku@kingslanduniversity.com'
    );
  });

  it('view questions on  exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(questionsOnExamQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.exam.questions).toBeArrayOfSize(3);
  });
});

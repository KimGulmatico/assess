import request from 'supertest';
import jwt from 'jsonwebtoken';
import { Server } from 'http';
import { startApolloServer } from '../../server';
import {
  mockAdminJwtPayload,
  mockCookie,
  mockStudentJwtPayload,
} from '../utils/setupTests';

const jwtSpy = jest.spyOn(jwt, 'verify');

let app: Server;

const createMutation = {
  query: `
    mutation Mutation($input: CreateExamInput!) {
      createExam(input: $input) {
        id
        title
        description
        program
        year
        quarter
        isPublished
        createdAt
        updatedAt
        createdById
        startDate
        endDate
        duration
        examType
        maxQuestionCount
      }
    }
  `,
  variables: {
    input: {},
  },
};

const updateMutation = {
  query: `
    mutation UpdateExam($input: UpdateExamInput!) {
      updateExam(input: $input) {
        id
        title
        description
        program
        year
        quarter
        isPublished
        createdAt
        updatedAt
        createdById
        startDate
        endDate
        duration
        examType
        maxQuestionCount
      }
    }
  `,
  variables: {
    input: {},
  },
};

const deleteMutation = {
  query: `
    mutation DeleteExam($input: DeleteExamInput!) {
      deleteExam(input: $input) {
        id
      }
    }
  `,
  variables: {
    input: {
      id: 'exam-0003-1234-1234-examffffffff',
    },
  },
};

beforeAll(async () => {
  app = await startApolloServer();
});

afterAll(() => {
  app.close();
});

describe('create exam auth errors', () => {
  beforeEach(() => {
    createMutation.variables = {
      input: {
        title: 'Pre Test 1',
        description: 'Pre test 1',
        program: 'BLOCKCHAIN',
        module: 'M2',
        year: 2022,
        quarter: 'Q1',
        examType: 'FREE',
      },
    };
  });

  it('create exam as unauthenticated user', async () => {
    const response = await request(app)
      .post('/')
      .send(createMutation)
      .set('Accept', 'application/json')
      .set('Cookie', []);

    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });

  it('create exam as student', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(createMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'You must be logged in as ADMIN'
    );
    expect(response.body.errors[0].extensions.code).toBe('FORBIDDEN');
  });
});

describe('create exam user input errors', () => {
  it('invalid input for free exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    createMutation.variables = {
      input: {
        title: 'Free exam',
        description: 'Pre test 1',
        program: 'BLOCKCHAIN',
        module: 'M2',
        year: 2022,
        quarter: 'Q1',
        startDate: '2022-04-27T04:33:28.068Z',
        endDate: '2022-04-30T04:33:28.068Z',
        duration: 10,
        examType: 'FREE',
      },
    };
    const response = await request(app)
      .post('/')
      .send(createMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'Providing duration, start and end dates on flexible type exam is not supported'
    );
    expect(response.body.errors[0].extensions.code).toBe('BAD_USER_INPUT');
  });

  it('no input of duration for flexible exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    createMutation.variables = {
      input: {
        title: 'Flexible exam',
        description: 'Pre test 1',
        program: 'BLOCKCHAIN',
        module: 'M2',
        year: 2022,
        quarter: 'Q1',
        examType: 'FLEXIBLE',
      },
    };
    const response = await request(app)
      .post('/')
      .send(createMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'Must provide duration on flexible type exam'
    );
  });

  it('start and end dates on flexible exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    createMutation.variables = {
      input: {
        title: 'Flexible exam',
        description: 'Pre test 1',
        program: 'BLOCKCHAIN',
        module: 'M2',
        year: 2022,
        quarter: 'Q1',
        startDate: '2022-04-27T04:33:28.068Z',
        endDate: '2022-04-30T04:33:28.068Z',
        duration: 10,
        examType: 'FLEXIBLE',
      },
    };
    const response = await request(app)
      .post('/')
      .send(createMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'Providing start and end dates on flexible type exam is not supported'
    );
  });

  it('no input start and end dates on fixed exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    createMutation.variables = {
      input: {
        title: 'Fixed exam',
        description: 'Pre test 1',
        program: 'BLOCKCHAIN',
        module: 'M2',
        year: 2022,
        quarter: 'Q1',
        examType: 'FIXED',
      },
    };
    const response = await request(app)
      .post('/')
      .send(createMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'Must provide start and end date on fixed type exam'
    );
  });

  it('duration on fixed exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    createMutation.variables = {
      input: {
        title: 'Fixed exam',
        description: 'Pre test 1',
        program: 'BLOCKCHAIN',
        module: 'M2',
        year: 2022,
        quarter: 'Q1',
        startDate: '2022-04-27T04:33:28.068Z',
        endDate: '2022-04-30T04:33:28.068Z',
        duration: 10,
        examType: 'FIXED',
      },
    };
    const response = await request(app)
      .post('/')
      .send(createMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'Providing duration on fixed type exam is not supported'
    );
  });
});

describe('valid inputs on create exams', () => {
  it('create free exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    createMutation.variables = {
      input: {
        title: 'Pre Test 1',
        description: 'Pre test 1',
        program: 'BLOCKCHAIN',
        module: 'M2',
        year: 2022,
        quarter: 'Q1',
        examType: 'FREE',
        maxQuestionCount: 3,
      },
    };
    const response = await request(app)
      .post('/')
      .send(createMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data).toBeObject();
    expect(response.body.data.createExam.title).toBe('Pre Test 1');
    expect(response.body.data.createExam.quarter).toBe('Q1');
    expect(response.body.data.createExam.program).toBe('BLOCKCHAIN');
    expect(response.body.data.createExam.maxQuestionCount).toBe(3);
    expect(response.body.data.createExam.isPublished).toBeFalse();
  });
});

describe('update an exam', () => {
  it('update exam details exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    updateMutation.variables = {
      input: {
        id: 'exam-0002-1234-1234-examffffffff',
        title: 'New Midterm exam',
        description: 'This exam covers the Cybersecurity lessons 1, 2 and 3',
        program: 'BLOCKCHAIN',
        year: 2020,
        quarter: 'Q3',
        examType: 'FREE',
        maxQuestionCount: 1,
      },
    };

    const response = await request(app)
      .post('/')
      .send(updateMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.updateExam.title).toBe('New Midterm exam');
    expect(response.body.data.updateExam.quarter).toBe('Q3');
    expect(response.body.data.updateExam.year).toBe(2020);
    expect(response.body.data.updateExam.program).toBe('BLOCKCHAIN');
    expect(response.body.data.updateExam.maxQuestionCount).toBe(1);
    expect(response.body.data.updateExam.description).toBe(
      'This exam covers the Cybersecurity lessons 1, 2 and 3'
    );
  });

  it('update exam type exam only', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    updateMutation.variables = {
      input: {
        id: 'exam-0002-1234-1234-examffffffff',
        duration: 100,
        examType: 'FLEXIBLE',
      },
    };

    const response = await request(app)
      .post('/')
      .send(updateMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.updateExam.examType).toBe('FLEXIBLE');
    expect(response.body.data.updateExam.duration).toBe(100);
  });

  it('update exam type on error', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    updateMutation.variables = {
      input: {
        id: 'exam-0002-1234-1234-examffffffff',
        startDate: '2022-04-27T04:33:28.068Z',
        endDate: '2022-04-30T04:33:28.068Z',
        duration: 100,
        examType: 'FIXED',
      },
    };

    const response = await request(app)
      .post('/')
      .send(updateMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'Providing duration on fixed type exam is not supported'
    );
  });
});

describe('delete exam', () => {
  it('delete an exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(deleteMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.deleteExam.id).toBe(
      'exam-0003-1234-1234-examffffffff'
    );
  });
});

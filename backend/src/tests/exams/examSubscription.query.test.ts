import request from 'supertest';
import jwt from 'jsonwebtoken';
import { Server } from 'http';
import { startApolloServer } from '../../server';
import { mockAdminJwtPayload, mockCookie } from '../utils/setupTests';

const jwtSpy = jest.spyOn(jwt, 'verify');

let app: Server;

const subscriptionQuery = {
  query: `
    query Subscription($examId: String!) {
      subscription(examId: $examId) {
        examId
      }
    }
  `,
  variables: {
    examId: 'exam-0003-1234-1234-examffffffff',
  },
};

describe('subscription query', () => {
  beforeAll(async () => {
    app = await startApolloServer();
  });

  afterAll(() => {
    app.close();
  });

  it('throws error when user is unauthenticated', async () => {
    const response = await request(app)
      .post('/')
      .send(subscriptionQuery)
      .set('Accept', 'application/json')
      .set('Cookie', []);

    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });

  it('returns the examId if the subscription exists', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(subscriptionQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.subscription.examId).toBe(
      'exam-0003-1234-1234-examffffffff'
    );
  });
});

import request from 'supertest';
import jwt from 'jsonwebtoken';
import { Server } from 'http';
import { startApolloServer } from '../../server';
import { mockCookie, mockAdminJwtPayload } from '../utils/setupTests';

const jwtSpy = jest.spyOn(jwt, 'verify');

let app: Server;

const addMultipleChoiceQuestionOnExamQuery = {
  query: `
    mutation AddQuestionOnExam($input: QuestionOnExamInput!) {
      addQuestionOnExam(input: $input) {
        examId
        questionId
      }
    }
  `,
  variables: {
    input: {
      examId: 'exam-0002-1234-1234-examffffffff',
      questionId: 'mc12-0002-1234-1234-mcquestionff',
    },
  },
};

const addMultipleSelectionQuestionOnExamQuery = {
  query: `
    mutation AddQuestionOnExam($input: QuestionOnExamInput!) {
      addQuestionOnExam(input: $input) {
        examId
        questionId
      }
    }
  `,
  variables: {
    input: {
      examId: 'exam-0002-1234-1234-examffffffff',
      questionId: 'ms12-0002-1234-1234-msquestionff',
    },
  },
};

const removeMultipleChoiceQuestionOnExamQuery = {
  query: `
    mutation RemoveQuestionOnExam($input: QuestionOnExamInput!) {
      removeQuestionOnExam(input: $input) {
        examId
        questionId
      }
    }
  `,
  variables: {
    input: {
      examId: 'exam-0002-1234-1234-examffffffff',
      questionId: 'mc12-0004-1234-1234-mcquestionff',
    },
  },
};

const removeMultipleSelectionQuestionOnExamQuery = {
  query: `
    mutation RemoveQuestionOnExam($input: QuestionOnExamInput!) {
      removeQuestionOnExam(input: $input) {
        examId
        questionId
      }
    }
  `,
  variables: {
    input: {
      examId: 'exam-0002-1234-1234-examffffffff',
      questionId: 'ms12-0004-1234-1234-msquestionff',
    },
  },
};

beforeAll(async () => {
  app = await startApolloServer();
});

afterAll(() => {
  app.close();
});

describe('adding and removing of questions', () => {
  it('add multiple choice question', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(addMultipleChoiceQuestionOnExamQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.addQuestionOnExam.questionId).toBe(
      'mc12-0002-1234-1234-mcquestionff'
    );
  });

  it('add multiple selection question', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(addMultipleSelectionQuestionOnExamQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.addQuestionOnExam.questionId).toBe(
      'ms12-0002-1234-1234-msquestionff'
    );
  });
});

describe('removing questions', () => {
  it('remove multiple choice question', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(removeMultipleChoiceQuestionOnExamQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.removeQuestionOnExam.questionId).toBe(
      'mc12-0004-1234-1234-mcquestionff'
    );
  });

  it('remove multiple selection question', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(removeMultipleSelectionQuestionOnExamQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.removeQuestionOnExam.questionId).toBe(
      'ms12-0004-1234-1234-msquestionff'
    );
  });
});

describe('handling errors', () => {
  it('the question has already been added', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    addMultipleChoiceQuestionOnExamQuery.variables = {
      input: {
        examId: 'exam-0001-1234-1234-examffffffff',
        questionId: 'mc12-0001-1234-1234-mcquestionff',
      },
    };

    const response = await request(app)
      .post('/')
      .send(addMultipleChoiceQuestionOnExamQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'This question has already been added'
    );
  });

  it('invalid exam id', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    addMultipleSelectionQuestionOnExamQuery.variables = {
      input: {
        examId: 'exam-0005-1234-1234-examffffffff',
        questionId: 'ms12-0001-1234-1234-msquestionff',
      },
    };
    const response = await request(app)
      .post('/')
      .send(addMultipleSelectionQuestionOnExamQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'Record to add does not exist'
    );
  });

  it('invalid multiple choice question to remove', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    removeMultipleChoiceQuestionOnExamQuery.variables = {
      input: {
        examId: 'exam-0005-1234-1234-examffffffff',
        questionId: 'mc12-0005-1234-1234-mcquestionff',
      },
    };
    const response = await request(app)
      .post('/')
      .send(removeMultipleChoiceQuestionOnExamQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'Record to delete does not exist'
    );
  });

  it('invalid multiple choice question to remove', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    removeMultipleSelectionQuestionOnExamQuery.variables = {
      input: {
        examId: 'exam-0005-1234-1234-examffffffff',
        questionId: 'ms12-0005-1234-1234-msquestionff',
      },
    };
    const response = await request(app)
      .post('/')
      .send(removeMultipleSelectionQuestionOnExamQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'Record to delete does not exist'
    );
  });
});

import request from 'supertest';
import jwt from 'jsonwebtoken';
import { Server } from 'http';
import { startApolloServer } from '../../server';
import {
  mockCookie,
  mockAdminJwtPayload,
  mockStudentJwtPayload,
} from '../utils/setupTests';

const jwtSpy = jest.spyOn(jwt, 'verify');

let app: Server;

const subscribeToExamMutation = {
  query: `
    mutation SubscribeToExam($examId: String!) {
      subscribe(examId: $examId) {
        examId
      }
    }
  `,
  variables: {
    examId: 'exam-0001-1234-1234-examffffffff',
  },
};

const unsubscribeFromExamMutation = {
  query: `
    mutation UnsubscribeFromExam($examId: String!) {
      unsubscribe(examId: $examId) {
        examId
      }
    }
  `,
  variables: {
    examId: 'exam-0001-1234-1234-examffffffff',
  },
};

const sendEmailNotificationMutation = {
  query: `
    mutation SendEmailNotification($input: SendEmailNotificationInput!) {
      sendEmailNotification(input: $input) {
        examId
        userId
      }
    }
  `,
  variables: {
    input: {
      examId: 'exam-0001-1234-1234-examffffffff',
      event: 'started',
    },
  },
};

beforeAll(async () => {
  app = await startApolloServer();
});

afterAll(() => {
  app.close();
});

describe('subscribing to exam', () => {
  it('throws error when subscribing to an exam while being unauthenticated', async () => {
    const response = await request(app)
      .post('/')
      .send(subscribeToExamMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });

  it('subscribes the admin user to an exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(subscribeToExamMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.subscribe.examId).toBe(
      'exam-0001-1234-1234-examffffffff'
    );
  });

  it('throws error when subscribing to a preexisting exam subscription', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(subscribeToExamMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'Already subscribed to this exam'
    );
  });
});

describe('sending email notification', () => {
  it('sends email notification to admin when user takes an exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(sendEmailNotificationMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.sendEmailNotification[0].examId).toBe(
      'exam-0001-1234-1234-examffffffff'
    );
    expect(response.body.data.sendEmailNotification[0].userId).toBe(
      'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee'
    );
  });
});

describe('unsubscribing from exam', () => {
  it('unsubscribes the admin user to an exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(unsubscribeFromExamMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.unsubscribe.examId).toBe(
      'exam-0001-1234-1234-examffffffff'
    );
  });

  it('throws error when unsubscribing to a nonexistent exam subscription', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(unsubscribeFromExamMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'Exam to unsubscribe with does not exist'
    );
  });
});

import { paginateResults } from '../../schema/utils';

const results = [
  {
    id: '227ed040-1be0-40f8-abf5-7292bdbc8e99',
    title: 'Exam 1',
    description: 'first',
  },
  {
    id: 'bdbe5263-e177-4add-b2ff-53afcb4deb1c',
    title: 'Exam 2',
    description: 'second',
  },
  {
    id: '16404282-f853-421f-822e-495d23fa384b',
    title: 'Pre-Test 1',
    description: 'Pre test for Blockchain',
  },
  {
    id: '8a8fbc0d-906e-49a5-9dc0-4c97881f24ad',
    title: 'Final Exam 4',
    description: 'finals',
  },
  {
    id: '994fbf56-93ee-4b44-9345-6b36f7becf7e',
    title: 'Midterms 1',
    description: 'midterms',
  },
  {
    id: '6fe33b1b-4755-46a4-af1b-1ae54a471743',
    title: 'Final Exam 4',
    description: 'new',
  },
];

describe('test error handing', () => {
  it('both first and last are not provided', () => {
    expect(() => paginateResults(results)).toThrowError(
      'You must provide one of first or last'
    );
  });

  it('provided both first and last', () => {
    expect(() => paginateResults(results, 3, 3)).toThrowError(
      'Providing both first and last is not supported'
    );
  });

  it('used first and before together', () => {
    expect(() =>
      paginateResults(
        results,
        3,
        null,
        null,
        'bdbe5263-e177-4add-b2ff-53afcb4deb1c'
      )
    ).toThrowError('Using first with before is not supported');
  });

  it('used last and after together', () => {
    expect(() =>
      paginateResults(results, null, 3, 'bdbe5263-e177-4add-b2ff-53afcb4deb1c')
    ).toThrowError('Using last with after is not supported');
  });

  it('negative limit', () => {
    expect(() => paginateResults(results, -3)).toThrowError(
      'Limit must not be negative'
    );
    expect(() => paginateResults(results, null, -2)).toThrowError(
      'Limit must not be negative'
    );
  });

  it('invalid cursor', () => {
    expect(() => paginateResults(results, 1, null, 'in-va-lid')).toThrowError(
      'Invalid cursor'
    );
    expect(() =>
      paginateResults(results, null, 1, null, 'in-va-lid')
    ).toThrowError('Invalid cursor');
  });
});

describe('supplied empty results', () => {
  it('supplied empty array to paginate', () => {
    const emptyResults = paginateResults([], 5);
    expect(emptyResults.paginatedResults).toBeArrayOfSize(0);
    expect(emptyResults.pageInfo.endCursor).toBe(null);
    expect(emptyResults.pageInfo.startCursor).toBe(null);
    expect(emptyResults.pageInfo.hasNextPage).toBeFalse();
    expect(emptyResults.pageInfo.hasPreviousPage).toBeFalse();
  });
});

describe('valid results', () => {
  it('first items after specified items', () => {
    const validResults = paginateResults(
      results,
      3,
      null,
      'bdbe5263-e177-4add-b2ff-53afcb4deb1c'
    );
    expect(validResults.paginatedResults).toBeArrayOfSize(3);
    expect(validResults.pageInfo.endCursor).toBe(
      '994fbf56-93ee-4b44-9345-6b36f7becf7e'
    );
    expect(validResults.pageInfo.startCursor).toBe(
      '16404282-f853-421f-822e-495d23fa384b'
    );
    expect(validResults.pageInfo.hasNextPage).toBeTrue();
    expect(validResults.pageInfo.hasPreviousPage).toBeTrue();
  });

  it('last items before specified items', () => {
    const validResults = paginateResults(
      results,
      null,
      5,
      null,
      '8a8fbc0d-906e-49a5-9dc0-4c97881f24ad'
    );
    expect(validResults.paginatedResults).toBeArrayOfSize(3);
    expect(validResults.pageInfo.endCursor).toBe(
      '16404282-f853-421f-822e-495d23fa384b'
    );
    expect(validResults.pageInfo.startCursor).toBe(
      '227ed040-1be0-40f8-abf5-7292bdbc8e99'
    );
    expect(validResults.pageInfo.hasNextPage).toBeTrue();
    expect(validResults.pageInfo.hasPreviousPage).toBeFalse();
  });
});

import request from 'supertest';
import jwt from 'jsonwebtoken';
import { Server } from 'http';
import { startApolloServer } from '../../server';
import {
  mockAdminJwtPayload,
  mockCookie,
  mockStudentJwtPayload,
} from '../utils/setupTests';

const jwtSpy = jest.spyOn(jwt, 'verify');

let app: Server;

const createMutation = {
  query: `
    mutation CreateMultipleChoiceQuestion(
      $input: CreateMultipleChoiceQuestionInput!
    ) {
      createMultipleChoiceQuestion(input: $input) {
        id
        question
        createdAt
        updatedAt
        options {
          id
          description
          isCorrect
        }
      }
    }
  `,
  variables: {
    input: {
      question: 'Who lives on the ocean?',
      program: 'TECH_SALES',
      module: 'M4',
      options: [
        {
          description: 'cats',
          isCorrect: false,
        },
        {
          description: 'dogs',
          isCorrect: false,
        },
        {
          description: 'ants',
          isCorrect: false,
        },
        {
          description: 'sharks',
          isCorrect: true,
        },
      ],
    },
  },
};

const updateMutation = {
  query: `
    mutation UpdateMultipleChoiceQuestion(
      $input: UpdateMultipleChoiceQuestionInput!
    ) {
      updateMultipleChoiceQuestion(input: $input) {
        id
        question
        createdAt
        updatedAt
        options {
          id
          description
          isCorrect
        }
      }
    }
  `,
  variables: {
    input: {
      id: 'mc12-0002-1234-1234-mcquestionff',
      question: "what is man's bestfriend?",
    },
  },
};

const deleteMutation = {
  query: `
    mutation DeleteMultipleChoiceQuestion(
      $input: DeleteMultipleChoiceQuestionInput!
    ) {
      deleteMultipleChoiceQuestion(input: $input) {
        id
        question
        createdAt
        updatedAt
        options {
          id
          description
          isCorrect
        }
      }
    }
  `,
  variables: {
    input: {
      id: 'mc12-0003-1234-1234-mcquestionff',
    },
  },
};

const createOptionMutation = {
  query: `
    mutation CreateMultipleChoiceOption($input: CreateOptionInput!) {
      createMultipleChoiceOption(input: $input) {
        id
        description
        isCorrect
      }
    }
  `,
  variables: {
    input: {
      description: 'cow',
      isCorrect: false,
      multipleChoiceQuestionId: 'mc12-0002-1234-1234-mcquestionff',
    },
  },
};

const updateOptionMutation = {
  query: `
    mutation UpdateMultipleChoiceOption($input: UpdateOptionInput!) {
      updateMultipleChoiceOption(input: $input) {
        id
        description
        isCorrect
      }
    }
  `,
  variables: {
    input: {
      id: 'mc12-0002-1234-1234-mcoptionffff',
      description: 'dolphin',
      isCorrect: false,
    },
  },
};

const deleteOptionMutation = {
  query: `
    mutation DeleteMultipleChoiceOption($input: DeleteOptionInput!) {
      deleteMultipleChoiceOption(input: $input) {
        id
        description
        isCorrect
      }
    }
  `,
  variables: {
    input: {
      id: 'mc12-0003-1234-1234-mcoptionffff',
    },
  },
};

beforeAll(async () => {
  app = await startApolloServer();
});

afterAll(() => {
  app.close();
});

describe('create multiple choice question', () => {
  it('view MC questions as unauthenticated user', async () => {
    const response = await request(app)
      .post('/')
      .send(createMutation)
      .set('Accept', 'application/json')
      .set('Cookie', []);

    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });

  it('add questions as student', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(createMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'You must be logged in as ADMIN'
    );
    expect(response.body.errors[0].extensions.code).toBe('FORBIDDEN');
  });

  it('add questions as admin', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(createMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data).toBeObject();
  });
});

describe('update multiple choice question', () => {
  it('update questions as admin', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(updateMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.updateMultipleChoiceQuestion).toBeObject();
    expect(response.body.data.updateMultipleChoiceQuestion.question).toBe(
      "what is man's bestfriend?"
    );
  });
});

describe('delete question', () => {
  test('delete question as admin', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    const response = await request(app)
      .post('/')
      .send(deleteMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.deleteMultipleChoiceQuestion).toBeObject();
    expect(response.body.data.deleteMultipleChoiceQuestion.id).toBe(
      'mc12-0003-1234-1234-mcquestionff'
    );
  });
});

describe('MC question options', () => {
  test('create an option', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    const response = await request(app)
      .post('/')
      .send(createOptionMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.createMultipleChoiceOption).toBeObject();
    expect(response.body.data.createMultipleChoiceOption.description).toBe(
      'cow'
    );
  });

  test('update an option', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    const response = await request(app)
      .post('/')
      .send(updateOptionMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.updateMultipleChoiceOption).toBeObject();
    expect(response.body.data.updateMultipleChoiceOption.id).toBe(
      'mc12-0002-1234-1234-mcoptionffff'
    );
    expect(response.body.data.updateMultipleChoiceOption.description).toBe(
      'dolphin'
    );
  });

  test('delete an option', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    const response = await request(app)
      .post('/')
      .send(deleteOptionMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.deleteMultipleChoiceOption).toBeObject();
    expect(response.body.data.deleteMultipleChoiceOption.id).toBe(
      'mc12-0003-1234-1234-mcoptionffff'
    );
  });
});

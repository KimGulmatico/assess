import request from 'supertest';
import jwt from 'jsonwebtoken';
import { Server } from 'http';
import { startApolloServer } from '../../server';
import {
  mockAdminJwtPayload,
  mockCookie,
  mockStudentJwtPayload,
} from '../utils/setupTests';

const jwtSpy = jest.spyOn(jwt, 'verify');

let app: Server;

const multipleSelectionQuestionsQuery = {
  query: `
    query MultipleSelectionQuestions {
      multipleSelectionQuestions {
        id
        question
        createdAt
        updatedAt
        choices {
          id
          description
        }
      }
    }
  `,
};

const viewCorrectAnswerQuery = {
  query: `
    query MultipleSelectionQuestions {
      multipleSelectionQuestions {
        choices {
          id
          isCorrect
        }
      }
    }
  `,
};

const multipleSelectionQuestionQuery = {
  query: `
    query MultipleSelectionQuestion($multipleSelectionQuestionId: String!) {
      multipleSelectionQuestion(id: $multipleSelectionQuestionId) {
        id
        question
        createdAt
        updatedAt
        choices {
          id
          description
          isCorrect
        }
      }
    }
  `,
  variables: {
    multipleSelectionQuestionId: 'ms12-0001-1234-1234-msquestionff',
  },
};

beforeAll(async () => {
  app = await startApolloServer();
});

afterAll(() => {
  app.close();
});

describe('retrieve multiple selection questions', () => {
  it('view MS questions as unauthenticated user', async () => {
    const response = await request(app)
      .post('/')
      .send(multipleSelectionQuestionsQuery)
      .set('Accept', 'application/json')
      .set('Cookie', []);

    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });

  it('view questions as student', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(multipleSelectionQuestionsQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.multipleSelectionQuestions).toBeArray();
  });

  it('view correct answer as student', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(viewCorrectAnswerQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(
      response.body.data.multipleSelectionQuestions[0].choices[0].isCorrect
    ).toBe(null);
    expect(response.body.errors[0].message).toBe(
      'You must be logged in as ADMIN'
    );
    expect(response.body.errors[0].extensions.code).toBe('FORBIDDEN');
  });

  it('view correct answer as ADMIN', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(viewCorrectAnswerQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.multipleSelectionQuestions).toBeArray();
    expect(response.body.data.multipleSelectionQuestions[0]).toBeObject();
  });
});

describe('view single multiple selection question', () => {
  it('view multiple selection question as ADMIN', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(multipleSelectionQuestionQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.multipleSelectionQuestion).toBeObject();
    expect(response.body.data.multipleSelectionQuestion.question).toBe(
      'What are domestic animals?'
    );
  });
});

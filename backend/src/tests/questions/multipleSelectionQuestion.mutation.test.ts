import request from 'supertest';
import jwt from 'jsonwebtoken';
import { Server } from 'http';
import { startApolloServer } from '../../server';
import {
  mockAdminJwtPayload,
  mockCookie,
  mockStudentJwtPayload,
} from '../utils/setupTests';

const jwtSpy = jest.spyOn(jwt, 'verify');

let app: Server;

const query = {
  query: `
    mutation CreateMultipleSelectionQuestion(
      $input: CreateMultipleSelectionQuestionInput!
    ) {
      createMultipleSelectionQuestion(input: $input) {
        id
        question
        createdAt
        updatedAt
        program
        module
        choices {
          id
          description
          isCorrect
        }
      }
    }
  `,
  variables: {
    input: {
      question: 'Check all that applies',
      program: 'TECH_SALES',
      module: 'M3',
      choices: [
        {
          description: 'dog',
          isCorrect: true,
        },
        {
          description: 'cat',
          isCorrect: true,
        },
        {
          description: 'cow',
          isCorrect: false,
        },
      ],
    },
  },
};

const updateQuery = {
  query: `
    mutation UpdateMultipleSelectionQuestion(
      $input: UpdateMultipleSelectionQuestionInput!
    ) {
      updateMultipleSelectionQuestion(input: $input) {
        id
        question
        createdAt
        updatedAt
        choices {
          id
          description
          isCorrect
        }
      }
    }
  `,
  variables: {
    input: {
      id: 'ms12-0002-1234-1234-msquestionff',
      question: 'Select all true',
    },
  },
};

const deleteQuery = {
  query: `
    mutation DeleteMultipleSelectionQuestion(
      $input: DeleteMultipleSelectionQuestionInput!
    ) {
      deleteMultipleSelectionQuestion(input: $input) {
        id
        question
        createdAt
        updatedAt
        choices {
          id
          description
          isCorrect
        }
      }
    }
  `,
  variables: {
    input: {
      id: 'ms12-0003-1234-1234-msquestionff',
    },
  },
};

const createChoiceMutation = {
  query: `
    mutation CreateMultipleSelectionChoice($input: CreateChoiceInput!) {
      createMultipleSelectionChoice(input: $input) {
        description
        isCorrect
        id
      }
    }
  `,
  variables: {
    input: {
      description: 'carabao',
      isCorrect: false,
      multipleSelectionQuestionId: 'ms12-0002-1234-1234-msquestionff',
    },
  },
};

const updateChoiceMutation = {
  query: `
    mutation UpdateMultipleSelectionChoice($input: UpdateChoiceInput!) {
      updateMultipleSelectionChoice(input: $input) {
        id
        description
        isCorrect
      }
    }
  `,
  variables: {
    input: {
      id: 'ms12-0002-1234-1234-mschoiceffff',
      description: 'chicken',
      isCorrect: false,
    },
  },
};

const deleteChoiceMutation = {
  query: `
    mutation DeleteMultipleSelectionChoice($input: DeleteChoiceInput!) {
      deleteMultipleSelectionChoice(input: $input) {
        id
        description
        isCorrect
      }
    }
  `,
  variables: {
    input: {
      id: 'ms12-0003-1234-1234-mschoiceffff',
    },
  },
};

beforeAll(async () => {
  app = await startApolloServer();
});

afterAll(() => {
  app.close();
});

describe('create multiple selection questions', () => {
  it('add MS questions as unauthenticated user', async () => {
    const response = await request(app)
      .post('/')
      .send(query)
      .set('Accept', 'application/json')
      .set('Cookie', []);

    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });

  it('add questions as student', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(query)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'You must be logged in as ADMIN'
    );
    expect(response.body.errors[0].extensions.code).toBe('FORBIDDEN');
  });

  it('add questions as admin', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(query)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data).toBeObject();
    expect(response.body.data.createMultipleSelectionQuestion.program).toBe(
      'TECH_SALES'
    );
    expect(response.body.data.createMultipleSelectionQuestion.module).toBe(
      'M3'
    );
    expect(response.body.data.createMultipleSelectionQuestion.question).toBe(
      'Check all that applies'
    );
  });
});

describe('update multiple choice questions', () => {
  it('update questions as admin', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(updateQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.updateMultipleSelectionQuestion).toBeObject();
    expect(response.body.data.updateMultipleSelectionQuestion.question).toBe(
      'Select all true'
    );
  });
});

describe('delete question', () => {
  test('delete question', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    const response = await request(app)
      .post('/')
      .send(deleteQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.deleteMultipleSelectionQuestion).toBeObject();
  });
});

describe('Multiple selection question choices', () => {
  test('create an option', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    const response = await request(app)
      .post('/')
      .send(createChoiceMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.createMultipleSelectionChoice).toBeObject();
    expect(response.body.data.createMultipleSelectionChoice.description).toBe(
      'carabao'
    );
  });

  test('update an option', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    const response = await request(app)
      .post('/')
      .send(updateChoiceMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.updateMultipleSelectionChoice).toBeObject();
    expect(response.body.data.updateMultipleSelectionChoice.id).toBe(
      'ms12-0002-1234-1234-mschoiceffff'
    );
    expect(response.body.data.updateMultipleSelectionChoice.description).toBe(
      'chicken'
    );
  });

  test('delete an option', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));
    const response = await request(app)
      .post('/')
      .send(deleteChoiceMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.deleteMultipleSelectionChoice).toBeObject();
    expect(response.body.data.deleteMultipleSelectionChoice.id).toBe(
      'ms12-0003-1234-1234-mschoiceffff'
    );
  });
});

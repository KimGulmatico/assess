import request from 'supertest';
import jwt from 'jsonwebtoken';
import { Server } from 'http';
import { startApolloServer } from '../../server';
import { mockCookie, mockStudentJwtPayload } from '../utils/setupTests';

const jwtSpy = jest.spyOn(jwt, 'verify');

let app: Server;

const questionsQuery = {
  query: `
    query Query {
      questions {
        __typename
        id
        question
        createdAt
        createdBy {
          id
          email
          firstName
          lastName
        }
        updatedAt
        ... on MultipleChoiceQuestion {
          options {
            id
            description
          }
        }
        ... on MultipleSelectionQuestion {
          choices {
            id
            description
          }
        }
      }
    }
  `,
};

const filterQuestionsQuery = {
  query: `
    query Questions($program: Program, $module: String) {
      questions(program: $program, module: $module) {
        __typename
        id
        question
        program
        module
        createdAt
        updatedAt
      }
    }
  `,
  variables: {
    program: 'BLOCKCHAIN',
    module: 'M1',
  },
};

const searchQuestionsQuery = {
  query: `
    query SearchQuestions($keywords: String!) {
      searchQuestions(keywords: $keywords) {
        __typename
        id
        question
        createdAt
        updatedAt
        ... on MultipleChoiceQuestion {
          options {
            id
            description
            isCorrect
          }
        }
        ... on MultipleSelectionQuestion {
          choices {
            id
            description
            isCorrect
          }
        }
      }
    }
  `,
  variables: {
    keywords: 'domestic, pig',
  },
};

const onNextQuestionsConnectionQuery = {
  query: `
    query QuestionsConnection($first: Int) {
      questionsConnection(first: $first) {
        nodes {
          id
          question
          createdAt
          updatedAt
        }
        pageInfo {
          hasNextPage
          hasPreviousPage
          startCursor
          endCursor
        }
      }
    }
  `,
  variables: {
    first: 2,
  },
};

const onPreviousQuestionsConnectionQuery = {
  query: `
    query QuestionsConnection($last: Int) {
      questionsConnection(last: $last) {
        nodes {
          id
          question
          createdAt
          updatedAt
        }
        pageInfo {
          hasNextPage
          hasPreviousPage
          startCursor
          endCursor
        }
      }
    }
  `,
  variables: {
    last: 2,
  },
};

beforeAll(async () => {
  app = await startApolloServer();
});

afterAll(() => {
  app.close();
});

describe('retrieve all questions', () => {
  it('view all questions as unauthenticated user', async () => {
    const response = await request(app)
      .post('/')
      .send(questionsQuery)
      .set('Accept', 'application/json')
      .set('Cookie', []);

    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });

  it('view questions as authenticated user', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(questionsQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.questions).toBeArray();
    expect(response.body.data.questions[0].createdBy.email).toBe(
      'ku@kingslanduniversity.com'
    );
  });
});

describe('filter questions', () => {
  it('search question as authenticated user', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(filterQuestionsQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.questions).toBeArrayOfSize(4);
  });
});

describe('search questions', () => {
  it('search question as authenticated user', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(searchQuestionsQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.searchQuestions).toBeArrayOfSize(2);
  });
});

describe('paginate questions', () => {
  it('view first two questions as authenticated user', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(onNextQuestionsConnectionQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.questionsConnection.nodes).toBeArrayOfSize(2);
    expect(
      response.body.data.questionsConnection.pageInfo.hasPreviousPage
    ).toBeFalse();
  });
  it('view last two questions as authenticated user', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(onPreviousQuestionsConnectionQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.questionsConnection.nodes).toBeArrayOfSize(2);
    expect(
      response.body.data.questionsConnection.pageInfo.hasNextPage
    ).toBeFalse();
  });
});

import 'jest-extended';
import { UserJwtPayload } from '../../middleware/auth';

export const mockAdminJwtPayload: UserJwtPayload = {
  userId: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
  email: 'ku@kingslanduniversity.com',
  firstName: 'Kingsland',
  lastName: 'Assess',
  roles: ['STUDENT', 'ADMIN'],
  iat: 1647495757,
  exp: 1647754957,
};

export const mockStudentJwtPayload: UserJwtPayload = {
  userId: 'ffffffff-gggg-hhhh-iiii-jjjjjjjjjjjj',
  email: 'jdoe@gmail.com',
  firstName: 'John',
  lastName: 'Doe',
  roles: ['STUDENT'],
  iat: 1647495757,
  exp: 1647754957,
};

export const mockCookie = 'jwt=eyJhbGci; Path=/; Secure; HttpOnly;';

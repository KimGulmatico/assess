import db from '../../context/database';

const seedDatabase = async () => {
  await db.admin.create({
    data: {
      id: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
      email: 'ku@kingslanduniversity.com',
      createdById: 'vvvvvvvv-wwww-xxxx-yyyy-zzzzzzzzzzzz',
    },
  });

  const adminUser = await db.user.create({
    data: {
      id: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
      email: 'ku@kingslanduniversity.com',
      firstName: 'Kingsland',
      lastName: 'Assess',
    },
  });

  await db.user.create({
    data: {
      id: 'ffffffff-gggg-hhhh-iiii-jjjjjjjjjjjj',
      email: 'jdoe@gmail.com',
      firstName: 'John',
      lastName: 'Doe',
    },
  });

  await db.questionBase.create({
    data: {
      id: 'mc12-0001-1234-1234-mcquestionff',
      question: 'Which is not a mammal?',
      program: 'BLOCKCHAIN',
      module: 'M1',
      createdById: adminUser.id,
      questionType: 'MULTIPLE_CHOICE_QUESTION',
      multipleChoiceQuestion: {
        create: {
          options: {
            createMany: {
              data: [
                {
                  id: 'mc12-0001-0001-1234-mcoptionffff',
                  description: 'dog',
                  isCorrect: false,
                },
                {
                  id: 'mc12-0001-0002-1234-mcoptionffff',
                  description: 'bird',
                  isCorrect: true,
                },
                {
                  id: 'mc12-0001-0003-1234-mcoptionffff',
                  description: 'pig',
                  isCorrect: false,
                },
              ],
            },
          },
        },
      },
    },
  });

  await db.questionBase.create({
    data: {
      id: 'mc12-0002-1234-1234-mcquestionff',
      question: 'Which is a primary color',
      program: 'TECH_SALES',
      module: 'M2',
      createdById: adminUser.id,
      questionType: 'MULTIPLE_CHOICE_QUESTION',
      multipleChoiceQuestion: {
        create: {
          options: {
            createMany: {
              data: [
                {
                  description: 'purple',
                  isCorrect: false,
                },
                {
                  description: 'blue',
                  isCorrect: true,
                },
                {
                  description: 'green',
                  isCorrect: false,
                },
              ],
            },
          },
        },
      },
    },
  });

  await db.questionBase.create({
    data: {
      id: 'mc12-0003-1234-1234-mcquestionff',
      question: 'Which does not belong to the group?',
      program: 'CYBERSECURITY',
      module: 'M3',
      createdById: adminUser.id,
      questionType: 'MULTIPLE_CHOICE_QUESTION',
      multipleChoiceQuestion: {
        create: {
          options: {
            createMany: {
              data: [
                {
                  description: 'nickel',
                  isCorrect: false,
                },
                {
                  description: 'helium',
                  isCorrect: true,
                },
                {
                  description: 'gold',
                  isCorrect: false,
                },
              ],
            },
          },
        },
      },
    },
  });

  await db.questionBase.create({
    data: {
      id: 'mc12-0004-1234-1234-mcquestionff',
      question: 'Select',
      program: 'FULL_STACK',
      module: 'M4',
      createdById: adminUser.id,
      questionType: 'MULTIPLE_CHOICE_QUESTION',
      multipleChoiceQuestion: {
        create: {
          options: {
            createMany: {
              data: [
                {
                  id: 'mc12-0004-0001-1234-mcoptionffff',
                  description: 'nickel',
                  isCorrect: false,
                },
                {
                  id: 'mc12-0004-0002-1234-mcoptionffff',
                  description: 'helium',
                  isCorrect: true,
                },
                {
                  id: 'mc12-0004-0003-1234-mcoptionffff',
                  description: 'gold',
                  isCorrect: false,
                },
              ],
            },
          },
        },
      },
    },
  });

  await db.questionBase.create({
    data: {
      id: 'ms12-0001-1234-1234-msquestionff',
      question: 'What are domestic animals?',
      program: 'TECH_SALES',
      module: 'M1',
      createdById: adminUser.id,
      questionType: 'MULTIPLE_SELECTION_QUESTION',
      multipleSelectionQuestion: {
        create: {
          choices: {
            createMany: {
              data: [
                {
                  id: 'ms12-0001-0001-1234-msoptionffff',
                  description: 'dog',
                  isCorrect: true,
                },
                {
                  id: 'ms12-0001-0002-1234-msoptionffff',
                  description: 'cat',
                  isCorrect: true,
                },
                {
                  id: 'ms12-0001-0003-1234-msoptionffff',
                  description: 'sharks',
                  isCorrect: false,
                },
              ],
            },
          },
        },
      },
    },
  });

  await db.questionBase.create({
    data: {
      id: 'ms12-0002-1234-1234-msquestionff',
      question: 'Select all primes',
      program: 'FULL_STACK',
      module: 'M2',
      createdById: adminUser.id,
      questionType: 'MULTIPLE_SELECTION_QUESTION',
      multipleSelectionQuestion: {
        create: {
          choices: {
            createMany: {
              data: [
                {
                  description: '31',
                  isCorrect: true,
                },
                {
                  description: '103',
                  isCorrect: true,
                },
                {
                  description: '207',
                  isCorrect: false,
                },
              ],
            },
          },
        },
      },
    },
  });

  await db.questionBase.create({
    data: {
      id: 'ms12-0003-1234-1234-msquestionff',
      question: 'Select all composite',
      program: 'CYBERSECURITY',
      module: 'M3',
      createdById: adminUser.id,
      questionType: 'MULTIPLE_SELECTION_QUESTION',
      multipleSelectionQuestion: {
        create: {
          choices: {
            createMany: {
              data: [
                {
                  description: '200',
                  isCorrect: true,
                },
                {
                  description: '104',
                  isCorrect: true,
                },
                {
                  description: '207',
                  isCorrect: false,
                },
              ],
            },
          },
        },
      },
    },
  });

  await db.questionBase.create({
    data: {
      id: 'ms12-0004-1234-1234-msquestionff',
      question: 'Identify...',
      program: 'BLOCKCHAIN',
      module: 'M4',
      createdById: adminUser.id,
      questionType: 'MULTIPLE_SELECTION_QUESTION',
      multipleSelectionQuestion: {
        create: {
          choices: {
            createMany: {
              data: [
                {
                  id: 'ms12-0004-0001-1234-msoptionffff',
                  description: '200',
                  isCorrect: true,
                },
                {
                  id: 'ms12-0004-0002-1234-msoptionffff',
                  description: '104',
                  isCorrect: true,
                },
                {
                  id: 'ms12-0004-0003-1234-msoptionffff',
                  description: '207',
                  isCorrect: false,
                },
              ],
            },
          },
        },
      },
    },
  });

  await db.exam.create({
    data: {
      id: 'exam-0001-1234-1234-examffffffff',
      title: 'Prelim Examination',
      description: 'Provide answers to the following',
      program: 'BLOCKCHAIN',
      module: 'M1',
      year: 2022,
      quarter: 'Q1',
      createdById: adminUser.id,
      examType: 'FREE',
    },
  });

  await db.exam.create({
    data: {
      id: 'exam-0002-1234-1234-examffffffff',
      title: 'Midterm Examination',
      description: 'This exam covers the Cybersecurity lessons 1 and 2',
      program: 'CYBERSECURITY',
      module: 'M2',
      year: 2022,
      quarter: 'Q2',
      createdById: adminUser.id,
      examType: 'FLEXIBLE',
      duration: 10,
    },
  });

  await db.exam.create({
    data: {
      id: 'exam-0003-1234-1234-examffffffff',
      title: 'Final Examination',
      description:
        'The following are questions for you final examination in full stack',
      program: 'FULL_STACK',
      module: 'M3',
      year: 2022,
      quarter: 'Q3',
      createdById: adminUser.id,
      examType: 'FREE',
    },
  });

  await db.multipleChoiceOption.create({
    data: {
      id: 'mc12-0002-1234-1234-mcoptionffff',
      description: 'shark',
      isCorrect: false,
      multipleChoiceQuestionId: 'mc12-0002-1234-1234-mcquestionff',
    },
  });
  await db.multipleChoiceOption.create({
    data: {
      id: 'mc12-0003-1234-1234-mcoptionffff',
      description: 'bat',
      isCorrect: false,
      multipleChoiceQuestionId: 'mc12-0002-1234-1234-mcquestionff',
    },
  });

  await db.multipleSelectionOption.create({
    data: {
      id: 'ms12-0002-1234-1234-mschoiceffff',
      description: 'lizard',
      isCorrect: true,
      multipleSelectionQuestionId: 'ms12-0002-1234-1234-msquestionff',
    },
  });

  await db.multipleSelectionOption.create({
    data: {
      id: 'ms12-0003-1234-1234-mschoiceffff',
      description: 'mouse',
      isCorrect: false,
      multipleSelectionQuestionId: 'ms12-0002-1234-1234-msquestionff',
    },
  });

  await db.questionsOnExams.create({
    data: {
      questionId: 'mc12-0001-1234-1234-mcquestionff',
      examId: 'exam-0001-1234-1234-examffffffff',
    },
  });

  await db.questionsOnExams.create({
    data: {
      questionId: 'ms12-0001-1234-1234-msquestionff',
      examId: 'exam-0001-1234-1234-examffffffff',
    },
  });

  await db.questionsOnExams.create({
    data: {
      questionId: 'mc12-0004-1234-1234-mcquestionff',
      examId: 'exam-0002-1234-1234-examffffffff',
    },
  });

  await db.questionsOnExams.create({
    data: {
      questionId: 'ms12-0004-1234-1234-msquestionff',
      examId: 'exam-0002-1234-1234-examffffffff',
    },
  });

  await db.studentExam.create({
    data: {
      studentId: 'ffffffff-gggg-hhhh-iiii-jjjjjjjjjjjj',
      examId: 'exam-0001-1234-1234-examffffffff',
      examDetails: {
        id: 'exam-0001-1234-1234-examffffffff',
        title: 'Prelim Examination',
        description: 'Provide answers to the following',
        program: 'BLOCKCHAIN',
        module: 'M1',
        year: 2022,
        quarter: 'Q1',
        createdById: adminUser.id,
        examType: 'FREE',
        endDate: null,
        startDate: null,
        duration: null,
        maxQuestionCount: null,
        createdAt: '2022-05-17T13:33:53.039Z',
        updatedAt: '2022-05-17T13:33:53.039Z',
      },
      examQuestions: [
        {
          id: 'mc12-0001-1234-1234-mcquestionff',
          question: 'Which is not a mammal?',
          program: 'BLOCKCHAIN',
          module: 'M1',
          createdById: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
          questionType: 'MULTIPLE_CHOICE_QUESTION',
          options: [
            {
              id: 'mc12-0001-0001-1234-mcoptionffff',
              description: 'dog',
            },
            {
              id: 'mc12-0001-0002-1234-mcoptionffff',
              description: 'bird',
            },
            {
              id: 'mc12-0001-0003-1234-mcoptionffff',
              description: 'pig',
            },
          ],
        },
        {
          id: 'ms12-0001-1234-1234-msquestionff',
          question: 'What are domestic animals?',
          program: 'TECH_SALES',
          module: 'M1',
          createdById: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
          questionType: 'MULTIPLE_SELECTION_QUESTION',
          choices: [
            {
              id: 'ms12-0001-0001-1234-msoptionffff',
              description: 'dog',
            },
            {
              id: 'ms12-0001-0002-1234-msoptionffff',
              description: 'cat',
            },
            {
              id: 'ms12-0001-0003-1234-msoptionffff',
              description: 'sharks',
            },
          ],
        },
      ],
      studentAnswers: [
        {
          questionId: 'ms12-0001-1234-1234-msquestionff',
          choices: [
            'ms12-0001-0002-1234-msoptionffff',
            'ms12-0001-0003-1234-msoptionffff',
          ],
        },
        {
          questionId: 'mc12-0001-1234-1234-mcquestionff',
          option: 'mc12-0001-0001-1234-mcoptionffff',
        },
      ],
      score: 1.166666666666667,
      examSummary: [
        {
          output: [
            {
              inputs: ['270', '519', '1386', '1375', '1741'],
              passed: true,
              actualOutput: '9308369967.75',
              expectedOutput: '9308369967.75',
            },
            {
              inputs: ['222', '673', '1621', '501', '1250'],
              passed: true,
              actualOutput: '10438915046.70',
              expectedOutput: '10438915046.70',
            },
            {
              inputs: ['226', '378', '958', '1285', '907'],
              passed: true,
              actualOutput: '3996511917.30',
              expectedOutput: '3996511917.30',
            },
          ],
          points: 1,
        },
        {
          output: [
            {
              option: {
                id: 'mc12-0001-0001-1234-mcoptionffff',
                isCorrect: false,
                description: 'dog',
                multipleChoiceQuestionId: 'mc12-0001-1234-1234-mcquestionff',
              },
              selected: true,
            },
            {
              option: {
                id: 'mc12-0001-0002-1234-mcoptionffff',
                isCorrect: true,
                description: 'bird',
                multipleChoiceQuestionId: 'mc12-0001-1234-1234-mcquestionff',
              },
              selected: false,
            },
            {
              option: {
                id: 'mc12-0001-0003-1234-mcoptionffff',
                isCorrect: false,
                description: 'pig',
                multipleChoiceQuestionId: 'mc12-0001-1234-1234-mcquestionff',
              },
              selected: false,
            },
          ],
          points: 0,
          question: {
            id: 'mc12-0001-1234-1234-mcquestionff',
            module: 'M1',
            options: [
              { id: 'mc12-0001-0001-1234-mcoptionffff', description: 'dog' },
              { id: 'mc12-0001-0002-1234-mcoptionffff', description: 'bird' },
              { id: 'mc12-0001-0003-1234-mcoptionffff', description: 'pig' },
            ],
            program: 'BLOCKCHAIN',
            question: 'Which is not a mammal?',
            createdById: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
            questionType: 'MULTIPLE_CHOICE_QUESTION',
          },
          studentAnswer: {
            option: 'mc12-0001-0001-1234-mcoptionffff',
            questionId: 'mc12-0001-1234-1234-mcquestionff',
          },
        },
        {
          output: [
            {
              choice: {
                id: 'ms12-0001-0001-1234-msoptionffff',
                isCorrect: true,
                description: 'dog',
                multipleSelectionQuestionId: 'ms12-0001-1234-1234-msquestionff',
              },
              selected: false,
            },
            {
              choice: {
                id: 'ms12-0001-0002-1234-msoptionffff',
                isCorrect: true,
                description: 'cat',
                multipleSelectionQuestionId: 'ms12-0001-1234-1234-msquestionff',
              },
              selected: true,
            },
            {
              choice: {
                id: 'ms12-0001-0003-1234-msoptionffff',
                isCorrect: false,
                description: 'sharks',
                multipleSelectionQuestionId: 'ms12-0001-1234-1234-msquestionff',
              },
              selected: true,
            },
          ],
          points: 0.16666666666666669,
          question: {
            id: 'ms12-0001-1234-1234-msquestionff',
            module: 'M1',
            choices: [
              { id: 'ms12-0001-0001-1234-msoptionffff', description: 'dog' },
              { id: 'ms12-0001-0002-1234-msoptionffff', description: 'cat' },
              { id: 'ms12-0001-0003-1234-msoptionffff', description: 'sharks' },
            ],
            program: 'TECH_SALES',
            question: 'What are domestic animals?',
            createdById: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
            questionType: 'MULTIPLE_SELECTION_QUESTION',
          },
          studentAnswer: {
            choices: [
              'ms12-0001-0002-1234-msoptionffff',
              'ms12-0001-0003-1234-msoptionffff',
            ],
            questionId: 'ms12-0001-1234-1234-msquestionff',
          },
        },
      ],
    },
  });

  await db.examSubscription.create({
    data: {
      examId: 'exam-0003-1234-1234-examffffffff',
      userId: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
    },
  });
};

export default seedDatabase;

import db from '../../context/database';

const deleteData = async () => {
  await db.studentExam.deleteMany();
  await db.questionBase.deleteMany();
  await db.examSubscription.deleteMany();
  await db.exam.deleteMany();
  await db.user.deleteMany();
  await db.admin.deleteMany();
};

export default deleteData;

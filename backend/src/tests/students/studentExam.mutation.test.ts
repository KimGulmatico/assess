import request from 'supertest';
import jwt from 'jsonwebtoken';
import { Server } from 'http';
import { startApolloServer } from '../../server';
import { mockCookie, mockStudentJwtPayload } from '../utils/setupTests';

const jwtSpy = jest.spyOn(jwt, 'verify');

const mockSubmitExam = jest.fn().mockReturnValue({
  id: 'PRINT-PRIMES',
  output: [
    {
      actualOutput: '9308369967.75',
      expectedOutput: '9308369967.75',
      inputs: ['270', '519', '1386', '1375', '1741'],
      passed: true,
    },
    {
      actualOutput: '10438915046.70',
      expectedOutput: '10438915046.70',
      inputs: ['222', '673', '1621', '501', '1250'],
      passed: true,
    },
    {
      actualOutput: '3996511917.30',
      expectedOutput: '4006511917.30',
      inputs: ['226', '378', '958', '1285', '907'],
      passed: false,
    },
  ],
  showTestCase: false,
});

let app: Server;

const submitStudentExamMutation = {
  query: `
    mutation SubmitStudentExam($input: SubmitStudentExamInput!) {
      submitStudentExam(input: $input) {
        examDetails {
          title
          id
        }
        score
        student {
          id
          email
          firstName
          lastName
        }
      }
    }
  `,
  variables: {
    input: {
      examDetails: {
        id: 'exam-0002-1234-1234-examffffffff',
        title: 'Midterm Examination',
        description: 'This exam covers the Cybersecurity lessons 1 and 2',
        program: 'CYBERSECURITY',
        module: 'M2',
        year: 2022,
        quarter: 'Q2',
        createdById: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
        examType: 'FLEXIBLE',
        maxQuestionCount: null,
        createdAt: 1652794433039,
        updatedAt: 1652794433039,
        startDate: null,
        endDate: null,
        duration: 10,
      },
      examQuestions: [
        {
          id: 'mc12-0004-1234-1234-mcquestionff',
          question: 'Select',
          program: 'FULL_STACK',
          module: 'M4',
          createdById: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
          createdAt: 1652794361664,
          updatedAt: 1652794361664,
          questionType: 'MULTIPLE_CHOICE_QUESTION',
          options: [
            {
              id: 'mc12-0004-0001-1234-mcoptionffff',
              description: 'nickel',
            },
            {
              id: 'mc12-0004-0002-1234-mcoptionffff',
              description: 'helium',
            },
            {
              id: 'mc12-0004-0003-1234-mcoptionffff',
              description: 'gold',
            },
          ],
        },
        {
          id: 'ms12-0004-1234-1234-msquestionff',
          question: 'Identify...',
          program: 'BLOCKCHAIN',
          module: 'M4',
          createdById: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
          createdAt: 1652794361664,
          updatedAt: 1652794361664,
          questionType: 'MULTIPLE_SELECTION_QUESTION',
          choices: [
            {
              id: 'ms12-0004-0001-1234-msoptionffff',
              description: '200',
            },
            {
              id: 'ms12-0004-0002-1234-msoptionffff',
              description: '104',
            },
            {
              id: 'ms12-0004-0003-1234-msoptionffff',
              description: '207',
            },
          ],
        },
      ],
      studentAnswers: [
        {
          questionId: 'ms12-0004-1234-1234-msquestionff',
          choices: [
            'ms12-0004-0002-1234-msoptionffff',
            'ms12-0004-0003-1234-msoptionffff',
          ],
        },
        {
          questionId: 'mc12-0004-1234-1234-mcquestionff',
          option: 'mc12-0004-0002-1234-mcoptionffff',
        },
      ],
      submissionDetails: {
        time: 8,
      },
    },
  },
};

const invalidQuestionsOnStudentExamMutation = {
  query: `
    mutation SubmitStudentExam($input: SubmitStudentExamInput!) {
      submitStudentExam(input: $input) {
        examDetails {
          title
          id
        }
        score
        student {
          id
          email
          firstName
          lastName
        }
      }
    }
  `,
  variables: {
    input: {
      examDetails: {
        id: 'exam-0002-1234-1234-examffffffff',
        title: 'Midterm Examination',
        description: 'This exam covers the Cybersecurity lessons 1 and 2',
        program: 'CYBERSECURITY',
        module: 'M2',
        year: 2022,
        quarter: 'Q2',
        createdById: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
        examType: 'FLEXIBLE',
        maxQuestionCount: null,
        createdAt: 1652794433039,
        updatedAt: 1652794433039,
        startDate: null,
        endDate: null,
        duration: 10,
      },
      examQuestions: [
        {
          id: 'mc12-0004-1234-1234-mcquestionff',
          createdById: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
          createdAt: 1652794361664,
          updatedAt: 1652794361664,
          questionType: 'MULTIPLE_CHOICE_QUESTION',
        },
        {
          id: 'ms12-0004-1234-1234-msquestionff',
          question: 'Identify...',
          program: 'BLOCKCHAIN',
          module: 'M4',
          createdById: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
          createdAt: 1652794361664,
          updatedAt: 1652794361664,
          questionType: 'MULTIPLE_SELECTION_QUESTION',
          choices: [],
        },
      ],
      studentAnswers: [],
    },
  },
};

const invalidExamDetailsOnStudentExamMutation = {
  query: `
    mutation SubmitStudentExam($input: SubmitStudentExamInput!) {
      submitStudentExam(input: $input) {
        examDetails {
          title
          id
        }
        score
        student {
          id
          email
          firstName
          lastName
        }
      }
    }
  `,
  variables: {
    input: {
      examDetails: {
        id: 'exam-0002-1234-1234-examffffffff',
        title: 'Midterm Examination',
        description: 'This exam covers the Cybersecurity lessons 1 and 2',
        program: 'CYBERSECURITY',
        module: 'M2',
        year: 2022,
        quarter: 'Q2',
        createdById: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
        examType: 'FIXED',
        maxQuestionCount: null,
        createdAt: 1652794433039,
        updatedAt: 1652794433039,
        startDate: null,
        endDate: null,
        duration: null,
      },
      examQuestions: [],
      studentAnswers: [],
      submissionDetails: { time: null },
    },
  },
};

const invalidSubmissionDetailsOnStudentExamMutation = {
  query: `
    mutation SubmitStudentExam($input: SubmitStudentExamInput!) {
      submitStudentExam(input: $input) {
        examDetails {
          title
          id
        }
        score
        student {
          id
          email
          firstName
          lastName
        }
      }
    }
  `,
  variables: {
    input: {
      examDetails: {
        id: 'exam-0002-1234-1234-examffffffff',
        title: 'Midterm Examination',
        description: 'This exam covers the Cybersecurity lessons 1 and 2',
        program: 'CYBERSECURITY',
        module: 'M2',
        year: 2022,
        quarter: 'Q2',
        createdById: 'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
        examType: 'FLEXIBLE',
        maxQuestionCount: null,
        createdAt: 1652794433039,
        updatedAt: 1652794433039,
        startDate: null,
        endDate: null,
        duration: 10,
      },
      examQuestions: [],
      studentAnswers: [],
      submissionDetails: { time: null },
    },
  },
};

beforeAll(async () => {
  app = await startApolloServer();
});

afterAll(() => {
  app.close();
});

describe('submit exam errors', () => {
  it('submit student exam as unauthenticated user', async () => {
    const response = await request(app)
      .post('/')
      .send(submitStudentExamMutation)
      .set('Accept', 'application/json')
      .set('Cookie', []);

    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });
});

describe('valid inputs on submit exams', () => {
  it('successful submission of student exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(submitStudentExamMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.submitStudentExam).toBeObject();
    expect(response.body.data.submitStudentExam.examDetails).toBeObject();
    expect(response.body.data.submitStudentExam.examDetails.title).toBe(
      'Midterm Examination'
    );
    expect(response.body.data.submitStudentExam.student).toBeObject();
    expect(response.body.data.submitStudentExam.score).toBeNumber();
    expect(response.body.data.submitStudentExam.score).toBeCloseTo(1.833333, 5);
  });

  it('sending another exam submission on a previously taken exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(submitStudentExamMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe('Exam has already been taken');
  });
});

describe('invalid inputs on submit exams', () => {
  it('invalid questions on student exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(invalidQuestionsOnStudentExamMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe('Invalid question');
  });

  it('invalid exam details on student exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(invalidExamDetailsOnStudentExamMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'Must provide start and end date on fixed type exam'
    );
  });

  it('invalid submission details on student exam', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(invalidSubmissionDetailsOnStudentExamMutation)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'Provide time consumed for flexible exam'
    );
  });
});

import request from 'supertest';
import jwt from 'jsonwebtoken';
import { Server } from 'http';
import { startApolloServer } from '../../server';
import {
  mockAdminJwtPayload,
  mockCookie,
  mockStudentJwtPayload,
} from '../utils/setupTests';

const jwtSpy = jest.spyOn(jwt, 'verify');

let app: Server;

const studentExamQuery = {
  query: `
    query StudentExam($studentId: String!, $examId: String!) {
      studentExam(studentId: $studentId, examId: $examId) {
        student {
          id
          email
          firstName
          lastName
        }
        score
        examDetails {
          id
          title
          description
          program
          module
          year
          quarter
          maxQuestionCount
          createdAt
          updatedAt
          createdById
          startDate
          endDate
          duration
          examType
        }
        examSummary {
          question {
            __typename
            id
            question
            program
            module
            createdAt
            updatedAt
            createdById
            createdBy {
              id
              email
              firstName
              lastName
            }
          }
          points
          output {
            
            ... on MultipleChoiceQuestionOutput {
              option {
                id
                description
                isCorrect
              }
              selected
            }
            ... on MultipleSelectionQuestionOutput {
              choice {
                id
                description
                isCorrect
              }
              selected
            }
          }
          studentAnswer {
            ... on MultipleChoiceAnswer {
              questionId
              option
            }
            ... on MultipleSelectionAnswer {
              questionId
              choices
            }
          }
        }
      }
    }
  `,
  variables: {
    studentId: 'ffffffff-gggg-hhhh-iiii-jjjjjjjjjjjj',
    examId: 'exam-0001-1234-1234-examffffffff',
  },
};

const allStudentExamsQuery = {
  query: `query AllStudentExams(
    $first: Int
    $last: Int
    $after: String
    $before: String
  ) {
    studentExamsConnection(
      first: $first
      last: $last
      after: $after
      before: $before
    ) {
      nodes {
        id
        score
        submittedAt
        examDetails {
          description
          title
          module
          maxQuestionCount
          endDate
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }`,
  variables: {
    first: 10,
    last: null,
    after: null,
    before: null,
  },
};

beforeAll(async () => {
  app = await startApolloServer();
});

afterAll(() => {
  app.close();
});

describe('retrieve student exam', () => {
  it('view all questions as unauthenticated user', async () => {
    const response = await request(app)
      .post('/')
      .send(studentExamQuery)
      .set('Accept', 'application/json')
      .set('Cookie', []);

    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });

  it('view exam summary as student', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(studentExamQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.errors[0].message).toBe(
      'You must be logged in as ADMIN'
    );
    expect(response.body.errors[0].extensions.code).toBe('FORBIDDEN');
  });

  it('view questions as admin', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(studentExamQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.studentExam).toBeObject();
  });
});

describe('student exam history', () => {
  it('retrieve exams as unauthenticated user', async () => {
    const response = await request(app)
      .post('/')
      .send(allStudentExamsQuery)
      .set('Accept', 'application/json')
      .set('Cookie', []);

    expect(response.body.errors[0].message).toBe('You must be logged in');
    expect(response.body.errors[0].extensions.code).toBe('UNAUTHENTICATED');
  });

  it('returns empty list of exams when retrieving as admin', async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockAdminJwtPayload));

    const response = await request(app)
      .post('/')
      .send(allStudentExamsQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.studentExamsConnection.nodes).toStrictEqual([]);
  });

  it("retrieve all student's exams", async () => {
    jwtSpy.mockImplementation(jest.fn().mockReturnValue(mockStudentJwtPayload));

    const response = await request(app)
      .post('/')
      .send(allStudentExamsQuery)
      .set('Accept', 'application/json')
      .set('Cookie', [mockCookie]);

    expect(response.body.data.studentExamsConnection).toBeObject();
  });
});

import { makeExecutableSchema } from '@graphql-tools/schema';
import resolvers from './resolvers';
import typeDefs from './typeDefs';
import authDirectiveTransformer from './directives/auth';

let executableSchema = makeExecutableSchema({ resolvers, typeDefs });
// apply any directive transformer here before exporting schema
executableSchema = authDirectiveTransformer(executableSchema, 'auth');

const schema = executableSchema;

export default schema;

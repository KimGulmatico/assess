import { gql } from 'apollo-server';

const typeDefs = gql`
  type User {
    id: String
    email: String
    password: String
    firstName: String
    lastName: String
  }

  type UsersConnection {
    nodes: [User]
    pageInfo: PageInfo!
  }

  input GoogleSignInInput {
    tokenId: String!
  }

  input SignInInput {
    email: String!
    password: String!
  }

  input RegisterInput {
    id: String!
    email: String!
    password: String!
    firstName: String!
    lastName: String!
    roles: [Role]
  }

  input UpdateUserInput {
    id: String!
    firstName: String!
    lastName: String!
  }

  type UserProfile {
    id: String
    email: String
    password: String
    firstName: String
    lastName: String
    roles: [Role]
  }
  type Query {
    users: [User] @auth(requires: ADMIN)
    user(id: String!): User @auth
    currentUser: UserProfile @auth
    createCheckoutSession: String
    usersConnection(
      first: Int
      last: Int
      after: String
      before: String
    ): UsersConnection @auth(requires: ADMIN)
  }

  type Mutation {
    googleSignIn(input: GoogleSignInInput): UserProfile
    register(input: RegisterInput): UserProfile
    signIn(input: SignInInput): UserProfile
    signOut: Boolean!
    deleteUser(id: String!): User @auth(requires: ADMIN)
    updateUserName(input: UpdateUserInput!): User @auth(requires: ADMIN)
  }
`;

export default typeDefs;

import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { UserInputError, AuthenticationError } from 'apollo-server-express';
import { CookieOptions } from 'express';
import { OAuth2Client } from 'google-auth-library';
import crypto from 'crypto';
import { generateToken } from '../../middleware/auth';
import { UserProfile, Resolvers, Role } from '../../resolvers-types.generated';
import {
  daysToMilliseconds,
  determinePaginationArgs,
  getNodesAndPageInfo,
  prismaErrorHandler,
} from '../utils';
require('dotenv').config();

const stripe = require('stripe')(process.env.STRIPE_API_KEY);

const googleClient = new OAuth2Client({
  clientId: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
});

const defaultRole = 'STUDENT';

const cookieConfig: CookieOptions = {
  httpOnly: true,
  sameSite: 'none',
  secure: true,
  maxAge: daysToMilliseconds(3),
};

const resolvers: Resolvers = {
  Query: {
    users: async (_parent, _args, { db }) => {
      try {
        const users = await db.user.findMany();

        return users;
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    user: async (_parent, { id }, { db }) => {
      try {
        const user = await db.user.findUnique({ where: { id } });

        return user;
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    currentUser: async (_parent, _args, { payload }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { userId, email, firstName, lastName, roles } = payload;
      const currentUser: UserProfile = {
        id: userId,
        email,
        firstName,
        lastName,
        roles: roles as Role[],
      };

      return currentUser;
    },
    usersConnection: async (_parent, args, { db }) => {
      const { take, limit } = determinePaginationArgs(args);

      const { after, before } = args;
      const cursor =
        after || before ? { id: after || before || undefined } : undefined;

      try {
        const users = await db.user.findMany({
          take,
          cursor,
          orderBy: {
            createdAt: 'desc',
          },
        });

        const { nodes, pageInfo } = getNodesAndPageInfo(users, limit, args);

        return {
          nodes,
          pageInfo,
        };
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    createCheckoutSession: async (_parent, _args, { payload, db, rdb }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { email, firstName, lastName, userId } = payload;

      const exam = await db.exam.findMany({
        where: { isPublished: true },
      });

      if (!exam[0]) throw new Error('No exam is published');
      const examId = exam[0].id;

      const customer = await stripe.customers.create({
        email: email,
        name: `${firstName} ${lastName}`,
      });

      const session = await stripe.checkout.sessions.create({
        customer: customer.id,
        line_items: [
          {
            price: process.env.STRIPE_PRODUCT_PRICE_ID,
            quantity: 1,
          },
        ],
        mode: 'payment',
        // update success url and cancel url
        success_url: `${process.env.DOMAIN_NAME}/exam/cover/${examId}`,
        cancel_url: `${process.env.DOMAIN_NAME}/`,
      });

      return session.url;
    },
  },
  Mutation: {
    deleteUser: async (_parent, { id }, { payload, db }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');

      try {
        const user = await db.user.delete({ where: { id } });

        return user;
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    updateUserName: async (_parent, { input }, { payload, db }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { id, firstName, lastName } = input;

      try {
        const user = await db.user.update({
          where: { id },
          data: {
            firstName,
            lastName,
          },
        });

        return user;
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    googleSignIn: async (_parent, { input }, { res, db }) => {
      if (!input?.tokenId?.trim())
        throw new UserInputError('Token id not provided');

      let ticket;
      try {
        ticket = await googleClient.verifyIdToken({
          idToken: input.tokenId,
          audience: process.env.GOOGLE_CLIENT_ID,
        });
      } catch (error) {
        throw new UserInputError('Invalid google token id');
      }

      const payload = ticket.getPayload();

      if (payload) {
        const {
          email,
          given_name: firstName,
          family_name: lastName,
          sub: googleId,
        } = payload;

        if (!(email && firstName && lastName && googleId)) {
          throw new Error('Internal Server Error');
        }

        const user = await db.user.upsert({
          create: {
            password: '',
            email,
            firstName,
            lastName,
            googleAccounts: { create: { googleId } },
          },
          where: { email },
          update: { email },
        });

        const roles = [defaultRole];

        const adminUser = await db.admin.findFirst({ where: { email } });

        if (adminUser) {
          roles.push('ADMIN');
        }

        const token = generateToken({
          email,
          firstName,
          lastName,
          roles,
          userId: user.id,
        });

        const userProfile: UserProfile = {
          id: user.id,
          email,
          firstName,
          lastName,
          roles: roles as Role[],
        };

        res.cookie('jwt', token, cookieConfig);

        return userProfile;
      }

      throw new Error('Internal Server Error');
    },
    signOut: async (_parent, _args, { res }) => {
      res.clearCookie('jwt', cookieConfig);
      return true;
    },
    signIn: async (_parent, { input }, { res, req, db }) => {
      if (!input) throw new Error('Internal Server Error');

      const { email, password: pwd } = input;

      if (!(email && pwd)) {
        throw new Error('Internal Server Error');
      }

      const password = crypto.createHash('sha256').update(pwd).digest('hex');

      const user = await db.user.findUnique({ where: { email } });

      if (user?.password !== password) {
        throw new AuthenticationError('Wrong email and password');
      }

      const roles = [defaultRole];

      const { firstName, lastName } = user;

      const token = generateToken({
        ...{
          email,
          firstName,
          lastName,
          roles,
        },
        userId: user.id,
      });

      const userProfile: UserProfile = {
        id: user.id,
        password: user.password,
        email,
        firstName,
        lastName,
        roles: roles as Role[],
      };

      res.cookie('jwt', token, cookieConfig);

      return userProfile;
    },
    register: async (_parent, { input }, { res, req, db }) => {
      if (!input) throw new Error('Internal Server Error');

      const { id, email, firstName, lastName, password: pwd } = input;

      if (!(id && email && firstName && lastName)) {
        throw new Error('Internal Server Error');
      }

      const password = crypto.createHash('sha256').update(pwd).digest('hex');
      console.log(password);

      const user = await db.user.upsert({
        create: {
          password,
          email,
          firstName,
          lastName,
        },
        where: { email },
        update: { email },
      });

      const roles = [defaultRole];

      const token = generateToken({
        email,
        firstName,
        lastName,
        roles,
        userId: user.id,
      });

      const userProfile: UserProfile = {
        id: user.id,
        password: user.password,
        email,
        firstName,
        lastName,
        roles: roles as Role[],
      };

      res.cookie('jwt', token, cookieConfig);

      return userProfile;
    },
  },
};

export default resolvers;

export const daysToSeconds = (days: number) => days * 60 * 60 * 24;
export const daysToMilliseconds = (days: number) => daysToSeconds(days) * 1000;

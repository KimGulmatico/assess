import { UserInputError } from 'apollo-server-core';
import {
  CreateExamInput,
  ExamDetailsInput,
  UpdateExamInput,
} from '../../resolvers-types.generated';

const inputsOnExamTypesErrorHandler = (
  input: CreateExamInput | UpdateExamInput | ExamDetailsInput
) => {
  const { examType, startDate, endDate, duration } = input;

  switch (examType) {
    case 'FIXED':
      if (!(startDate && endDate))
        throw new UserInputError(
          'Must provide start and end date on fixed type exam'
        );
      if (duration)
        throw new UserInputError(
          'Providing duration on fixed type exam is not supported'
        );
      break;
    case 'FLEXIBLE':
      if (!duration)
        throw new UserInputError('Must provide duration on flexible type exam');
      if (startDate || endDate)
        throw new UserInputError(
          'Providing start and end dates on flexible type exam is not supported'
        );
      break;
    case 'FREE':
      if (startDate || endDate || duration)
        throw new UserInputError(
          'Providing duration, start and end dates on flexible type exam is not supported'
        );
      break;
    default:
      throw new UserInputError('Invalid exam type');
  }
};

export default inputsOnExamTypesErrorHandler;

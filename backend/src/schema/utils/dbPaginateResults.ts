import { UserInputError } from 'apollo-server-express';
import { PageInfo } from '../../resolvers-types.generated';

interface ConnectionArgs {
  /*
  Forward pagination
    first - The requested number of nodes per page
    after - The cursor to retrieve nodes after in the connection
  Backward pagination
    last - The requested number of nodes per page
    before - The cursor to retrieve nodes before in the connection

   */
  first?: number | null;
  last?: number | null;
  after?: string | null;
  before?: string | null;
}

const determinePaginationArgs = (args: any) => {
  const { first, last, after, before } = args;

  if (!(first || last))
    throw new UserInputError('You must provide one of first or last');

  if (first && last)
    throw new UserInputError('Providing both first and last is not supported');

  if ((first && before) || (last && after)) {
    const message = first
      ? { limit: 'first', cursor: 'before' }
      : { limit: 'last', cursor: 'after' };
    throw new UserInputError(
      `Using ${message.limit} with ${message.cursor} is not supported`
    );
  }

  if ((first && first < 0) || (last && last < 0))
    throw new UserInputError('Limit must not be negative');

  // limit returns the original length of nodes to be sent to the client
  const limit = first || last || 0;

  // take returns the number of nodes to be retrieve from the database
  let take = limit;

  // additional nodes are used to determine the next and previous page,
  // any extra nodes are removed before sending to client
  if (first || last) take += 1;
  if (after || before) take += 1;

  // indicates paginating backwards
  if (last) take *= -1;

  return { take, limit };
};

const getNodesAndPageInfo = (
  nodes: any[],
  limit: number,
  args: ConnectionArgs
) => {
  const { first, last, after, before } = args;

  // remove any cursor used
  if (nodes.length) {
    if (nodes[0].id === after) {
      nodes.shift();
    } else if (nodes[nodes.length - 1].id === before) {
      nodes.pop();
    }
  }

  // used to check if there will be next or previous page
  const hasExtraNode = nodes.length > limit;

  // remove extra node
  if (nodes.length > limit) {
    if (first) {
      nodes.pop();
    }
    if (last) {
      nodes.shift();
    }
  }

  const pageInfo: PageInfo = {
    startCursor: nodes.length ? nodes[0].id : null,
    endCursor: nodes.length ? nodes[nodes.length - 1].id : null,
    hasNextPage: first ? hasExtraNode : !!before,
    hasPreviousPage: first ? !!after : hasExtraNode,
  };

  return {
    nodes,
    pageInfo,
  };
};

export { determinePaginationArgs, getNodesAndPageInfo };

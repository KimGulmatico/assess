import Handlebars from 'handlebars';
import puppeteer from 'puppeteer';

const fs = require('fs');
const path = require('path');
const crypto = require('crypto');

const generateCertificate = async (data: any) => {
  let browser;
  try {
    fs.mkdirSync(path.join(__dirname, '../../public/certificates'), {
      recursive: true,
    });
    const templateHtml = fs.readFileSync(
      path.join(__dirname, '../../schema/utils/certificateTemplate.html'),
      'utf8'
    );

    const template = Handlebars.compile(templateHtml);
    const content = template(data);

    const filename = `${crypto.randomBytes(6).toString('hex')}_certificate`;

    if (!browser) {
      browser = await puppeteer.launch({
        args: [
          '--no-sandbox',
          '--disable-setuid-sandbox',
          '--disable-dev-shm-usage',
        ],
        headless: true,
      });
    }
    const context = await browser.createIncognitoBrowserContext();

    const page = await context.newPage();
    await page.goto(`data: text/html, ${content}`, {
      waitUntil: 'networkidle0',
    });

    await page.setContent(content);

    await page.emulateMediaType('screen');
    const pdfFile = await page.pdf({
      format: 'A4',
      printBackground: true,
      // path: path.join(__dirname, `../../public/certificates/${filename}.pdf`),
      preferCSSPageSize: true,
    });

    await context.close();

    return pdfFile.toJSON();
  } catch (err) {
    console.log(err);
    throw new Error('Internal server error');
  }
};

export default generateCertificate;

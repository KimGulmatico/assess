const getQuery = (keywords: string) => {
  const specialCharacters = /[()|&:*!]/g;

  return keywords
    .replace(specialCharacters, ' ')
    .trim()
    .split(/\s+/)
    .join(' | ');
};

export default getQuery;

import rdb from '../../context/redis';
import db from '../../context/database';
import { generateToken } from '../../middleware/auth';
import { mailgun } from '../../API/mailgun';

export const checkoutCompleteCallback = async (email: string) => {
  const user = await db.user.findFirst({ where: { email } });

  const userId = user?.id as string;

  const exam = await db.exam.findMany({
    where: { isPublished: true },
  });

  if (!exam[0]) throw new Error('No exam is published');
  const examId = exam[0].id;
  const token = generateToken({
    examId,
    email,
    userId,
  });

  const tokenData = JSON.stringify({
    examId,
    email,
    userId,
    token,
  });
  const seperator = '---';

  // tracks generated exam tokens and user by exams
  await (await rdb).APPEND(examId, tokenData + seperator);

  // tracks generated exam tokens per user
  await (await rdb).APPEND(userId, tokenData + seperator);

  const link = `${process.env.DOMAIN_NAME}/take/${examId}/${encodeURIComponent(
    token
  )}`;

  await db.examSubscription.upsert({
    where: { userId_examId: { userId, examId } },
    update: { status: 'PAID', examLink: link },
    create: { userId, examId, status: 'PAID', examLink: link },
  });

  const fname = user?.firstName as string;
  const lname = user?.lastName as string;
  const name = `${fname} ${lname}`;

  let variables = {
    name,
    exam_link: link,
  };

  let data = {
    from: 'Merkle Trees <assess@notifications.kingsland.io>',
    to: email,
    subject: 'Tezos Blockchain Certification',
    template: 'mk3-exam-link-template',
    'h:X-Mailgun-Variables': JSON.stringify(variables),
  };

  await mailgun.messages.create(process.env.MAILGUN_DOMAIN as string, data);
};

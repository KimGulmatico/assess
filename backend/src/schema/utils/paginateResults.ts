import { UserInputError } from 'apollo-server-express';
import { findIndex, slice } from 'lodash';
import { PageInfo } from '../../resolvers-types.generated';

const paginateResults = (
  results: any[],
  first?: number | null,
  last?: number | null,
  after?: string | null,
  before?: string | null
) => {
  let minIndex = 0;
  let maxIndex = results.length - 1;

  if (!(first || last))
    throw new UserInputError('You must provide one of first or last');

  if (first && last)
    throw new UserInputError('Providing both first and last is not supported');

  if ((first && before) || (last && after)) {
    const message = first
      ? { limit: 'first', cursor: 'before' }
      : { limit: 'last', cursor: 'after' };
    throw new UserInputError(
      `Using ${message.limit} with ${message.cursor} is not supported`
    );
  }

  if ((first && first < 0) || (last && last < 0))
    throw new UserInputError('Limit must not be negative');

  if (after) {
    const afterIndex = findIndex(results, { id: after });
    if (afterIndex < 0) throw new UserInputError('Invalid cursor');
    minIndex = afterIndex + 1;
  }

  if (first) {
    maxIndex = minIndex + first - 1;
  }

  if (before) {
    const beforeIndex = findIndex(results, { id: before });
    if (beforeIndex < 0) throw new UserInputError('Invalid cursor');
    maxIndex = beforeIndex - 1;
  }

  if (last) {
    minIndex = maxIndex - last + 1;
  }

  if (maxIndex >= results.length) {
    maxIndex = results.length - 1;
  }

  if (minIndex < 0) {
    minIndex = 0;
  }

  if (!results.length || minIndex >= results.length || maxIndex < 0)
    return {
      paginatedResults: [],
      pageInfo: {
        startCursor: null,
        endCursor: null,
        hasNextPage: false,
        hasPreviousPage: false,
      },
    };

  const startCursor = results[minIndex].id;
  const endCursor = results[maxIndex].id;
  const hasNextPage = maxIndex + 1 < results.length;
  const hasPreviousPage = minIndex - 1 > 0;

  const paginatedResults = slice(results, minIndex, maxIndex + 1);

  const pageInfo: PageInfo = {
    startCursor,
    endCursor,
    hasNextPage,
    hasPreviousPage,
  };

  return { paginatedResults, pageInfo };
};

export default paginateResults;

import { createClient } from 'redis';
import { verifyToken } from '../../middleware/auth';
import { UserJwtPayload } from '../../middleware/auth';

export const verifyOneTimeToken = async (
  token: string,
  rdb: ReturnType<typeof createClient>,
  userPayload: UserJwtPayload
) => {
  const tokenPayload = verifyToken(token);
  const verifiedUser = userPayload.userId === tokenPayload?.userId;
  if (!tokenPayload || !tokenPayload.examId || !verifiedUser) {
    throw new Error('Invalid token');
  }

  const exist = await rdb.HEXISTS('blacklist', token);

  return !exist;
};

export const blacklistToken = async (
  token: string,
  rdb: ReturnType<typeof createClient>
) => {
  const payload = verifyToken(token);
  if (!payload) {
    throw new Error('Invalid token');
  }

  const res = await rdb.HSET(
    'blacklist',
    token,
    Date.parse(new Date().toString())
  );

  return res === 1;
};

import { merge } from 'lodash';
import global from './global/resolvers';
import users from './users/resolvers';
import questions from './questions/resolvers';
import exams from './exams/resolvers';
import studentExams from './students/resolvers';
import admins from './admins/resolvers';

const resolvers = merge(global, users, questions, exams, studentExams, admins);

export default resolvers;

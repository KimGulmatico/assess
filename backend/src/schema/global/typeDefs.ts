import { gql } from 'apollo-server';

const typeDefs = gql`
  scalar Date

  directive @auth(requires: Role = STUDENT) on OBJECT | FIELD_DEFINITION

  enum Role {
    ADMIN
    STUDENT
  }

  type PageInfo {
    hasNextPage: Boolean
    hasPreviousPage: Boolean
    startCursor: String
    endCursor: String
  }

  enum CacheControlScope {
    PUBLIC
    PRIVATE
  }

  directive @cacheControl(
    maxAge: Int
    scope: CacheControlScope
    inheritMaxAge: Boolean
  ) on FIELD_DEFINITION | OBJECT | INTERFACE | UNION

  scalar JSON
`;

export default typeDefs;

import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { AuthenticationError, UserInputError } from 'apollo-server-express';
import { UserProfile, Resolvers, Role } from '../../resolvers-types.generated';
import {
  determinePaginationArgs,
  getNodesAndPageInfo,
  prismaErrorHandler,
} from '../utils';

const resolvers: Resolvers = {
  Query: {
    admins: async (_parent, _args, { db }) => {
      try {
        const admins = await db.admin.findMany();

        return admins;
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    admin: async (_parent, { id }, { db }) => {
      try {
        const admin = await db.admin.findUnique({ where: { id } });

        return admin;
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    adminsConnection: async (_parent, args, { db }) => {
      const { take, limit } = determinePaginationArgs(args);

      const { after, before } = args;
      const cursor =
        after || before ? { id: after || before || undefined } : undefined;

      try {
        const admins = await db.admin.findMany({
          take,
          cursor,
          orderBy: {
            createdAt: 'desc',
          },
        });

        const { nodes, pageInfo } = getNodesAndPageInfo(admins, limit, args);

        return {
          nodes,
          pageInfo,
        };
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
  },
  Mutation: {
    createAdmin: async (_parent, { input }, { payload, db }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { email } = input;
      const { userId } = payload;

      try {
        const admin = await db.admin.create({
          data: {
            email: email,
            createdById: userId,
          },
        });

        return admin;
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    deleteAdmin: async (_parent, { input }, { payload, db }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { id } = input;

      try {
        const admin = await db.admin.delete({ where: { id } });

        return admin;
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    updateAdmin: async (_parent, { input }, { payload, db }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { id, email } = input;
      try {
        const admin = await db.admin.update({
          where: { id },
          data: {
            email,
          },
        });

        return admin;
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
  },
};

export default resolvers;

import { gql } from 'apollo-server';

const typeDefs = gql`
  input QuestionOnExamInput {
    examId: String!
    questionId: String!
  }

  type QuestionOnExamPayload {
    examId: String
    questionId: String
  }

  type MultipleChoiceQuestionOnExamPayload {
    examId: String
    multipleChoiceQuestionId: String
  }

  type MultipleChoiceQuestionOnExamPayload {
    examId: String
    multipleChoiceQuestionId: String
  }

  type MultipleSelectionQuestionOnExamPayload {
    examId: String
    multipleSelectionQuestionId: String
  }

  type Mutation {
    addQuestionOnExam(input: QuestionOnExamInput!): QuestionOnExamPayload
      @auth(requires: ADMIN)
    removeQuestionOnExam(input: QuestionOnExamInput!): QuestionOnExamPayload
      @auth(requires: ADMIN)
  }
`;

export default typeDefs;

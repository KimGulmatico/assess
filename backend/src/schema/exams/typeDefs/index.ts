import Exam from './Exam';
import QuestionsOnExams from './QuestionsOnExams';
import ExamSubscription from './ExamSubscription';

const typeDefs = [Exam, QuestionsOnExams, ExamSubscription];

export default typeDefs;

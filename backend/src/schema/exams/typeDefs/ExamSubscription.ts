import { gql } from 'apollo-server';

const typeDefs = gql`
  input SendEmailNotificationInput {
    examId: String!
    event: String!
  }

  type ExamSubscription {
    userId: String!
    examId: String!
  }

  enum Status {
    PAID
    PASSED
    FAILED
  }

  type SubscriptionStatusPayload {
    status: Status
    examLink: String
  }

  type SubscriptionPayload {
    examId: String
  }

  type Query {
    subscription(examId: String!): SubscriptionPayload @auth(requires: ADMIN)
    subscriptionStatus: SubscriptionStatusPayload @auth
  }

  type Mutation {
    subscribe(examId: String!): SubscriptionPayload @auth(requires: ADMIN)
    unsubscribe(examId: String!): SubscriptionPayload @auth(requires: ADMIN)
    sendEmailNotification(
      input: SendEmailNotificationInput!
    ): [ExamSubscription]
  }
`;
export default typeDefs;

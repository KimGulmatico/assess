import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { UserInputError } from 'apollo-server-express';
import { Resolvers } from '../../../resolvers-types.generated';

const addQuestionOnExamErrorHandling = (
  error: PrismaClientKnownRequestError
) => {
  switch (error.code) {
    case 'P2002':
      throw new UserInputError('This question has already been added');
    case 'P2003':
      throw new UserInputError('Record to add does not exist');
    default:
      throw new Error('Internal server error');
  }
};

const removeQuestionOnExamErrorHandling = (
  error: PrismaClientKnownRequestError
) => {
  switch (error.code) {
    case 'P2025':
      throw new UserInputError('Record to delete does not exist');
    default:
      throw new Error('Internal server error');
  }
};

const resolvers: Resolvers = {
  Mutation: {
    addQuestionOnExam: async (_parent, { input }, { db }) => {
      const { examId, questionId } = input;

      try {
        const addQuestionOnExamPayload = await db.questionsOnExams.create({
          data: { questionId, examId },
        });

        return addQuestionOnExamPayload;
      } catch (error) {
        return addQuestionOnExamErrorHandling(
          error as PrismaClientKnownRequestError
        );
      }
    },
    removeQuestionOnExam: async (_parent, { input }, { db }) => {
      const { examId, questionId } = input;

      try {
        const removeQuestionOnExamPayload = await db.questionsOnExams.delete({
          where: {
            examId_questionId: {
              examId,
              questionId,
            },
          },
        });

        return removeQuestionOnExamPayload;
      } catch (error) {
        return removeQuestionOnExamErrorHandling(
          error as PrismaClientKnownRequestError
        );
      }
    },
  },
};

export default resolvers;

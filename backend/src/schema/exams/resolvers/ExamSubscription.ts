import { AuthenticationError } from 'apollo-server-core';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { UserInputError } from 'apollo-server-express';
import {
  Resolvers,
  SubscriptionStatusPayload,
} from '../../../resolvers-types.generated';
import { mailgun } from '../../../API/mailgun';

const subscribeMutationErrorHandling = (
  error: PrismaClientKnownRequestError
) => {
  switch (error.code) {
    case 'P2002':
      throw new UserInputError('Already subscribed to this exam');
    default:
      throw new Error('Internal server error');
  }
};

const unsubscribeMutationErrorHandling = (
  error: PrismaClientKnownRequestError
) => {
  switch (error.code) {
    case 'P2025':
      throw new UserInputError('Exam to unsubscribe with does not exist');
    default:
      throw new Error('Internal server error');
  }
};

const resolvers: Resolvers = {
  Query: {
    subscription: async (_parent, { examId }, { db, payload }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { userId } = payload;
      try {
        const subscription = await db.examSubscription.findUnique({
          where: {
            userId_examId: {
              userId,
              examId,
            },
          },
        });
        return subscription;
      } catch (error) {
        throw new Error('Internal server error');
      }
    },
    subscriptionStatus: async (_parent, _args, { db, payload }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { userId } = payload;

      const exam = await db.exam.findFirst({
        where: { isPublished: true },
      });

      if (!exam) throw new Error('No exam is published');
      const examId = exam.id;

      try {
        const subscription = await db.examSubscription.findUnique({
          where: {
            userId_examId: {
              userId,
              examId,
            },
          },
        });
        return subscription as SubscriptionStatusPayload;
      } catch (error) {
        throw new Error('Internal server error');
      }
    },
  },
  Mutation: {
    subscribe: async (_parent, { examId }, { db, payload }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { userId } = payload;

      try {
        const subscribeToExamPayload = await db.examSubscription.create({
          data: { userId, examId },
        });

        return subscribeToExamPayload;
      } catch (error) {
        return subscribeMutationErrorHandling(
          error as PrismaClientKnownRequestError
        );
      }
    },
    unsubscribe: async (_parent, { examId }, { db, payload }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { userId } = payload;

      try {
        const unsubscribeToExamPayload = await db.examSubscription.delete({
          where: {
            userId_examId: {
              userId,
              examId,
            },
          },
        });

        return unsubscribeToExamPayload;
      } catch (error) {
        return unsubscribeMutationErrorHandling(
          error as PrismaClientKnownRequestError
        );
      }
    },
    sendEmailNotification: async (_parent, { input }, { db, payload }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { firstName, lastName, userId } = payload;
      const { examId, event } = input;

      try {
        const subscriptions = await db.examSubscription.findMany({
          where: {
            examId,
          },
        });
        subscriptions.map(async (subscription) => {
          const admin = await db.user.findUnique({
            where: {
              id: subscription.userId,
            },
          });
          const exam = await db.exam.findUnique({
            where: {
              id: examId,
            },
          });

          let variables = {
            student_fullname: `${firstName} ${lastName}`,
            event: `${event}`,
            exam_title: `${exam?.title}`,
            viewexam_link: '',
          };

          let data = {
            from: 'Kingsland Assess <assess@notifications.kingsland.io>',
            to: `${admin?.email}`,
            subject: '',
            template: '',
            'h:X-Mailgun-Variables': JSON.stringify(variables),
          };

          if (event === 'started') {
            data = {
              ...data,
              template: 'kingsland-assess-start-exam',
              subject: `Start Exam Notification - ${firstName} ${lastName} - ${exam?.title}`,
            };
          } else if (event === 'finished') {
            variables = {
              ...variables,
              viewexam_link: `https://assess.kingsland.io/viewexam/${userId}/${examId}`,
            };
            data = {
              ...data,
              template: 'kingsland-assess-complete-exam',
              subject: `Exam Complete Notification - ${firstName} ${lastName} - ${exam?.title}`,
              'h:X-Mailgun-Variables': JSON.stringify(variables),
            };
          }

          await mailgun.messages.create(
            process.env.MAILGUN_DOMAIN as string,
            data
          );
        });

        return subscriptions;
      } catch (error) {
        throw new Error('Internal server error');
      }
    },
  },
};

export default resolvers;

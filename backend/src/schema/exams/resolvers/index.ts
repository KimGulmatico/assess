import { merge } from 'lodash';
import Exam from './Exam';
import QuestionsOnExams from './QuestionsOnExams';
import StudentExam from '../../students/resolvers/studentExam';
import Student from '../../users/resolvers';
import ExamSubscription from './ExamSubscription';

const resolvers = merge(
  Exam,
  QuestionsOnExams,
  StudentExam,
  Student,
  ExamSubscription
);

export default resolvers;

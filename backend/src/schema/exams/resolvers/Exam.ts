import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { AuthenticationError, UserInputError } from 'apollo-server-express';
import {
  DeleteExamPayload,
  Exam,
  Question,
  Resolvers,
} from '../../../resolvers-types.generated';
import {
  determinePaginationArgs,
  getNodesAndPageInfo,
  inputsOnExamTypesErrorHandler,
  prismaErrorHandler,
} from '../../utils';
import { generateToken } from '../../../middleware/auth';
import { blacklistToken, verifyOneTimeToken } from '../../utils/oneTimeToken';
import generateCertiticate from '../../utils/generateCertificate';
import { mailgun } from '../../../API/mailgun';
import { checkoutCompleteCallback } from '../../utils/checkoutCompleteCallback';

const resolvers: Resolvers = {
  Query: {
    exams: async (_parent, _args, { db }) => {
      try {
        const exams = await db.exam.findMany({
          orderBy: { createdAt: 'desc' },
        });

        return exams as Exam[];
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    examsConnection: async (_parent, args, { db }) => {
      const { take, limit } = determinePaginationArgs(args);

      const { after, before } = args;
      const cursor =
        after || before ? { id: after || before || undefined } : undefined;

      try {
        const exams = await db.exam.findMany({
          take,
          cursor,
          orderBy: {
            createdAt: 'desc',
          },
        });

        const examsWithHistoryQuery = exams.map(async (exam) => {
          const studentExams = await db.studentExam.findMany({
            where: {
              examId: exam.id,
            },
            include: {
              student: true,
            },
          });

          return { ...exam, studentExams };
        });

        const examsWithHistory = await Promise.all(examsWithHistoryQuery);

        const { nodes, pageInfo } = getNodesAndPageInfo(
          examsWithHistory,
          limit,
          args
        );
        return {
          nodes,
          pageInfo,
        };
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    exam: async (_parent, { id }, { db }) => {
      try {
        const exam = await db.exam.findFirst({
          where: { id },
        });

        return exam as Exam;
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
  },
  Exam: {
    questions: async (parent, _args, { db }) => {
      const { id: examId } = parent;
      if (!examId) throw new UserInputError('Exam Id must be provided');

      try {
        const questionsOnExams = await db.questionsOnExams.findMany({
          where: { examId },
          orderBy: { addedAt: 'asc' },
          select: { question: true },
        });

        return questionsOnExams.map(
          (questionBase) => questionBase.question as Question
        );
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    questionsConnection: async (parent, args, { db }) => {
      const { id: examId } = parent;
      if (!examId) throw new UserInputError('Exam Id must be provided');

      const { take, limit } = determinePaginationArgs(args);

      const { after, before } = args;
      const cursor =
        after || before
          ? {
              examId_questionId: {
                questionId: after || before || '',
                examId: after || before || '',
              },
            }
          : undefined;

      try {
        const questionsOnExams = await db.questionsOnExams.findMany({
          take,
          cursor,
          where: { examId },
          orderBy: { addedAt: 'asc' },
          select: { question: true },
        });

        const questions = questionsOnExams.map(
          (questionBase) => questionBase.question as Question
        );

        const { nodes, pageInfo } = getNodesAndPageInfo(questions, limit, args);
        return { nodes, pageInfo };
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    createdBy: async (parent, _args, { db }) => {
      const { id } = parent;
      if (!id) throw new UserInputError('CreatedBy Id must be provided');

      try {
        const user = await db.exam.findFirst({
          where: { id },
          select: {
            createdBy: {
              select: {
                id: true,
                email: true,
                firstName: true,
                lastName: true,
              },
            },
          },
        });

        if (!user) throw new Error('User not found');

        return user.createdBy;
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
  },
  Mutation: {
    createExam: async (_parent, { input }, { payload, db }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      inputsOnExamTypesErrorHandler(input);

      const { userId } = payload;
      const {
        title,
        description,
        program,
        module,
        year,
        quarter,
        maxQuestionCount,
        examType,
        startDate,
        endDate,
        duration,
        passingRate,
      } = input;
      try {
        const createExamPayload = await db.exam.create({
          data: {
            title,
            description,
            program,
            module,
            year,
            quarter,
            maxQuestionCount,
            endDate,
            startDate,
            duration,
            examType,
            createdById: userId,
            passingRate,
          },
        });

        return createExamPayload as Exam;
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    updateExam: async (_parent, { input }, { db }) => {
      inputsOnExamTypesErrorHandler(input);

      const {
        id,
        title,
        description,
        program,
        module,
        year,
        quarter,
        maxQuestionCount,
        examType,
        startDate,
        endDate,
        duration,
        isPublished,
      } = input;

      try {
        if (isPublished) {
          await db.exam.updateMany({
            where: { isPublished: true },
            data: {
              isPublished: false,
            },
          });
        }

        const updateExamPayload = await db.exam.update({
          where: { id },
          data: {
            title: title || undefined,
            description: description || undefined,
            program: program || undefined,
            module: module || undefined,
            year: year || undefined,
            quarter: quarter || undefined,
            maxQuestionCount,
            endDate: endDate || null,
            startDate: startDate || null,
            duration: duration || null,
            isPublished: isPublished ?? undefined,
            examType,
          },
        });

        return updateExamPayload as Exam;
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    deleteExam: async (_parent, { input }, { db }) => {
      const { id } = input;

      try {
        const deleteExamPayload = await db.exam.delete({ where: { id } });

        return deleteExamPayload as DeleteExamPayload;
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    generateExamLink: async (_parent, _args, { payload, db, rdb }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { userId, email } = payload;

      // returns the single published exam. --logic handled in updateExam mutation
      const exam = await db.exam.findMany({
        where: { isPublished: true },
      });

      if (!exam[0]) throw new Error('No exam is published');
      const examId = exam[0].id;
      const token = generateToken({
        examId,
        email,
        userId,
      });

      const tokenData = JSON.stringify({ examId, email, userId, token });
      const seperator = '---';

      // tracks generated exam tokens and user by exams
      await rdb.APPEND(examId, tokenData + seperator);

      // tracks generated exam tokens per user
      await rdb.APPEND(userId, tokenData + seperator);

      const link = `${
        process.env.DOMAIN_NAME
      }/take/${examId}/${encodeURIComponent(token)}`;

      const user = await db.user.findUnique({
        where: { id: userId },
      });

      const name = ((user?.firstName as string) + user?.lastName) as string;

      let variables = {
        name,
        exam_link: link,
      };

      let data = {
        from: 'Merkle Trees <assess@notifications.kingsland.io>',
        to: email,
        subject: 'Tezos Blockchain Certification',
        template: 'mk3-exam-link-template',
        'h:X-Mailgun-Variables': JSON.stringify(variables),
      };

      await mailgun.messages.create(process.env.MAILGUN_DOMAIN as string, data);

      return link;
    },
    sendEmailCertificate: async (_parent, { input }, { payload }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { email, firstName, lastName } = payload;
      const { certificateLink } = input;
      const name = firstName + ' ' + lastName;
      try {
        let variables = {
          name,
          certificate_link: certificateLink,
        };

        let data = {
          from: 'Merkle Trees <assess@notifications.kingsland.io>',
          to: email,
          subject: 'Tezos Blockchain Certificate',
          template: 'mk3-certificate-link',
          'h:X-Mailgun-Variables': JSON.stringify(variables),
        };

        const response = await mailgun.messages.create(
          process.env.MAILGUN_DOMAIN as string,
          data
        );

        const successful = response.status === 200 ? true : false;

        return { successful };
      } catch (error) {
        throw new Error('Internal server error');
      }
    },
    sendExamLink: async (_parent, { email }, { payload }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      try {
        await checkoutCompleteCallback(email);

        return true;
      } catch (error) {
        throw new Error('Internal server error');
      }
    },
    generateCertificate: async (
      _parent,
      { studentExamId },
      { payload, db }
    ) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { userId } = payload;

      const studentExamDetails = await db.studentExam.findUnique({
        where: {
          id: studentExamId,
        },
        include: {
          student: true,
        },
      });

      if (userId !== studentExamDetails?.studentId) {
        throw new Error('Authorization error');
      }
      const name =
        studentExamDetails?.student.firstName +
        ' ' +
        studentExamDetails?.student.lastName;
      const date = studentExamDetails?.submittedAt?.toLocaleDateString(
        'en-US',
        {
          year: 'numeric',
          month: '2-digit',
          day: '2-digit',
        }
      );

      const certUrl = await generateCertiticate({
        name,
        date,
      });

      return certUrl;
    },
  },
};

export default resolvers;

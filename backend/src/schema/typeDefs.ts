import { gql } from 'apollo-server-express';
import global from './global/typeDefs';
import users from './users/typeDefs';
import questions from './questions/typeDefs';
import exam from './exams/typeDefs';
import students from './students/typeDefs';
import admins from './admins/typeDefs'

const defaultTypeDefs = gql`
  type Query {
    _: Boolean
  }
  type Mutation {
    _: Boolean
  }
`;

const typeDefs = [defaultTypeDefs, global, users, questions, exam, students, admins];

export default typeDefs;

import { gql } from 'apollo-server';

const typeDefs = gql`
  type ExamDetails {
    id: String
    title: String
    description: String
    program: Program
    module: String
    year: Int
    quarter: Quarter
    maxQuestionCount: Int
    createdAt: Date
    updatedAt: Date
    createdById: String
    startDate: Date
    endDate: Date
    duration: Int
    examType: ExamType
    passingRate: Int
  }

  type MultipleChoiceAnswer {
    questionId: String!
    option: String
  }

  type MultipleSelectionAnswer {
    questionId: String!
    choices: [String!]
  }

  union Answer = MultipleChoiceAnswer | MultipleSelectionAnswer

  type MultipleChoiceQuestionOutput {
    option: Option
    selected: Boolean
  }

  type MultipleSelectionQuestionOutput {
    choice: Choice
    selected: Boolean
  }

  union Output = MultipleChoiceQuestionOutput | MultipleSelectionQuestionOutput

  type QuestionOutput {
    question: Question
    studentAnswer: Answer
    points: Float!
    output: [Output]
  }

  type SubmissionDetails {
    examStartTimestamp: Date
    examEndTimestamp: Date
  }

  type StudentHistoryByExam {
    studentExams: [StudentExam]
  }

  type StudentExam {
    id: String
    student: User
    examDetails: ExamDetails
    examQuestions: [Question]
    studentAnswers: [Answer]
    examSummary: [QuestionOutput] @auth(requires: ADMIN)
    score: Float
    submittedAt: Date
    openedAt: Date
    passed: Boolean
    attempts: Int
  }

  type StudentExamResult {
    id: String
    student: User
    examDetails: ExamDetails
    examSummary: [QuestionOutput] @auth(requires: ADMIN)
    score: Float
    submittedAt: Date
    openedAt: Date
    passed: Boolean
    attempts: Int
  }

  input ExamDetailsInput {
    id: String!
    title: String!
    description: String!
    program: Program!
    module: String!
    year: Int!
    quarter: Quarter!
    maxQuestionCount: Int
    createdAt: Date
    updatedAt: Date
    createdById: String!
    startDate: Date
    endDate: Date
    duration: Int
    examType: ExamType!
    passingRate: Int
  }

  input AnswerInput {
    questionId: String!
    option: String
    choices: [String!]
  }

  input OpenStudentExamInput {
    examDetails: ExamDetailsInput!
    examQuestions: [JSON!]!
    openedAt: Date!
    token: String!
  }

  type OpenStudentExamPayload {
    id: String!
    student: User
    examDetails: ExamDetails
    studentAnswers: [JSON]
    examQuestions: [JSON]
    openedAt: Date!
  }

  input SubmitStudentExamInput {
    id: String!
    examDetails: ExamDetailsInput!
    examQuestions: [JSON!]!
    studentAnswers: [AnswerInput!]!
    submittedAt: Date
    token: String!
  }

  input CheckExamInput {
    examId: String!
    token: String!
  }

  type SubmitStudentExamPayload {
    student: User
    examDetails: ExamDetails
    score: Float
    submittedAt: Date
    passed: Boolean
  }

  input UpdateStudentExamAnswersInput {
    id: String!
    studentAnswers: [AnswerInput!]!
  }

  type UpdateStudentExamAnswersPayload {
    id: String
    student: User
    examDetails: ExamDetails
    openedAt: Date
  }

  type StudentExamsConnection {
    nodes: [StudentExam]
    pageInfo: PageInfo!
  }
  type studentExamsConnectionByStudentIdPayload {
    nodes: [StudentHistoryByExam]
    pageInfo: PageInfo!
  }

  type Query {
    studentExamResults(
      studentId: String!
      examId: String!
    ): [StudentExamResult] @auth
    studentExamResult(studentExamId: String!): StudentExamResult @auth
    studentExam(studentId: String!, examId: String!): StudentExam @auth
    studentExams(studentId: String!): [StudentExam] @auth
    studentExamsConnectionByStudentId(
      studentId: String!
      first: Int
      last: Int
      after: String
      before: String
    ): studentExamsConnectionByStudentIdPayload @auth
    studentExamsConnection(
      first: Int
      last: Int
      after: String
      before: String
    ): StudentExamsConnection @auth
  }

  type Mutation {
    submitStudentExam(input: SubmitStudentExamInput!): SubmitStudentExamPayload
      @auth
    openStudentExam(input: OpenStudentExamInput!): OpenStudentExamPayload @auth
    isExamLinkValid(input: CheckExamInput!): Boolean @auth
    updateStudentExamAnswers(
      input: UpdateStudentExamAnswersInput!
    ): UpdateStudentExamAnswersPayload @auth
  }
`;

export default typeDefs;

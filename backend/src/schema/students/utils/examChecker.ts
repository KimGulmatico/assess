import { PrismaClient } from '@prisma/client';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { UserInputError } from 'apollo-server-core';
import { filter, find, includes } from 'lodash';
import {
  Answer,
  Choice,
  MultipleChoiceAnswer,
  MultipleChoiceQuestion,
  MultipleChoiceQuestionOutput,
  MultipleSelectionAnswer,
  MultipleSelectionQuestion,
  MultipleSelectionQuestionOutput,
  Option,
  Question,
  QuestionOutput,
} from '../../../resolvers-types.generated';
import { prismaErrorHandler } from '../../utils';

const multipleChoiceQuestionChecker = async (
  multipleChoiceQuestion: MultipleChoiceQuestion,
  studentAnswer: MultipleChoiceAnswer,
  db: PrismaClient
) => {
  const questionOutput: QuestionOutput = {
    points: 0,
    question: multipleChoiceQuestion,
    studentAnswer,
    output: [],
  };

  const { id: multipleChoiceQuestionId } = multipleChoiceQuestion;
  if (!multipleChoiceQuestionId)
    throw new UserInputError('Question id not provided');

  let allOptions: Option[];

  try {
    allOptions = await db.multipleChoiceOption.findMany({
      where: {
        multipleChoiceQuestionId,
      },
    });
  } catch (error) {
    if (error instanceof PrismaClientKnownRequestError)
      prismaErrorHandler(error);
    throw new Error('Internal Server Error');
  }

  allOptions.forEach((option) => {
    const output: MultipleChoiceQuestionOutput = { option, selected: false };
    if (studentAnswer?.option === option.id) {
      output.selected = true;
      if (option.isCorrect) questionOutput.points += 1;
    }
    questionOutput.output?.push(output);
  });

  return questionOutput;
};

const multipleSelectionQuestionChecker = async (
  multipleSelectionQuestion: MultipleSelectionQuestion,
  studentAnswer: MultipleSelectionAnswer,
  db: PrismaClient
) => {
  const questionSummary: QuestionOutput = {
    points: 0,
    question: multipleSelectionQuestion,
    studentAnswer,
    output: [],
  };

  const { id: multipleSelectionQuestionId, choices } =
    multipleSelectionQuestion;
  if (!multipleSelectionQuestionId)
    throw new UserInputError('Question Id not provided');
  if (!choices) return questionSummary;

  let allChoices: Choice[];
  try {
    allChoices = await db.multipleSelectionOption.findMany({
      where: { multipleSelectionQuestionId },
    });
  } catch (error) {
    if (error instanceof PrismaClientKnownRequestError)
      prismaErrorHandler(error);
    throw new Error('Internal Server Error');
  }

  const correctAnswers = filter(allChoices, { isCorrect: true });

  const deductionPoints = 1 / allChoices.length;
  const pointsPerCorrectAnswer = 1 / correctAnswers.length;

  allChoices.forEach((choice: Choice) => {
    const output: MultipleSelectionQuestionOutput = { choice, selected: false };
    if (includes(studentAnswer?.choices, choice.id)) {
      output.selected = true;
      if (choice.isCorrect) {
        questionSummary.points += pointsPerCorrectAnswer;
      } else {
        questionSummary.points -= deductionPoints;
      }
    }
    questionSummary.output?.push(output);
  });
  questionSummary.points =
    questionSummary.points < 0 ? 0 : questionSummary.points;

  return questionSummary;
};

const checkExam = async (
  examQuestions: Question[],
  studentAnswers: Answer[],
  studentId: string,
  db: PrismaClient
) => {
  let score = 0;
  const examSummary: QuestionOutput[] = [];

  await Promise.all(
    examQuestions.map(async (examQuestion: Question) => {
      const { questionType, id } = examQuestion;
      if (!questionType) throw new UserInputError('Question type not provided');

      const studentAnswer = find(studentAnswers, { questionId: id }) as Answer;

      switch (questionType) {
        case 'MULTIPLE_CHOICE_QUESTION': {
          const questionOutput = await multipleChoiceQuestionChecker(
            examQuestion,
            studentAnswer as MultipleChoiceAnswer,
            db
          );
          score += questionOutput.points;
          examSummary.push(questionOutput);
          break;
        }
        case 'MULTIPLE_SELECTION_QUESTION': {
          const questionOutput = await multipleSelectionQuestionChecker(
            examQuestion,
            studentAnswer as MultipleSelectionAnswer,
            db
          );

          score += questionOutput.points;
          examSummary.push(questionOutput);
          break;
        }
        default:
          throw new UserInputError('Invalid question type');
      }
    })
  );
  return { score, examSummary };
};

export {
  multipleChoiceQuestionChecker,
  multipleSelectionQuestionChecker,
  checkExam,
};

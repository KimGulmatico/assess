import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import {
  AuthenticationError,
  ForbiddenError,
  UserInputError,
} from 'apollo-server-core';
import {
  ExamType,
  OpenStudentExamPayload,
  Question,
  Resolvers,
  StudentExam,
  StudentExamResult,
  SubmitStudentExamPayload,
  UpdateStudentExamAnswersPayload,
} from '../../../resolvers-types.generated';
import {
  determinePaginationArgs,
  getNodesAndPageInfo,
  inputsOnExamTypesErrorHandler,
  prismaErrorHandler,
} from '../../utils';
import { checkExam } from '../utils/examChecker';
import { blacklistToken, verifyOneTimeToken } from '../../utils/oneTimeToken';

const examQuestionsErrorHandler = (examQuestions: any[]) => {
  examQuestions.forEach((examQuestion) => {
    if (typeof examQuestion !== 'object')
      throw new UserInputError('Please provide an array of questions');

    if (
      !(
        'id' in examQuestion &&
        'question' in examQuestion &&
        'program' in examQuestion &&
        'module' in examQuestion &&
        'createdById' in examQuestion &&
        'questionType' in examQuestion
      )
    )
      throw new UserInputError('Invalid question');

    switch ((examQuestion as Question).questionType) {
      case 'MULTIPLE_CHOICE_QUESTION':
        if (!('options' in examQuestion))
          throw new UserInputError('Invalid multiple choice question');
        break;
      case 'MULTIPLE_SELECTION_QUESTION':
        if (!('choices' in examQuestion))
          throw new UserInputError('Invalid multiple selection question');
        break;
      default:
        throw new UserInputError('Invalid questionType');
    }
  });
};

const submissionDetailsErrorHandler = (
  examType: ExamType,
  submittedAt: Date
) => {
  switch (examType) {
    case 'FLEXIBLE':
      if (!submittedAt)
        throw new UserInputError('SubmittedAt not provided correctly');
      break;
    default:
      break;
  }
};

const resolvers: Resolvers = {
  Output: {
    __resolveType: (output) => {
      if ('choice' in output) return 'MultipleSelectionQuestionOutput';
      if ('option' in output) return 'MultipleChoiceQuestionOutput';
      return null;
    },
  },
  Answer: {
    __resolveType: (answer) => {
      if ('option' in answer) return 'MultipleChoiceAnswer';
      if ('choices' in answer) return 'MultipleSelectionAnswer';
      return null;
    },
  },
  Query: {
    studentExamResults: async (
      _parent,
      { studentId, examId },
      { db, payload }
    ) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { userId } = payload;
      if (!userId)
        throw new ForbiddenError(
          'You must be logged in as an ADMIN or user of this account'
        );

      const studentExamResults = await db.studentExam.findMany({
        where: {
          studentId,
          examId,
        },
        include: {
          student: true,
        },
        orderBy: { attempts: 'asc' },
      });

      return studentExamResults as StudentExamResult[];
    },
    studentExamResult: async (_parent, { studentExamId }, { db, payload }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { userId } = payload;
      if (!userId)
        throw new ForbiddenError(
          'You must be logged in as an ADMIN or user of this account'
        );

      const studentExamResults = await db.studentExam.findFirst({
        where: {
          id: studentExamId,
        },
        include: {
          student: true,
        },
      });

      return studentExamResults as StudentExamResult;
    },
    studentExam: async (_parent, { examId, studentId }, { db, payload }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { userId, roles } = payload;
      if (!(userId === studentId || roles.includes('ADMIN')))
        throw new ForbiddenError(
          'You must be logged in as an ADMIN or user of this account'
        );

      const studentExam = await db.studentExam.findFirst({
        where: {
          examId,
          studentId,
        },
        include: {
          student: true,
        },
      });

      const response = { ...studentExam } as StudentExam;

      return response;
    },
    studentExams: async (_parent, { studentId }, { db, payload }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { userId } = payload;
      if (!userId)
        throw new ForbiddenError(
          'You must be logged in as an ADMIN or user of this account'
        );

      const studentExams = await db.studentExam.findMany({
        where: {
          studentId,
        },
        include: {
          student: true,
        },
        orderBy: { attempts: 'asc' },
      });

      return studentExams as StudentExam[];
    },
    studentExamsConnection: async (_parent, args, { db, payload }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');
      const { userId } = payload;

      const { take, limit } = determinePaginationArgs(args);

      const { after, before } = args;
      const cursor =
        after || before ? { id: after || before || undefined } : undefined;

      try {
        const exams = await db.studentExam.findMany({
          take,
          cursor,
          orderBy: {
            submittedAt: 'desc',
          },
          where: {
            studentId: userId,
          },
        });

        const examsWithHistoryQuery = exams.map(async (exam) => {
          const studentExams = await db.studentExam.findMany({
            where: {
              studentId: userId,
            },
            include: {
              student: true,
            },
          });

          return { ...exam, studentExams };
        });

        const examsWithHistory = await Promise.all(examsWithHistoryQuery);

        const { nodes, pageInfo } = getNodesAndPageInfo(
          examsWithHistory,
          limit,
          args
        );
        return {
          nodes,
          pageInfo,
        };
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    studentExamsConnectionByStudentId: async (
      _parent,
      args,
      { db, payload }
    ) => {
      if (!payload) throw new AuthenticationError('You must be logged in');

      const { take, limit } = determinePaginationArgs(args);

      const { after, before, studentId } = args;
      const cursor =
        after || before ? { id: after || before || undefined } : undefined;

      try {
        const exams = await db.studentExam.findMany({
          take,
          cursor,
          orderBy: {
            submittedAt: 'desc',
          },
          where: {
            studentId,
          },
        });

        const examIds = [...new Set(exams.map((exam) => exam.examId))];

        const studentExamHistoryByExam = examIds.map(async (examId) => {
          const studentExams = await db.studentExam.findMany({
            where: {
              studentId,
              examId,
            },
            orderBy: {
              submittedAt: 'desc',
            },
            include: {
              student: true,
            },
          });

          return { studentExams };
        });

        const studentExamsList = await Promise.all(studentExamHistoryByExam);

        const { nodes, pageInfo } = getNodesAndPageInfo(
          studentExamsList,
          limit,
          args
        );
        return {
          nodes,
          pageInfo,
        };
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
  },
  Mutation: {
    isExamLinkValid: async (_parent, { input }, { payload, db, rdb }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');

      const { examId, token } = input;

      try {
        const isExamTokenNotUsed = await verifyOneTimeToken(
          token,
          rdb,
          payload
        );

        const examExists = await db.exam.findFirst({
          where: {
            id: examId,
          },
        });

        if (!isExamTokenNotUsed || !examExists) {
          return false;
        }
      } catch (error) {
        return false;
      }

      return true;
    },
    openStudentExam: async (_parent, { input }, { payload, db }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');

      const { examQuestions, examDetails, openedAt, token } = input;

      examQuestionsErrorHandler(examQuestions);
      inputsOnExamTypesErrorHandler(examDetails);

      const { userId: studentId } = payload;
      const { id: examId } = examDetails;

      const examExists = await db.studentExam.findMany({
        where: {
          studentId,
          examId,
        },
        include: {
          student: true,
        },
      });
      if (examExists.length > 0) {
        const lastAttempt = examExists[examExists.length - 1];
        if (!lastAttempt.submittedAt) {
          return lastAttempt as OpenStudentExamPayload;
        }
      }
      try {
        const studentExamPayload = await db.studentExam.create({
          data: {
            studentId,
            examDetails,
            examQuestions,
            examId,
            openedAt,
          },
          include: {
            student: true,
          },
        });
        return studentExamPayload as OpenStudentExamPayload;
      } catch (error) {
        if (
          error instanceof PrismaClientKnownRequestError &&
          error.code === 'P2002'
        )
          throw new Error('Exam has already been taken');
        throw new Error('Internal Server Error');
      }
    },
    updateStudentExamAnswers: async (_parent, { input }, { db, payload }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');

      const { id, studentAnswers } = input;

      try {
        const studentExamPayload = await db.studentExam.update({
          where: { id },
          data: {
            studentAnswers,
          },
          include: {
            student: true,
          },
        });

        return studentExamPayload as UpdateStudentExamAnswersPayload;
      } catch (error) {
        if (
          error instanceof PrismaClientKnownRequestError &&
          error.code === 'P2002'
        )
          throw new Error('Exam has already been taken');
        throw new Error('Internal Server Error');
      }
    },
    submitStudentExam: async (_parent, { input }, { db, payload, rdb }) => {
      if (!payload) throw new AuthenticationError('You must be logged in');

      const {
        id,
        examQuestions,
        examDetails,
        studentAnswers,
        submittedAt,
        token,
      } = input;

      examQuestionsErrorHandler(examQuestions);
      inputsOnExamTypesErrorHandler(examDetails);
      submissionDetailsErrorHandler(examDetails.examType, submittedAt);

      const { userId: studentId } = payload;

      let examResult;

      try {
        examResult = await checkExam(
          examQuestions,
          studentAnswers,
          studentId,
          db
        );
      } catch (error) {
        throw new Error((error as Error).message);
      }

      const { score, examSummary } = examResult;
      let passed: boolean | undefined = undefined;

      const calculateScorePercentage = (
        score: number,
        totalQuestionCount: number
      ) => {
        const rawPercentage = (score / totalQuestionCount) * 100;
        return Math.round(rawPercentage * 10) / 10;
      };

      if (examDetails.maxQuestionCount && examDetails.passingRate) {
        const finalPercentage = calculateScorePercentage(
          score,
          examDetails.maxQuestionCount
        );
        passed = finalPercentage >= examDetails.passingRate;
      }

      const attempts = await db.studentExam.count({
        where: { studentId, examId: examDetails.id },
      });

      const isExamTokenNotUsed = await verifyOneTimeToken(token, rdb, payload);
      if (!isExamTokenNotUsed) {
        throw new Error('You have already submitted');
      }

      await db.examSubscription.update({
        where: { userId_examId: { userId: studentId, examId: examDetails.id } },
        data: {
          status: passed ? 'PASSED' : 'FAILED',
          examLink: null,
        },
      });

      try {
        const studentExamPayload = await db.studentExam.update({
          where: { id },
          data: {
            examDetails,
            examQuestions,
            studentAnswers,
            score,
            examSummary,
            submittedAt: submittedAt || undefined,
            passed,
            attempts,
          },
          include: {
            student: true,
          },
        });

        await blacklistToken(token, rdb);

        return studentExamPayload as SubmitStudentExamPayload;
      } catch (error) {
        if (
          error instanceof PrismaClientKnownRequestError &&
          error.code === 'P2002'
        )
          throw new Error('Exam has already been taken');
        throw new Error('Internal Server Error');
      }
    },
  },
};

export default resolvers;

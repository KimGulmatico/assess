import { merge } from 'lodash';
import StudentExam from './studentExam';

const resolvers = merge(StudentExam);

export default resolvers;

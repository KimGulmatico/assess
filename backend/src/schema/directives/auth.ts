import { defaultFieldResolver, GraphQLSchema } from 'graphql';
import { mapSchema, MapperKind, getDirective } from '@graphql-tools/utils';
import { AuthenticationError, ForbiddenError } from 'apollo-server-express';
import { Context } from '../../context';

const authDirectiveTransformer = (
  schema: GraphQLSchema,
  directiveName: string
) => {
  const typeDirectiveArgumentMaps: Record<string, any> = {};
  return mapSchema(schema, {
    [MapperKind.TYPE]: (type) => {
      const authDirective = getDirective(schema, type, directiveName)?.[0];
      if (authDirective) {
        typeDirectiveArgumentMaps[type.name] = authDirective;
      }
      return undefined;
    },
    [MapperKind.OBJECT_FIELD]: (fieldConfig, _fieldName, typeName) => {
      const authDirective =
        getDirective(schema, fieldConfig, directiveName)?.[0] ||
        typeDirectiveArgumentMaps[typeName];
      if (authDirective) {
        const { requires } = authDirective;
        if (requires) {
          const { resolve = defaultFieldResolver } = fieldConfig;
          // eslint-disable-next-line no-param-reassign
          fieldConfig.resolve = (parent, args, context: Context, info) => {
            const { payload } = context;
            if (!payload)
              return new AuthenticationError('You must be logged in');
            if (!payload?.roles?.includes(requires))
              return new ForbiddenError(`You must be logged in as ${requires}`);
            return resolve(parent, args, context, info);
          };
          return fieldConfig;
        }
      }
      return undefined;
    },
  });
};

export default authDirectiveTransformer;

import { merge } from 'lodash';
import Question from './Question';
import MultipleChoiceQuestion from './MultipleChoiceQuestion';
import MultipleSelectionQuestion from './MultipleSelectionQuestion';

const resolvers = merge(
  Question,
  MultipleChoiceQuestion,
  MultipleSelectionQuestion
);

export default resolvers;

import { Prisma } from '@prisma/client';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { Question, Resolvers } from '../../../resolvers-types.generated';
import {
  getQuery,
  getNodesAndPageInfo,
  determinePaginationArgs,
  prismaErrorHandler,
} from '../../utils';

const resolvers: Resolvers = {
  Question: {
    __resolveType: (question) => {
      if (!question.questionType) return null;
      if (question.questionType === 'MULTIPLE_CHOICE_QUESTION')
        return 'MultipleChoiceQuestion';
      if (question.questionType === 'MULTIPLE_SELECTION_QUESTION')
        return 'MultipleSelectionQuestion';
      return null;
    },
  },
  Query: {
    questions: async (_parent, { program, module }, { db }) => {
      const filters = [];
      if (program) filters.push({ program });
      if (module)
        filters.push({
          module: { equals: module, mode: 'insensitive' as Prisma.QueryMode },
        });

      try {
        const questions = await db.questionBase.findMany({
          where: {
            OR: filters.length ? filters : undefined,
          },
          orderBy: { createdAt: 'desc' },
        });

        return questions as Question[];
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    questionsConnection: async (_parent, args, { db }) => {
      const { take, limit } = determinePaginationArgs(args);

      const { after, before } = args;
      const cursor =
        after || before ? { id: after || before || undefined } : undefined;

      try {
        const questions = await db.questionBase.findMany({
          take,
          cursor,
          orderBy: { createdAt: 'desc' },
        });

        const { nodes, pageInfo } = getNodesAndPageInfo(questions, limit, args);

        return {
          nodes,
          pageInfo,
        };
      } catch (error) {
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
    searchQuestions: async (_parent, args, { db }) => {
      const { take, limit } = determinePaginationArgs(args);
      const { keywords, filters, after, before } = args;
      const cursor =
        after || before ? { id: after || before || undefined } : undefined;

      let query = '';
      let search = {};

      try {
        if (keywords) {
          query = keywords;

          let searchFilter: any = [
            { question: { contains: query, mode: 'insensitive' } },
            {
              multipleChoiceQuestion: {
                options: {
                  some: {
                    description: { contains: query, mode: 'insensitive' },
                  },
                },
              },
            },
            {
              multipleSelectionQuestion: {
                choices: {
                  some: {
                    description: { contains: query, mode: 'insensitive' },
                  },
                },
              },
            },
          ];
          search = { ...search, OR: searchFilter };
        }

        if (filters) {
          const textObj = JSON.parse(filters);
          const objKeys = Object.keys(textObj);

          const extraFilters = objKeys.map((key) => {
            return { [key]: textObj[key] };
          });
          search = { ...search, AND: extraFilters };
        }

        const searchResults = await db.questionBase.findMany({
          take,
          cursor,
          orderBy: { createdAt: 'desc' },
          where: search,
        });

        const { nodes, pageInfo } = getNodesAndPageInfo(
          searchResults,
          limit,
          args
        );

        return {
          nodes,
          pageInfo,
        };
      } catch (error) {
        console.log(error);
        if (error instanceof PrismaClientKnownRequestError)
          prismaErrorHandler(error);
        throw new Error('Internal Server Error');
      }
    },
  },
};

export default resolvers;

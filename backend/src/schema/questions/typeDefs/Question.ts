import { gql } from 'apollo-server';

const typeDefs = gql`
  interface Question {
    id: String
    question: String
    program: Program
    module: String
    createdAt: Date
    updatedAt: Date
    createdById: String
    createdBy: User
    questionType: QuestionType
  }

  enum QuestionType {
    MULTIPLE_CHOICE_QUESTION
    MULTIPLE_SELECTION_QUESTION
  }

  type QuestionConnection {
    nodes: [Question]
    pageInfo: PageInfo!
  }

  type QuestionConnection {
    nodes: [Question]
    pageInfo: PageInfo!
  }

  type Query {
    questions(program: Program, module: String): [Question] @auth
    searchQuestions(
      first: Int
      last: Int
      after: String
      before: String
      keywords: String
      filters: String
    ): QuestionConnection @auth
    questionsConnection(
      first: Int
      last: Int
      after: String
      before: String
    ): QuestionConnection @auth
  }
`;

export default typeDefs;

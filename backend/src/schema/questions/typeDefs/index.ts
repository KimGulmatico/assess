import Question from './Question';
import MultipleChoiceQuestion from './MultipleChoiceQuestion';
import MultipleSelectionQuestion from './MultipleSelectionQuestion';

const typeDefs = [Question, MultipleChoiceQuestion, MultipleSelectionQuestion];

export default typeDefs;

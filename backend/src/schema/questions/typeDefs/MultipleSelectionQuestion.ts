import { gql } from 'apollo-server';

const typeDefs = gql`
  type Choice {
    id: String
    description: String
    isCorrect: Boolean @auth(requires: ADMIN)
  }

  type MultipleSelectionQuestion implements Question {
    id: String
    question: String
    program: Program
    module: String
    createdAt: Date
    updatedAt: Date
    createdById: String
    createdBy: User
    questionType: QuestionType
    choices: [Choice]
  }

  input CreateChoiceInput {
    description: String!
    isCorrect: Boolean!
    multipleSelectionQuestionId: String
  }

  input CreateMultipleSelectionQuestionInput {
    question: String!
    program: Program!
    module: String!
    choices: [CreateChoiceInput!]!
  }

  input UpdateChoiceInput {
    id: String!
    description: String!
    isCorrect: Boolean!
  }

  input UpdateMultipleSelectionQuestionInput {
    id: String!
    question: String!
    program: Program
    module: String
  }

  input DeleteMultipleSelectionQuestionInput {
    id: String!
  }

  input DeleteChoiceInput {
    id: String!
  }

  type Query {
    multipleSelectionQuestions: [MultipleSelectionQuestion] @auth
    multipleSelectionQuestion(id: String!): MultipleSelectionQuestion @auth
  }

  type Mutation {
    createMultipleSelectionQuestion(
      input: CreateMultipleSelectionQuestionInput!
    ): MultipleSelectionQuestion @auth(requires: ADMIN)
    updateMultipleSelectionQuestion(
      input: UpdateMultipleSelectionQuestionInput!
    ): MultipleSelectionQuestion @auth(requires: ADMIN)
    deleteMultipleSelectionQuestion(
      input: DeleteMultipleSelectionQuestionInput!
    ): MultipleSelectionQuestion @auth(requires: ADMIN)
    createMultipleSelectionChoice(input: CreateChoiceInput!): Choice
      @auth(requires: ADMIN)
    updateMultipleSelectionChoice(input: UpdateChoiceInput!): Choice
      @auth(requires: ADMIN)
    deleteMultipleSelectionChoice(input: DeleteChoiceInput!): Choice
      @auth(requires: ADMIN)
  }
`;

export default typeDefs;

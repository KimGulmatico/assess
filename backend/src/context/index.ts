import { PrismaClient } from '@prisma/client';
import { ExpressContext } from 'apollo-server-express';
import { Request, Response } from 'express';
import { createClient } from 'redis';
import { verifyToken, UserJwtPayload } from '../middleware/auth';
import db from './database';
import redis from './redis';

export interface Context {
  req: Request;
  res: Response;
  db: PrismaClient;
  payload: UserJwtPayload | null;
  rdb: ReturnType<typeof createClient>;
}

const context = async ({ req, res }: ExpressContext): Promise<Context> => {
  const authHeader = req.cookies.jwt || '';
  const payload = verifyToken(authHeader);
  
  return { req, res, payload, db, rdb: await redis };
};

export default context;

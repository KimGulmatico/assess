import { createClient } from 'redis';

const redis = async () => {
    const client = createClient();
    client.on("error", (error) => console.error(`Error : ${error}`));
    await client.connect();
    return client;
}

export default redis();

import formData from 'form-data';
import Mailgun from 'mailgun.js';
const mg = new Mailgun(formData);

export const mailgun = mg.client({
  username: process.env.MAILGUN_USERNAME as string,
  key: process.env.MAILGUN_API_KEY as string,
});

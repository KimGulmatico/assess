import { sign, verify } from 'jsonwebtoken';

require('dotenv').config();

export interface UserJwtPayload {
  userId: string;
  email: string;
  firstName: string;
  lastName: string;
  roles: string[];
  examId?: string;
  iat: number;
  exp: number;
}

export const generateToken = (payload: any) => {
  if (typeof process.env.JWT_SECRET !== 'string') {
    throw new Error('provide a JWT secret');
  }
  return sign(payload, process.env.JWT_SECRET, { expiresIn: '3days' });
};

export const verifyToken = (token: any): UserJwtPayload | null => {
  if (!token) {
    return null;
  }

  if (typeof process.env.JWT_SECRET !== 'string') {
    throw new Error('provide a JWT secret');
  }

  try {
    const payload = verify(token, process.env.JWT_SECRET);
    return payload as UserJwtPayload;
  } catch (error) {
    return null;
  }
};

const id = 'c60b7c64-98f5-4e87-9c07-4a2201a78b9a'; // dummy id

const data = [
  { email: 'rave.arevalo@kingsland.io', createdById: id },
  { email: 'ubol@kingslanduniversity.com', createdById: id },
  { email: 'simeonm@kingslanduniversity.com', createdById: id },
  { email: 'mgavilla@kingslanduniversity.com', createdById: id },
  { email: 'robert@kingslanduniversity.com', createdById: id },
  { email: 'josh@kingslanduniversity.com', createdById: id },
  { email: 'zulein@kingslanduniversity.com', createdById: id },
  { email: 'mjoni@kingslanduniversity.com', createdById: id },
  { email: 'danielle@kingslanduniversity.com', createdById: id },
  { email: 'junichiro@kingslanduniversity.com', createdById: id },
  { email: 'meryll@kingslanduniversity.com', createdById: id },
  { email: 'jannessah@kingslanduniversity.com', createdById: id },
  { email: 'relen@kingslanduniversity.com', createdById: id },
  { email: 'rhochellek@kingslanduniversity.com', createdById: id },
  { email: 'sean@kingsland.io', createdById: id },
  { email: 'preslav@kingslanduniversity.com', createdById: id },
  { email: 'patrick@kingslanduniversity.com', createdById: id },
  { email: 'ron@kingslanduniversity.com', createdById: id },
  { email: 'danddie@kingsland.io', createdById: id },
  { email: 'pia@kingsland.io', createdById: id },
  { email: 'james.suminguit@kingslanduniversity.com', createdById: id },
  { email: 'kim.gulmatico@kingsland.io', createdById: id},
];

export default data;

-- AlterTable
ALTER TABLE "Exam" ADD COLUMN     "passingRate" INTEGER;

-- AlterTable
ALTER TABLE "StudentExam" ADD COLUMN     "passed" BOOLEAN;

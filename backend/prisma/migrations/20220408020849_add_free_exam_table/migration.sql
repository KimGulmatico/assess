-- CreateEnum
CREATE TYPE "Program" AS ENUM ('BLOCKCHAIN', 'FULL_STACK', 'CYBERSECURITY', 'TECH_SALES');

-- CreateEnum
CREATE TYPE "Quarter" AS ENUM ('Q1', 'Q2', 'Q3', 'Q4');

-- CreateTable
CREATE TABLE "FreeExam" (
    "id" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "program" "Program" NOT NULL,
    "year" TIMESTAMP(3) NOT NULL,
    "quarter" "Quarter" NOT NULL,
    "isPublished" BOOLEAN NOT NULL DEFAULT false,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "FreeExam_pkey" PRIMARY KEY ("id")
);

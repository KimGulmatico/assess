/*
  Warnings:

  - The primary key for the `StudentExam` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `submissionDetails` on the `StudentExam` table. All the data in the column will be lost.
  - The required column `id` was added to the `StudentExam` table with a prisma-level default value. This is not possible if the table is not empty. Please add this column as optional, then populate it before making it required.

*/
-- AlterTable
ALTER TABLE "StudentExam" DROP CONSTRAINT "StudentExam_pkey",
DROP COLUMN "submissionDetails",
ADD COLUMN     "id" TEXT NOT NULL,
ADD COLUMN     "openedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
ALTER COLUMN "examSummary" DROP NOT NULL,
ALTER COLUMN "submittedAt" DROP NOT NULL,
ALTER COLUMN "submittedAt" DROP DEFAULT,
ADD CONSTRAINT "StudentExam_pkey" PRIMARY KEY ("studentId", "examId", "id");

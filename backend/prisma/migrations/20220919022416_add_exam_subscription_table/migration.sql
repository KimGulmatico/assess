-- CreateTable
CREATE TABLE "ExamSubscription" (
    "userId" TEXT NOT NULL,
    "examId" TEXT NOT NULL,

    CONSTRAINT "ExamSubscription_pkey" PRIMARY KEY ("userId","examId")
);

-- AddForeignKey
ALTER TABLE "ExamSubscription" ADD CONSTRAINT "ExamSubscription_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ExamSubscription" ADD CONSTRAINT "ExamSubscription_examId_fkey" FOREIGN KEY ("examId") REFERENCES "Exam"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

/*
  Warnings:

  - The primary key for the `StudentExam` table will be changed. If it partially fails, the table could be left without primary key constraint.

*/
-- AlterTable
ALTER TABLE "StudentExam" DROP CONSTRAINT "StudentExam_pkey",
ADD CONSTRAINT "StudentExam_pkey" PRIMARY KEY ("id");

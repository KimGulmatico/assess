/*
  Warnings:

  - Added the required column `status` to the `ExamSubscription` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "Status" AS ENUM ('PAID', 'PASSED', 'FAILED');

-- AlterTable
ALTER TABLE "ExamSubscription" ADD COLUMN     "status" "Status" NOT NULL;

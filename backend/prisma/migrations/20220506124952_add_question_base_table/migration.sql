/*
  Warnings:

  - You are about to drop the column `createdAt` on the `CodeQuestion` table. All the data in the column will be lost.
  - You are about to drop the column `createdById` on the `CodeQuestion` table. All the data in the column will be lost.
  - You are about to drop the column `question` on the `CodeQuestion` table. All the data in the column will be lost.
  - You are about to drop the column `updatedAt` on the `CodeQuestion` table. All the data in the column will be lost.
  - You are about to drop the column `createdAt` on the `MultipleChoiceQuestion` table. All the data in the column will be lost.
  - You are about to drop the column `createdById` on the `MultipleChoiceQuestion` table. All the data in the column will be lost.
  - You are about to drop the column `question` on the `MultipleChoiceQuestion` table. All the data in the column will be lost.
  - You are about to drop the column `updatedAt` on the `MultipleChoiceQuestion` table. All the data in the column will be lost.
  - You are about to drop the column `createdAt` on the `MultipleSelectionQuestion` table. All the data in the column will be lost.
  - You are about to drop the column `createdById` on the `MultipleSelectionQuestion` table. All the data in the column will be lost.
  - You are about to drop the column `question` on the `MultipleSelectionQuestion` table. All the data in the column will be lost.
  - You are about to drop the column `updatedAt` on the `MultipleSelectionQuestion` table. All the data in the column will be lost.
  - You are about to drop the `CodeQuestionsOnExams` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `MultipleChoiceQuestionsOnExams` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `MultipleSelectionQuestionsOnExams` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[id]` on the table `CodeQuestion` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[id]` on the table `MultipleChoiceQuestion` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[id]` on the table `MultipleSelectionQuestion` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateEnum
CREATE TYPE "QuestionType" AS ENUM ('CODE_QUESTION', 'MULTIPLE_CHOICE_QUESTION', 'MULTIPLE_SELECTION_QUESTION');

-- DropForeignKey
ALTER TABLE "CodeQuestion" DROP CONSTRAINT "CodeQuestion_createdById_fkey";

-- DropForeignKey
ALTER TABLE "CodeQuestionsOnExams" DROP CONSTRAINT "CodeQuestionsOnExams_codeQuestionId_fkey";

-- DropForeignKey
ALTER TABLE "CodeQuestionsOnExams" DROP CONSTRAINT "CodeQuestionsOnExams_examId_fkey";

-- DropForeignKey
ALTER TABLE "MultipleChoiceQuestion" DROP CONSTRAINT "MultipleChoiceQuestion_createdById_fkey";

-- DropForeignKey
ALTER TABLE "MultipleChoiceQuestionsOnExams" DROP CONSTRAINT "MultipleChoiceQuestionsOnExams_examId_fkey";

-- DropForeignKey
ALTER TABLE "MultipleChoiceQuestionsOnExams" DROP CONSTRAINT "MultipleChoiceQuestionsOnExams_multipleChoiceQuestionId_fkey";

-- DropForeignKey
ALTER TABLE "MultipleSelectionQuestion" DROP CONSTRAINT "MultipleSelectionQuestion_createdById_fkey";

-- DropForeignKey
ALTER TABLE "MultipleSelectionQuestionsOnExams" DROP CONSTRAINT "MultipleSelectionQuestionsOnExams_examId_fkey";

-- DropForeignKey
ALTER TABLE "MultipleSelectionQuestionsOnExams" DROP CONSTRAINT "MultipleSelectionQuestionsOnExams_multipleSelectionQuestio_fkey";

-- AlterTable
ALTER TABLE "CodeQuestion" DROP COLUMN "createdAt",
DROP COLUMN "createdById",
DROP COLUMN "question",
DROP COLUMN "updatedAt";

-- AlterTable
ALTER TABLE "MultipleChoiceQuestion" DROP COLUMN "createdAt",
DROP COLUMN "createdById",
DROP COLUMN "question",
DROP COLUMN "updatedAt";

-- AlterTable
ALTER TABLE "MultipleSelectionQuestion" DROP COLUMN "createdAt",
DROP COLUMN "createdById",
DROP COLUMN "question",
DROP COLUMN "updatedAt";

-- DropTable
DROP TABLE "CodeQuestionsOnExams";

-- DropTable
DROP TABLE "MultipleChoiceQuestionsOnExams";

-- DropTable
DROP TABLE "MultipleSelectionQuestionsOnExams";

-- CreateTable
CREATE TABLE "QuestionBase" (
    "id" TEXT NOT NULL,
    "question" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "createdById" TEXT NOT NULL,
    "questionType" "QuestionType" NOT NULL,

    CONSTRAINT "QuestionBase_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "QuestionsOnExams" (
    "examId" TEXT NOT NULL,
    "questionId" TEXT NOT NULL,
    "addedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "QuestionsOnExams_pkey" PRIMARY KEY ("examId","questionId")
);

-- CreateIndex
CREATE UNIQUE INDEX "CodeQuestion_id_key" ON "CodeQuestion"("id");

-- CreateIndex
CREATE UNIQUE INDEX "MultipleChoiceQuestion_id_key" ON "MultipleChoiceQuestion"("id");

-- CreateIndex
CREATE UNIQUE INDEX "MultipleSelectionQuestion_id_key" ON "MultipleSelectionQuestion"("id");

-- AddForeignKey
ALTER TABLE "QuestionBase" ADD CONSTRAINT "QuestionBase_createdById_fkey" FOREIGN KEY ("createdById") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MultipleChoiceQuestion" ADD CONSTRAINT "MultipleChoiceQuestion_id_fkey" FOREIGN KEY ("id") REFERENCES "QuestionBase"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CodeQuestion" ADD CONSTRAINT "CodeQuestion_id_fkey" FOREIGN KEY ("id") REFERENCES "QuestionBase"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MultipleSelectionQuestion" ADD CONSTRAINT "MultipleSelectionQuestion_id_fkey" FOREIGN KEY ("id") REFERENCES "QuestionBase"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "QuestionsOnExams" ADD CONSTRAINT "QuestionsOnExams_questionId_fkey" FOREIGN KEY ("questionId") REFERENCES "QuestionBase"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "QuestionsOnExams" ADD CONSTRAINT "QuestionsOnExams_examId_fkey" FOREIGN KEY ("examId") REFERENCES "Exam"("id") ON DELETE CASCADE ON UPDATE CASCADE;

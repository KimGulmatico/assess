/*
  Warnings:

  - Added the required column `module` to the `Exam` table without a default value. This is not possible if the table is not empty.
  - Added the required column `module` to the `QuestionBase` table without a default value. This is not possible if the table is not empty.
  - Added the required column `program` to the `QuestionBase` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Exam" ADD COLUMN     "module" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "QuestionBase" ADD COLUMN     "module" TEXT NOT NULL,
ADD COLUMN     "program" "Program" NOT NULL;

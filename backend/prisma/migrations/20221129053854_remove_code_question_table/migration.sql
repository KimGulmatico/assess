/*
  Warnings:

  - The values [CODE_QUESTION] on the enum `QuestionType` will be removed. If these variants are still used in the database, this will fail.
  - You are about to drop the `CodeQuestion` table. If the table is not empty, all the data it contains will be lost.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "QuestionType_new" AS ENUM ('MULTIPLE_CHOICE_QUESTION', 'MULTIPLE_SELECTION_QUESTION');
ALTER TABLE "QuestionBase" ALTER COLUMN "questionType" TYPE "QuestionType_new" USING ("questionType"::text::"QuestionType_new");
ALTER TYPE "QuestionType" RENAME TO "QuestionType_old";
ALTER TYPE "QuestionType_new" RENAME TO "QuestionType";
DROP TYPE "QuestionType_old";
COMMIT;

-- DropForeignKey
ALTER TABLE "CodeQuestion" DROP CONSTRAINT "CodeQuestion_id_fkey";

-- DropTable
DROP TABLE "CodeQuestion";

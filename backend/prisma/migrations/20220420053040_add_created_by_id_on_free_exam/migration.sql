/*
  Warnings:

  - Added the required column `createdById` to the `FreeExam` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "FreeExam" ADD COLUMN     "createdById" TEXT NOT NULL;

-- AddForeignKey
ALTER TABLE "FreeExam" ADD CONSTRAINT "FreeExam_createdById_fkey" FOREIGN KEY ("createdById") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

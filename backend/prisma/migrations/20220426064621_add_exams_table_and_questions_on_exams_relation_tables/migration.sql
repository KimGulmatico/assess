/*
  Warnings:

  - You are about to drop the `FreeExam` table. If the table is not empty, all the data it contains will be lost.

*/
-- CreateEnum
CREATE TYPE "ExamType" AS ENUM ('FREE', 'FIXED', 'FLEXIBLE');

-- DropForeignKey
ALTER TABLE "FreeExam" DROP CONSTRAINT "FreeExam_createdById_fkey";

-- DropTable
DROP TABLE "FreeExam";

-- CreateTable
CREATE TABLE "Exam" (
    "id" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "program" "Program" NOT NULL,
    "year" INTEGER NOT NULL,
    "quarter" "Quarter" NOT NULL,
    "isPublished" BOOLEAN NOT NULL DEFAULT false,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "createdById" TEXT NOT NULL,
    "examType" "ExamType" NOT NULL,
    "startDate" TIMESTAMP(3),
    "endDate" TIMESTAMP(3),
    "duration" INTEGER,

    CONSTRAINT "Exam_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "MultipleChoiceQuestionsOnExams" (
    "examId" TEXT NOT NULL,
    "multipleChoiceQuestionId" TEXT NOT NULL,
    "addedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "MultipleChoiceQuestionsOnExams_pkey" PRIMARY KEY ("examId","multipleChoiceQuestionId")
);

-- CreateTable
CREATE TABLE "MultipleSelectionQuestionsOnExams" (
    "examId" TEXT NOT NULL,
    "multipleSelectionQuestionId" TEXT NOT NULL,
    "addedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "MultipleSelectionQuestionsOnExams_pkey" PRIMARY KEY ("examId","multipleSelectionQuestionId")
);

-- CreateTable
CREATE TABLE "CodeQuestionsOnExams" (
    "examId" TEXT NOT NULL,
    "codeQuestionId" TEXT NOT NULL,
    "addedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "CodeQuestionsOnExams_pkey" PRIMARY KEY ("examId","codeQuestionId")
);

-- AddForeignKey
ALTER TABLE "Exam" ADD CONSTRAINT "Exam_createdById_fkey" FOREIGN KEY ("createdById") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MultipleChoiceQuestionsOnExams" ADD CONSTRAINT "MultipleChoiceQuestionsOnExams_multipleChoiceQuestionId_fkey" FOREIGN KEY ("multipleChoiceQuestionId") REFERENCES "MultipleChoiceQuestion"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MultipleChoiceQuestionsOnExams" ADD CONSTRAINT "MultipleChoiceQuestionsOnExams_examId_fkey" FOREIGN KEY ("examId") REFERENCES "Exam"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MultipleSelectionQuestionsOnExams" ADD CONSTRAINT "MultipleSelectionQuestionsOnExams_multipleSelectionQuestio_fkey" FOREIGN KEY ("multipleSelectionQuestionId") REFERENCES "MultipleSelectionQuestion"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MultipleSelectionQuestionsOnExams" ADD CONSTRAINT "MultipleSelectionQuestionsOnExams_examId_fkey" FOREIGN KEY ("examId") REFERENCES "Exam"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CodeQuestionsOnExams" ADD CONSTRAINT "CodeQuestionsOnExams_codeQuestionId_fkey" FOREIGN KEY ("codeQuestionId") REFERENCES "CodeQuestion"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CodeQuestionsOnExams" ADD CONSTRAINT "CodeQuestionsOnExams_examId_fkey" FOREIGN KEY ("examId") REFERENCES "Exam"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

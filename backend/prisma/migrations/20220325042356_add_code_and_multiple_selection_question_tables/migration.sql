-- CreateTable
CREATE TABLE "CodeQuestion" (
    "id" TEXT NOT NULL,
    "question" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "activityId" TEXT NOT NULL,

    CONSTRAINT "CodeQuestion_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "MultipleSelectionQuestion" (
    "id" TEXT NOT NULL,
    "question" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "MultipleSelectionQuestion_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "MultipleSelectionOption" (
    "id" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "isCorrect" BOOLEAN NOT NULL DEFAULT false,
    "multipleSelectionQuestionId" TEXT NOT NULL,

    CONSTRAINT "MultipleSelectionOption_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "MultipleSelectionOption" ADD CONSTRAINT "MultipleSelectionOption_multipleSelectionQuestionId_fkey" FOREIGN KEY ("multipleSelectionQuestionId") REFERENCES "MultipleSelectionQuestion"("id") ON DELETE CASCADE ON UPDATE CASCADE;

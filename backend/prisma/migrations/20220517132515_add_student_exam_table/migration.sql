-- CreateTable
CREATE TABLE "StudentExam" (
    "studentId" TEXT NOT NULL,
    "examId" TEXT NOT NULL,
    "examDetails" JSONB NOT NULL,
    "examQuestions" JSONB NOT NULL,
    "studentAnswers" JSONB,
    "score" DOUBLE PRECISION,
    "examSummary" JSONB NOT NULL,
    "submittedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "StudentExam_pkey" PRIMARY KEY ("studentId","examId")
);

-- AddForeignKey
ALTER TABLE "StudentExam" ADD CONSTRAINT "StudentExam_studentId_fkey" FOREIGN KEY ("studentId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

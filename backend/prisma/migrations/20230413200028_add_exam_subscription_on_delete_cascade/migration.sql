-- DropForeignKey
ALTER TABLE "ExamSubscription" DROP CONSTRAINT "ExamSubscription_userId_fkey";

-- AddForeignKey
ALTER TABLE "ExamSubscription" ADD CONSTRAINT "ExamSubscription_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

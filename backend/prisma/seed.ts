import prisma from '../src/context/database';
import data from './seedData/admins';

async function main() {
  for (let i = 0; i < data.length; i++) {
    await prisma.admin.upsert({
      where: {
        email: data[i].email,
      },
      update: {
        email: data[i].email,
        createdAt: new Date(),
        createdById: data[i].createdById,
      },
      create: {
        email: data[i].email,
        createdById: data[i].createdById,
      },
    });
  }
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });

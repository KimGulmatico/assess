# Kingsland Assess Backend Server

## Available Scripts

### `npm run start:dev`

Runs the app in the development mode.\
Open [http://localhost:4000] to view it in the browser.

### `npm run migrate:dev`

Apply existing migrations to your database

### `npm run migrate:make <migration-name>`

Generate new migrations.\
Provide migration name to save new migrations.

### `npm run generate`

Generate resolvers types.

### `npm run prisma:studio`

Opens Prisma studio in the browser.\
View and edit data inside your database.

### `npm run prisma:push`

Use to prototype a new schema or a new feature for an existing schema.\
This does not save any changes in the migration history

### `npm run prisma:seed`

Allows you to consistently re-create the same data in the database

## Run Tests

Create `env.test` file following `env.example` format

Run `npm run test`

## API Documentation

Please refer to this documentation for mutations and queries.\
https://documenter.getpostman.com/view/20135807/2s8Z711sDz

const esModules = ['@agm', 'ngx-bootstrap', 'mui-rte'].join('|');

module.exports = {
  roots: ['<rootDir>/src'],
  testEnvironment: 'jsdom',
  preset: 'ts-jest',
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
    [`(${esModules}).+\\.js$`]: 'ts-jest',
  },
  setupFilesAfterEnv: ['@testing-library/jest-dom'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/src/__mocks__/fileMock.ts',
    '\\.(css|less)$': 'identity-obj-proxy',
    '@uiw/react-textarea-code-editor':
      '<rootDir>/node_modules/@uiw/react-textarea-code-editor/dist/editor.js',
  },
  transformIgnorePatterns: [`/node_modules/(?!${esModules})`],
};

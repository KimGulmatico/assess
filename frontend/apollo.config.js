module.exports = {
  client: {
    service: {
      name: 'backend',
      uri: 'https://tzcert-api.merkle-trees.com.com/',
    },
    excludes: ['**/*.generated.ts'],
  },
};

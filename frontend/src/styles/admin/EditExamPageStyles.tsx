import { styled } from '@mui/material';

const EditExamContainer = styled('div')(() => ({
  margin: '30px 40px 30px 40px',
}));

export default EditExamContainer;

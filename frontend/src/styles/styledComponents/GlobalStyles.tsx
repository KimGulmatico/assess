export const TablePaginationStyles = {
  border: 0,
  justifyContent: 'flex-end',
  alignItems: 'center',
  display: 'flex',
  marginRight: '20px',
  '& .MuiTablePagination-selectLabel': {
    color: '#a9a9a9',
  },
  '& .MuiTablePagination-selectIcon': {
    color: '#374EA2',
  },
  '& .MuiTablePagination-select': {
    color: '#374EA2',
    border: '1px solid #374EA2',
    borderRadius: '5px',
    backgroundColor: '#fffff',
    '&:focus': {
      borderRadius: '5px',
    },
  },
  '& .MuiTablePagination-displayedRows': {
    color: '#374EA2',
  },
  '& .MuiTablePagination-actions > button': {
    margin: '0 5px',
    '&:hover': {
      color: '#374EA2',
    },
    '&:focus': {
      color: '#374EA2',
    },
  },
};

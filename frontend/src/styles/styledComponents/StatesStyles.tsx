import { styled } from '@mui/material/styles';

const LoaderImage = styled('img')(() => ({
  maxHeight: '100px',
  maxWidth: '100px',
}));

export default LoaderImage;

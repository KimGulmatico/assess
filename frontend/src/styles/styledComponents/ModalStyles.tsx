import { styled, Box } from '@mui/material';

export const BottomScrollBox = styled(Box)(() => ({
  maxHeight: 180,
  overflowY: 'auto',
  display: 'flex',
  flexDirection: 'column-reverse',
}));

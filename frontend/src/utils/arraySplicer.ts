import { AllQuestions } from '../pages/admin/editExam/editExamProps';

const spliceArray = (array: AllQuestions[], baseData: AllQuestions) => {
  const index = array.map((data) => data.id).indexOf(baseData.id);
  array.splice(index, 1);
};

export default spliceArray;

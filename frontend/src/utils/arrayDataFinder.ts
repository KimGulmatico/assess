import { AllQuestions } from '../pages/admin/editExam/editExamProps';

const findById = (array: AllQuestions[], id: string) => {
  return array.find((data) => data.id === id);
};

export default findById;

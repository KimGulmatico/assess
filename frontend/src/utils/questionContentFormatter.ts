/* eslint-disable @typescript-eslint/no-unsafe-member-access */
function formatQuestionContent(questionContent: string) {
  const blocks = JSON.parse(questionContent).blocks as Record<string, string>[];
  let question = '';
  const contents = Object.entries(blocks);
  contents.forEach((content) => {
    question = question.concat(content[1].text, ' ');
  });
  return question;
}

export default formatQuestionContent;

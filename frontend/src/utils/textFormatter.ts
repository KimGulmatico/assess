function formatText(text: string) {
  return (
    text.charAt(0).toUpperCase() +
    text.slice(1).split('_').join(' ').toLowerCase()
  );
}

export default formatText;

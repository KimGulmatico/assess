function generateExamLink(examId: string) {
  return `${window.location.host}/take/${examId}`;
}

export default generateExamLink;

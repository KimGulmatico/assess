const calculateScorePercentage = (
  numericalScore: number,
  totalScore: number
) => {
  const rawPercentage = (numericalScore / totalScore) * 100;
  return Math.round(rawPercentage * 10) / 10;
};

export default calculateScorePercentage;

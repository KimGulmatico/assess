import { gql } from '@apollo/client';

export const DELETE_MS_CHOICE_MUTATION = gql`
  mutation DeleteMultipleSelectionChoice($input: DeleteChoiceInput!) {
    deleteMultipleSelectionChoice(input: $input) {
      id
    }
  }
`;

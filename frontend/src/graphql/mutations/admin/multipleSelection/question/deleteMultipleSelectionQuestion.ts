import { gql } from '@apollo/client';

export const DELETE_MS_QUESTION_MUTATION = gql`
  mutation DeleteMultipleSelectionQuestion(
    $input: DeleteMultipleSelectionQuestionInput!
  ) {
    deleteMultipleSelectionQuestion(input: $input) {
      id
    }
  }
`;

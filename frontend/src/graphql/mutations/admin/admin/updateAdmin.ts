import { gql } from '@apollo/client';

const UPDATE_ADMIN_MUTATION = gql`
  mutation UpdateAdmin($input: UpdateAdminInput!) {
    updateAdmin(input: $input) {
      email
      id
    }
  }
`;

export default UPDATE_ADMIN_MUTATION;

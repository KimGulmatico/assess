import { gql } from '@apollo/client';

const SEND_EXAM_LINK_MUTATION = gql`
  mutation SendExamLink($email: String!) {
    sendExamLink(email: $email)
  }
`;

export default SEND_EXAM_LINK_MUTATION;

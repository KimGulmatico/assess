import { gql } from '@apollo/client';

const UNSUBSCRIBE_TO_EXAM_MUTATION = gql`
  mutation UnsubscribeFromExam($examId: String!) {
    unsubscribe(examId: $examId) {
      examId
    }
  }
`;

export default UNSUBSCRIBE_TO_EXAM_MUTATION;

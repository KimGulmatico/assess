import { gql } from '@apollo/client';

const SEND_EMAIL_NOTIFICATION_MUTATION = gql`
  mutation SendEmailNotification($input: SendEmailNotificationInput!) {
    sendEmailNotification(input: $input) {
      userId
      examId
    }
  }
`;

export default SEND_EMAIL_NOTIFICATION_MUTATION;

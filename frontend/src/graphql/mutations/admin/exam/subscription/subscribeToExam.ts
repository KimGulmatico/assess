import { gql } from '@apollo/client';

const SUBSCRIBE_TO_EXAM_MUTATION = gql`
  mutation SubscribeToExam($examId: String!) {
    subscribe(examId: $examId) {
      examId
    }
  }
`;

export default SUBSCRIBE_TO_EXAM_MUTATION;

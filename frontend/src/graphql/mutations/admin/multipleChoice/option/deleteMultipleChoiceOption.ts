import { gql } from '@apollo/client';

export const DELETE_MC_OPTION_MUTATION = gql`
  mutation DeleteMultipleChoiceOption($input: DeleteOptionInput!) {
    deleteMultipleChoiceOption(input: $input) {
      id
    }
  }
`;

import { gql } from '@apollo/client';

export const UPDATE_MC_QUESTION_MUTATION = gql`
  mutation UpdateMultipleChoiceQuestion(
    $input: UpdateMultipleChoiceQuestionInput!
  ) {
    updateMultipleChoiceQuestion(input: $input) {
      id
      program
      module
      question
    }
  }
`;

import { gql } from '@apollo/client';

const UPDATE_USER_NAME_MUTATION = gql`
  mutation UpdateUserName($input: UpdateUserInput!) {
    updateUserName(input: $input) {
      email
      id
    }
  }
`;

export default UPDATE_USER_NAME_MUTATION;

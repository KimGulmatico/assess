import { gql } from '@apollo/client';

const DELETE_USER_MUTATION = gql`
  mutation DeleteUser($id: String!) {
    deleteUser(id: $id) {
      id
    }
  }
`;

export default DELETE_USER_MUTATION;

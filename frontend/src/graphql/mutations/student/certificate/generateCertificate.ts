import { gql } from '@apollo/client';

const GENERATE_CERTIFICATE = gql`
  mutation GenerateCertificate($studentExamId: String!) {
    generateCertificate(studentExamId: $studentExamId)
  }
`;

export default GENERATE_CERTIFICATE;

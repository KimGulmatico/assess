import { gql } from '@apollo/client';

const SEND_EMAIL_CERTIFICATE = gql`
  mutation SendEmailCertificate($input: SendEmailCertificateInput!) {
    sendEmailCertificate(input: $input) {
      successful
    }
  }
`;

export default SEND_EMAIL_CERTIFICATE;

import { gql } from '@apollo/client';

const GENERATE_EXAM_LINK_MUTATION = gql`
  mutation GenerateExamLink {
    generateExamLink
  }
`;

export default GENERATE_EXAM_LINK_MUTATION;

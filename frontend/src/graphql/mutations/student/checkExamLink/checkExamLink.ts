import { gql } from '@apollo/client';

const IS_EXAM_LINK_VALID_MUTATION = gql`
  mutation IsExamLinkValid($input: CheckExamInput!) {
    isExamLinkValid(input: $input)
  }
`;

export default IS_EXAM_LINK_VALID_MUTATION;

import { gql } from '@apollo/client';

export const SIGN_OUT_MUTATION = gql`
  mutation SignOut {
    signOut
  }
`;

export default SIGN_OUT_MUTATION;

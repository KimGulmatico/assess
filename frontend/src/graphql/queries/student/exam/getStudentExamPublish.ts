import { gql } from '@apollo/client';

const GET_STUDENT_EXAM_PUBLISH_STATUS_QUERY = gql`
  query StudentExamPublishStatus($examId: String!) {
    exam(id: $examId) {
      isPublished
    }
  }
`;

export default GET_STUDENT_EXAM_PUBLISH_STATUS_QUERY;

import { gql } from '@apollo/client';

const GET_STUDENT_EXAM_DETAILS_QUERY = gql`
  query StudentExamDetails($examId: String!) {
    exam(id: $examId) {
      id
      title
      description
      program
      module
      year
      quarter
      maxQuestionCount
      createdAt
      updatedAt
      createdById
      startDate
      endDate
      duration
      examType
      passingRate
    }
  }
`;

export default GET_STUDENT_EXAM_DETAILS_QUERY;

import { gql } from '@apollo/client';

const GET_STUDENT_EXAMS_BY_STUDENT_ID_QUERY = gql`
  query GetStudentExamsByStudentId(
    $studentId: String!
    $first: Int
    $last: Int
    $after: String
    $before: String
  ) {
    studentExamsConnectionByStudentId(
      studentId: $studentId
      first: $first
      last: $last
      after: $after
      before: $before
    ) {
      nodes {
        studentExams {
          id
          score
          submittedAt
          passed
          examDetails {
            id
            description
            title
            module
            maxQuestionCount
            endDate
            passingRate
          }
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

export default GET_STUDENT_EXAMS_BY_STUDENT_ID_QUERY;

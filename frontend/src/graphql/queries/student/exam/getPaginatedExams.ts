import { gql } from '@apollo/client';

const GET_ALL_STUDENT_EXAMS_QUERY = gql`
  query AllStudentExams(
    $first: Int
    $last: Int
    $after: String
    $before: String
  ) {
    studentExamsConnection(
      first: $first
      last: $last
      after: $after
      before: $before
    ) {
      nodes {
        id
        score
        submittedAt
        passed
        examDetails {
          description
          title
          module
          maxQuestionCount
          endDate
          passingRate
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

export default GET_ALL_STUDENT_EXAMS_QUERY;

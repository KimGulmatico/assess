import { gql } from '@apollo/client';

const GET_STUDENT_EXAM_QUESTIONS_QUERY = gql`
  query StudentExamQuestions($examId: String!) {
    exam(id: $examId) {
      questions {
        id
        question
        program
        module
        createdAt
        updatedAt
        createdById
        createdBy {
          id
          email
          firstName
          lastName
        }
        questionType
        ... on MultipleChoiceQuestion {
          options {
            id
            description
          }
        }
        ... on MultipleSelectionQuestion {
          choices {
            id
            description
          }
        }
      }
    }
  }
`;

export default GET_STUDENT_EXAM_QUESTIONS_QUERY;

import { gql } from '@apollo/client';

const GET_STUDENT_EXAMS_QUERY = gql`
  query StudentExams($studentId: String!) {
    studentExams(studentId: $studentId) {
      id
      score
      submittedAt
      passed
      examDetails {
        id
        description
        title
      }
    }
  }
`;

export default GET_STUDENT_EXAMS_QUERY;

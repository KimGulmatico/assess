import { gql } from '@apollo/client';

export const GET_STUDENT_MC_QUESTION_QUERY = gql`
  query GetStudentMultipleChoiceQuestion($id: String!) {
    multipleChoiceQuestion(id: $id) {
      id
      question
      options {
        id
        description
      }
    }
  }
`;

export default GET_STUDENT_MC_QUESTION_QUERY;

import { gql } from '@apollo/client';

export const GET_STUDENT_MS_QUESTION_QUERY = gql`
  query GetStudentMultipleSelectionQuestion($id: String!) {
    multipleSelectionQuestion(id: $id) {
      id
      question
      choices {
        id
        description
      }
    }
  }
`;

export default GET_STUDENT_MS_QUESTION_QUERY;

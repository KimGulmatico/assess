import { gql } from '@apollo/client';

const CREATE_CHECKOUT_SESSION_QUERY = gql`
  query CreateCheckoutSessionQuery {
    createCheckoutSession
  }
`;

export default CREATE_CHECKOUT_SESSION_QUERY;

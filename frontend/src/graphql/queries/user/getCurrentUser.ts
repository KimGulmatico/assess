import { gql } from '@apollo/client';

export const GET_CURRENT_USER_QUERY = gql`
  query GetCurrentUserProfile {
    currentUser {
      id
      email
      firstName
      lastName
      roles
    }
  }
`;

import { gql } from '@apollo/client';

const GET_ALL_USERS_QUERY = gql`
  query AllUsers($first: Int, $last: Int, $after: String, $before: String) {
    usersConnection(
      first: $first
      last: $last
      after: $after
      before: $before
    ) {
      nodes {
        email
        firstName
        id
        lastName
      }
      pageInfo {
        endCursor
        hasNextPage
        startCursor
        hasPreviousPage
      }
    }
  }
`;

export default GET_ALL_USERS_QUERY;

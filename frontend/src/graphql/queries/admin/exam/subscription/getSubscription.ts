import { gql } from '@apollo/client';

const SUBSCRIPTION_QUERY = gql`
  query Subscription($examId: String!) {
    subscription(examId: $examId) {
      examId
    }
  }
`;

export default SUBSCRIPTION_QUERY;

import { gql } from '@apollo/client';

const GET_STUDENT_EXAM_QUERY = gql`
  query GetStudentExam($studentId: String!, $examId: String!) {
    studentExam(studentId: $studentId, examId: $examId) {
      id
      student {
        id
        firstName
        lastName
        email
      }
      examDetails {
        id
        title
        maxQuestionCount
        duration
      }
      examSummary {
        question {
          id
          questionType
          question
        }
        studentAnswer {
          ... on MultipleChoiceAnswer {
            questionId
            option
          }
          ... on MultipleSelectionAnswer {
            questionId
            choices
          }
        }
        points
        output {
          ... on MultipleChoiceQuestionOutput {
            option {
              id
              description
              isCorrect
            }
            selected
          }
          ... on MultipleSelectionQuestionOutput {
            choice {
              id
              description
              isCorrect
            }
            selected
          }
        }
      }
      score
      submittedAt
      openedAt
    }
  }
`;

export default GET_STUDENT_EXAM_QUERY;

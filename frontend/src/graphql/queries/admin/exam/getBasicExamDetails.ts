import { gql } from '@apollo/client';

const GET_BASIC_EXAM_DETAILS_QUERY = gql`
  query GetBasicExamDetails($examId: String!) {
    exam(id: $examId) {
      id
      title
      description
      duration
    }
  }
`;

export default GET_BASIC_EXAM_DETAILS_QUERY;

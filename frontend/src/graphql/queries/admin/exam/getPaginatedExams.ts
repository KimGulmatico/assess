import { gql } from '@apollo/client';

const GET_ALL_EXAMS_QUERY = gql`
  query AllExams($first: Int, $last: Int, $after: String, $before: String) {
    examsConnection(
      first: $first
      last: $last
      after: $after
      before: $before
    ) {
      nodes {
        id
        title
        description
        program
        module
        year
        quarter
        isPublished
        createdAt
        updatedAt
        createdById
        studentExams {
          id
          score
          student {
            id
            firstName
            lastName
            email
          }
          submittedAt
          passed
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

export default GET_ALL_EXAMS_QUERY;

import { gql } from '@apollo/client';

const GET_EXAM_QUERY = gql`
  query Exam($examId: String!) {
    exam(id: $examId) {
      id
      title
      description
      program
      module
      year
      quarter
      isPublished
      maxQuestionCount
      createdAt
      updatedAt
      createdById
      startDate
      endDate
      duration
      examType
      questions {
        id
        question
        updatedAt
        createdById
      }
    }
  }
`;

export default GET_EXAM_QUERY;

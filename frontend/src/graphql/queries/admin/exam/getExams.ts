import { gql } from '@apollo/client';

const GET_EXAMS_QUERY = gql`
  query GetExams {
    exams {
      id
      title
      description
      program
      module
      year
      quarter
      isPublished
      createdAt
      updatedAt
      createdById
    }
  }
`;

export default GET_EXAMS_QUERY;

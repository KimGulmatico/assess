import { gql } from '@apollo/client';

export const SEARCH_QUESTIONS_QUERY = gql`
  query SearchQuestions(
    $first: Int
    $after: String
    $last: Int
    $before: String
    $keywords: String
    $filters: String
  ) {
    searchQuestions(
      first: $first
      after: $after
      last: $last
      before: $before
      keywords: $keywords
      filters: $filters
    ) {
      nodes {
        id
        question
        program
        module
        updatedAt
        createdById
      }
      pageInfo {
        hasPreviousPage
        hasNextPage
        endCursor
        startCursor
      }
    }
  }
`;

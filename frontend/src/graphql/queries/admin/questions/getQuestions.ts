import { gql } from '@apollo/client';

export const GET_QUESTIONS_QUERY = gql`
  query Questions($program: Program, $module: String) {
    questions(program: $program, module: $module) {
      id
      question
      program
      module
      updatedAt
      createdById
    }
  }
`;

import { gql } from '@apollo/client';

export const GET_UNPAGINATED_QUESTIONS_QUERY = gql`
  query GetAllUnpaginatedQuestions {
    questions {
      id
      question
      program
      module
      updatedAt
      createdById
    }
  }
`;

import { gql } from '@apollo/client';

export const GET_ALL_QUESTIONS_QUERY = gql`
  query AllQuestions($first: Int, $after: String, $last: Int, $before: String) {
    questionsConnection(
      first: $first
      after: $after
      last: $last
      before: $before
    ) {
      nodes {
        id
        question
        createdById
        updatedAt
        module
        program
      }
      pageInfo {
        hasPreviousPage
        hasNextPage
        endCursor
        startCursor
      }
    }
  }
`;

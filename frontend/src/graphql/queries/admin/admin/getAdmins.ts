import { gql } from '@apollo/client';

export const GET_ADMINS_QUERY = gql`
  query Admins {
    admins {
      id
      email
    }
  }
`;

export default GET_ADMINS_QUERY;

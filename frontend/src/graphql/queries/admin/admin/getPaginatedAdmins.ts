import { gql } from '@apollo/client';

const GET_ALL_ADMINS_QUERY = gql`
  query AllAdmins($first: Int, $last: Int, $after: String, $before: String) {
    adminsConnection(
      first: $first
      last: $last
      after: $after
      before: $before
    ) {
      nodes {
        email
        id
      }
      pageInfo {
        endCursor
        hasNextPage
        hasPreviousPage
        startCursor
      }
    }
  }
`;

export default GET_ALL_ADMINS_QUERY;

import { gql } from '@apollo/client';

export const GET_ADMIN_QUERY = gql`
  query Admin($adminId: String!) {
    admin(id: $adminId) {
      id
      email
    }
  }
`;

export default GET_ADMIN_QUERY;

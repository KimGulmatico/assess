import React from 'react';
import { Stack } from '@mui/material';
import { ResourcesPageContainer } from '../../styles/styledComponents/ResourcesPageStyles';
import ResourceSection from '../../components/resources/ResourceSection';
import ResourceHeader from '../../components/resources/ResourceHeader';
import data from './resourcesPagesTexts.json';

const HarassmentPolicyPage = () => {
  const introduction = data.texts[0].harassmentPolicy?.introduction as string[];
  const basisAndApplication = data.texts[0].harassmentPolicy
    ?.basisAndApplication as string[];
  const unlawfulDiscrimination = data.texts[0].harassmentPolicy
    ?.unlawfulDiscrimination as string[];
  const sexualMisconduct = data.texts[0].harassmentPolicy
    ?.sexualMisconduct as string[];
  const understandingConsent = data.texts[0].harassmentPolicy
    ?.understandingConsent as string[];
  const preventionAndEducationProgram = data.texts[0].harassmentPolicy
    ?.preventionAndEducationProgram as string[];

  return (
    <ResourcesPageContainer>
      <Stack direction="column" spacing={4}>
        <ResourceHeader title="Harassment Policy" />
        <ResourceSection title="Introduction" contents={introduction} />
        <ResourceSection
          title="Basis and Application"
          contents={basisAndApplication}
        />
        <ResourceSection
          title="Unlawful Discrimination and Harassment"
          contents={unlawfulDiscrimination}
        />
        <ResourceSection
          title="Sexual Misconduct and Definitions of Prohibited Behavior"
          contents={sexualMisconduct}
        />
        <ResourceSection
          title="Understanding Consent"
          contents={understandingConsent}
        />
        <ResourceSection
          title="Prevention and Education Programs"
          contents={preventionAndEducationProgram}
        />
      </Stack>
    </ResourcesPageContainer>
  );
};

export default HarassmentPolicyPage;

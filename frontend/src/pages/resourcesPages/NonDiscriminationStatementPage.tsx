import React from 'react';
import { ResourcesPageContainer } from '../../styles/styledComponents/ResourcesPageStyles';
import ResourceHeader from '../../components/resources/ResourceHeader';
import ResourceSection from '../../components/resources/ResourceSection';
import data from './resourcesPagesTexts.json';

const NonDiscriminationStatementPage = () => {
  const statement = data.texts[2].nonDiscriminationStatement as string[];

  return (
    <ResourcesPageContainer>
      <ResourceHeader title="Non-Discrimination Statement" />
      <ResourceSection title="" contents={statement} />
    </ResourcesPageContainer>
  );
};

export default NonDiscriminationStatementPage;

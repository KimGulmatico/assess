import React from 'react';
import { ResourcesPageContainer } from '../../styles/styledComponents/ResourcesPageStyles';
import ResourceHeader from '../../components/resources/ResourceHeader';
import ResourceSection from '../../components/resources/ResourceSection';
import data from './resourcesPagesTexts.json';

const HonourCodePage = () => {
  const honourCode = data.texts[1].honourCode as string[];

  return (
    <ResourcesPageContainer>
      <ResourceHeader title="The Kingsland University Honour Code" />
      <ResourceSection title="" contents={honourCode} />
    </ResourcesPageContainer>
  );
};

export default HonourCodePage;

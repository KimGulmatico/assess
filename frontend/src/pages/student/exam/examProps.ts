import { Option } from '../../../components/student/QuestionComponent/MCQuestion/mcQuestionComponentProps';
import {
  InputMaybe,
  MultipleChoiceQuestion,
  MultipleSelectionQuestion,
} from '../../../types.generated';

export interface ExamAnswerableQuestionInterface {
  __typename?: string;
  id: string;
  question: string;
  program: string;
  module: string;
  createdAt: number;
  updatedAt: number;
  createdById: string;
  createdBy: {
    id: string;
    email: string;
    firstName: string;
    lastName: string;
  };
  questionType: 'MULTIPLE_CHOICE_QUESTION' | 'MULTIPLE_SELECTION_QUESTION';
  activityId?: string;
  options?: Option[];
  choices?: Option[];
}

export type AllQuestions = MultipleChoiceQuestion | MultipleSelectionQuestion;

export interface Exam {
  __typename?: string;
  id: string;
  title: string;
  description: string;
  program: string;
  module: string;
  year: number;
  quarter: string;
  maxQuestionCount: number;
  createdAt: number | null | InputMaybe<string>;
  updatedAt: number | null | InputMaybe<string>;
  createdById: string;
  startDate: number | null | InputMaybe<string>;
  endDate: number | null | InputMaybe<string>;
  duration: number | null;
  examType: string;
  passingRate: number | null;
}

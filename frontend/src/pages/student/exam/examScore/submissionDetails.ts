import { Dispatch, SetStateAction } from 'react';
import { Exam } from '../../../../types.generated';

export interface ExamDetailsCustomState {
  examDetails: Exam;
  score: number;
  examStartTimestamp: Date;
  examEndTimestamp: Date;
  instanceId: string;
  passed: boolean;
}

export interface SubmissionDetailsProps {
  typeOfExam: string;
  examStartTimestamp: Date;
  examEndTimestamp: Date;
  score: number;
  titleGridSize: number;
  detailGridSize: number;
}

export interface ScorePercentageChartProps {
  scorePercentage: number;
  endAngle: number;
}

import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Box, Button, Stack, Typography } from '@mui/material';

const SampleViewExamPage: React.FC = () => {
  const params = useParams();
  const navigate = useNavigate();

  return (
    <Stack
      direction="column"
      spacing={2}
      sx={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 30,
      }}
    >
      <Typography>Exam Details</Typography>
      <Typography>ID: {params.id}</Typography>
      <Box height={10} />
      <Button
        variant="contained"
        onClick={() => navigate(`/exam/edit/${String(params.id)}`)}
      >
        Edit
      </Button>
    </Stack>
  );
};

export default SampleViewExamPage;

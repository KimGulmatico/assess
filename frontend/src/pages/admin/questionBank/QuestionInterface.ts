export interface QuestionAnswerInterface {
  id: string | null;
  description: string | null;
  isCorrect: boolean | null;
}

export interface UserInterface {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
}

type OriginalQuestionType =
  | 'MULTIPLE_CHOICE_QUESTION'
  | 'MULTIPLE_SELECTION_QUESTION';

export type QuestionType =
  | 'MultipleChoiceQuestion'
  | 'MultipleSelectionQuestion';

export interface QuestionFilterInterface {
  questionType?: OriginalQuestionType;
  program?: string;
  module?: string;
}

export interface QuestionInterface {
  __typename: QuestionType;
  id: string;
  question: string;
  program: string;
  module: string;
  createdAt: Date;
  updatedAt: Date;
  createdById: string;
}

import {
  ExamType,
  MultipleChoiceQuestion,
  MultipleSelectionQuestion,
} from '../../../types.generated';

export type AllQuestions = MultipleChoiceQuestion | MultipleSelectionQuestion;

export interface ExamDetails {
  id: string;
  title: string;
  description: string;
  program: string;
  module: string;
  year: number;
  quarter: string;
  isPublished: boolean;
  createdAt: Date;
  updatedAt: Date;
  createdById: string;
  startDate: Date | number | null;
  endDate: Date | number | null;
  duration: number | null;
  questions: AllQuestions[];
  examType: ExamType.Fixed | ExamType.Free | ExamType.Flexible;
}

export interface EditExamModalProps {
  exam: ExamDetails;
}

export interface EditExamPageProps {
  questions: AllQuestions[];
  exam: ExamDetails;
}

export interface ExamQuestionInterface {
  id: string;
  __typename: 'MultipleChoiceQuestion' | 'MultipleSelectionQuestion';
}

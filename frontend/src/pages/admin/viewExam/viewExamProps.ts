import { ExamDetailsProps } from '../../../components/admin/viewExam/viewExamComponentsProps';
import { ExamQuestionInterface } from './examQuestionInterface';

export interface ViewExamProps {
  id: string;
  title: string;
  year: number;
  quarter: string;
  program: string;
  module: string;
  description: string;
  examDetails: ExamDetailsProps;
  questions: ExamQuestionInterface[];
  published: boolean;
}

export interface PropState {
  prevPath: string;
}

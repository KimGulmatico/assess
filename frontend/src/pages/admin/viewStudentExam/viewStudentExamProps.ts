import { GetStudentExamQuery } from '../../../graphql/queries/admin/exam/getStudentExam.generated';

export interface ViewStudentExamProps {
  exam: GetStudentExamQuery | undefined;
}

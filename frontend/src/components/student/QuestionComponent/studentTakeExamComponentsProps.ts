import { Option } from './MCQuestion/mcQuestionComponentProps';
import { Choice } from './MSQuestion/msQuestionComponentProps';
import { AnswerSheetInterface } from '../../../utils/generateAnswerSheet';

export interface ExamDetailsProps {
  startDate?: number | null;
  endDate?: number | null;
  duration?: number | null;
  numOfItems: number;
  typeOfExam: string;
  titleGridSize: number;
  detailGridSize: number;
}

export interface ExamDescriptionProps {
  content: string;
}

export interface QuestionComponentProps {
  questionContent: string;
  details: JSX.Element;
}

export interface QuestionComponentDetailsProps {
  id: string;
  question: string;
  options?: Option[];
  choices?: Choice[];
  type: 'MULTIPLE_CHOICE_QUESTION' | 'MULTIPLE_SELECTION_QUESTION';
  handleMCAnswerChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleMSAnswerChange?: (
    event: React.ChangeEvent<HTMLInputElement>,
    checked: boolean
  ) => void;
  existingAnswer?: AnswerSheetInterface;
}

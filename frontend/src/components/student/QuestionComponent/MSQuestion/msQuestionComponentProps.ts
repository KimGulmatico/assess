import { AnswerSheetInterface } from '../../../../utils/generateAnswerSheet';
export interface Choice {
  id: string;
  description: string;
}

export interface MSQuestionComponentProps {
  question: string;
  choices: Choice[];
  existingAnswer?: AnswerSheetInterface;
  handleMSAnswerChange?: (
    event: React.ChangeEvent<HTMLInputElement>,
    checked: boolean
  ) => void;
}

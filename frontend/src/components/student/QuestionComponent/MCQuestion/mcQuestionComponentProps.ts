import { AnswerSheetInterface } from '../../../../utils/generateAnswerSheet';

export interface Option {
  id: string;
  description: string;
}

export interface MCQuestionComponentProps {
  id: string;
  options: Option[];
  question: string;
  existingAnswer?: AnswerSheetInterface;
  handleMCAnswerChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

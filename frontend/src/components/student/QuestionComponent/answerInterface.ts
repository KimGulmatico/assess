export interface StudentAnswerInterface {
  id: string | null;
  description: string | null;
}

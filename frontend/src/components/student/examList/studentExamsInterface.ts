export interface StudentExamDetails {
  id: string;
  title: string;
  description: string;
  module: string;
  maxQuestionCount: number;
  endDate: Date;
  passingRaate: number;
}

export interface StudentExamsListInterface {
  id: string;
  examDetails: StudentExamDetails;
  score: number;
  submittedAt: Date;
  passed: boolean;
}

export interface StudentExamsListRowInterface {
  exam: StudentExamsListInterface;
}

export interface CustomDialogProps {
  isOpen: boolean;
  handleClose: React.MouseEventHandler<HTMLButtonElement>;
  handleConfirm: React.MouseEventHandler<HTMLButtonElement>;
  content: string;
  title: string;
}

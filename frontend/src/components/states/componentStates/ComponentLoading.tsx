import React from 'react';
import LoaderImage from '../../../styles/styledComponents/StatesStyles';
import Loader from '../../assets/merkle_trees_loader.gif';

function ComponentLoading() {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
      }}
    >
      <div
        style={{
          height: '300px',
          width: '300px',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <LoaderImage src={Loader} alt="loader" />
      </div>
    </div>
  );
}

export default ComponentLoading;

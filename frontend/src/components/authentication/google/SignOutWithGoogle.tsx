import React from 'react';
import { Button } from '@mui/material';
import { useGoogleLogout } from 'react-google-login';

const SignOutWithGoogle = () => {
  const clientId = process.env.REACT_APP_GOOGLE_CLIENT_ID || '';
  const onLogoutSuccess = () => {
    alert('Logged out successfully');
  };

  const onFailure = () => {
    throw new Error('Failure signing out');
  };

  const { signOut } = useGoogleLogout({
    clientId,
    onLogoutSuccess,
    onFailure,
  });

  return (
    <Button
      onClick={signOut}
      disableElevation
      variant="contained"
      sx={{
        '&.MuiButton-contained': {
          backgroundColor: '#006CFF',
          fontSize: '18px',
          textTransform: 'none',
          fontWeight: 400,
        },
        borderRadius: '10px',
        paddingTop: '6px',
        paddingBottom: '6px',
        maxWidth: '459px',
      }}
    >
      Sign out
    </Button>
  );
};
export default SignOutWithGoogle;

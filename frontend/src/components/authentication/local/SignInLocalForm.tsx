import React from 'react';
import { useState } from 'react';
import * as Yup from 'yup';
import { useFormik, Form, FormikProvider } from 'formik';
import {
  Stack,
  Link,
  Typography,
  Box,
  IconButton,
  InputAdornment,
} from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import { TextFieldWrapper } from '../../../styles/styledComponents/SignInPageStyles';

const SignInLocalForm = () => {
  const [showPassword, setShowPassword] = useState(false);
  const [loginMessage, setLoginMessage] = useState('');

  const SignInSchema = Yup.object().shape({
    email: Yup.string()
      .email('Email must be a valid email address')
      .required('Email is required'),
    password: Yup.string().required('Password is required'),
  });

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      remember: true,
    },
    validationSchema: SignInSchema,
    onSubmit: (values) => {
      alert(`${values.email} ${values.password}`);
    },
  });

  const { errors, touched, values, isSubmitting, handleSubmit, getFieldProps } =
    formik;

  const handleShowPassword = () => {
    setShowPassword((show) => !show);
  };

  return (
    <div>
      {loginMessage !== '' ? (
        <span>
          <Typography style={{ color: 'red' }}>{loginMessage}</Typography>
          <br />
        </span>
      ) : (
        <span />
      )}

      <FormikProvider value={formik}>
        <Form
          autoComplete="off"
          noValidate
          onSubmit={handleSubmit}
          data-testid="signin-local-form"
        >
          <Stack spacing={3}>
            <TextFieldWrapper
              autoComplete="username"
              type="email"
              label="Email address"
              {...getFieldProps('email')}
              error={Boolean(touched.email && errors.email)}
              helperText={touched.email && errors.email}
            />

            <TextFieldWrapper
              autoComplete="current-password"
              type={showPassword ? 'text' : 'password'}
              label="Password"
              {...getFieldProps('password')}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={handleShowPassword} edge="end">
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              error={Boolean(touched.password && errors.password)}
              helperText={touched.password && errors.password}
            />
          </Stack>

          <Box height={45} />

          <Stack
            direction="row"
            sx={{ display: 'flex', justifyContent: 'space-between' }}
          >
            <Link href="#" underline="none">
              <Typography sx={{ fontSize: '16px', color: '#539536' }}>
                Forgot Password?
              </Typography>
            </Link>
            <LoadingButton
              disableElevation
              type="submit"
              variant="contained"
              loading={isSubmitting}
              sx={{
                '&.MuiButton-contained': {
                  backgroundColor: '#374EA2',
                  fontSize: '16px',
                  textTransform: 'none',
                  fontWeight: 400,
                },
                borderRadius: '10px',
                width: '135px',
              }}
            >
              Sign in
            </LoadingButton>
          </Stack>
        </Form>
      </FormikProvider>
    </div>
  );
};

export default SignInLocalForm;

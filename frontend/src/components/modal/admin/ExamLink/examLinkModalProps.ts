export interface ExamLinkModalProps {
  isOpen: boolean;
  handleClose: React.MouseEventHandler<HTMLButtonElement>;
  examLink: string;
}

export interface MCQOptionInterface {
  id: string;
  description: string;
  isCorrect: boolean;
}

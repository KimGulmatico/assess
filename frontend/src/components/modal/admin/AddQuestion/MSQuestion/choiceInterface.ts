export interface MSQChoiceInterface {
  id: string;
  description: string;
  isCorrect: boolean;
}

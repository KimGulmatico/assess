import { Program } from '../../../../types.generated';

export interface QuestionType {
  id: number;
  value: string;
  element: JSX.Element;
}

export interface QuestionTagsProps {
  program:
    | Program.Cybersecurity
    | Program.FullStack
    | Program.TechSales
    | Program.Blockchain;
  module: string;
}

import { ExamType } from '../../../../types.generated';

export interface PublishDialogProps {
  examId: string;
  examType: ExamType.Fixed | ExamType.Free | ExamType.Flexible;
  questionsLength?: number;
  duration: number | null;
  startDate: number | null;
  endDate: number | null;
}

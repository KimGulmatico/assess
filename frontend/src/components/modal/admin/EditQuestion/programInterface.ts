export interface ProgramInterface {
  id: number;
  title: string;
  value: string;
}

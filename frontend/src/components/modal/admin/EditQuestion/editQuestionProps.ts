import { QuestionAnswerInterface } from '../../../../pages/admin/questionBank/QuestionInterface';
import { Program } from '../../../../types.generated';
import { ProgramInterface } from './programInterface';

export interface EditAnswerTextFieldProps {
  textValue: string;
  onChange: React.ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>;
  removeFunction: React.MouseEventHandler<HTMLButtonElement>;
}

export interface NewAnswerTextFieldProps {
  optionChange: React.ChangeEventHandler<
    HTMLTextAreaElement | HTMLInputElement
  >;
  optionValue: string;
  handleClick: React.MouseEventHandler<HTMLButtonElement>;
}

export interface EditTextFieldProps {
  textValue: string;
  onChange: React.ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>;
}

export interface EditProgramProps {
  program: string;
  programs: ProgramInterface[];
  onChange: React.ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>;
}

export interface EditDialogProps {
  program: Program;
  module: string;
  questionId: string;
  questionType: 'MultipleChoiceQuestion' | 'MultipleSelectionQuestion';
  question: string;
  possibleAnswers: QuestionAnswerInterface[];
}

export interface EditCodeDialogProps {
  program: Program;
  module: string;
  questionId: string;
  questionType: 'MultipleChoiceQuestion' | 'MultipleSelectionQuestion';
  question: string;
  activityId: string;
}

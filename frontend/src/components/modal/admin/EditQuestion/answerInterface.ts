export interface AnswerInterface {
  description: string;
  isCorrect: boolean;
}

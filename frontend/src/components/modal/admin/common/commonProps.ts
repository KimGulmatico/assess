export interface QuestionTypeHeaderProps {
  questionType: 'MultipleChoiceQuestion' | 'MultipleSelectionQuestion';
}

export interface CustomDialogTitleProps {
  text: string;
  handleClose: React.MouseEventHandler<HTMLButtonElement>;
}

export interface QModalCloseProps {
  handleClose: React.MouseEventHandler<HTMLButtonElement>;
}

export interface QButtonProps {
  handleClick: React.MouseEventHandler<HTMLButtonElement> | undefined;
  isMatch: boolean;
  label: string;
}

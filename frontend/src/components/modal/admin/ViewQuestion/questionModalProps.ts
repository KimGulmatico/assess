import { GetMultipleChoiceQuestionQuery } from '../../../../graphql/queries/admin/multipleChoice/getMultipleChoiceQuestion.generated';
import { GetMultipleSelectionQuestionQuery } from '../../../../graphql/queries/admin/multipleSelection/getMultipleSelectionQuestion.generated';
import { GetUserQuery } from '../../../../graphql/queries/user/user.generated';
import { QuestionAnswerInterface } from '../../../../pages/admin/questionBank/QuestionInterface';
import { Program } from '../../../../types.generated';

export interface QDialogDataProps {
  questionId: string;
  authorId: string;
}

export interface QDialogProps {
  questionId: string;
  program: Program;
  module: string;
  questionType: 'MultipleChoiceQuestion' | 'MultipleSelectionQuestion';
  question: string;
  possibleAnswers: QuestionAnswerInterface[];
  author: string;
  createdAt: Date;
}

export interface MSDialogProps {
  data: GetMultipleSelectionQuestionQuery;
  author: GetUserQuery;
}

export interface MCDialogProps {
  data: GetMultipleChoiceQuestionQuery;
  author: GetUserQuery;
}

export interface AnswerTextFieldProps {
  textValue: string;
  testId: string;
  isCorrectAnswer: boolean;
}

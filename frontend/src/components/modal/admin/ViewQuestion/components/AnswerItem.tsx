import React from 'react';
import { Box, TextField, InputAdornment } from '@mui/material';
import CheckIcon from '@mui/icons-material/Check';
import ClearIcon from '@mui/icons-material/Clear';
import { AnswerTextFieldProps } from '../questionModalProps';

function AnswerItem({
  textValue,
  testId,
  isCorrectAnswer,
}: AnswerTextFieldProps) {
  return (
    <Box sx={{ px: '30px' }}>
      <TextField
        value={textValue}
        label={isCorrectAnswer ? 'Correct Answer' : 'Incorrect Answer'}
        fullWidth
        focused
        color={isCorrectAnswer ? 'success' : 'error'}
        InputProps={{
          endAdornment: (
            <InputAdornment position="start">
              {isCorrectAnswer ? (
                <CheckIcon color="success" />
              ) : (
                <ClearIcon color="error" />
              )}
            </InputAdornment>
          ),
          readOnly: true,
        }}
        variant="filled"
        margin="dense"
        inputProps={{
          style: { fontSize: 14 },
          'data-testid': testId,
        }}
      />
    </Box>
  );
}

export default AnswerItem;

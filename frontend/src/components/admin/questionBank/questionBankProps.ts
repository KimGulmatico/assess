import { QuestionInterface } from '../../../pages/admin/questionBank/QuestionInterface';
import { FilterInterface } from '../../../components/filters/filtersInterface';

export type QBHeaderProps = {
  data: QuestionInterface[];
  searchValue: string;
  changeFunction: React.ChangeEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  >;
  handleClearInput: React.MouseEventHandler<HTMLButtonElement> | undefined;
};

export type QuestionTableProps = {
  questions: QuestionInterface[];
  onNext: React.MouseEventHandler<HTMLButtonElement>;
  onPrev: React.MouseEventHandler<HTMLButtonElement>;
  nextDisabled: boolean;
  pageNum: number;
  searchValue: string;
  noMatchFound: boolean;
  isSearchLoading: boolean;
  onFilter: (filters: FilterInterface[]) => void;
};

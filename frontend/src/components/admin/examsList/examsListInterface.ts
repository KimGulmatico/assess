export interface StudentExam {
  __typename: string;
  score: number;
  student: Student;
  submittedAt: number;
  passed: boolean;
}

export interface Student {
  __typename: string;
  id: string;
  firstName: string;
  lastName: string;
  email: string;
}

export interface ExamsListInterface {
  id: string;
  title: string;
  description: string;
  program: string;
  module: string;
  year: number;
  quarter: string;
  isPublished: boolean;
  createdAt: Date;
  updatedAt: Date;
  createdById: string;
  studentExams: [StudentExam];
}

export interface ExamsListRowInterface {
  exam: ExamsListInterface;
}

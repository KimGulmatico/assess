import { ExamType } from '../../../types.generated';

export interface ExamHeaderProps {
  examId: string;
  examType: ExamType.Fixed | ExamType.Free | ExamType.Flexible;
  title: string;
  program: string;
  module: string;
  quarter: string;
  year: number;
  questionsLength: number;
  isPublished: boolean;
  duration: number | null;
  startDate: number | null;
  endDate: number | null;
  handleEdit: React.MouseEventHandler<HTMLButtonElement | HTMLAnchorElement>;
  handleDelete: React.MouseEventHandler<HTMLButtonElement | HTMLAnchorElement>;
}

export interface ExamDetailsProps {
  startDate?: number | null;
  endDate?: number | null;
  duration?: number | null;
  numOfItems: number;
  typeOfExam: ExamType.Fixed | ExamType.Free | ExamType.Flexible;
  titleGridSize: number;
  detailGridSize: number;
}

export interface ExamDescriptionProps {
  content: string;
}

export interface QuestionComponentDetailsProps {
  id?: string;
  questionContent?: string;
  details?: JSX.Element;
  studentAnswer?: string[] | string;
}

export interface QuestionComponentProps {
  id: string;
  type: 'MultipleChoiceQuestion' | 'MultipleSelectionQuestion';
  studentAnswer?: any; //replace after everything works
}

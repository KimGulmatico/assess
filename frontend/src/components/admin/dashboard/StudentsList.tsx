/* eslint-disable @typescript-eslint/no-unsafe-call */
import React, { useState, useEffect } from 'react';
import TableLoadingState from '../../states/componentStates/TableLoadingState';
import ErrorPage from '../../states/ErrorPage';
import { UserInterface } from '../../../pages/admin/questionBank/QuestionInterface';
import { useAllUsersQuery } from '../../../graphql/queries/user/getPaginatedUsers.generated';
import { TableBody } from '@mui/material';
import StudentsListRow from './StudentsListRow';
import EmptyDataComponent from '../../states/componentStates/EmptyDataComponent';
import TablePagination from '../../common/TablePagination';

const StudentsList = () => {
  const { data, loading, error, fetchMore } = useAllUsersQuery({
    fetchPolicy: 'network-only',
    nextFetchPolicy: 'cache-first',
    variables: {
      first: 10,
      last: null,
      after: null,
      before: null,
    },
    pollInterval: 2000,
  });
  const [page, setPage] = useState(1);

  const endCursor = data?.usersConnection?.pageInfo.endCursor;
  const startCursor = data?.usersConnection?.pageInfo.startCursor;
  const hasPrevPage = data?.usersConnection?.pageInfo.hasPreviousPage;
  const hasNextPage = data?.usersConnection?.pageInfo.hasNextPage;

  const moreResults = () => {
    fetchMore({
      variables: {
        first: 10,
        last: null,
        after: endCursor,
        before: null,
      },
    });
  };

  const prevResults = () => {
    fetchMore({
      variables: {
        first: null,
        last: 10,
        after: null,
        before: startCursor,
      },
    });
  };

  const [disableButton, setDisableButton] = useState<boolean>(!hasNextPage);

  useEffect(() => {
    if (hasNextPage) {
      setDisableButton(false);
    } else {
      setDisableButton(true);
    }
  }, [hasNextPage]);

  const handleClickNext = () => {
    if (hasNextPage) {
      moreResults();
    }
    setPage(page + 1);
  };

  const handleClickPrev = () => {
    if (hasPrevPage) {
      prevResults();
      setPage(page - 1);
      setDisableButton(false);
    }
  };

  useEffect(() => {
    if (!hasPrevPage && page !== 1) {
      setPage(1);
    }
  }, [data]);

  if (loading) return <></>;
  if (error) return <ErrorPage errorMessage={error.message} />;

  const students: UserInterface[] = data?.usersConnection
    ?.nodes as unknown as UserInterface[];

  return (
    <>
      {students.length > 0 ? (
        <TableBody sx={{ backgroundColor: '#ffffff' }}>
          {students.map((student: UserInterface) => (
            <StudentsListRow key={student.id} student={student} />
          ))}
        </TableBody>
      ) : (
        <EmptyDataComponent message="No students have been added" />
      )}
      <TablePagination
        pageNum={page}
        buttonDisabled={disableButton}
        handleNext={handleClickNext}
        handlePrev={handleClickPrev}
      />
    </>
  );
};

export default StudentsList;

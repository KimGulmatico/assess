/* eslint-disable @typescript-eslint/no-unsafe-argument */
import React, { useState } from 'react';
import {
  Collapse,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
  Stack,
  TableHead,
} from '@mui/material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';

import {
  RowContainer,
  ExamContainer,
  StyledTableCell,
} from '../../../styles/admin/DashboardStyles';
import { UserInterface } from '../../../pages/admin/questionBank/QuestionInterface';
import StudentExams from './StudentExams';

interface UsersListRowInterface {
  student: UserInterface;
}

const StudentsListRow = (props: UsersListRowInterface) => {
  const { student } = props;
  const [open, setOpen] = useState(false);
  const studentId = student.id;

  return (
    <>
      <TableRow>
        <StyledTableCell>
          <RowContainer>
            <ExamContainer>
              <Typography sx={{ fontSize: '14px', color: '#242526' }}>
                {student.firstName} {student.lastName}
              </Typography>
              <Typography
                sx={{ fontSize: '11px', color: '#808080', marginLeft: '8px' }}
              >
                {student.email}
              </Typography>
            </ExamContainer>
            <Stack
              direction="row"
              component="span"
              display="flex"
              justifyContent="space-between"
              alignItems="center"
              spacing={0.2}
            >
              <IconButton size="small" onClick={() => setOpen(!open)}>
                {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
              </IconButton>
            </Stack>
          </RowContainer>
        </StyledTableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ border: 0, padding: 0 }}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Table size="small" sx={{ width: '100%', maxHeight: '50vh' }}>
              <TableHead sx={{ background: 'rgb(33,30,71,0.08)' }}>
                <TableRow>
                  <TableCell>Title</TableCell>
                  <TableCell>Description</TableCell>
                  <TableCell align="center">Score</TableCell>
                  <TableCell align="center">Remarks</TableCell>
                  <TableCell>Submit Date</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {open && <StudentExams studentId={studentId} />}
              </TableBody>
            </Table>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
};

export default StudentsListRow;

/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import React from 'react';
import { Table, TableContainer, TableHead, Paper } from '@mui/material';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import ExamsList from './ExamsList';
import StudentsList from './StudentsList';
import { StyledTabs, StyledTab } from '../../../styles/admin/DashboardStyles';

const DashboardListTable = () => {
  const [tabIndex, setTabIndex] = React.useState(0);

  const handleChangeTab = (newValue: number) => {
    setTabIndex(newValue);
  };

  const onTabChange = (event: React.SyntheticEvent, newValue: number) => {
    setTabIndex(newValue);
  };

  return (
    <Paper sx={{ boxShadow: 'none' }}>
      <TableContainer sx={{ marginTop: 2 }}>
        <Table stickyHeader>
          <TableHead>
            <Box
              sx={{
                background: '#374EA2',
                height: '41px',
                paddingLeft: '12px',
                borderRadius: '4px 4px 0px 0px',
                display: 'flex',
                alignItems: 'start',
              }}
            >
              <StyledTabs
                TabIndicatorProps={{
                  sx: { background: 'transparent', height: '0px' },
                }}
                value={tabIndex}
                onChange={onTabChange}
                aria-label="basic tabs"
              >
                <StyledTab
                  disableRipple
                  label="Exams"
                  onClick={() => {
                    handleChangeTab(0);
                  }}
                />
                <StyledTab
                  disableRipple
                  label="Candidates"
                  onClick={() => {
                    handleChangeTab(1);
                  }}
                />
              </StyledTabs>
            </Box>
          </TableHead>
          {tabIndex === 0 ? <ExamsList /> : <></>}
          {tabIndex === 1 ? <StudentsList /> : <></>}
        </Table>
      </TableContainer>
    </Paper>
  );
};

export default DashboardListTable;

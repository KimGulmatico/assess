import React from 'react';
import ErrorPage from '../../states/ErrorPage';
import LoadingPage from '../../states/LoadingPage';

import { useStudentExamsQuery } from '../../../graphql/queries/student/exam/getStudentExams.generated';
import { TableCell, TableRow } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import Tag from '../../common/ChipTags';

interface StudentExamDetailsInterface {
  id: string;
  description: string;
  title: string;
}

export interface StudentExamsInterface {
  id: string;
  score: number;
  submittedAt: Date;
  examDetails: StudentExamDetailsInterface;
  passed: boolean;
}

interface StudentExamsProps {
  studentId: string;
}

const StudentExams = (props: StudentExamsProps) => {
  const navigate = useNavigate();
  const { studentId } = props;

  const { data, loading, error } = useStudentExamsQuery({
    variables: {
      studentId: studentId,
    },
    pollInterval: 2000,
  });

  if (loading) return null;
  if (error) return null;

  const exams: StudentExamsInterface[] =
    data?.studentExams as unknown as StudentExamsInterface[];

  return (
    <>
      {exams?.map((exam: StudentExamsInterface) => (
        <TableRow
          sx={{ background: 'rgb(33,30,71,0.03)' }}
          key={exam.id}
          onClick={() => {
            navigate(`/viewexam/${studentId}/${exam?.examDetails.id}`, {
              state: { prevPath: location.pathname },
            });
          }}
        >
          <TableCell>{exam.examDetails.title}</TableCell>
          <TableCell>{exam.examDetails.description}</TableCell>
          <TableCell align="center">{exam.score}</TableCell>
          <TableCell align="center">
            {exam.passed ? (
              <Tag label="Passed" bg="#BDE7BD" />
            ) : exam.submittedAt ? (
              <Tag label="Failed" bg="#FF9997" />
            ) : (
              <></>
            )}
          </TableCell>

          <TableCell>
            {exam.submittedAt
              ? new Date(exam.submittedAt).toLocaleString()
              : 'In Progress'}
          </TableCell>
        </TableRow>
      ))}
    </>
  );
};

export default StudentExams;

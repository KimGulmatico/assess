import React from 'react';
import { Popover, Button, Box } from '@mui/material';
import AttachEmailOutlinedIcon from '@mui/icons-material/AttachEmailOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';

const CandidateRowPopover = (props: {
  isOpen: boolean;
  anchorEl: HTMLButtonElement | null;
  handleClose: () => void;
}) => {
  return (
    <Popover
      open={props.isOpen}
      anchorEl={props.anchorEl}
      onClose={props.handleClose}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'left',
      }}
      sx={{ marginLeft: 2 }}
    >
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          background: '#fafafa',
          alignItems: 'start',
          justifyContent: 'end',
          gap: '8px',
          color: '#808080',
          padding: '8px',
        }}
      >
        <Button
          startIcon={<EditIcon />}
          sx={{
            textTransform: 'none',
            justifyContent: 'start',
            color: '#808080',
          }}
          onClick={() => {}}
          fullWidth
        >
          Edit
        </Button>
        <Button
          startIcon={<DeleteIcon />}
          sx={{
            textTransform: 'none',
            justifyContent: 'start',
            color: '#808080',
          }}
          onClick={() => {}}
          fullWidth
        >
          Delete
        </Button>
        <Button
          startIcon={<AttachEmailOutlinedIcon />}
          sx={{
            textTransform: 'none',
            justifyContent: 'start',
            color: '#808080',
          }}
          onClick={() => {}}
          fullWidth
        >
          Send Exam Link
        </Button>
      </Box>
    </Popover>
  );
};

export default CandidateRowPopover;

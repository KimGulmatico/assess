import React from 'react';
import {
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableBody,
  Typography,
  IconButton,
  Stack,
} from '@mui/material';
import { Add } from '@mui/icons-material';
import { StyledTableCell } from '../../../styles/admin/QuestionsPageStyles';

const fontStyle = { fontSize: '18px', color: '#9F9F9F' };

const EditExamQuestionBank = () => {
  return (
    <Paper sx={{ width: '100%', boxShadow: 'none' }}>
      <TableContainer>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              <StyledTableCell>Questions</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {/* map question here */}
            <TableRow>
              <StyledTableCell component="th" scope="row">
                <Stack direction="row" alignItems="center" spacing={1}>
                  <IconButton aria-label="add">
                    <Add sx={fontStyle} />
                  </IconButton>
                  <Typography sx={{ fontSize: '14px' }}>
                    This is a question.
                  </Typography>
                </Stack>
              </StyledTableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};

export default EditExamQuestionBank;

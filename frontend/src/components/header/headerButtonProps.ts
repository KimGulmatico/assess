import { MouseEventHandler } from 'react';

export type HeaderButtonProps = {
  isMainMenu?: boolean;
  label: string;
  handleClick: MouseEventHandler<HTMLButtonElement>;
  testId: string;
  redirectLink?: string;
};

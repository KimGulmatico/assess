import { GetUserQuery } from '../../../graphql/queries/user/user.generated';

export interface QuestionCardProps {
  questionType: string;
  content: JSX.Element;
  details: JSX.Element;
  showDetails: boolean;
  handleDelete: React.MouseEventHandler<HTMLButtonElement>;
}

export interface QuestionDisplayProps {
  questionId: string;
  questionType: string;
  authorId: string;
  showDetails: boolean;
  handleDelete: React.MouseEventHandler<HTMLButtonElement>;
}

export interface MCQDisplayProps {
  id: string;
  author: GetUserQuery;
  showDetails: boolean;
  handleDelete: React.MouseEventHandler<HTMLButtonElement>;
}

export interface MSQDisplayProps {
  id: string;
  author: GetUserQuery;
  showDetails: boolean;
  handleDelete: React.MouseEventHandler<HTMLButtonElement>;
}

export interface AnswerDisplayProps {
  questionType: 'MultipleChoiceQuestion' | 'MultipleSelectionQuestion';
  textValue: string;
  testId: string;
  isCorrectAnswer: boolean;
  optionId: string;
  studentAnswer?: string[] | string;
}

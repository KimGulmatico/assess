/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import React from 'react';
import { Box, Stack, Typography } from '@mui/material';
import QuestionCard from '../QuestionCard';
import { MSQDisplayProps } from '../displayProps';
import { UserInterface } from '../../../../pages/admin/questionBank/QuestionInterface';
import AnswerItemDisplay from '../components/AnswerItemDisplay';
import { useGetMultipleSelectionQuestionQuery } from '../../../../graphql/queries/admin/multipleSelection/getMultipleSelectionQuestion.generated';

function MSQDisplay({
  id,
  author,
  showDetails,
  handleDelete,
}: MSQDisplayProps) {
  const { data, loading, error } = useGetMultipleSelectionQuestionQuery({
    variables: {
      id: id,
    },
    pollInterval: 2000,
  });

  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error...</div>;

  const questionType =
    data?.multipleSelectionQuestion?.__typename ?? 'MultipleSelectionQuestion';
  const question = data?.multipleSelectionQuestion?.question ?? '';
  const choices = data?.multipleSelectionQuestion?.choices ?? [];

  const user: UserInterface = author?.user as UserInterface;
  const firstName: string = user.firstName;
  const lastName: string = user.lastName;
  const createdBy = `${firstName} ${lastName}`;

  return (
    <div>
      <QuestionCard
        questionType={questionType}
        content={
          <Typography component="pre" variant="body2" color="black">
            {question}
          </Typography>
        }
        details={
          <Stack direction="column" paddingLeft={3.6} paddingRight={3.6}>
            {choices?.map((choice, index) => (
              <AnswerItemDisplay
                key={choice?.id as string}
                optionId={choice?.id as string}
                questionType={questionType}
                textValue={choice?.description ?? ''}
                testId={`possible-answer-${index + 1}`}
                isCorrectAnswer={choice?.isCorrect as boolean}
              />
            ))}
            <Box height={10} />
            <Typography
              data-testid="created-by"
              sx={{
                fontSize: 12,
                color: '#9F9F9F',
                display: 'flex',
                alignItems: 'flex-end',
                justifyContent: 'flex-end',
              }}
            >
              Created by {createdBy}
            </Typography>
          </Stack>
        }
        showDetails={showDetails}
        handleDelete={handleDelete}
      />
    </div>
  );
}

export default MSQDisplay;

export interface FilterInterface {
  type: string;
  value: string;
}

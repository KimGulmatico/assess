export interface FilterSelectOption {
  label: string;
  value: string;
}

export interface FilterSliderComponent {
  label: string;
  filterFor: string;
  componentType: string;
  options?: FilterSelectOption[];
  onChange: (e: string) => void;
}

export interface FilterSliderProps {
  isOpen: boolean;
  handleClose: () => void;
  filterSchema: FilterSliderComponent[];
  onSubmit: () => void;
}

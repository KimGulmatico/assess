import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { List, ListItemButton, ListItemText, Typography } from '@mui/material';
import { useGoogleLogout } from 'react-google-login';
import DashboardIcon from '@mui/icons-material/Dashboard';
import ExamBankIcon from '@mui/icons-material/QuestionAnswer';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import {
  AccountContainer,
  LogoContainer,
  ListItemIconContainer,
  Drawer,
  Logo,
} from '../../../styles/styledComponents/SideNavigationStyles';
import sideNavigationLogo from '../../../assets/merkle_trees_reversed.png';
import { useMutation } from '@apollo/client';
import SIGN_OUT_MUTATION from '../../../graphql/mutations/auth/signOut';
import { removeUser } from '../../../routes/userInfo/user';
import AccountPopover from '../common/AccountPopover';
import LoadingPage from '../../states/LoadingPage';

interface MenuInterface {
  index: number;
  text: string;
  icon: React.Component;
  onClick: string;
}

const textStyle = { color: '#ffffff', fontSize: '14px' };
const buttonStyle = { color: '#ffffff', fontSize: '16px' };

const menus: MenuInterface[] = [
  /**
   * Hide menus as of the moment, no interactions available yet.
   */
  // {
  //   index: 1,
  //   text: 'Dashboard',
  //   icon: <DashboardIcon sx={buttonStyle} />,
  //   onClick: '/studentdashboard',
  // },
  // {
  //   index: 2,
  //   text: 'Exams',
  //   icon: <ExamBankIcon sx={buttonStyle} />,
  //   onClick: '/exams', // student exams page not yet added
  // },
];

const StudentSideNavigation = (props: { name: string }) => {
  const [anchorEl, setAnchorEl] = useState<HTMLDivElement | null>(null);
  const DASHBOARD_MENU_INDEX = 1;
  const [selectedMenu, setSelectedMenu] =
    useState<number>(DASHBOARD_MENU_INDEX);
  const open = Boolean(anchorEl);
  const navigate = useNavigate();

  const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleMenuClick = (index: number) => {
    setSelectedMenu(index);
  };

  const clientId = process.env.REACT_APP_GOOGLE_CLIENT_ID || '';

  const [signOutUser] = useMutation(SIGN_OUT_MUTATION);

  const [loading, setLoading] = useState(false);

  const onLogoutSuccess = () => {
    setLoading(true);
    signOutUser();
    removeUser();
  };

  const onFailure = () => {
    throw new Error('Failure signing out');
  };

  const { signOut } = useGoogleLogout({
    clientId,
    onLogoutSuccess,
    onFailure,
  });

  const handleSignOut = () => {
    signOut();
    setTimeout(() => navigate('/'), 2000);
  };

  if (loading) return <LoadingPage />;

  return (
    <Drawer variant="permanent" anchor="left">
      <LogoContainer>
        <Logo src={sideNavigationLogo} alt="assess-logo" />
      </LogoContainer>
      <List>
        {menus.map((menu) => (
          <ListItemButton
            key={menu.index}
            data-testid={menu.text}
            onClick={() => {
              navigate(`${menu.onClick}`);
              handleMenuClick(menu.index);
            }}
            selected={selectedMenu === menu.index}
          >
            <ListItemIconContainer>{menu.icon}</ListItemIconContainer>
            <ListItemText
              primary={menu.text}
              primaryTypographyProps={textStyle}
            />
          </ListItemButton>
        ))}
      </List>
      <AccountContainer>
        <ListItemButton
          data-testid="Account"
          onClick={(event) => {
            handleClick(event);
          }}
        >
          <ListItemIconContainer>
            <AccountCircleIcon sx={buttonStyle} />
          </ListItemIconContainer>
          <ListItemText primary="Account" primaryTypographyProps={textStyle} />
        </ListItemButton>
        <AccountPopover
          userName={props.name}
          userType="Student"
          isOpen={open}
          anchorEl={anchorEl}
          handleClose={handleClose}
          handleSignout={handleSignOut}
        />
      </AccountContainer>
    </Drawer>
  );
};

export default StudentSideNavigation;

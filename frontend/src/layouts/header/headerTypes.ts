export type ResourceType = { label: string; path: string };
export type ScrollType = { children: React.ReactElement };

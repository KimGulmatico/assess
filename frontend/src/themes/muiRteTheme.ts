import theme from './theme';

const muiRteTheme = Object.assign(theme, {
  overrides: {
    MUIRichTextEditor: {
      root: {
        position: 'relative',
        width: '100%',
        height: '50%',
        border: '1px solid #C4C4C4',
        borderRadius: '5px',
        paddingTop: '28px',
        paddingBottom: '15px',
        paddingLeft: '10px',
        paddingRight: '10px',
      },
      container: {
        display: 'flex',
        flexDirection: 'column-reverse',
      },
      editor: {
        maxHeight: '308px',
        overflowY: 'auto',
        '&::-webkit-scrollbar': {
          width: '10px',
        },
        '&::-webkit-scrollbar-track': {
          background: '#f1f1f1',
        },
        '&::-webkit-scrollbar-thumb': {
          background: '#888',
        },
        '&::-webkit-scrollbar-thumb:hover': {
          background: '#555',
        },
      },
      toolbar: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        borderBottom: '1px solid gray',
        backgroundColor: '#ebebeb',
        borderRadius: '5px 5px 0 0',
      },
      placeHolder: {
        paddingTop: '10px',
        width: 'inherit',
        color: '#899499',
      },
    },
  },
});

export default muiRteTheme;

import { AllQuestions } from '../../pages/admin/editExam/editExamProps';

export const mockQuestions: AllQuestions[] = [
  {
    id: 'fb692b21-0e21-47ac-bc40-a41fd113f983',
    __typename: 'MultipleChoiceQuestion',
    question: 'This is a sample question 1',
    options: [
      {
        id: 'cfa6fc0d-375c-40ab-8556-deaf2dd57c23',
        description: 'First Option',
        isCorrect: true,
      },
      {
        id: 'p9asx9sd-375c-40ab-8556-deaf2dd57c23',
        description: 'Second Option',
        isCorrect: false,
      },
      {
        id: 'v5v5wwdfs-375c-40ab-8556-deaf2dd57c23',
        description: 'Third Option',
        isCorrect: false,
      },
    ],
    createdById: 'c9694769-d89c-4bd9-9bdf-d3e238ddb46b',
  },
  {
    id: 'e14291df-561c-47ab-8649-ea69fb0a4e22',
    __typename: 'MultipleSelectionQuestion',
    question: 'This is a sample question 2',
    choices: [
      {
        id: 'cfa6fc0d-375c-40ab-8556-deaf2dd57c23',
        description: 'First Choice',
        isCorrect: true,
      },
      {
        id: 'p9asx9sd-375c-40ab-8556-deaf2dd57c23',
        description: 'Second Choice',
        isCorrect: false,
      },
      {
        id: 'v5v5wwdfs-375c-40ab-8556-deaf2dd57c23',
        description: 'Third Choice',
        isCorrect: false,
      },
    ],
    createdById: 'c9694769-d89c-4bd9-9bdf-d3e238ddb46b',
  },
];

import { ExamAnswerableQuestionInterface } from '../../pages/student/exam/examProps';

export const mockStudentQuestions: ExamAnswerableQuestionInterface[] = [
  {
    id: 'fb692b21-0e21-47ac-bc40-a41fd113f983',
    question: 'This is a sample question 1',
    program: 'Cybersecurity',
    module: 'module',
    createdAt: 1646186146000,
    updatedAt: 1646186146000,
    options: [
      {
        id: 'cfa6fc0d-375c-40ab-8556-deaf2dd57c23',
        description: 'First Option',
      },
      {
        id: 'p9asx9sd-375c-40ab-8556-deaf2dd57c23',
        description: 'Second Option',
      },
      {
        id: 'v5v5wwdfs-375c-40ab-8556-deaf2dd57c23',
        description: 'Third Option',
      },
    ],
    createdById: 'c9694769-d89c-4bd9-9bdf-d3e238ddb46b',
    createdBy: {
      id: 'student-id-yall',
      email: 'teofyrbns@gmail.com',
      firstName: 'Teofy',
      lastName: 'Rabanes',
    },
    questionType: 'MULTIPLE_CHOICE_QUESTION',
  },
  {
    id: 'e14291df-561c-47ab-8649-ea69fb0a4e22',
    question: 'This is a sample question 2',
    program: 'Cybersecurity',
    module: 'module',
    createdAt: 1646186146000,
    updatedAt: 1646186146000,
    choices: [
      {
        id: 'cfa6fc0d-375c-40ab-8556-deaf2dd57c23',
        description: 'First Choice',
      },
      {
        id: 'p9asx9sd-375c-40ab-8556-deaf2dd57c23',
        description: 'Second Choice',
      },
      {
        id: 'v5v5wwdfs-375c-40ab-8556-deaf2dd57c23',
        description: 'Third Choice',
      },
    ],
    createdById: 'c9694769-d89c-4bd9-9bdf-d3e238ddb46b',
    createdBy: {
      id: 'student-id-yall',
      email: 'teofyrbns@gmail.com',
      firstName: 'Teofy',
      lastName: 'Rabanes',
    },
    questionType: 'MULTIPLE_SELECTION_QUESTION',
  },
];

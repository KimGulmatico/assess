import React from 'react';
import {
  render,
  RenderResult,
  cleanup,
  fireEvent,
} from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import HeaderButton from '../components/header/HeaderButton';
import Header from '../layouts/header/Header';

let documentBody: RenderResult;

function renderHeader() {
  return render(
    <Router>
      <Header />
    </Router>
  );
}

describe('Header', () => {
  beforeEach(() => {
    documentBody = renderHeader();
  });

  afterEach(cleanup);

  it('should render kingsland university seal img', () => {
    expect(documentBody.getByAltText('logo')).toBeInTheDocument();
  });

  it('should render kingsland university text', () => {
    expect(documentBody.getByText(/kingsland assess/i)).toBeInTheDocument();
  });

  it('should render resources menu button', () => {
    expect(documentBody.getByTestId('resources-button')).toBeInTheDocument();
  });

  it('should render login button', () => {
    expect(documentBody.getByTestId('login-button')).toBeInTheDocument();
  });
});

describe('Header Login or Logout Button Component', () => {
  it('calls function inside onClick() when button is pressed', () => {
    const handleClick = jest.fn();

    const { getByTestId } = render(
      <HeaderButton
        label="Login"
        handleClick={handleClick}
        testId="login-button"
      />
    );
    fireEvent.click(getByTestId('login-button'));
    expect(handleClick).toHaveBeenCalled();
  });
});

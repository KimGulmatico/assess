import React from 'react';
import { render, RenderResult, cleanup } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import * as Types from '../../types.generated';
import ViewExamPage from '../../pages/admin/viewExam/ViewExamPage';
import { ExamQuestionInterface } from '../../pages/admin/viewExam/examQuestionInterface';
import { timeConverter } from '../../utils/timeConverter';

let documentBody: RenderResult;

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
  useLocation: () => ({ state: { prevPath: 's/exambank' } }),
}));

const questions: ExamQuestionInterface[] = [
  {
    id: 'fb692b21-0e21-47ac-bc40-a41fd113f983',
    __typename: 'MultipleChoiceQuestion',
  },
  {
    id: 'e14291df-561c-47ab-8649-ea69fb0a4e22',
    __typename: 'MultipleChoiceQuestion',
  },
  {
    id: '27092ef6-9d95-4cc7-9f29-6b2454678d39',
    __typename: 'MultipleChoiceQuestion',
  },
  {
    id: '51e1150d-b68a-434d-9401-016fd6e88817',
    __typename: 'MultipleChoiceQuestion',
  },
  {
    id: 'e0a8b506-a0de-472d-8135-65d6788f7521',
    __typename: 'MultipleChoiceQuestion',
  },
];

const mockData = {
  id: '7b39a5a5-992c-4102-893c-4b177972730d',
  examTitle: 'Flexible Type of Exam',
  year: 2022,
  quarter: Types.Quarter.Q1,
  program: Types.Program.Blockchain,
  module: 'Module 1',
  description: 'This is a flexible type of exam',
  examDetails: {
    startDate: null,
    endDate: null,
    duration: 90,
    examType: Types.ExamType.Flexible,
    numOfItems: questions.length,
    titleGridSize: 3,
    detailGridSize: 6,
  },
  questions: questions,
  published: false,
};

function renderViewExam() {
  return render(
    <MockedProvider addTypename={false}>
      <ViewExamPage
        id={mockData.id}
        title={mockData.examTitle}
        year={mockData.year}
        quarter={mockData.quarter}
        program={mockData.program}
        module={mockData.module}
        description={mockData.description}
        examDetails={{
          startDate: mockData.examDetails.startDate,
          endDate: mockData.examDetails.endDate,
          duration: mockData.examDetails.duration,
          typeOfExam: mockData.examDetails.examType,
          numOfItems: questions.length,
          titleGridSize: mockData.examDetails.titleGridSize,
          detailGridSize: mockData.examDetails.detailGridSize,
        }}
        questions={questions}
        published={mockData.published}
      />
    </MockedProvider>
  );
}

describe('View Flexible Type Exam Header', () => {
  beforeEach(() => {
    documentBody = renderViewExam();
  });

  afterEach(cleanup);

  it('should display exam title', () => {
    expect(documentBody.getByText('Flexible Type of Exam')).toBeInTheDocument();
  });

  it('should display exams program chip', () => {
    expect(documentBody.getByText('Blockchain')).toBeInTheDocument();
  });

  it('should display exams module chip', () => {
    expect(documentBody.getByText('Module 1')).toBeInTheDocument();
  });

  it('should display exams cohort chip', () => {
    expect(documentBody.getByText('2022Q1')).toBeInTheDocument();
  });

  it('should render publish button', () => {
    expect(documentBody.getByTestId('publish-button')).toBeInTheDocument();
  });

  it('should render unpublish button', () => {
    expect(
      documentBody.queryByTestId('unpublish-button')
    ).not.toBeInTheDocument();
  });
});

describe('View Flexible Type Exam Details', () => {
  beforeEach(() => {
    documentBody = renderViewExam();
  });

  afterEach(cleanup);

  it('should render duration text', () => {
    expect(documentBody.getByTestId('duration')).toBeInTheDocument();
  });

  it('should render converted duration', () => {
    const convertedTime = timeConverter(mockData.examDetails.duration);
    expect(documentBody.getByText(convertedTime)).toBeInTheDocument();
  });

  it('should render items text', () => {
    expect(documentBody.getByTestId('items')).toBeInTheDocument();
  });

  it('should render number of items', () => {
    const questionsLength = questions.length;
    expect(documentBody.getByText(questionsLength)).toBeInTheDocument();
  });

  it('should not render available text for fixed type of exam', () => {
    expect(documentBody.queryByText('Available')).not.toBeInTheDocument();
  });

  it('should not render due text for fixed type of exam', () => {
    expect(documentBody.queryByText('Due')).not.toBeInTheDocument();
  });
});

describe('View Flexible Type Exam Content', () => {
  beforeEach(() => {
    documentBody = renderViewExam();
  });

  afterEach(cleanup);

  it('should display exam description', () => {
    expect(
      documentBody.getByText('This is a flexible type of exam')
    ).toBeInTheDocument();
  });
});

describe('Time converter', () => {
  it('should display correct converted time with hour and minutes', () => {
    const minutes = 90;
    const expectedTimeFormat = '1 hour and 30 minutes';

    expect(timeConverter(minutes)).toBe(expectedTimeFormat);
  });

  it('should display correct converted time with hour but without minutes', () => {
    const minutes = 60;
    const expectedTimeFormat = '1 hour';

    expect(timeConverter(minutes)).toBe(expectedTimeFormat);
  });

  it('should display correct converted time with hour and a minute', () => {
    const minutes = 61;
    const expectedTimeFormat = '1 hour and 1 minute';

    expect(timeConverter(minutes)).toBe(expectedTimeFormat);
  });

  it('should display correct converted time with hours and minutes', () => {
    const minutes = 165;
    const expectedTimeFormat = '2 hours and 45 minutes';

    expect(timeConverter(minutes)).toBe(expectedTimeFormat);
  });

  it('should display correct converted time with hours but without minutes', () => {
    const minutes = 120;
    const expectedTimeFormat = '2 hours';

    expect(timeConverter(minutes)).toBe(expectedTimeFormat);
  });

  it('should display correct converted time with hours and a minute', () => {
    const minutes = 121;
    const expectedTimeFormat = '2 hours and 1 minute';

    expect(timeConverter(minutes)).toBe(expectedTimeFormat);
  });

  it('should display correct converted time with minutes', () => {
    const minutes = 59;
    const expectedTimeFormat = '59 minutes';

    expect(timeConverter(minutes)).toBe(expectedTimeFormat);
  });

  it('should display correct converted time with a minute', () => {
    const minute = 1;
    const expectedTimeFormat = '1 minute';

    expect(timeConverter(minute)).toBe(expectedTimeFormat);
  });
});

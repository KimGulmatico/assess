import React from 'react';
import {
  render,
  RenderResult,
  cleanup,
  fireEvent,
  waitFor,
} from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import { ExamType } from '../../types.generated';
import UPDATE_PUBLISH_MUTATION from '../../graphql/mutations/admin/exam/updatePublishStatus/updatePublishStatus';
import UnpublishExamDialog from '../../components/modal/admin/PublishExam/UnpublishExamDialog';

let documentBody: RenderResult;

const mockData = {
  examId: 'mock-id-for-testing',
  examType: ExamType.Free,
};

const mockFunction = [
  {
    request: {
      query: UPDATE_PUBLISH_MUTATION,
    },
    updateExam: jest.fn(),
  },
];

function renderUnpublishExamDialog() {
  return render(
    <MockedProvider mocks={mockFunction} addTypename={false}>
      <UnpublishExamDialog
        examId={mockData.examId}
        examType={mockData.examType}
        duration={null}
        startDate={null}
        endDate={null}
      />
    </MockedProvider>
  );
}

describe('Publish Exam Dialog after button click', () => {
  beforeEach(() => {
    documentBody = renderUnpublishExamDialog();
  });

  afterEach(cleanup);

  it('should click save button', () => {
    const button = documentBody.getByTestId('unpublish-button');
    button.click();

    const saveButton = documentBody.getByTestId('confirm-button');
    fireEvent.click(saveButton);

    waitFor(() => {
      const update = mockFunction[0].updateExam;

      expect(update).toHaveBeenCalled();
    });
  });
});

import React from 'react';
import { render, RenderResult, cleanup } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import * as Types from '../../types.generated';
import ViewExamPage from '../../pages/admin/viewExam/ViewExamPage';
import { ExamQuestionInterface } from '../../pages/admin/viewExam/examQuestionInterface';

let documentBody: RenderResult;

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
  useLocation: () => ({ state: { prevPath: 's/exambank' } }),
}));

const questions: ExamQuestionInterface[] = [
  {
    id: 'fb692b21-0e21-47ac-bc40-a41fd113f983',
    __typename: 'MultipleChoiceQuestion',
  },
  {
    id: 'e14291df-561c-47ab-8649-ea69fb0a4e22',
    __typename: 'MultipleSelectionQuestion',
  },
];

const mockData = {
  id: '028f4d90-4c82-40b2-870c-71eebd64760a',
  examTitle: 'Free Type of Exam',
  year: 2021,
  quarter: Types.Quarter.Q3,
  program: Types.Program.FullStack,
  module: 'Module 1',
  description: 'This is a free type of exam',
  examDetails: {
    startDate: null,
    endDate: null,
    duration: null,
    examType: Types.ExamType.Free,
    numOfItems: questions.length,
    titleGridSize: 3,
    detailGridSize: 6,
  },
  questions: questions,
  published: false,
};

function renderViewExam() {
  return render(
    <MockedProvider addTypename={false}>
      <ViewExamPage
        id={mockData.id}
        title={mockData.examTitle}
        year={mockData.year}
        quarter={mockData.quarter}
        program={mockData.program}
        module={mockData.module}
        description={mockData.description}
        examDetails={{
          startDate: mockData.examDetails.startDate,
          endDate: mockData.examDetails.endDate,
          duration: mockData.examDetails.duration,
          typeOfExam: mockData.examDetails.examType,
          numOfItems: questions.length,
          titleGridSize: mockData.examDetails.titleGridSize,
          detailGridSize: mockData.examDetails.detailGridSize,
        }}
        questions={questions}
        published={mockData.published}
      />
    </MockedProvider>
  );
}

describe('View Free Type Exam Header', () => {
  beforeEach(() => {
    documentBody = renderViewExam();
  });

  afterEach(cleanup);

  it('should display exam title', () => {
    expect(documentBody.getByText('Free Type of Exam')).toBeInTheDocument();
  });

  it('should display exams program chip', () => {
    expect(documentBody.getByText('Full stack')).toBeInTheDocument();
  });

  it('should display exams module chip', () => {
    expect(documentBody.getByText('Module 1')).toBeInTheDocument();
  });

  it('should display exams cohort chip', () => {
    expect(documentBody.getByText('2021Q3')).toBeInTheDocument();
  });

  it('should render publish button', () => {
    expect(documentBody.queryByTestId('publish-button')).toBeInTheDocument();
  });

  it('should render unpublish button', () => {
    expect(
      documentBody.queryByTestId('unpublish-button')
    ).not.toBeInTheDocument();
  });
});

describe('View Free Type Exam Details', () => {
  beforeEach(() => {
    documentBody = renderViewExam();
  });

  afterEach(cleanup);

  it('should render due text', () => {
    expect(documentBody.getByTestId('no-due')).toBeInTheDocument();
  });

  it('should render no due text', () => {
    expect(documentBody.getByText('No due date')).toBeInTheDocument();
  });

  it('should render items text', () => {
    expect(documentBody.getByTestId('items')).toBeInTheDocument();
  });

  it('should render number of items', () => {
    const questionsLength = questions.length;
    expect(documentBody.getByText(questionsLength)).toBeInTheDocument();
  });

  it('should not render available text for fixed type of exam', () => {
    expect(documentBody.queryByText('Available')).not.toBeInTheDocument();
  });

  it('should not render duration text for flexible type of exam', () => {
    expect(documentBody.queryByText('Duration')).not.toBeInTheDocument();
  });
});

describe('View Free Type Exam Content', () => {
  beforeEach(() => {
    documentBody = renderViewExam();
  });

  afterEach(cleanup);

  it('should display exam description', () => {
    expect(
      documentBody.getByText('This is a free type of exam')
    ).toBeInTheDocument();
  });
});

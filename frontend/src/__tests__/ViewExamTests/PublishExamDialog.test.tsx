import React from 'react';
import {
  render,
  RenderResult,
  cleanup,
  fireEvent,
  waitFor,
} from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import PublishExamDialog from '../../components/modal/admin/PublishExam/PublishExamDialog';
import { ExamType } from '../../types.generated';
import UPDATE_PUBLISH_MUTATION from '../../graphql/mutations/admin/exam/updatePublishStatus/updatePublishStatus';

let documentBody: RenderResult;

const mockData = {
  examId: 'mock-id-for-testing',
  examType: ExamType.Free,
};

const mockFunction = [
  {
    request: {
      query: UPDATE_PUBLISH_MUTATION,
    },
    updateExam: jest.fn(),
  },
];

function renderPublishExamDialog() {
  return render(
    <MockedProvider mocks={mockFunction} addTypename={false}>
      <PublishExamDialog
        examId={mockData.examId}
        examType={mockData.examType}
        duration={null}
        startDate={null}
        endDate={null}
      />
    </MockedProvider>
  );
}

describe('Publish Exam Dialog after button click', () => {
  beforeEach(() => {
    documentBody = renderPublishExamDialog();
  });

  afterEach(cleanup);

  it('should render Publish Exam Modal', () => {
    const publishButton = documentBody.getByTestId('publish-button');
    publishButton.click();

    expect(documentBody.getByText('Publish Exam')).toBeInTheDocument();

    expect(documentBody.getByTestId('max-questions')).toBeInTheDocument();
  });

  it('renders max questions label', () => {
    const button = documentBody.getByTestId('publish-button');
    button.click();
    expect(documentBody.getByTestId('max-questions-label')).toBeInTheDocument();
  });

  it('renders the textfield for max questions', () => {
    const button = documentBody.getByTestId('publish-button');
    button.click();
    expect(documentBody.getByTestId('max-questions')).toBeInTheDocument();
  });

  it('should change max questions text input', () => {
    const button = documentBody.getByTestId('publish-button');
    button.click();
    const maxQuestionsInput = documentBody.getByTestId('max-questions');
    fireEvent.change(maxQuestionsInput, {
      target: { value: 5 },
    });

    expect(maxQuestionsInput).toHaveValue(5);
  });

  it('should click save button', () => {
    const button = documentBody.getByTestId('publish-button');
    button.click();

    const saveButton = documentBody.getByTestId('save-button');
    fireEvent.click(saveButton);

    waitFor(() => {
      const update = mockFunction[0].updateExam;

      expect(update).toHaveBeenCalled();
    });
  });
});

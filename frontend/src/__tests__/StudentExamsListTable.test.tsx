import React from 'react';
import { MockedProvider } from '@apollo/client/testing';
import { render, RenderResult, cleanup } from '@testing-library/react';
import StudentExamsListTable from '../components/student/examList/StudentExamListTable';

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
  useLocation: () => ({ state: { prevPath: 's/exambank' } }),
}));

let documentBody: RenderResult;

const mockExamsData = [
  {
    id: '07b972d1-e7ca-4fc7-b271-aa4caf701d17',
    examDetails: {
      title: 'Exam 1',
      description: 'Insert Description 1',
      module: 'Module 1',
      maxQuestionCount: 10,
      endDate: new Date(1646186146000),
    },
    score: 5,
    submittedAt: new Date(1646186146000),
  },
  {
    id: '11567ce6-adae-4aa9-8038-86592739edb3',
    examDetails: {
      title: 'Exam 2',
      description: 'Insert Description 2',
      module: 'Module 2',
      maxQuestionCount: 5,
      endDate: new Date(2050160546),
    },
    score: 4,
    submittedAt: new Date(1650160546),
  },
];

const handleNext = jest.fn();
const handlePrev = jest.fn();

function renderExamsListTable() {
  return render(
    <MockedProvider>
      <StudentExamsListTable
        exams={mockExamsData}
        examTableTitle="Exams History"
        pageNum={1}
        onNext={handleNext}
        onPrev={handlePrev}
        nextDisabled={true}
      />
    </MockedProvider>
  );
}

describe('Exams List Table', () => {
  beforeEach(() => {
    documentBody = renderExamsListTable();
  });

  afterEach(cleanup);

  it('should display student exams list header', () => {
    expect(documentBody.getByText('Exams History')).toBeInTheDocument();
  });

  it('should display student exams list table ', () => {
    expect(documentBody.getByRole('table')).toBeInTheDocument();
  });

  it('should display student exam 1 title', () => {
    expect(documentBody.getByText('Exam 1')).toBeInTheDocument();
  });

  it('should display student exam 2 title', () => {
    expect(documentBody.getByText('Exam 2')).toBeInTheDocument();
  });

  it('should display student exam 2 ongoing chip', () => {
    expect(documentBody.getByText('Ongoing')).toBeInTheDocument();
  });

  it('should display student exam 1 score percentage', () => {
    expect(documentBody.getByText('50%')).toBeInTheDocument();
  });

  it('should display student exam 2 score percentage', () => {
    expect(documentBody.getByText('80%')).toBeInTheDocument();
  });
});

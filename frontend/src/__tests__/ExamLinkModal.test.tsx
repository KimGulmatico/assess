import React from 'react';
import { render, RenderResult, cleanup, waitFor } from '@testing-library/react';
import ExamLinkModal from '../components/modal/admin/ExamLink/ExamLinkModal';

let documentBody: RenderResult;
const isOpen = true;
const handleClose = jest.fn();
const examLink =
  'https://assess.kingsland.io/take/ae62c101-0fec-4f78-9c9c-ebe38c003ac2';

function renderExamLinkModal() {
  return render(
    <ExamLinkModal
      isOpen={isOpen}
      handleClose={handleClose}
      examLink={examLink}
    />
  );
}

describe('Exam Link Modal ', () => {
  beforeEach(() => {
    documentBody = renderExamLinkModal();
  });

  afterEach(cleanup);

  it('renders title header', () => {
    expect(documentBody.getByText('Link')).toBeInTheDocument();
  });

  it('renders the exam link', () => {
    const examLinkField = documentBody.getByDisplayValue(examLink);
    expect(examLinkField).toBeInTheDocument();
  });

  it('renders the copy button', () => {
    expect(
      documentBody.getByRole('button', { name: 'Copy' })
    ).toBeInTheDocument();
  });

  it('renders the cancel button', () => {
    expect(
      documentBody.getByRole('button', { name: 'Cancel' })
    ).toBeInTheDocument();
  });

  it('shows an alert when copy button is clicked', async () => {
    const copyButton = documentBody.getByRole('button', { name: 'Copy' });
    copyButton.click();

    await waitFor(() => {
      expect(
        documentBody.getByText('Copied to clipboard.')
      ).toBeInTheDocument();
    });
  });
});

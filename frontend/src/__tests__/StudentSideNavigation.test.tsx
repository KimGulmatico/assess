import React from 'react';
import {
  render,
  RenderResult,
  cleanup,
  waitFor,
  fireEvent,
} from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import { BrowserRouter as Router } from 'react-router-dom';
import SIGN_OUT_MUTATION from '../graphql/mutations/auth/signOut';
import StudentSideNavigation from '../components/navigation/student/StudentSideNavigation';

const mockData = [
  {
    request: {
      query: SIGN_OUT_MUTATION,
    },
    signOut: jest.fn(() => ({
      data: {
        signOut: true,
      },
    })),
  },
];

function renderStudentSideNavigation() {
  return render(
    <MockedProvider mocks={mockData} addTypename={false}>
      <Router>
        <StudentSideNavigation name={'Sachiko Gubat'} />
      </Router>
    </MockedProvider>
  );
}

describe('Student Side Navigation', () => {
  let documentBody: RenderResult;

  beforeEach(() => {
    documentBody = renderStudentSideNavigation();
  });

  afterEach(cleanup);

  it('should display kingsland university logo', () => {
    expect(documentBody.getByAltText('kingsland-logo')).toBeInTheDocument();
  });

  it('should display kingsland assess title', () => {
    expect(documentBody.getByText('Kingsland Assess')).toBeInTheDocument();
  });

  it('should display account button', () => {
    expect(documentBody.getByTestId('Account')).toBeInTheDocument();
  });

  it('should display account menu text', () => {
    expect(documentBody.getByText('Account')).toBeInTheDocument();
  });

  it('should display account popover contents', async () => {
    const accountButton = documentBody.getByTestId('Account');
    accountButton.click();

    await waitFor(() => {
      expect(documentBody.getByText('Sachiko Gubat')).toBeInTheDocument();
      expect(documentBody.getByTestId('account-name')).toBeInTheDocument();
      expect(documentBody.getByText('Student')).toBeInTheDocument();
      expect(documentBody.getByTestId('signout-button')).toBeInTheDocument();
    });
  });

  it('should be able to call the function with signout mutation', () => {
    const accountButton = documentBody.getByTestId('Account');
    accountButton.click();

    const signOutButton = documentBody.getByTestId('signout-button');
    fireEvent.click(signOutButton);

    waitFor(() => {
      const signOutMutation = mockData[0].signOut;
      expect(signOutMutation).toHaveBeenCalled();
    });
  });
});

import React from 'react';
import {
  render,
  RenderResult,
  cleanup,
  waitFor,
  fireEvent,
} from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import AddExamModal from '../components/modal/admin/AddExam/AddExamModal';
import CREATE_EXAM_MUTATION from '../graphql/mutations/admin/exam/createExam';

const mockedUsedNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
}));

let documentBody: RenderResult;

const mockFunctions = [
  {
    request: {
      query: CREATE_EXAM_MUTATION,
    },
    createExam: jest.fn(),
  },
];

function renderAddExamModal() {
  return render(
    <MockedProvider mocks={mockFunctions} addTypename={false}>
      <AddExamModal />
    </MockedProvider>
  );
}

describe('Add exam modal', () => {
  beforeEach(() => {
    documentBody = renderAddExamModal();
  });

  afterEach(cleanup);

  it('renders add exam button', () => {
    expect(documentBody.getByTestId('add-exam-button')).toBeInTheDocument();
  });

  it('opens add exam modal after button click', async () => {
    const button = documentBody.getByTestId('add-exam-button');

    button.click();

    await waitFor(() => {
      expect(documentBody.getByTestId('add-exam-modal')).toBeInTheDocument();
      expect(documentBody.getByText('Exam Title')).toBeInTheDocument();
      expect(documentBody.getByText('Description')).toBeInTheDocument();
      expect(documentBody.getByText('Exam Type')).toBeInTheDocument();
      expect(documentBody.getByText('Program')).toBeInTheDocument();
      expect(documentBody.getByText('Cohort')).toBeInTheDocument();
    });
  });

  it('displays add exam textfields and save button after button click', async () => {
    const button = documentBody.getByTestId('add-exam-button');

    button.click();

    await waitFor(() => {
      expect(documentBody.getByTestId('program-textfield')).toBeInTheDocument();
      expect(
        documentBody.getByPlaceholderText('Type description here')
      ).toBeInTheDocument();
      expect(
        documentBody.getByPlaceholderText('Type exam title here')
      ).toBeInTheDocument();
      expect(
        documentBody.getByTestId('exam-type-textfield')
      ).toBeInTheDocument();
      expect(documentBody.getByTestId('year-date-picker')).toBeInTheDocument();
      expect(documentBody.getByTestId('quarter-textfield')).toBeInTheDocument();
      expect(documentBody.getByTestId('save-exam-button')).toBeInTheDocument();
    });
  });

  it('should click save exam button', () => {
    const addButton = documentBody.getByTestId('add-exam-button');
    addButton.click();

    const saveButton = documentBody.getByTestId('save-exam-button');
    fireEvent.click(saveButton);

    waitFor(() => {
      const createExam = mockFunctions[0].createExam;
      expect(createExam).toHaveBeenCalled();
    });
  });
});

import React, { useState } from 'react';
import {
  render,
  RenderResult,
  cleanup,
  fireEvent,
} from '@testing-library/react';
import QuestionBankHeader from '../../components/admin/questionBank/QuestionBankHeader';

let documentBody: RenderResult;

function QuestionBankHeaderRender() {
  const [searchValue, setSearchValue] = useState('');

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setSearchValue(value);
  };

  return (
    <QuestionBankHeader
      searchValue={searchValue}
      changeFunction={handleChange}
      handleClearInput={() => setSearchValue('')}
      data={[]}
    />
  );
}

const renderQuestionBankHeader = () => {
  const questionBankHeader = render(<QuestionBankHeaderRender />);
  return questionBankHeader;
};

describe('Question Bank Page Header Components', () => {
  beforeEach(() => {
    documentBody = renderQuestionBankHeader();
  });

  afterEach(cleanup);

  it('should display Question Bank Heading', () => {
    expect(documentBody.getByText(/question bank/i)).toBeInTheDocument();
  });

  it('should display Question Bank add question button', () => {
    expect(documentBody.getByTestId('add-question')).toBeInTheDocument();
  });

  it('should display Question Bank search bar', () => {
    expect(documentBody.getByRole('textbox')).toBeInTheDocument();
  });

  it('should change input on search bar', () => {
    const searchInput = documentBody.getByRole('textbox');
    fireEvent.change(searchInput, { target: { value: 'sample question' } });
    expect(searchInput).toHaveValue('sample question');
  });
});

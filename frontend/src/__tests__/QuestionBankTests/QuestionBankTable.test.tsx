import React from 'react';
import { render, RenderResult, cleanup } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import QuestionBankTable from '../../components/admin/questionBank/QuestionBankTable';
import { QuestionInterface } from '../../pages/admin/questionBank/QuestionInterface';

let documentBody: RenderResult;

const mockQuestionsData: QuestionInterface[] = [
  {
    id: 'cfa6fc0d-375c-40ab-8556-deaf2dd57c23',
    question: 'What is a boink boink?',
    __typename: 'MultipleChoiceQuestion',
    createdAt: new Date(1647495451),
    updatedAt: new Date(1647495451),
    createdById: 'cfssfc0d-375c-40ab-9876-deaf2dd57c23',
  },
  {
    id: 'yaha0jsa-375c-40ab-8556-deaf2dd57c23',
    question: 'What is a choink choink?',
    __typename: 'MultipleChoiceQuestion',
    createdAt: new Date(1618612651),
    updatedAt: new Date(1618612651),
    createdById: 'cfssfc0d-375c-40ab-9876-deaf2dd57c23',
  },
  {
    id: 'p9asx9sd-375c-40ab-8556-deaf2dd57c23',
    question: 'What is a a question? That is a question',
    __typename: 'MultipleChoiceQuestion',
    createdAt: new Date(1590532651),
    updatedAt: new Date(1590532651),
    createdById: 'cfssfc0d-375c-40ab-9876-deaf2dd57c23',
  },
];

const mockEmptyData: QuestionInterface[] = [];
const handleNext = jest.fn();
const handlePrev = jest.fn();

function questionBankTableRender() {
  return render(
    <MockedProvider addTypename={false}>
      <QuestionBankTable
        questions={mockQuestionsData}
        searchValue={''}
        noMatchFound={true}
        isSearchLoading={false}
        pageNum={1}
        onNext={handleNext}
        onPrev={handlePrev}
        nextDisabled={true}
      />
    </MockedProvider>
  );
}

function searchedQuestionsTableRender() {
  return render(
    <MockedProvider addTypename={false}>
      <QuestionBankTable
        questions={mockQuestionsData}
        searchValue="boink"
        noMatchFound={false}
        isSearchLoading={false}
        pageNum={1}
        onNext={handleNext}
        onPrev={handlePrev}
        nextDisabled={true}
      />
    </MockedProvider>
  );
}

function questionBankNoDataTableRender() {
  return render(
    <MockedProvider addTypename={false}>
      <QuestionBankTable
        questions={mockEmptyData}
        searchValue={''}
        noMatchFound={true}
        isSearchLoading={false}
        pageNum={1}
        onNext={handleNext}
        onPrev={handlePrev}
        nextDisabled={true}
      />
    </MockedProvider>
  );
}

function searchResultsNoDataTableRender() {
  return render(
    <MockedProvider addTypename={false}>
      <QuestionBankTable
        questions={mockQuestionsData}
        searchValue="mochi"
        noMatchFound={true}
        isSearchLoading={false}
        pageNum={1}
        onNext={handleNext}
        onPrev={handlePrev}
        nextDisabled={true}
      />
    </MockedProvider>
  );
}

function searchLoadingComponentRender() {
  return render(
    <MockedProvider addTypename={false}>
      <QuestionBankTable
        questions={mockQuestionsData}
        searchValue={''}
        noMatchFound={false}
        isSearchLoading={true}
        pageNum={1}
        onNext={handleNext}
        onPrev={handlePrev}
        nextDisabled={true}
      />
    </MockedProvider>
  );
}

describe('Question Bank Table render', () => {
  beforeEach(() => {
    documentBody = questionBankTableRender();
  });

  afterEach(cleanup);

  it('should display Questions Heading', () => {
    expect(documentBody.getByRole('columnheader')).toBeInTheDocument();
  });

  it('should display table ', () => {
    expect(documentBody.getByRole('table')).toBeInTheDocument();
  });
});

describe('Retrieved questions date from all questions', () => {
  beforeEach(() => {
    documentBody = questionBankTableRender();
  });

  afterEach(cleanup);

  it('should display correct date for 1st question"', () => {
    const expectedDate = 'January 20, 1970, 9:38:15 AM';

    expect(
      documentBody.getByText(`updated last ${expectedDate}`)
    ).toBeInTheDocument();
  });

  it('should display correct date for 2nd question"', () => {
    const expectedDate = 'January 20, 1970, 1:36:52 AM';

    expect(
      documentBody.getByText(`updated last ${expectedDate}`)
    ).toBeInTheDocument();
  });

  it('should display correct date for 3rd question"', () => {
    const expectedDate = 'January 19, 1970, 5:48:52 PM';

    expect(
      documentBody.getByText(`updated last ${expectedDate}`)
    ).toBeInTheDocument();
  });
});

describe('Retrieved questions date from searched questions', () => {
  beforeEach(() => {
    documentBody = searchedQuestionsTableRender();
  });

  afterEach(cleanup);

  it('should display correct date for 1st question"', () => {
    const expectedDate = 'January 20, 1970, 9:38:15 AM';

    expect(
      documentBody.getByText(`updated last ${expectedDate}`)
    ).toBeInTheDocument();
  });
});

describe('Retrieved no questions', () => {
  beforeEach(() => {
    documentBody = questionBankNoDataTableRender();
  });

  afterEach(cleanup);

  it('should display empty table', () => {
    expect(
      documentBody.getByText('No questions have been added yet')
    ).toBeInTheDocument();
  });
});

describe('Retrieved no search results', () => {
  beforeEach(() => {
    documentBody = searchResultsNoDataTableRender();
  });

  afterEach(cleanup);

  it('should display empty table', () => {
    expect(
      documentBody.getByText("We couldn't find any results for")
    ).toBeInTheDocument();
    expect(documentBody.getByText(`"mochi"`)).toBeInTheDocument();
  });
});

describe('Loading search results', () => {
  beforeEach(() => {
    documentBody = searchLoadingComponentRender();
  });

  afterEach(cleanup);

  it('should display search loading component', () => {
    expect(documentBody.getByText('please wait')).toBeInTheDocument();
  });
});

describe('Data render', () => {});

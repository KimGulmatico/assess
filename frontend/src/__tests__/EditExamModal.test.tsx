import React from 'react';
import {
  render,
  RenderResult,
  cleanup,
  waitFor,
  fireEvent,
} from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import UPDATE_EXAM_MUTATION from '../graphql/mutations/admin/exam/updateExam';
import EditExamModal from '../components/modal/admin/EditExam/EditExamModal';
import { ExamType } from '../types.generated';

let documentBody: RenderResult;

const data = {
  id: 'ae62c101-0fec-4f78-9c9c-ebe38c003ac2',
  title: 'Exam 2022',
  description: 'This is an exam description',
  program: 'CYBERSECURITY',
  module: 'module',
  year: 2022,
  quarter: 'Q1',
  isPublished: false,
  createdAt: new Date(1651232078634),
  updatedAt: new Date(1651232078639),
  createdById: '80849c54-d27d-41e0-8b42-f5de76784fdf',
  startDate: new Date(1651188676000),
  endDate: new Date(1651318225000),
  duration: 0,
  examType: ExamType.Fixed,
  questions: [],
};

const mockFunctions = [
  {
    request: {
      query: UPDATE_EXAM_MUTATION,
    },
    updateExam: jest.fn(),
  },
];

const renderEditExamModal = () => {
  return render(
    <MockedProvider mocks={mockFunctions} addTypename={false}>
      <EditExamModal exam={data} />
    </MockedProvider>
  );
};

describe('Edit exam modal after button click', () => {
  beforeEach(() => {
    documentBody = renderEditExamModal();
  });

  afterEach(cleanup);

  it('renders edit exam button', () => {
    expect(documentBody.getByTestId('edit-exam-button')).toBeInTheDocument();
  });

  it('opens edit exam modal after button click', async () => {
    const editExamButton = documentBody.getByTestId('edit-exam-button');

    editExamButton.click();

    await waitFor(() => {
      expect(documentBody.getByTestId('edit-exam-modal')).toBeInTheDocument();
    });
  });

  it('displays edit exam texts, textfields and edit exam button', async () => {
    const editExamButton = documentBody.getByTestId('edit-exam-button');

    editExamButton.click();

    await waitFor(() => {
      expect(documentBody.getByTestId('edit-exam-modal')).toBeInTheDocument();
      expect(documentBody.getByText('Exam Title')).toBeInTheDocument();
      expect(
        documentBody.getByTestId('exam-title-textfield')
      ).toBeInTheDocument();
      expect(documentBody.getByText('Description')).toBeInTheDocument();
      expect(
        documentBody.getByTestId('exam-description-textfield')
      ).toBeInTheDocument();
      expect(documentBody.getByText('Exam Type')).toBeInTheDocument();
      expect(
        documentBody.getByTestId('exam-type-textfield')
      ).toBeInTheDocument();
      expect(documentBody.getByText('Program')).toBeInTheDocument();
      expect(documentBody.getByTestId('program-textfield')).toBeInTheDocument();
      expect(documentBody.getByText('Cohort')).toBeInTheDocument();
      expect(documentBody.getByTestId('year-date-picker')).toBeInTheDocument();
      expect(documentBody.getByTestId('quarter-textfield')).toBeInTheDocument();
      expect(documentBody.getByText('Start')).toBeInTheDocument();
      expect(documentBody.getByText('End')).toBeInTheDocument();
      expect(
        documentBody.getByTestId('save-edit-exam-button')
      ).toBeInTheDocument();
    });
  });

  it('should click save button', () => {
    const editButton = documentBody.getByTestId('edit-exam-button');
    editButton.click();

    const saveButton = documentBody.getByTestId('save-edit-exam-button');
    fireEvent.click(saveButton);

    waitFor(() => {
      const updateExam = mockFunctions[0].updateExam;

      expect(updateExam).toHaveBeenCalled();
    });
  });
});

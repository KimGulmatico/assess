import React from 'react';
import {
  render,
  RenderResult,
  cleanup,
  waitFor,
  fireEvent,
} from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import SideNavigation from '../components/navigation/admin/SideNavigation';
import { MockedProvider } from '@apollo/client/testing';
import SIGN_OUT_MUTATION from '../graphql/mutations/auth/signOut';

const mockData = [
  {
    request: {
      query: SIGN_OUT_MUTATION,
    },
    signOut: jest.fn(() => ({
      data: {
        signOut: true,
      },
    })),
  },
];

const mockName = 'Teofy Rabanes';

function renderSideNavigation() {
  return render(
    <MockedProvider mocks={mockData} addTypename={false}>
      <Router>
        <SideNavigation name={mockName} />
      </Router>
    </MockedProvider>
  );
}

describe('Side Navigation', () => {
  let documentBody: RenderResult;

  beforeEach(() => {
    documentBody = renderSideNavigation();
  });

  afterEach(cleanup);

  it('should render kingsland university logo', () => {
    expect(documentBody.getByAltText('kingsland-logo')).toBeInTheDocument();
  });

  it('should render kingsland assess title', () => {
    expect(documentBody.getByText('Kingsland Assess')).toBeInTheDocument();
  });

  it('should render dashboard menu button', () => {
    expect(documentBody.getByTestId('Dashboard')).toBeInTheDocument();
  });

  it('should render exam bank menu button', () => {
    expect(documentBody.getByTestId('Exam Bank')).toBeInTheDocument();
  });

  it('should render question bank button', () => {
    expect(documentBody.getByTestId('Question Bank')).toBeInTheDocument();
  });

  it('should render account button', () => {
    expect(documentBody.getByTestId('Account')).toBeInTheDocument();
  });

  it('should render dashboard menu text', () => {
    expect(documentBody.getByText('Dashboard')).toBeInTheDocument();
  });

  it('should render exam bank menu text', () => {
    expect(documentBody.getByText('Exam Bank')).toBeInTheDocument();
  });

  it('should render question bank menu text', () => {
    expect(documentBody.getByText('Question Bank')).toBeInTheDocument();
  });

  it('should render account menu text', () => {
    expect(documentBody.getByText('Account')).toBeInTheDocument();
  });
  it('should render sign out button', async () => {
    const accountButton = documentBody.getByTestId('Account');
    accountButton.click();

    await waitFor(() => {
      expect(documentBody.getByTestId('signout-button')).toBeInTheDocument();
    });
  });
});

describe('Sign out Button', () => {
  it('should display current user name', () => {
    const { getByTestId } = render(
      <MockedProvider mocks={mockData} addTypename={false}>
        <Router>
          <SideNavigation name={mockName} />
        </Router>
      </MockedProvider>
    );

    const accountButton = getByTestId('Account');
    accountButton.click();

    const adminName = getByTestId('account-name');

    expect(adminName).toBeInTheDocument();
  });

  it('should click sign out button and call the function with mutation', () => {
    const { getByTestId } = render(
      <MockedProvider mocks={mockData} addTypename={false}>
        <Router>
          <SideNavigation name={mockName} />
        </Router>
      </MockedProvider>
    );

    const accountButton = getByTestId('Account');
    accountButton.click();

    const signOutButton = getByTestId('signout-button');
    fireEvent.click(signOutButton);

    waitFor(() => {
      const signOutMutation = mockData[0].signOut;
      expect(signOutMutation).toHaveBeenCalled();
    });
  });
});

import React from 'react';
import { MockedProvider } from '@apollo/client/testing';
import { render, RenderResult, cleanup } from '@testing-library/react';
import ExamsListTable from '../components/admin/examsList/ExamsListTable';

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
  useLocation: () => ({ state: { prevPath: 's/exambank' } }),
}));

let documentBody: RenderResult;

const mockExamsData = [
  {
    id: '0f88c87a-7743-4a3c-9f1f-e743d5ca7587',
    title: 'Exam 1',
    description: 'Insert Description 1',
    program: 'Blockchain',
    module: 'Module 1',
    isPublished: true,
    year: 2022,
    quarter: 'Q1',
    createdAt: new Date(1646186146000),
    updatedAt: new Date(1646186146000),
    createdById: 'Marsry Jan Sevilla',
  },
  {
    id: '46499338-4e09-466a-a82b-2d252a9ae9bb',
    title: 'Exam 2',
    description: 'Insert Description 2',
    program: 'Cybersecurity',
    module: 'Module 2',
    isPublished: false,
    year: 2021,
    quarter: 'Q2',
    createdAt: new Date(1650160546),
    updatedAt: new Date(1650160546),
    createdById: 'Marsry Jan Sevilla',
  },
];

const handleNext = jest.fn();
const handlePrev = jest.fn();

function renderExamsListTable() {
  return render(
    <MockedProvider>
      <ExamsListTable
        examTableTitle="Exams"
        exams={mockExamsData}
        pageNum={1}
        onNext={handleNext}
        onPrev={handlePrev}
        nextDisabled={true}
      />
    </MockedProvider>
  );
}

describe('Exams List Table', () => {
  beforeEach(() => {
    documentBody = renderExamsListTable();
  });

  afterEach(cleanup);

  it('should display exams list header', () => {
    expect(documentBody.getByText('Exams')).toBeInTheDocument();
  });

  it('should display exams list table ', () => {
    expect(documentBody.getByRole('table')).toBeInTheDocument();
  });

  it('should display exam 1 title', () => {
    expect(documentBody.getByText('Exam 1')).toBeInTheDocument();
  });

  it('should display exams program chip 1', () => {
    expect(documentBody.getByText('Blockchain')).toBeInTheDocument();
  });

  it('should display exams module chip 1', () => {
    expect(documentBody.getByText('Module 1')).toBeInTheDocument();
  });

  it('should display exams cohort chip 1', () => {
    expect(documentBody.getByText('2022Q1')).toBeInTheDocument();
  });

  it('should display exams table title 2', () => {
    expect(documentBody.getByText('Exam 2')).toBeInTheDocument();
  });

  it('should display exams program chip 2', () => {
    expect(documentBody.getByText('Cybersecurity')).toBeInTheDocument();
  });

  it('should display exams module chip 2', () => {
    expect(documentBody.getByText('Module 2')).toBeInTheDocument();
  });

  it('should display exams cohort chip 2', () => {
    expect(documentBody.getByText('2021Q2')).toBeInTheDocument();
  });
});

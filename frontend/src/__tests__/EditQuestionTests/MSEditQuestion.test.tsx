import React from 'react';
import {
  render,
  RenderResult,
  cleanup,
  waitFor,
  fireEvent,
} from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import EditMSQModal from '../../components/modal/admin/EditQuestion/MSQuestion/EditMSQModal';
import { UPDATE_MS_QUESTION_MUTATION } from '../../graphql/mutations/admin/multipleSelection/question/updateMultipleSelectionQuestion';
import { UPDATE_MS_CHOICE_MUTATION } from '../../graphql/mutations/admin/multipleSelection/choice/updateMultipleSeectionChoice';
import { CREATE_MS_CHOICE_MUTATION } from '../../graphql/mutations/admin/multipleSelection/choice/createMultipleSelectionChoice';
import { DELETE_MS_CHOICE_MUTATION } from '../../graphql/mutations/admin/multipleSelection/choice/deleteMultipleSelectionChoice';
import { Program } from '../../types.generated';

let documentBody: RenderResult;

const initialData = {
  id: 'mock-id-for-testing',
  program: Program.Cybersecurity,
  module: 'module',
  questionType: 'MultipleSelectionQuestion',
  question: 'Which of the following is considered a primary color?',
  possibleAnswers: [
    { id: 'choice-id-1', description: 'Green', isCorrect: false },
    { id: 'choice-id-2', description: 'Blue', isCorrect: true },
    { id: 'choice-id-3', description: 'Yellow', isCorrect: true },
  ],
};

const mockFunctions = [
  {
    request: {
      query: UPDATE_MS_QUESTION_MUTATION,
    },
    updateQuestion: jest.fn(),
  },
  {
    request: {
      query: UPDATE_MS_CHOICE_MUTATION,
    },
    updateChoice: jest.fn(),
  },
  {
    request: {
      query: CREATE_MS_CHOICE_MUTATION,
    },
    createChoice: jest.fn(),
  },
  {
    request: {
      query: DELETE_MS_CHOICE_MUTATION,
    },
    deleteChoice: jest.fn(),
  },
];

const renderMSQuestionEdit = () => {
  return render(
    <MockedProvider mocks={mockFunctions} addTypename={false}>
      <EditMSQModal
        program={initialData.program}
        module={initialData.module}
        questionId={initialData.id}
        questionType="MultipleSelectionQuestion"
        question={initialData.question}
        possibleAnswers={initialData.possibleAnswers}
      />
    </MockedProvider>
  );
};

describe('Edit Multiple Selection Question after button click', () => {
  beforeEach(() => {
    documentBody = renderMSQuestionEdit();
  });

  afterEach(cleanup);

  it('should render Edit Question Modal', () => {
    const editButton = documentBody.getByTestId('action-button');
    editButton.click();

    expect(documentBody.getByTestId('edit-msq')).toBeInTheDocument();

    expect(
      documentBody.getByTestId(initialData.questionType)
    ).toBeInTheDocument();

    expect(documentBody.getByTestId('question-content')).toBeInTheDocument();

    expect(documentBody.getByTestId('program-textfield')).toBeInTheDocument();

    expect(documentBody.getByTestId('module-textfield')).toBeInTheDocument();

    expect(documentBody.getByTestId('checkbox-1')).toBeInTheDocument();

    expect(documentBody.getByTestId('checkbox-2')).toBeInTheDocument();

    expect(documentBody.getByTestId('checkbox-3')).toBeInTheDocument();

    expect(documentBody.getByTestId('add-new-answer')).toBeInTheDocument();

    expect(documentBody.getByTestId('save-button')).toBeInTheDocument();
  });

  it('should change module input', () => {
    const editButton = documentBody.getByTestId('action-button');
    editButton.click();

    const moduleInput = documentBody.getByTestId('module-textfield');
    fireEvent.change(moduleInput, { target: { value: 'changed module' } });

    expect(moduleInput).toHaveValue('changed module');
  });

  it('should change question input', () => {
    const editButton = documentBody.getByTestId('action-button');
    editButton.click();

    const questionInput = documentBody.getByTestId('question-content');
    fireEvent.change(questionInput, { target: { value: 'changed question' } });

    expect(questionInput).toHaveValue('changed question');
  });

  it('should change new choice input', () => {
    const editButton = documentBody.getByTestId('action-button');
    editButton.click();

    const newAnswerInput = documentBody.getByTestId('add-new-answer');
    fireEvent.change(newAnswerInput, { target: { value: 'added choice' } });

    expect(newAnswerInput).toHaveValue('added choice');
  });

  it('should click save button', () => {
    const editButton = documentBody.getByTestId('action-button');
    editButton.click();

    const saveButton = documentBody.getByTestId('save-button');
    fireEvent.click(saveButton);

    waitFor(() => {
      const updateQuestion = mockFunctions[0].updateQuestion;
      const updateChoice = mockFunctions[1].updateChoice;
      const createChoice = mockFunctions[2].createChoice;
      const deleteChoice = mockFunctions[3].deleteChoice;

      expect(updateQuestion).toHaveBeenCalled();

      expect(updateChoice).toHaveBeenCalled();

      expect(createChoice).toHaveBeenCalled();

      expect(deleteChoice).toHaveBeenCalled();
    });
  });
});

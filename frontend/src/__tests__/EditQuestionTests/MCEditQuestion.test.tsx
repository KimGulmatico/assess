import React from 'react';
import {
  render,
  RenderResult,
  cleanup,
  waitFor,
  fireEvent,
} from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import EditMCQModal from '../../components/modal/admin/EditQuestion/MCQuestion/EditMCQModal';
import { UPDATE_MC_QUESTION_MUTATION } from '../../graphql/mutations/admin/multipleChoice/question/updateMultipleChoiceQuestion';
import { UPDATE_MC_OPTION_MUTATION } from '../../graphql/mutations/admin/multipleChoice/option/updateMultipleChoiceOption';
import { CREATE_MC_OPTION_MUTATION } from '../../graphql/mutations/admin/multipleChoice/option/createMultipleChoiceOption';
import { DELETE_MC_OPTION_MUTATION } from '../../graphql/mutations/admin/multipleChoice/option/deleteMultipleChoiceOption';
import { formatQuestionType } from '../../utils/wordFormatter';
import { Program } from '../../types.generated';

let documentBody: RenderResult;

const initialData = {
  id: 'mock-id-for-testing',
  program: Program.Cybersecurity,
  module: 'module',
  questionType: 'MultipleChoiceQuestion',
  question: 'What do you call a fake noodle?',
  possibleAnswers: [
    { id: 'option-id-1', description: 'An impasta!', isCorrect: true },
    { id: 'option-id-2', description: 'Mac N Cheese', isCorrect: false },
    { id: 'option-id-3', description: 'Lucky Me', isCorrect: false },
  ],
};

const mockFunctions = [
  {
    request: {
      query: UPDATE_MC_QUESTION_MUTATION,
    },
    updateQuestion: jest.fn(),
  },
  {
    request: {
      query: UPDATE_MC_OPTION_MUTATION,
    },
    updateOption: jest.fn(),
  },
  {
    request: {
      query: CREATE_MC_OPTION_MUTATION,
    },
    createOption: jest.fn(),
  },
  {
    request: {
      query: DELETE_MC_OPTION_MUTATION,
    },
    deleteOption: jest.fn(),
  },
];

const renderMCQuestionEdit = () => {
  return render(
    <MockedProvider mocks={mockFunctions} addTypename={false}>
      <EditMCQModal
        program={initialData.program}
        module={initialData.module}
        questionId={initialData.id}
        questionType="MultipleChoiceQuestion"
        question={initialData.question}
        possibleAnswers={initialData.possibleAnswers}
      />
    </MockedProvider>
  );
};

describe('Edit Multiple Choice Question after button click', () => {
  beforeEach(() => {
    documentBody = renderMCQuestionEdit();
  });

  afterEach(cleanup);

  it('should render Edit Question Modal', () => {
    const editButton = documentBody.getByTestId('action-button');
    editButton.click();

    expect(documentBody.getByTestId('edit-mcq')).toBeInTheDocument();

    expect(
      documentBody.getByTestId(initialData.questionType)
    ).toBeInTheDocument();

    expect(documentBody.getByTestId('question-content')).toBeInTheDocument();

    expect(documentBody.getByTestId('program-textfield')).toBeInTheDocument();

    expect(documentBody.getByTestId('module-textfield')).toBeInTheDocument();

    expect(documentBody.getByTestId('radio-1')).toBeInTheDocument();

    expect(documentBody.getByTestId('radio-2')).toBeInTheDocument();

    expect(documentBody.getByTestId('radio-3')).toBeInTheDocument();

    expect(documentBody.getByTestId('add-new-answer')).toBeInTheDocument();

    expect(documentBody.getByTestId('save-button')).toBeInTheDocument();
  });

  it('should render text content', () => {
    const editButton = documentBody.getByTestId('action-button');
    editButton.click();

    expect(
      documentBody.getByText(formatQuestionType(initialData.questionType))
    ).toBeInTheDocument();

    expect(
      documentBody.getByText('What do you call a fake noodle?')
    ).toBeInTheDocument();
  });

  it('should change module input', () => {
    const editButton = documentBody.getByTestId('action-button');
    editButton.click();

    const moduleInput = documentBody.getByTestId('module-textfield');
    fireEvent.change(moduleInput, { target: { value: 'changed module' } });

    expect(moduleInput).toHaveValue('changed module');
  });

  it('should change question input', () => {
    const editButton = documentBody.getByTestId('action-button');
    editButton.click();

    const questionInput = documentBody.getByTestId('question-content');
    fireEvent.change(questionInput, { target: { value: 'changed question' } });

    expect(questionInput).toHaveValue('changed question');
  });

  it('should change new option input', () => {
    const editButton = documentBody.getByTestId('action-button');
    editButton.click();

    const newAnswerInput = documentBody.getByTestId('add-new-answer');
    fireEvent.change(newAnswerInput, { target: { value: 'added option' } });

    expect(newAnswerInput).toHaveValue('added option');
  });

  it('should click save button', () => {
    const editButton = documentBody.getByTestId('action-button');
    editButton.click();

    const saveButton = documentBody.getByTestId('save-button');
    fireEvent.click(saveButton);

    waitFor(() => {
      const updateQuestion = mockFunctions[0].updateQuestion;
      const updateOption = mockFunctions[1].updateOption;
      const createOption = mockFunctions[2].createOption;
      const deleteOption = mockFunctions[3].deleteOption;

      expect(updateQuestion).toHaveBeenCalled();

      expect(updateOption).toHaveBeenCalled();

      expect(createOption).toHaveBeenCalled();

      expect(deleteOption).toHaveBeenCalled();
    });
  });
});

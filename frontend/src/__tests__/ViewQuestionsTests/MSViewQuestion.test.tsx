import React from 'react';
import { render, RenderResult, cleanup, waitFor } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import MSQuestionModal from '../../components/modal/admin/ViewQuestion/MSQuestion/MSQuestionModal';
import { GetUserQuery } from '../../graphql/queries/user/user.generated';
import { GetMultipleSelectionQuestionQuery } from '../../graphql/queries/admin/multipleSelection/getMultipleSelectionQuestion.generated';
import { Program } from '../../types.generated';

let documentBody: RenderResult;

const mockData: GetMultipleSelectionQuestionQuery = {
  __typename: 'Query',
  multipleSelectionQuestion: {
    id: 'cfa6fc0d-375c-40ab-8556-deaf2dd57c23',
    program: Program.Blockchain,
    module: 'module',
    question: 'This is a sample question for multiple selection?',
    choices: [
      {
        id: 'cfa6fc0d-375c-40ab-8556-deaf2dd57c23',
        description: 'First choice',
        isCorrect: true,
      },
      {
        id: 'p9asx9sd-375c-40ab-8556-deaf2dd57c23',
        description: 'Second choice',
        isCorrect: false,
      },
      {
        id: 'v5v5wwdfs-375c-40ab-8556-deaf2dd57c23',
        description: 'Third choice',
        isCorrect: false,
      },
      {
        id: 'v5v5wwdfs-375c-40ab-8556-wiwu5tybas8a',
        description: 'Fourth choice',
        isCorrect: true,
      },
    ],
    createdAt: new Date(1647495451),
  },
};

const mockAuthor: GetUserQuery = {
  __typename: 'Query',
  user: {
    firstName: 'Teofy',
    lastName: 'Rabanes',
  },
};

function MSViewQuestionRender() {
  return <MSQuestionModal data={mockData} author={mockAuthor} />;
}

const renderMSQuestionView = () => {
  const msViewQuestion = render(
    <MockedProvider addTypename={false}>
      <MSViewQuestionRender />
    </MockedProvider>
  );
  return msViewQuestion;
};

describe('Multiple Selection Question View', () => {
  beforeEach(() => {
    documentBody = renderMSQuestionView();
  });

  afterEach(cleanup);

  it('should display Question', () => {
    expect(
      documentBody.getByText(
        'This is a sample question for multiple selection?'
      )
    ).toBeInTheDocument();
  });

  it('should render User that created the question', () => {
    const question = documentBody.getByTestId('question');
    question.click();

    expect(documentBody.getByTestId('created-by')).toBeInTheDocument();
    expect(documentBody.getByText(/Teofy Rabanes/i)).toBeInTheDocument();
  });

  it('should render date the question is created', () => {
    const question = documentBody.getByTestId('question');
    question.click();

    expect(documentBody.getByTestId('date-created')).toBeInTheDocument();
    const expectedDate = 'January 20, 1970, 9:38:15 AM';

    expect(documentBody.getByText(`${expectedDate}`)).toBeInTheDocument();
  });

  it('should display correct answer choices', async () => {
    const question = documentBody.getByTestId('question');
    question.click();

    await waitFor(() => {
      expect(documentBody.getByTestId('possible-answer-1')).toBeInTheDocument();
      expect(documentBody.getByTestId('possible-answer-4')).toBeInTheDocument();
    });
  });

  it('should display first incorrect answer choices', async () => {
    const question = documentBody.getByTestId('question');
    question.click();

    await waitFor(() => {
      expect(documentBody.getByTestId('possible-answer-2')).toBeInTheDocument();
      expect(documentBody.getByTestId('possible-answer-3')).toBeInTheDocument();
    });
  });
});

describe('Multiple Selection choices View', () => {
  afterEach(cleanup);

  it('should display correct answers choice value', async () => {
    const { getByTestId } = renderMSQuestionView();

    const question = getByTestId('question');
    question.click();

    await waitFor(() => {
      expect(documentBody.getByLabelText('First choice')).toBeInTheDocument();
      expect(documentBody.getByLabelText('Fourth choice')).toBeInTheDocument();
    });
  });

  it('should display incorrect answers choice value', async () => {
    const { getByTestId } = renderMSQuestionView();

    const question = getByTestId('question');
    question.click();

    await waitFor(() => {
      expect(documentBody.getByLabelText('Second choice')).toBeInTheDocument();
      expect(documentBody.getByLabelText('Third choice')).toBeInTheDocument();
    });
  });
});

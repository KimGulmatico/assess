import React from 'react';
import { render, RenderResult, cleanup, waitFor } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import MCQuestionModal from '../../components/modal/admin/ViewQuestion/MCQuestion/MCQuestionModal';
import { GetMultipleChoiceQuestionQuery } from '../../graphql/queries/admin/multipleChoice/getMultipleChoiceQuestion.generated';
import { GetUserQuery } from '../../graphql/queries/user/user.generated';
import { Program } from '../../types.generated';

let documentBody: RenderResult;

const mockData: GetMultipleChoiceQuestionQuery = {
  __typename: 'Query',
  multipleChoiceQuestion: {
    id: 'cfa6fc0d-375c-40ab-8556-deaf2dd57c23',
    program: Program.Blockchain,
    module: 'module',
    question: 'This is a sample question?',
    options: [
      {
        id: 'cfa6fc0d-375c-40ab-8556-deaf2dd57c23',
        description: 'First Option',
        isCorrect: true,
      },
      {
        id: 'p9asx9sd-375c-40ab-8556-deaf2dd57c23',
        description: 'Second Option',
        isCorrect: false,
      },
      {
        id: 'v5v5wwdfs-375c-40ab-8556-deaf2dd57c23',
        description: 'Third Option',
        isCorrect: false,
      },
    ],
    createdAt: new Date(1647495451),
  },
};

const mockAuthor: GetUserQuery = {
  __typename: 'Query',
  user: {
    firstName: 'Teofy',
    lastName: 'Rabanes',
  },
};

function MCViewQuestionRender() {
  return <MCQuestionModal data={mockData} author={mockAuthor} />;
}

const renderMCQuestionView = () => {
  const mcViewQuestion = render(
    <MockedProvider addTypename={false}>
      <MCViewQuestionRender />
    </MockedProvider>
  );
  return mcViewQuestion;
};

describe('Multiple Choice Question View', () => {
  beforeEach(() => {
    documentBody = renderMCQuestionView();
  });

  afterEach(cleanup);

  it('should display Question', () => {
    expect(
      documentBody.getByText(/This is a sample question\?/i)
    ).toBeInTheDocument();
  });

  it('should render User that created the question', () => {
    const question = documentBody.getByTestId('question');
    question.click();

    expect(documentBody.getByTestId('created-by')).toBeInTheDocument();
    expect(documentBody.getByText(/Teofy Rabanes/i)).toBeInTheDocument();
  });

  it('should render date the question is created', () => {
    const question = documentBody.getByTestId('question');
    question.click();

    expect(documentBody.getByTestId('date-created')).toBeInTheDocument();
    const expectedDate = 'January 20, 1970, 9:38:15 AM';

    expect(documentBody.getByText(`${expectedDate}`)).toBeInTheDocument();
  });

  it('should display correct answer option', async () => {
    const question = documentBody.getByTestId('question');
    question.click();

    await waitFor(() => {
      expect(documentBody.getByTestId('possible-answer-1')).toBeInTheDocument();
    });
  });

  it('should display first incorrect answer options', async () => {
    const question = documentBody.getByTestId('question');
    question.click();

    await waitFor(() => {
      expect(documentBody.getByTestId('possible-answer-2')).toBeInTheDocument();
      expect(documentBody.getByTestId('possible-answer-3')).toBeInTheDocument();
    });
  });
});

describe('Multiple Choice Options View', () => {
  afterEach(cleanup);
  it('should display correct answers option value', async () => {
    const { getByTestId } = renderMCQuestionView();

    const question = getByTestId('question');
    question.click();

    await waitFor(() => {
      expect(documentBody.getByLabelText('First Option')).toBeInTheDocument();
    });
  });

  it('should display incorrect answers option value', async () => {
    const { getByTestId } = renderMCQuestionView();

    const question = getByTestId('question');
    question.click();

    await waitFor(() => {
      expect(documentBody.getByLabelText('Second Option')).toBeInTheDocument();
      expect(documentBody.getByLabelText('Third Option')).toBeInTheDocument();
    });
  });
});

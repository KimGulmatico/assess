import React from 'react';
import {
  render,
  RenderResult,
  cleanup,
  waitFor,
  fireEvent,
} from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import * as Types from '../types.generated';
import EditExamPage from '../pages/admin/editExam/EditExamPage';
import { AllQuestions } from '../pages/admin/editExam/editExamProps';
import { mockQuestions } from '../__mocks__/data/mockQuestions';
import { createMemoryHistory } from 'history';
import { getExamDateAndTime } from '../utils/dateConverter';
import { timeConverter } from '../utils/timeConverter';

let documentBody: RenderResult;

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
}));

const questions: AllQuestions[] = mockQuestions;
const examQuestions: AllQuestions[] = mockQuestions;

const mockFreeExamData = {
  id: '028f4d90-4c82-40b2-870c-71eebd64760a',
  title: 'Free Type Exam',
  description: 'This is a free type exam',
  program: Types.Program.Blockchain,
  module: 'Module 1',
  year: 2022,
  quarter: Types.Quarter.Q2,
  isPublished: false,
  createdAt: new Date(1647495451),
  updatedAt: new Date(1647495451),
  createdById: 'c9694769-d89c-4bd9-9bdf-d3e238ddb46b',
  startDate: null,
  endDate: null,
  duration: null,
  questions: examQuestions,
  examType: Types.ExamType.Free,
};

const mockFixedExamData = {
  id: '028f4d90-4c82-40b2-870c-71eebd64760a',
  title: 'Fixed Type Exam',
  description: 'This is a fixed type exam',
  program: Types.Program.TechSales,
  module: 'Module 1',
  year: 2024,
  quarter: Types.Quarter.Q1,
  isPublished: false,
  createdAt: new Date(1647495451),
  updatedAt: new Date(1647495451),
  createdById: 'c9694769-d89c-4bd9-9bdf-d3e238ddb46b',
  startDate: 1651550484000,
  endDate: 1651741293000,
  duration: null,
  questions: examQuestions,
  examType: Types.ExamType.Fixed,
};

const mockFlexibleExamData = {
  id: '028f4d90-4c82-40b2-870c-71eebd64760a',
  title: 'Flexible Type Exam',
  description: 'This is a flexible type exam',
  program: Types.Program.FullStack,
  module: 'Module 1',
  year: 2023,
  quarter: Types.Quarter.Q4,
  isPublished: false,
  createdAt: new Date(1647495451),
  updatedAt: new Date(1647495451),
  createdById: 'c9694769-d89c-4bd9-9bdf-d3e238ddb46b',
  startDate: null,
  endDate: null,
  duration: 120,
  questions: examQuestions,
  examType: Types.ExamType.Flexible,
};

const mockNoQuestionsExamData = {
  id: '028f4d90-4c82-40b2-870c-71eebd64760a',
  title: 'Free Type Exam',
  description: 'This is a free type exam',
  program: Types.Program.Blockchain,
  module: 'Module 1',
  year: 2022,
  quarter: Types.Quarter.Q2,
  isPublished: false,
  createdAt: new Date(1647495451),
  updatedAt: new Date(1647495451),
  createdById: 'c9694769-d89c-4bd9-9bdf-d3e238ddb46b',
  startDate: null,
  endDate: null,
  duration: null,
  questions: [],
  examType: Types.ExamType.Free,
};

function renderEditFreeExam() {
  return render(
    <MockedProvider addTypename={false}>
      <EditExamPage questions={questions} exam={mockFreeExamData} />
    </MockedProvider>
  );
}

function renderEditFixedExam() {
  return render(
    <MockedProvider addTypename={false}>
      <EditExamPage questions={questions} exam={mockFixedExamData} />
    </MockedProvider>
  );
}

function renderEditFlexibleExam() {
  return render(
    <MockedProvider addTypename={false}>
      <EditExamPage questions={questions} exam={mockFlexibleExamData} />
    </MockedProvider>
  );
}

function renderEditExamNoExamQuestions() {
  return render(
    <MockedProvider addTypename={false}>
      <EditExamPage questions={questions} exam={mockNoQuestionsExamData} />
    </MockedProvider>
  );
}

function renderEditExamNoQuestions() {
  return render(
    <MockedProvider addTypename={false}>
      <EditExamPage questions={[]} exam={mockFreeExamData} />
    </MockedProvider>
  );
}

describe('Edit Free Type Exam Header and Details', () => {
  beforeEach(() => {
    documentBody = renderEditFreeExam();
  });

  afterEach(cleanup);

  it('should display exam title', () => {
    expect(documentBody.getByText('Free Type Exam')).toBeInTheDocument();
  });

  it('should display exams program chip', () => {
    expect(documentBody.getByText('Blockchain')).toBeInTheDocument();
  });

  it('should display exams module chip', () => {
    expect(documentBody.getByText('Module 1')).toBeInTheDocument();
  });

  it('should display exams cohort chip', () => {
    expect(documentBody.getByText('2022Q2')).toBeInTheDocument();
  });

  it('should render number of items', () => {
    const questionsLength = examQuestions.length;
    expect(documentBody.getByText(questionsLength)).toBeInTheDocument();
  });

  it('should display exam description', () => {
    expect(
      documentBody.getByText('This is a free type exam')
    ).toBeInTheDocument();
  });

  it('should display exam questions', () => {
    expect(
      documentBody.getByText('This is a sample question 1')
    ).toBeInTheDocument();

    expect(
      documentBody.getByText('This is a sample question 2')
    ).toBeInTheDocument();
  });
});

describe('Edit Fixed Type Exam Header and Details', () => {
  beforeEach(() => {
    documentBody = renderEditFixedExam();
  });

  afterEach(cleanup);

  it('should display exam title', () => {
    expect(documentBody.getByText('Fixed Type Exam')).toBeInTheDocument();
  });

  it('should display exams program chip', () => {
    expect(documentBody.getByText('Tech sales')).toBeInTheDocument();
  });

  it('should display exams module chip', () => {
    expect(documentBody.getByText('Module 1')).toBeInTheDocument();
  });

  it('should display exams cohort chip', () => {
    expect(documentBody.getByText('2024Q1')).toBeInTheDocument();
  });

  it('should render available text', () => {
    expect(documentBody.getByTestId('available')).toBeInTheDocument();
  });

  it('should render converted start date', () => {
    const examDateAndTime = getExamDateAndTime(mockFixedExamData.startDate);
    expect(documentBody.getByText(examDateAndTime)).toBeInTheDocument();
  });

  it('should render due text', () => {
    expect(documentBody.getByTestId('due')).toBeInTheDocument();
  });

  it('should render converted end date', () => {
    const examDateAndTime = getExamDateAndTime(mockFixedExamData.endDate);
    expect(documentBody.getByText(examDateAndTime)).toBeInTheDocument();
  });

  it('should render number of items', () => {
    const questionsLength = examQuestions.length;
    expect(documentBody.getByText(questionsLength)).toBeInTheDocument();
  });

  it('should display exam description', () => {
    expect(
      documentBody.getByText('This is a fixed type exam')
    ).toBeInTheDocument();
  });

  it('should display exam questions', () => {
    expect(
      documentBody.getByText('This is a sample question 1')
    ).toBeInTheDocument();

    expect(
      documentBody.getByText('This is a sample question 2')
    ).toBeInTheDocument();
  });
});

describe('Edit Flexible Type Exam Header and Details', () => {
  beforeEach(() => {
    documentBody = renderEditFlexibleExam();
  });

  afterEach(cleanup);

  it('should display exam title', () => {
    expect(documentBody.getByText('Flexible Type Exam')).toBeInTheDocument();
  });

  it('should display exams program chip', () => {
    expect(documentBody.getByText('Full stack')).toBeInTheDocument();
  });

  it('should display exams module chip', () => {
    expect(documentBody.getByText('Module 1')).toBeInTheDocument();
  });

  it('should display exams cohort chip', () => {
    expect(documentBody.getByText('2023Q4')).toBeInTheDocument();
  });

  it('should render duration text', () => {
    expect(documentBody.getByTestId('duration')).toBeInTheDocument();
  });

  it('should render converted duration', () => {
    const convertedTime = timeConverter(mockFlexibleExamData.duration);
    expect(documentBody.getByText(convertedTime)).toBeInTheDocument();
  });

  it('should render number of items', () => {
    const questionsLength = examQuestions.length;
    expect(documentBody.getByText(questionsLength)).toBeInTheDocument();
  });

  it('should display exam description', () => {
    expect(
      documentBody.getByText('This is a flexible type exam')
    ).toBeInTheDocument();
  });

  it('should display exam questions', () => {
    expect(
      documentBody.getByText('This is a sample question 1')
    ).toBeInTheDocument();

    expect(
      documentBody.getByText('This is a sample question 2')
    ).toBeInTheDocument();
  });
});

describe('Edit Exam Page Question Bank', () => {
  beforeEach(() => {
    documentBody = renderEditFreeExam();
  });

  afterEach(cleanup);

  it('should display Questions Heading', () => {
    expect(documentBody.getByText('Questions')).toBeInTheDocument();
  });

  it('should display table ', () => {
    expect(documentBody.getByRole('table')).toBeInTheDocument();
  });
});

describe('Edit Exam Page Action Buttons', () => {
  beforeEach(() => {
    documentBody = renderEditFreeExam();
  });

  afterEach(cleanup);

  it('should display save button', () => {
    expect(
      documentBody.getByRole('button', { name: 'Save' })
    ).toBeInTheDocument();
  });

  it('should display cancel button', () => {
    expect(
      documentBody.getByRole('button', { name: 'Cancel' })
    ).toBeInTheDocument();
  });
});

describe('Edit Exam Page Empty Exam Questions', () => {
  beforeEach(() => {
    documentBody = renderEditExamNoExamQuestions();
  });

  afterEach(cleanup);

  it('should display empty message', () => {
    expect(
      documentBody.getByText('There are no questions here!')
    ).toBeInTheDocument();
  });

  it('should display button to import questions', () => {
    expect(
      documentBody.getByRole('button', { name: 'Import questions' })
    ).toBeInTheDocument();
  });
});

describe('Edit Exam Page Empty Question Bank Questions', () => {
  beforeEach(() => {
    documentBody = renderEditExamNoQuestions();
  });

  afterEach(cleanup);

  it('should display empty message', () => {
    expect(
      documentBody.getByText(
        'No questions have been added under this program yet'
      )
    ).toBeInTheDocument();
  });
});

describe('Edit Exam Page add and save exam questions', () => {
  beforeEach(() => {
    documentBody = renderEditExamNoExamQuestions();
  });

  afterEach(cleanup);

  it('should add questions and click save button', async () => {
    const addQuestionButton1 = documentBody.getByTestId('add-question-1');
    const addQuestionButton2 = documentBody.getByTestId('add-question-2');
    addQuestionButton1.click();
    addQuestionButton2.click();

    const saveButton = documentBody.getByRole('button', { name: 'Save' });
    fireEvent.click(saveButton);

    const confirmButton = documentBody.getByRole('button', { name: 'Confirm' });
    fireEvent.click(confirmButton);

    await waitFor(() => {
      const history = createMemoryHistory({ initialEntries: ['/exambank'] });
      expect(history.location.pathname).toBe('/exambank');
    });
  });
});

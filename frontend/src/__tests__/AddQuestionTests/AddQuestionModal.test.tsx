import React from 'react';
import {
  render,
  RenderResult,
  cleanup,
  waitFor,
  within,
  fireEvent,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MockedProvider } from '@apollo/client/testing';
import AddQuestionModal from '../../components/modal/admin/AddQuestion/AddQuestionModal';

let documentBody: RenderResult;

function renderAddQuestionModal() {
  return render(
    <MockedProvider>
      <AddQuestionModal data={[]} />
    </MockedProvider>
  );
}

describe('<AddQuestionModal />', () => {
  beforeEach(() => {
    documentBody = renderAddQuestionModal();
  });

  afterEach(cleanup);

  it('renders add question button', () => {
    expect(documentBody.getByTestId('add-question')).toBeInTheDocument();
  });

  it('opens a modal after button click', async () => {
    const button = documentBody.getByTestId('add-question');
    button.click();

    await waitFor(() => {
      expect(
        documentBody.getByTestId('add-question-modal')
      ).toBeInTheDocument();
      expect(documentBody.getByText('Question Type')).toBeInTheDocument();
    });
  });

  it('allows to choose program from dropdown menu', async () => {
    const button = documentBody.getByTestId('add-question');
    button.click();
    const select = documentBody.getByTestId('select-program');
    userEvent.click(within(select).getByText('Blockchain'));

    await waitFor(() => {
      expect(
        (
          documentBody.getByRole('option', {
            name: 'Cybersecurity',
          }) as HTMLOptionElement
        ).selected
      ).toBe(false);
      expect(
        (
          documentBody.getByRole('option', {
            name: 'Blockchain',
          }) as HTMLOptionElement
        ).selected
      ).toBe(true);
      expect(
        (
          documentBody.getByRole('option', {
            name: 'Full Stack',
          }) as HTMLOptionElement
        ).selected
      ).toBe(false);
      expect(
        (
          documentBody.getByRole('option', {
            name: 'Tech Sales',
          }) as HTMLOptionElement
        ).selected
      ).toBe(false);
    });
  });

  it('renders module label', () => {
    const button = documentBody.getByTestId('add-question');
    button.click();
    expect(documentBody.getByTestId('module-label')).toBeInTheDocument();
  });

  it('renders the textfield for module label', () => {
    const button = documentBody.getByTestId('add-question');
    button.click();
    expect(
      documentBody.getByPlaceholderText('Type module here')
    ).toBeInTheDocument();
  });

  it('should change module text input', () => {
    const button = documentBody.getByTestId('add-question');
    button.click();
    const moduleInput = documentBody.getByTestId('module');
    fireEvent.change(moduleInput, {
      target: { value: 'changed module input' },
    });

    expect(moduleInput).toHaveValue('changed module input');
  });

  it('allows to choose a question type from dropdown menu', async () => {
    const button = documentBody.getByTestId('add-question');
    button.click();
    const select = documentBody.getByTestId('select-question-type');
    userEvent.click(within(select).getByText('Multiple Choice'));

    await waitFor(() => {
      expect(
        (
          documentBody.getByRole('option', {
            name: 'Multiple Choice',
          }) as HTMLOptionElement
        ).selected
      ).toBe(true);
      expect(
        (
          documentBody.getByRole('option', {
            name: 'Multiple Selection',
          }) as HTMLOptionElement
        ).selected
      ).toBe(false);
    });
  });
});

import React from 'react';
import { render, RenderResult, cleanup } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import MultipleChoiceComponent from '../../components/modal/admin/AddQuestion/MCQuestion/MCQComponent';
import CREATE_MC_QUESTION_MUTATION from '../../graphql/mutations/admin/multipleChoice/question/createMultipleChoiceQuestion';
import { Program } from '../../types.generated';

let documentBody: RenderResult;

const questionContent = 'Sample question';

const optionsContent = [
  { description: 'choice 1', isCorrect: false },
  { description: 'choice 2', isCorrect: true },
  { description: 'choice 3', isCorrect: false },
];

const mockMCQData = [
  {
    request: {
      query: CREATE_MC_QUESTION_MUTATION,
      variables: {
        input: {
          question: questionContent,
          options: optionsContent,
        },
      },
    },
    result: {
      data: {
        createMultipleChoiceQuestion: {
          question: 'Sample question',
          options: [
            {
              id: '5917f5a2-eb10-45b7-bd2e-c1d07c70cb5a',
              description: 'choice 1',
              isCorrect: false,
              __typename: 'Option',
            },
            {
              id: 'b6c6a8c7-9a5e-4d10-b9cb-534e64524f7a',
              description: 'choice 2',
              isCorrect: true,
              __typename: 'Option',
            },
            {
              id: '520e4301-835b-4a7f-ba32-a07a05818105',
              description: 'choice 3',
              isCorrect: false,
              __typename: 'Option',
            },
          ],
          __typename: 'MultipleChoiceQuestion',
        },
      },
    },
  },
];

function renderMultipleChoiceComponent() {
  return render(
    <MockedProvider mocks={mockMCQData}>
      <MultipleChoiceComponent program={Program.Blockchain} module="module" />
    </MockedProvider>
  );
}

describe('<MultipleChoiceComponent />', () => {
  beforeEach(() => {
    documentBody = renderMultipleChoiceComponent();
  });

  afterEach(cleanup);

  it('renders the label', () => {
    expect(documentBody.getByTestId('mcq-question-label')).toBeInTheDocument();
  });

  it('renders the textfield for question label', () => {
    expect(
      documentBody.getByPlaceholderText('Type question here')
    ).toBeInTheDocument();
  });

  it('renders textfield for choice options', () => {
    expect(documentBody.getByTestId('mcq-option')).toBeInTheDocument();
  });

  it('renders the add button for choices', () => {
    expect(
      documentBody.getByRole('button', { name: 'Add' })
    ).toBeInTheDocument();
  });

  it('renders the save question button', () => {
    expect(
      documentBody.getByRole('button', { name: 'Save Question' })
    ).toBeInTheDocument();
  });
});

import React from 'react';
import { render, RenderResult, cleanup } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import MultipleSelectionComponent from '../../components/modal/admin/AddQuestion/MSQuestion/MSQComponent';
import CREATE_MS_QUESTION_MUTATION from '../../graphql/mutations/admin/multipleSelection/question/createMultipleSelectionQuestion';
import { Program } from '../../types.generated';

let documentBody: RenderResult;

const questionContent = 'select all that are not flowers';

const choicesContent = [
  { description: 'sun', isCorrect: true },
  { description: 'rose', isCorrect: false },
  { description: 'daisy', isCorrect: false },
  { description: 'star', isCorrect: true },
  { description: 'dahlia', isCorrect: false },
  { description: 'bahu-bahu', isCorrect: false },
  { description: 'aloe vera', isCorrect: true },
  { description: 'oregano', isCorrect: true },
];

const mockMSQData = [
  {
    request: {
      query: CREATE_MS_QUESTION_MUTATION,
      variables: {
        input: {
          question: questionContent,
          choices: choicesContent,
        },
      },
    },
    result: {
      data: {
        createMultipleSelectionQuestion: {
          question: 'select all that are not flowers',
          choices: [
            {
              id: 'b8cba8cd-4531-420b-8579-a0e5200a9883',
              description: 'sun',
              isCorrect: true,
              __typename: 'Choice',
            },
            {
              id: '32c96b3b-ac2b-4e27-a58c-4ce73dbeb0e3',
              description: 'rose',
              isCorrect: false,
              __typename: 'Choice',
            },
            {
              id: 'ddea029f-b807-43a1-96b2-198c525a70f4',
              description: 'daisy',
              isCorrect: false,
              __typename: 'Choice',
            },
            {
              id: 'e92fa355-831b-414e-b940-7993ff13051c',
              description: 'star',
              isCorrect: true,
              __typename: 'Choice',
            },
            {
              id: 'b0525d92-ff0e-4396-a780-90389098707e',
              description: 'dahlia',
              isCorrect: false,
              __typename: 'Choice',
            },
            {
              id: '070b84c2-e193-4b19-a2da-309b139be5b4',
              description: 'bahu-bahu',
              isCorrect: false,
              __typename: 'Choice',
            },
            {
              id: '2aef3acd-39b5-46a3-bfda-e2ac92f84328',
              description: 'aloe vera',
              isCorrect: true,
              __typename: 'Choice',
            },
            {
              id: '9babd8af-c21e-4c7e-bd8e-2f967841184e',
              description: 'oregano',
              isCorrect: true,
              __typename: 'Choice',
            },
          ],
          __typename: 'MultipleSelectionQuestion',
        },
      },
    },
  },
];

function renderMultipleSelectionComponent() {
  return render(
    <MockedProvider mocks={mockMSQData}>
      <MultipleSelectionComponent
        program={Program.Blockchain}
        module="module"
      />
    </MockedProvider>
  );
}

describe('<MultipleSelectionComponent />', () => {
  beforeEach(() => {
    documentBody = renderMultipleSelectionComponent();
  });

  afterEach(cleanup);

  it('renders the label', () => {
    expect(documentBody.getByTestId('msq-question-label')).toBeInTheDocument();
  });

  it('renders the textfield for question label', () => {
    expect(
      documentBody.getByPlaceholderText('Type question here')
    ).toBeInTheDocument();
  });

  it('renders textfield for choice options', () => {
    expect(documentBody.getByTestId('msq-choice')).toBeInTheDocument();
  });

  it('renders the add button for choices', () => {
    expect(
      documentBody.getByRole('button', { name: 'Add' })
    ).toBeInTheDocument();
  });

  it('renders the save question button', () => {
    expect(
      documentBody.getByRole('button', { name: 'Save Question' })
    ).toBeInTheDocument();
  });
});

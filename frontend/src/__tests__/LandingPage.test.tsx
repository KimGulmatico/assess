import React from 'react';
import { render, RenderResult, cleanup } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import LandingPage from '../pages/landingPage/LandingPage';

let documentBody: RenderResult;

function renderLandingPage() {
  return render(
    <Router>
      <LandingPage />
    </Router>
  );
}

describe('Landing Page', () => {
  beforeEach(() => {
    documentBody = renderLandingPage();
  });

  afterEach(cleanup);

  it('should display welcome text', () => {
    expect(documentBody.getByText('Welcome,')).toBeInTheDocument();
  });

  it('should display kingsland students text', () => {
    expect(documentBody.getByText('Kingsland Students!')).toBeInTheDocument();
  });

  it('should display about the platform content text', () => {
    expect(
      documentBody.getByText(
        `As a Kingsland student, you will take your exams on this platform. We tailor your exam taking experience based on your cohort and we’ll add more features in the months to come to ensure that you get the support you need to be successful in your chosen field.`
      )
    ).toBeInTheDocument();
  });

  it('should display about the platform image', () => {
    expect(documentBody.getByAltText('about-image')).toBeInTheDocument();
  });

  it('should display about the platform title', () => {
    expect(documentBody.getByText('About the platform')).toBeInTheDocument();
  });

  it('should display features title', () => {
    expect(documentBody.getByText('Features')).toBeInTheDocument();
  });

  it('should display feature 1 text', () => {
    expect(
      documentBody.getByText('Streamlined Experience')
    ).toBeInTheDocument();
  });

  it('should display description 1 text', () => {
    expect(
      documentBody.getByText(
        'Your exam taking experience is consistent across Kingsland platforms and cohorts.'
      )
    ).toBeInTheDocument();
  });

  it('should display feature 2 text', () => {
    expect(documentBody.getByText('Tailored Assessments')).toBeInTheDocument();
  });

  it('should display description 2 text', () => {
    expect(
      documentBody.getByText(
        'Custom tools are provided to instructors to create unique exams and accurate assessments.'
      )
    ).toBeInTheDocument();
  });

  it('should display feature 3 text', () => {
    expect(documentBody.getByText('... And more to come!')).toBeInTheDocument();
  });

  it('should display description 3 text', () => {
    expect(
      documentBody.getByText(
        'We envision that this platform will become a good assessment ground for our students and instructors.'
      )
    ).toBeInTheDocument();
  });

  it('should display support title', () => {
    expect(documentBody.getByText('Support')).toBeInTheDocument();
  });

  it('should display support content 1 text', () => {
    expect(
      documentBody.getByText(
        `If you encounter any issues, please reach out to your cohorts respective #technical-questions channel.`
      )
    ).toBeInTheDocument();
  });

  it('should display support content 2 text', () => {
    expect(
      documentBody.getByText(
        `You may also write us a mail at support@kingslanduniversity.com`
      )
    ).toBeInTheDocument();
  });
});

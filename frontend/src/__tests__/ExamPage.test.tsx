import React from 'react';
import { render, RenderResult, cleanup } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import ExamsPage from '../pages/admin/examBank/ExamsPage';

const mockedUsedNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
}));

let documentBody: RenderResult;

function renderExamsPage() {
  return render(
    <MockedProvider>
      <ExamsPage />
    </MockedProvider>
  );
}

describe('Exam Page', () => {
  beforeEach(() => {
    documentBody = renderExamsPage();
  });

  afterEach(cleanup);

  it('should display exam page title', () => {
    expect(documentBody.getByText('Exam Bank')).toBeInTheDocument();
  });
});

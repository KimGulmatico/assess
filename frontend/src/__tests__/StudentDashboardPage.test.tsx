import React from 'react';
import { render, RenderResult, cleanup } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import StudentDashboardPage from '../pages/student/StudentDashboardPage';

const mockedUsedNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
}));

let documentBody: RenderResult;

function renderExamsPage() {
  return render(
    <MockedProvider>
      <StudentDashboardPage />
    </MockedProvider>
  );
}

describe('Exam Page', () => {
  beforeEach(() => {
    documentBody = renderExamsPage();
  });

  afterEach(cleanup);

  it('should display exam page title', () => {
    expect(documentBody.getByText('Student Dashboard')).toBeInTheDocument();
  });
});

import React from 'react';
import { render, RenderResult, cleanup } from '@testing-library/react';
import Footer from '../layouts/footer/Footer';

describe('Footer Components', () => {
  let documentBody: RenderResult;

  function renderFooter() {
    return render(<Footer />);
  }

  beforeEach(() => {
    documentBody = renderFooter();
  });

  afterEach(cleanup);

  it('should render kingsland university logo img', () => {
    expect(documentBody.getByAltText('logo')).toBeInTheDocument();
  });

  it('should render copyright text', () => {
    expect(documentBody.getByText(/Copyright ©/i)).toBeInTheDocument();
  });

  it('should render kingsland university link text', () => {
    expect(documentBody.getByText(/kingsland university/i)).toBeInTheDocument();
  });

  it('should navigate to kingsland university website when link is clicked', () => {
    expect(
      documentBody.getByText(/kingsland university/i).closest('a')
    ).toHaveAttribute('href', 'https://kingslanduniversity.com/');
  });
});

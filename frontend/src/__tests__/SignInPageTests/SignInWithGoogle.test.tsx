import React, { useState } from 'react';
import {
  render,
  RenderResult,
  cleanup,
  fireEvent,
  waitFor,
} from '@testing-library/react';
import { GoogleLogin } from 'react-google-login';

let documentBody: RenderResult;

const GoogleSignInButton = (props: any) => {
  const [isSignedIn, setIsSignedIn] = useState(true);
  const responseGoogle = isSignedIn ? 'name: Jane Doe' : 'Sign in failed';

  return (
    <div>
      <GoogleLogin
        onSuccess={() => {
          setIsSignedIn(true);
        }}
        {...props}
      />
      {responseGoogle}
    </div>
  );
};

function renderGoogleButton(props: any) {
  return render(<GoogleSignInButton {...props} />);
}

describe('<GoogleSignInButton /> with default props', () => {
  const props = {
    clientId: 'ad1csknxaxw12Adb',
    onSuccess() {},
    onFailure() {},
  };
  beforeEach(() => {
    documentBody = renderGoogleButton(props);
  });

  afterEach(cleanup);

  it('renders the sign in with google button', () => {
    expect(
      documentBody.getByRole('button', { name: 'Sign in with Google' })
    ).toBeInTheDocument();
  });

  it('does not have a class attribute', () => {
    expect(documentBody.getByRole('button')).not.toHaveClass();
  });

  it('displays a button type element', () => {
    expect(documentBody.getByRole('button')).toHaveAttribute('type', 'button');
  });
});

describe('<GoogleSignInButton /> with custom props and attributes', () => {
  const className = 'sign-in-button';
  const buttonText = 'Login';

  const props = {
    clientId: 'ad1csknxaxw12Adb',
    onSuccess() {},
    onFailure() {},
    className,
    buttonText,
  };

  beforeEach(() => {
    documentBody = renderGoogleButton(props);
  });

  afterEach(cleanup);

  it('has class name', () => {
    expect(documentBody.getByRole('button')).toHaveClass('sign-in-button');
  });

  it('has custom button text', () => {
    expect(documentBody.getByRole('button')).toHaveTextContent('Login');
  });
});

describe('<GoogleSignInButton /> onSuccess state', () => {
  const props = {
    clientId: 'ad1csknxaxw12Adb',
    onSuccess() {},
    onFailure() {},
  };

  beforeEach(() => {
    documentBody = renderGoogleButton(props);
  });

  afterEach(cleanup);

  it('shows response supposing sign in was successful', async () => {
    fireEvent.click(documentBody.getByRole('button'));

    await waitFor(() => {
      expect(documentBody.getByText('name: Jane Doe')).toBeInTheDocument();
    });
  });
});

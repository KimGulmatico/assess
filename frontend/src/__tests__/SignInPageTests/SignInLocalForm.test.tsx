import React from 'react';
import {
  render,
  RenderResult,
  cleanup,
  fireEvent,
  waitFor,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { SignInLocalForm } from '../../components/authentication/local';

let documentBody: RenderResult;

function renderSignInLocalForm() {
  return render(<SignInLocalForm />);
}

describe('<SignInLocalForm />', () => {
  beforeEach(() => {
    documentBody = renderSignInLocalForm();
  });

  afterEach(cleanup);

  it('should render the sign in local form', () => {
    expect(documentBody.getByTestId('signin-local-form')).toBeInTheDocument();
  });

  it('should render the input textfields', () => {
    expect(documentBody.getByLabelText('Email address')).toBeInTheDocument();
    expect(documentBody.getByLabelText('Password')).toBeInTheDocument();
  });

  it('should render sign in loading button', () => {
    expect(
      documentBody.getByRole('button', { name: 'Sign in' })
    ).toBeInTheDocument();
  });
});

describe('<SignInLocalForm /> events', () => {
  beforeEach(() => {
    documentBody = renderSignInLocalForm();
  });

  afterEach(cleanup);

  it('should change input on email address', async () => {
    const emailInput = documentBody.getByLabelText('Email address');
    userEvent.type(emailInput, 'username@gmail.com');
    await waitFor(() => {
      expect(emailInput).toHaveValue('username@gmail.com');
    });
  });

  it('should change input on password', async () => {
    const passwordInput = documentBody.getByLabelText('Password');
    userEvent.type(passwordInput, 'aASDsxsdf!');
    await waitFor(() => {
      expect(passwordInput).toHaveValue('aASDsxsdf!');
    });
  });

  it('should show error messages after blank submission', async () => {
    const signInLocalForm = documentBody.getByTestId('signin-local-form');

    fireEvent.submit(signInLocalForm);

    await waitFor(() => {
      expect(documentBody.getByText('Email is required')).toBeInTheDocument();

      expect(
        documentBody.getByText('Password is required')
      ).toBeInTheDocument();
    });
  });

  it('should show error message after invalid email input case 1', async () => {
    const signInLocalForm = documentBody.getByTestId('signin-local-form');
    const emailInput = documentBody.getByLabelText('Email address');

    fireEvent.change(emailInput, {
      target: { value: 'username' },
    });

    fireEvent.submit(signInLocalForm);

    await waitFor(() => {
      expect(
        documentBody.getByText('Email must be a valid email address')
      ).toBeInTheDocument();
    });
  });

  it('should show error message after invalid email input case 2', async () => {
    const signInLocalForm = documentBody.getByTestId('signin-local-form');
    const emailInput = documentBody.getByLabelText('Email address');

    fireEvent.change(emailInput, {
      target: { value: 'username@' },
    });

    fireEvent.submit(signInLocalForm);

    await waitFor(() => {
      expect(
        documentBody.getByText('Email must be a valid email address')
      ).toBeInTheDocument();
    });
  });

  it('should show error message after invalid email input case 3', async () => {
    const signInLocalForm = documentBody.getByTestId('signin-local-form');
    const emailInput = documentBody.getByLabelText('Email address');

    fireEvent.change(emailInput, {
      target: { value: 'username@.com' },
    });

    fireEvent.submit(signInLocalForm);

    await waitFor(() => {
      expect(
        documentBody.getByText('Email must be a valid email address')
      ).toBeInTheDocument();
    });
  });

  it('should show alert message after sucessful submit', async () => {
    const signInLocalForm = documentBody.getByTestId('signin-local-form');
    const emailInput = documentBody.getByLabelText('Email address');
    const passwordInput = documentBody.getByLabelText('Password');
    const jsdomAlert = window.alert;
    window.alert = () => {};

    fireEvent.change(emailInput, {
      target: { value: 'username@gmail.com' },
    });

    fireEvent.change(passwordInput, {
      target: { value: 'aASDsxsdf!' },
    });

    fireEvent.submit(signInLocalForm);

    await waitFor(() => (window.alert = jsdomAlert));
  });
});

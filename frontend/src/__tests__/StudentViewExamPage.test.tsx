import React from 'react';
import { render, RenderResult, cleanup } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import ExamPage from '../pages/student/exam/ExamPage';
import { mockStudentQuestions } from '../__mocks__/data/mockStudentQuestions';

let documentBody: RenderResult;

const examQuestions = mockStudentQuestions;

const mockExamsData = [
  {
    id: '028f4d90-4c82-40b2-870c-71eebd64760a',
    title: 'Published Exam',
    description: 'This is a free type exam description',
    program: 'Blockchain',
    module: 'module',
    year: 2022,
    quarter: 'Q1',
    maxQuestionCount: 5,
    createdAt: 1646186146000,
    updatedAt: 1646186146000,
    createdById: 'c9694769-d89c-4bd9-9bdf-d3e238ddb46b',
    startDate: null,
    endDate: null,
    duration: null,
    examType: 'FREE',
  },
  {
    id: '028f4d90-4c82-40b2-870c-71eebd64760a',
    title: 'Unpublished Exam',
    description: 'This is a fixed type exam description',
    program: 'Cybersecurity',
    module: 'module',
    year: 2022,
    quarter: 'Q3',
    maxQuestionCount: 5,
    createdAt: 1646186146000,
    updatedAt: 1646186146000,
    createdById: 'c9694769-d89c-4bd9-9bdf-d3e238ddb46b',
    startDate: 1651550484000,
    endDate: 1651741293000,
    duration: null,
    examType: 'FIXED',
  },
];

function renderPublishedExamInExamPage() {
  return render(
    <MockedProvider>
      <ExamPage
        exam={mockExamsData[0]}
        questions={examQuestions}
        publishStatus={true}
      />
    </MockedProvider>
  );
}

function renderUnpublishedExamInExamPage() {
  return render(
    <MockedProvider>
      <ExamPage
        exam={mockExamsData[1]}
        questions={examQuestions}
        publishStatus={false}
      />
    </MockedProvider>
  );
}

describe('Published Exam in Exam Page', () => {
  beforeEach(() => {
    documentBody = renderPublishedExamInExamPage();
  });

  afterEach(cleanup);

  it('should display published exam title', () => {
    expect(documentBody.getByText('Published Exam')).toBeInTheDocument();
  });

  it('should display published exam program', () => {
    expect(documentBody.getByText('Blockchain')).toBeInTheDocument();
  });

  it('should display published exam cohort', () => {
    expect(documentBody.getByText('2022Q1')).toBeInTheDocument();
  });

  it('should render number of items', () => {
    expect(documentBody.getByText('5')).toBeInTheDocument();
  });

  it('should display exam description', () => {
    expect(
      documentBody.getByText('This is a free type exam description')
    ).toBeInTheDocument();
  });

  it('should display take exam button', () => {
    expect(documentBody.getByTestId('take-exam-button')).toBeInTheDocument();
  });
});

describe('Unpublished Exam in Exam Page', () => {
  beforeEach(() => {
    documentBody = renderUnpublishedExamInExamPage();
  });

  afterEach(cleanup);

  it('should display unpublished exam title', () => {
    expect(documentBody.getByText('Unpublished Exam')).toBeInTheDocument();
  });

  it('should display published exam program', () => {
    expect(documentBody.getByText('Cybersecurity')).toBeInTheDocument();
  });

  it('should display published exam cohort', () => {
    expect(documentBody.getByText('2022Q3')).toBeInTheDocument();
  });

  it('should render number of items', () => {
    expect(documentBody.getByText('5')).toBeInTheDocument();
  });

  it('should display unpublished message', () => {
    expect(
      documentBody.getByText('The exam is unpublished and unavailable yet.')
    ).toBeInTheDocument();
  });
});

import React from 'react';
import { render, RenderResult, cleanup } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import LoadingPage from '../components/states/LoadingPage';
import ErrorPage from '../components/states/ErrorPage';
import PageNotFound from '../components/states/PageNotFound';

let documentBody: RenderResult;

function renderLoadingState() {
  return render(<LoadingPage />);
}

function renderErrorState() {
  return render(<ErrorPage errorMessage={undefined} />);
}

function renderPageNotFoundState() {
  return render(
    <Router>
      <PageNotFound redirectTo="/" />
    </Router>
  );
}

describe('Loading Page', () => {
  beforeEach(() => {
    documentBody = renderLoadingState();
  });

  afterEach(cleanup);

  it('should display loader spinner', () => {
    expect(documentBody.getByAltText('loader')).toBeInTheDocument();
  });
});

describe('Error Page', () => {
  beforeEach(() => {
    documentBody = renderErrorState();
  });

  afterEach(cleanup);

  it('should display error text', () => {
    expect(documentBody.getByText('Error')).toBeInTheDocument();
  });

  it('should display something went wrong text', () => {
    expect(
      documentBody.getByText('Something went wrong :(')
    ).toBeInTheDocument();
  });

  it('should display try again text', () => {
    expect(
      documentBody.getByText('You may check your connection and try again.')
    ).toBeInTheDocument();
  });
});

describe('Page not found', () => {
  beforeEach(() => {
    documentBody = renderPageNotFoundState();
  });

  afterEach(cleanup);

  it('should display error text', () => {
    expect(documentBody.getByText('404 Error')).toBeInTheDocument();
  });

  it('should display page not found text', () => {
    expect(
      documentBody.getByText('Sorry, page not found :(')
    ).toBeInTheDocument();
  });
});

# [2.3.0](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v2.2.0...v2.3.0) (2022-11-07)


### Features

* session expired handler ([dca32be](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/dca32be893eb600c51d9208ea86a3edfadccc01d))



# [2.2.0](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v1.8.0...v2.2.0) (2022-10-12)


### Bug Fixes

* **exam, subscription:** fix bad file name ([26150dd](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/26150dd07fab3a71f715975732a0d47f00c1fa15))


### Features

* **Admin Exam:** View student individual exam ([be054a5](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/be054a55c03d2b484577cc6d13df458ba3287ce9))
* **exam email notification:** implement exam notification for admin ([465d3cd](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/465d3cd04f620b22e06e5de5a4b006adabc47a93))
* **exam subscription:** add subscribe & unsubscribe error handler ([c67bf84](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/c67bf84cf0450d034aa53786da855be56528d7e0))
* **exam subscription:** implement exam subscription for admin ([3ef143b](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/3ef143bffd2dd813ccfb090a72a2923c4f75c30d))
* **Exam Subscription:** Improve email notification UX ([72c7220](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/72c7220416fed94de176692fca643d0532e9d84a))
* **Student Exams List:** Implement student exams list in student dashboard page ([cb444dd](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/cb444dd6073317ca0915c94623f6e4483b8371c5))



# [2.1.0](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v2.0.0...v2.1.0) (2022-09-28)


### Features

* **exam email notification:** implement exam notification for admin ([465d3cd](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/465d3cd04f620b22e06e5de5a4b006adabc47a93))
* **exam subscription:** add subscribe & unsubscribe error handler ([c67bf84](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/c67bf84cf0450d034aa53786da855be56528d7e0))
* **exam subscription:** implement exam subscription for admin ([3ef143b](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/3ef143bffd2dd813ccfb090a72a2923c4f75c30d))



# [2.0.0](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v1.6.0...v2.0.0) (2022-07-18)


### Bug Fixes

* **Filters:** changed filter to rely on backend to fix numerous issues ([9636add](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/9636add69e81ec928c8c482b379306963a572df2))
* **Questions Table:** applied pagination to search ([fe85f44](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/fe85f44b1db2b42bd7866bc670c44fbe27a373ef))


### Features

* **Admin Exam:** View student individual exam ([be054a5](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/be054a55c03d2b484577cc6d13df458ba3287ce9))
* **Exam:** save on answer, answers loading, question consistency ([42025d8](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/42025d8779b1f8e955ac0145980e1a6f9286baa2))



# [1.8.0](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v1.7.0...v1.8.0) (2022-07-08)


### Bug Fixes

* **Questions Table:** applied pagination to search ([fe85f44](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/fe85f44b1db2b42bd7866bc670c44fbe27a373ef))


### Features

* **Exam:** save on answer, answers loading, question consistency ([42025d8](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/42025d8779b1f8e955ac0145980e1a6f9286baa2))



# [1.7.0](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v1.6.0...v1.7.0) (2022-06-27)


### Bug Fixes

* **Filters:** changed filter to rely on backend to fix numerous issues ([9636add](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/9636add69e81ec928c8c482b379306963a572df2))



# [1.6.0](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v1.3.0...v1.6.0) (2022-06-17)


### Bug Fixes

* **Questions Formatting:** preserve formatting for MSQ and MCQ questions containing code ([8c89f66](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/8c89f6631d5c2f4b6c5ecf688b1bc67a2097a363))


### Features

* **Admin:** Misc UX improvements ([9209f3f](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/9209f3f9e2227a5e0d31d9dce97624d57fd563a2))
* **Exam Taking:** Submit automatically on time out ([66de735](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/66de7352dc28280f86b74c0e462a6413c7ebf7b4))
* **Filters:** Filters Reusable component, Applied in Question Bank ([2054d05](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/2054d0581cd8ec85e910ce961eaa58e57bf07ce5))
* **Timed Exams:** Added OpenedAt for timed exams, locking in duration ([5788d13](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/5788d136991a2e4da45957416fa9070247ac19c8))



# [1.5.0](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v1.4.1...v1.5.0) (2022-06-10)


### Bug Fixes

* **Questions Formatting:** preserve formatting for MSQ and MCQ questions containing code ([8c89f66](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/8c89f6631d5c2f4b6c5ecf688b1bc67a2097a363))



## [1.4.1](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v1.4.0...v1.4.1) (2022-06-09)


### Features

* **Admin:** Misc UX improvements ([9209f3f](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/9209f3f9e2227a5e0d31d9dce97624d57fd563a2))



# [1.4.0](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v1.3.0...v1.4.0) (2022-06-07)


### Features

* **Exam Taking:** Submit automatically on time out ([66de735](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/66de7352dc28280f86b74c0e462a6413c7ebf7b4))
* **Timed Exams:** Added OpenedAt for timed exams, locking in duration ([5788d13](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/5788d136991a2e4da45957416fa9070247ac19c8))



# [1.3.0](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v1.2.1...v1.3.0) (2022-05-26)


### Bug Fixes

* **user:** add null checker ([3c2f498](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/3c2f49885badf2d1d0c84a2172bfead04f034b87))


### Features

* **exam:** Implement flexible exam behavior and grade view ([bdb7d11](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/bdb7d113655564a75b615441779681031b69692b))



## [1.2.1](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v1.2.0...v1.2.1) (2022-05-26)



# [1.2.0](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v1.1.0...v1.2.0) (2022-05-25)


### Bug Fixes

* **Resources and Landing Page:** fix landing page texts and icons, and added resources pages ([a2e7873](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/a2e7873c64f106d683c8013f12dd84b9971e4aca))


### Features

* **Routing:** Refactored routing ([07a8af8](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/07a8af8445f73c908ffd5407f3aebe76318c8431))
* **User and Student Exam Fields:** Added roles on Google Sign in payload and Submission Details on student exam ([c622623](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/c622623019769b33ee204980e5714d83aeb42f2b))



# [1.1.0](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v1.0.1...v1.1.0) (2022-05-24)


### Bug Fixes

* **Utils:** Fix eslint errors in utils functions ([ce4beb3](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/ce4beb30288b5a8c5334cf654781f8c558d318bb))


### Features

* **Student Exam:** added student exam page, answer exams ([1c53acb](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/1c53acb779769f999516d76f19ce7cb5bbbf1111))



## [1.0.1](https://gitlab.com/kingsland-team-ph/kingsland-assess/compare/v1.0.0...v1.0.1) (2022-05-19)



# 1.0.0 (2022-05-19)


### Bug Fixes

* **Exams List:** Edit exams list table and created add exam button ([8ebb97c](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/8ebb97c387de21f58dccdc347159fca2c41d1fcb))
* landing page component bug ([c5e142e](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/c5e142e9c599085022ee52868bac9b91acca635a))
* **MSQ & MCQ Edit Bug:** Fix edit question bug ([89be92e](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/89be92e99fefc06da6552e897073870042faca1b))
* **MSQ:** Fix MSQ component bug when removing choice ([1272523](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/12725233ec897abfbb67f20a6ef82a4ae2f5e0fc))
* **Side Nav Function:** fix and test side nav functions ([2c3e111](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/2c3e11128b50255c2bb1f308e1e7fd931e945a8b))
* **Side Nav:** edit side navigation display, add test cases ([ef34243](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/ef3424317e57c7b89d1eb0aa6e61fcc25df6f529))
* **Tables and Modals:** edit exam and question tables and modals styles ([bba5e6c](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/bba5e6c48f38be3c2016c291a490d1f5b6d2fbde))


### Features

* **Add and View Exam:** Implement add and view exams to exam bank ([59f345e](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/59f345e8bfbf1e87db3d04bec922af20bbea9a69))
* **Add Created By:** added created by on exams table ([828edbc](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/828edbcc88c6d903f59f1744b35dd49dcfdacac2))
* **Add Exam:** created and test add exam modal ([1acf6a5](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/1acf6a5d141e3f31cb8079a537e73eeae45dd44f))
* **Add Exam:** Implement create free type exam and all exams query ([9a83a76](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/9a83a76593b2621d9226ba89ee466a0cd63d01f5))
* **Add Exams and tests:** Added created by id on exams and missing tests ([7ee42c1](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/7ee42c127990e8123e08cda5bf29a8623440941b))
* **Add Question:** CRUD MCQ & view all questions ([85e7b08](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/85e7b08d044bfc22789bd4d3d18e95fbb8cb5a93))
* **Add Question:** CRUD Multiple Selection & Code questions ([76b1388](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/76b138872c9fe180bf684f414dc6bd45430c8b0c))
* **Add Questions Exam:** Create Edit Exam Page component, Implement add and remove questions to Exam. ([c3cb9cd](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/c3cb9cd288cc9e5fa1f9fae5a6edfb6c34d95a01))
* added landing page and edit global theme ([35e45d2](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/35e45d2544989665d287f79ed81aee984bac2c53))
* **Added missing fields & queries:** Added program, module, maxQuestionCount columns and  filters on question queries ([e1ad23e](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/e1ad23e4067502e1dd1a5ba4c63496c02ca47ad4))
* **Admin Routing:** add routing for admin ([752f0fe](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/752f0fe9a045e52aff504a9362d1e219603e9b5c))
* **AdminDashboard:** added admin dashboard and exams list table ([72426e0](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/72426e050253d14af5de080442c2ece857330ae8))
* **Authentication:** Implement Google Sign in ([ea8f7c8](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/ea8f7c8cea42c4e9d3c46e3efc415972d8021d3a))
* **CodeQuestion:** Create Code Question component and Code Question modal ([5d9c088](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/5d9c088d2aab12ea4242c6fcd4d64ed9d57f5e92))
* create app bar ([b9c2e33](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/b9c2e334899e73fea383e9ece90bdd7d8a40144e))
* **Create Exams:** Implement creating different types of exams and adding questions on it. ([bd80ca4](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/bd80ca40b267ede3aace663dfebd22fa5dc96c1a))
* **Created By:** Add question author section and updated last date ([0957dc9](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/0957dc9c04ca853238751de3811f4e3120fca916))
* **Edit Add Exam:** Add exam types, details and implement create exam mutation ([ad3b23b](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/ad3b23b579ed71ed7f0aa8e3dcec0e25407d7f36))
* **Edit Fixed Exam:** Edit fixed exam date input ([ec52b18](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/ec52b18048878a493b04fe5c62e03ba1d67d04f2))
* **Edit-Delete Code Question:** Create Edit Code Question component and Implement Edit and Delete Code Question ([067f0ee](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/067f0eee4f0b8eb104265992ae30820e0940d12c))
* **Edit-DeleteMCQuestion:** Create Edit MC Question Modal and Delete Functionality ([09b6318](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/09b6318b731dfc98bf47c2a9d2ed8b291e039c7b))
* **Edit-DeleteMSQ:** implement edit and delete functionality ([201eb79](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/201eb798ff937cfa7985ba3a181f9c99092e75cc))
* **EditMCQ:** Add default option fields MCQ Component ([ac347be](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/ac347be52b8c7f537004bac6e342e327975659e7))
* **EditMSQ:** Add default choice fields MSQ Component ([f319885](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/f31988514ea2ab941ca28cfb6c921e24f235aea9))
* **Exam Link:** Create Exam Link Modal component and Create function to generate exam link ([151cdaa](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/151cdaa8694d54dd36c2d45a03436232bcf15d0c))
* **Frontend Pagination:** Displaying paginated data ([bb94ce4](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/bb94ce4108351c1fa3e07f37eba2e1553bcbf4ab))
* **MCQ:** Create add question modal and mcq type component ([934c59c](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/934c59c2b16764bb9802ecd007dcfe0fca49455e))
* **MCQ:** Create and implement create multiple choice question mutation ([baea27a](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/baea27ad3f67c748d6fb80e7b7efa275dfb8c7fc))
* **MSQ:** Create MSQ component, Implement Create Multiple Selection Question mutation and Create test cases ([e66a89b](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/e66a89b80b9b587db29fd2881bb34e5204afd682))
* **Pagination and Caching:** Implement pagination on questions and exams, and server-side cache on code activities ([fe570b8](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/fe570b8b63ccafc25a4a46c9134e84fceb31ce71))
* **Populate Exam Questions:** Rewrite Edit Exam Page, Create get questions query by module and program ([a3bfc40](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/a3bfc40be9805b21951f183e3389c0578e388673))
* **Queries and Mutations:** Implement sign out, submit code exam, get code activities, and search questions queries ([09622bd](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/09622bdcca3cff5338908a0719e5598f793f4a08))
* **Question Count, Publish Status and Tags:** Added max question count in exam, update publish status function, and added program & module fields ([dbeb913](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/dbeb9139c090940f1d04fd2059208175990b01d9))
* **QuestionBank:** table for questions and button to add a question ([d26188e](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/d26188e3dff5b3ad990bdd2050e67af0508d931a))
* **Retrieve Questions and Pagination:** Retrieve questions using question base table and implemented sorting and pagination on db ([b7d7eee](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/b7d7eee31ec5f809f3062cc342cf50fb52613de2))
* **Routing:** create simple private and public routes ([479690a](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/479690a6595a49f0dfb1103ba7f1e3685d719bdb))
* **SearchQuestions:** Create and Implement Search Questions query ([1263ebb](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/1263ebbb8b127f2fd967b1b2ea7bc893507382fe))
* **Side Navigation:** Create side navigation for admin dashboard ([c36fd66](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/c36fd666ea160752688ed0ef0890f22cde290640))
* **SignOut:** Implement Sign Out Functionality ([f66d588](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/f66d588b428b365e24fccbd32358379e092e29ec))
* **State Pages:** added state pages and table loading state ([52e3384](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/52e33846fb3de191dc2b11c860d95230dd9736b5))
* **Student exam:** Implement submit student exam , calculating score and exam summary ([0cbe11b](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/0cbe11bcdb3935ef18265492091f925c336990b0))
* **Student Side Nav:** Added student side nav in student view ([1943f9a](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/1943f9ab294ad43af4c6ef4f7dd7a3086e1a84ac))
* **User Queries:** Added current user and retrieve user by id queries ([80e110c](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/80e110cedf93e05ece4f7c32e96a358ac4daf767))
* **View and delete exam:** Implementation of viewing individual exam types and deletion of exam ([157a742](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/157a742fe5f626280555a333fc871e3ad8fe7c7f))
* **ViewQuestion:** Create View Question Modal and Implement Queries ([0db6b43](https://gitlab.com/kingsland-team-ph/kingsland-assess/commit/0db6b434240b1fedd35e39f2da6b619b0633820c))




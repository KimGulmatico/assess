# 1.1.0 (2023-02-28)


### Bug Fixes

* **backend, frontend:** fix tests related/caused by code question feature ([97ca44e](https://gitlab.com/KimGulmatico/assess/commit/97ca44e337b08fab041cb2f0c389b55b2541e816))
* remove code test case ([cf77788](https://gitlab.com/KimGulmatico/assess/commit/cf77788702e150a424a37e909d0195ec9dd4d989))
* student dashboard logo ([9e465e4](https://gitlab.com/KimGulmatico/assess/commit/9e465e4f31b1c350ebf2d77f7140ae2531feb3c3))
* update assess colors and font style & update frontend take exam routes ([db48686](https://gitlab.com/KimGulmatico/assess/commit/db486860a27272cd17d38c554135a7e05dc92bc1))


### Features

* add generate certificate function ([c68794b](https://gitlab.com/KimGulmatico/assess/commit/c68794bc8617ec8409758e73c3925581b65cddc5))
* add studentExamResult queries ([9f1b409](https://gitlab.com/KimGulmatico/assess/commit/9f1b409fbcee76d0a9193c460695162f039bdd5c))
* **admin management:** admin management page ([248b485](https://gitlab.com/KimGulmatico/assess/commit/248b485f1219577feec93961f3e0751223ceae1a))
* **admin management:** improve layout responsiveness ([04efb76](https://gitlab.com/KimGulmatico/assess/commit/04efb764d50e39484f4c5cbbf187932608d859bb))
* **admin:** update admin table, add resolvers, update admin seed ([191e542](https://gitlab.com/KimGulmatico/assess/commit/191e5429c3edac8a17f288293b965b6b1b3358b4))
* Certificate Generator Implementation ([5cba193](https://gitlab.com/KimGulmatico/assess/commit/5cba19339ffb61d9d853c6a5dd9baff44c2300f7))
* **code questions:** remove code question feature ([1b6f79f](https://gitlab.com/KimGulmatico/assess/commit/1b6f79f379be8d639ad383640b3cb87da7f264ea))
* integrate generate exam link for playground testing ([48c9211](https://gitlab.com/KimGulmatico/assess/commit/48c92113b40bf82aabccb555f630f74f188cff40))
* update api documentation ang app logos ([963e55b](https://gitlab.com/KimGulmatico/assess/commit/963e55bc8c3502c8f05aa88c518c33ce252cebb1))



# 1.0.0 (2022-11-24)



